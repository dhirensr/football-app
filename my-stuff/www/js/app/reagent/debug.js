// Compiled by ClojureScript 1.9.908 {}
goog.provide('reagent.debug');
goog.require('cljs.core');
reagent.debug.has_console = typeof console !== 'undefined';
reagent.debug.tracking = false;
if(typeof reagent.debug.warnings !== 'undefined'){
} else {
reagent.debug.warnings = cljs.core.atom.call(null,null);
}
if(typeof reagent.debug.track_console !== 'undefined'){
} else {
reagent.debug.track_console = (function (){var o = ({});
o.warn = ((function (o){
return (function() { 
var G__40858__delegate = function (args){
return cljs.core.swap_BANG_.call(null,reagent.debug.warnings,cljs.core.update_in,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"warn","warn",-436710552)], null),cljs.core.conj,cljs.core.apply.call(null,cljs.core.str,args));
};
var G__40858 = function (var_args){
var args = null;
if (arguments.length > 0) {
var G__40859__i = 0, G__40859__a = new Array(arguments.length -  0);
while (G__40859__i < G__40859__a.length) {G__40859__a[G__40859__i] = arguments[G__40859__i + 0]; ++G__40859__i;}
  args = new cljs.core.IndexedSeq(G__40859__a,0,null);
} 
return G__40858__delegate.call(this,args);};
G__40858.cljs$lang$maxFixedArity = 0;
G__40858.cljs$lang$applyTo = (function (arglist__40860){
var args = cljs.core.seq(arglist__40860);
return G__40858__delegate(args);
});
G__40858.cljs$core$IFn$_invoke$arity$variadic = G__40858__delegate;
return G__40858;
})()
;})(o))
;

o.error = ((function (o){
return (function() { 
var G__40861__delegate = function (args){
return cljs.core.swap_BANG_.call(null,reagent.debug.warnings,cljs.core.update_in,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"error","error",-978969032)], null),cljs.core.conj,cljs.core.apply.call(null,cljs.core.str,args));
};
var G__40861 = function (var_args){
var args = null;
if (arguments.length > 0) {
var G__40862__i = 0, G__40862__a = new Array(arguments.length -  0);
while (G__40862__i < G__40862__a.length) {G__40862__a[G__40862__i] = arguments[G__40862__i + 0]; ++G__40862__i;}
  args = new cljs.core.IndexedSeq(G__40862__a,0,null);
} 
return G__40861__delegate.call(this,args);};
G__40861.cljs$lang$maxFixedArity = 0;
G__40861.cljs$lang$applyTo = (function (arglist__40863){
var args = cljs.core.seq(arglist__40863);
return G__40861__delegate(args);
});
G__40861.cljs$core$IFn$_invoke$arity$variadic = G__40861__delegate;
return G__40861;
})()
;})(o))
;

return o;
})();
}
reagent.debug.track_warnings = (function reagent$debug$track_warnings(f){
reagent.debug.tracking = true;

cljs.core.reset_BANG_.call(null,reagent.debug.warnings,null);

f.call(null);

var warns = cljs.core.deref.call(null,reagent.debug.warnings);
cljs.core.reset_BANG_.call(null,reagent.debug.warnings,null);

reagent.debug.tracking = false;

return warns;
});

//# sourceMappingURL=debug.js.map?rel=1507227564402

// Compiled by ClojureScript 1.9.908 {}
goog.provide('reagent.dom');
goog.require('cljs.core');
goog.require('cljsjs.react.dom');
goog.require('reagent.impl.util');
goog.require('reagent.impl.template');
goog.require('reagent.debug');
goog.require('reagent.interop');
if(typeof reagent.dom.dom !== 'undefined'){
} else {
reagent.dom.dom = (function (){var or__30182__auto__ = (function (){var and__30170__auto__ = typeof ReactDOM !== 'undefined';
if(and__30170__auto__){
return ReactDOM;
} else {
return and__30170__auto__;
}
})();
if(cljs.core.truth_(or__30182__auto__)){
return or__30182__auto__;
} else {
var and__30170__auto__ = typeof require !== 'undefined';
if(and__30170__auto__){
return require("react-dom");
} else {
return and__30170__auto__;
}
}
})();
}
if(cljs.core.truth_(reagent.dom.dom)){
} else {
throw (new Error(["Assert failed: ","Could not find ReactDOM","\n","dom"].join('')));
}
if(typeof reagent.dom.roots !== 'undefined'){
} else {
reagent.dom.roots = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
}
reagent.dom.unmount_comp = (function reagent$dom$unmount_comp(container){
cljs.core.swap_BANG_.call(null,reagent.dom.roots,cljs.core.dissoc,container);

return (reagent.dom.dom["unmountComponentAtNode"])(container);
});
reagent.dom.render_comp = (function reagent$dom$render_comp(comp,container,callback){
var _STAR_always_update_STAR_41425 = reagent.impl.util._STAR_always_update_STAR_;
reagent.impl.util._STAR_always_update_STAR_ = true;

try{return (reagent.dom.dom["render"])(comp.call(null),container,((function (_STAR_always_update_STAR_41425){
return (function (){
var _STAR_always_update_STAR_41426 = reagent.impl.util._STAR_always_update_STAR_;
reagent.impl.util._STAR_always_update_STAR_ = false;

try{cljs.core.swap_BANG_.call(null,reagent.dom.roots,cljs.core.assoc,container,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [comp,container], null));

if(!((callback == null))){
return callback.call(null);
} else {
return null;
}
}finally {reagent.impl.util._STAR_always_update_STAR_ = _STAR_always_update_STAR_41426;
}});})(_STAR_always_update_STAR_41425))
);
}finally {reagent.impl.util._STAR_always_update_STAR_ = _STAR_always_update_STAR_41425;
}});
reagent.dom.re_render_component = (function reagent$dom$re_render_component(comp,container){
return reagent.dom.render_comp.call(null,comp,container,null);
});
/**
 * Render a Reagent component into the DOM. The first argument may be
 *   either a vector (using Reagent's Hiccup syntax), or a React element. The second argument should be a DOM node.
 * 
 *   Optionally takes a callback that is called when the component is in place.
 * 
 *   Returns the mounted component instance.
 */
reagent.dom.render = (function reagent$dom$render(var_args){
var G__41428 = arguments.length;
switch (G__41428) {
case 2:
return reagent.dom.render.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return reagent.dom.render.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

reagent.dom.render.cljs$core$IFn$_invoke$arity$2 = (function (comp,container){
return reagent.dom.render.call(null,comp,container,null);
});

reagent.dom.render.cljs$core$IFn$_invoke$arity$3 = (function (comp,container,callback){
var f = (function (){
return reagent.impl.template.as_element.call(null,((cljs.core.fn_QMARK_.call(null,comp))?comp.call(null):comp));
});
return reagent.dom.render_comp.call(null,f,container,callback);
});

reagent.dom.render.cljs$lang$maxFixedArity = 3;

reagent.dom.unmount_component_at_node = (function reagent$dom$unmount_component_at_node(container){
return reagent.dom.unmount_comp.call(null,container);
});
/**
 * Returns the root DOM node of a mounted component.
 */
reagent.dom.dom_node = (function reagent$dom$dom_node(this$){
return (reagent.dom.dom["findDOMNode"])(this$);
});
reagent.impl.template.find_dom_node = reagent.dom.dom_node;
/**
 * Force re-rendering of all mounted Reagent components. This is
 *   probably only useful in a development environment, when you want to
 *   update components in response to some dynamic changes to code.
 * 
 *   Note that force-update-all may not update root components. This
 *   happens if a component 'foo' is mounted with `(render [foo])` (since
 *   functions are passed by value, and not by reference, in
 *   ClojureScript). To get around this you'll have to introduce a layer
 *   of indirection, for example by using `(render [#'foo])` instead.
 */
reagent.dom.force_update_all = (function reagent$dom$force_update_all(){
var seq__41430_41434 = cljs.core.seq.call(null,cljs.core.vals.call(null,cljs.core.deref.call(null,reagent.dom.roots)));
var chunk__41431_41435 = null;
var count__41432_41436 = (0);
var i__41433_41437 = (0);
while(true){
if((i__41433_41437 < count__41432_41436)){
var v_41438 = cljs.core._nth.call(null,chunk__41431_41435,i__41433_41437);
cljs.core.apply.call(null,reagent.dom.re_render_component,v_41438);

var G__41439 = seq__41430_41434;
var G__41440 = chunk__41431_41435;
var G__41441 = count__41432_41436;
var G__41442 = (i__41433_41437 + (1));
seq__41430_41434 = G__41439;
chunk__41431_41435 = G__41440;
count__41432_41436 = G__41441;
i__41433_41437 = G__41442;
continue;
} else {
var temp__5278__auto___41443 = cljs.core.seq.call(null,seq__41430_41434);
if(temp__5278__auto___41443){
var seq__41430_41444__$1 = temp__5278__auto___41443;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__41430_41444__$1)){
var c__31113__auto___41445 = cljs.core.chunk_first.call(null,seq__41430_41444__$1);
var G__41446 = cljs.core.chunk_rest.call(null,seq__41430_41444__$1);
var G__41447 = c__31113__auto___41445;
var G__41448 = cljs.core.count.call(null,c__31113__auto___41445);
var G__41449 = (0);
seq__41430_41434 = G__41446;
chunk__41431_41435 = G__41447;
count__41432_41436 = G__41448;
i__41433_41437 = G__41449;
continue;
} else {
var v_41450 = cljs.core.first.call(null,seq__41430_41444__$1);
cljs.core.apply.call(null,reagent.dom.re_render_component,v_41450);

var G__41451 = cljs.core.next.call(null,seq__41430_41444__$1);
var G__41452 = null;
var G__41453 = (0);
var G__41454 = (0);
seq__41430_41434 = G__41451;
chunk__41431_41435 = G__41452;
count__41432_41436 = G__41453;
i__41433_41437 = G__41454;
continue;
}
} else {
}
}
break;
}

return "Updated";
});

//# sourceMappingURL=dom.js.map?rel=1507227565658

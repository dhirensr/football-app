// Compiled by ClojureScript 1.9.908 {}
goog.provide('figwheel.client.file_reloading');
goog.require('cljs.core');
goog.require('figwheel.client.utils');
goog.require('goog.Uri');
goog.require('goog.string');
goog.require('goog.object');
goog.require('goog.net.jsloader');
goog.require('goog.html.legacyconversions');
goog.require('clojure.string');
goog.require('clojure.set');
goog.require('cljs.core.async');
goog.require('goog.async.Deferred');
if(typeof figwheel.client.file_reloading.figwheel_meta_pragmas !== 'undefined'){
} else {
figwheel.client.file_reloading.figwheel_meta_pragmas = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
}
figwheel.client.file_reloading.on_jsload_custom_event = (function figwheel$client$file_reloading$on_jsload_custom_event(url){
return figwheel.client.utils.dispatch_custom_event.call(null,"figwheel.js-reload",url);
});
figwheel.client.file_reloading.before_jsload_custom_event = (function figwheel$client$file_reloading$before_jsload_custom_event(files){
return figwheel.client.utils.dispatch_custom_event.call(null,"figwheel.before-js-reload",files);
});
figwheel.client.file_reloading.on_cssload_custom_event = (function figwheel$client$file_reloading$on_cssload_custom_event(files){
return figwheel.client.utils.dispatch_custom_event.call(null,"figwheel.css-reload",files);
});
figwheel.client.file_reloading.namespace_file_map_QMARK_ = (function figwheel$client$file_reloading$namespace_file_map_QMARK_(m){
var or__30182__auto__ = (cljs.core.map_QMARK_.call(null,m)) && (typeof new cljs.core.Keyword(null,"namespace","namespace",-377510372).cljs$core$IFn$_invoke$arity$1(m) === 'string') && (((new cljs.core.Keyword(null,"file","file",-1269645878).cljs$core$IFn$_invoke$arity$1(m) == null)) || (typeof new cljs.core.Keyword(null,"file","file",-1269645878).cljs$core$IFn$_invoke$arity$1(m) === 'string')) && (cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"type","type",1174270348).cljs$core$IFn$_invoke$arity$1(m),new cljs.core.Keyword(null,"namespace","namespace",-377510372)));
if(or__30182__auto__){
return or__30182__auto__;
} else {
cljs.core.println.call(null,"Error not namespace-file-map",cljs.core.pr_str.call(null,m));

return false;
}
});
figwheel.client.file_reloading.add_cache_buster = (function figwheel$client$file_reloading$add_cache_buster(url){

return goog.Uri.parse(url).makeUnique();
});
figwheel.client.file_reloading.name__GT_path = (function figwheel$client$file_reloading$name__GT_path(ns){

return goog.object.get(goog.dependencies_.nameToPath,ns);
});
figwheel.client.file_reloading.provided_QMARK_ = (function figwheel$client$file_reloading$provided_QMARK_(ns){
return goog.object.get(goog.dependencies_.written,figwheel.client.file_reloading.name__GT_path.call(null,ns));
});
figwheel.client.file_reloading.immutable_ns_QMARK_ = (function figwheel$client$file_reloading$immutable_ns_QMARK_(name){
var or__30182__auto__ = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 3, ["cljs.nodejs",null,"goog",null,"cljs.core",null], null), null).call(null,name);
if(cljs.core.truth_(or__30182__auto__)){
return or__30182__auto__;
} else {
var or__30182__auto____$1 = goog.string.startsWith("clojure.",name);
if(cljs.core.truth_(or__30182__auto____$1)){
return or__30182__auto____$1;
} else {
return goog.string.startsWith("goog.",name);
}
}
});
figwheel.client.file_reloading.get_requires = (function figwheel$client$file_reloading$get_requires(ns){
return cljs.core.set.call(null,cljs.core.filter.call(null,(function (p1__44566_SHARP_){
return cljs.core.not.call(null,figwheel.client.file_reloading.immutable_ns_QMARK_.call(null,p1__44566_SHARP_));
}),goog.object.getKeys(goog.object.get(goog.dependencies_.requires,figwheel.client.file_reloading.name__GT_path.call(null,ns)))));
});
if(typeof figwheel.client.file_reloading.dependency_data !== 'undefined'){
} else {
figwheel.client.file_reloading.dependency_data = cljs.core.atom.call(null,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"pathToName","pathToName",-1236616181),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"dependents","dependents",136812837),cljs.core.PersistentArrayMap.EMPTY], null));
}
figwheel.client.file_reloading.path_to_name_BANG_ = (function figwheel$client$file_reloading$path_to_name_BANG_(path,name){
return cljs.core.swap_BANG_.call(null,figwheel.client.file_reloading.dependency_data,cljs.core.update_in,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"pathToName","pathToName",-1236616181),path], null),cljs.core.fnil.call(null,clojure.set.union,cljs.core.PersistentHashSet.EMPTY),cljs.core.PersistentHashSet.createAsIfByAssoc([name]));
});
/**
 * Setup a path to name dependencies map.
 * That goes from path -> #{ ns-names }
 */
figwheel.client.file_reloading.setup_path__GT_name_BANG_ = (function figwheel$client$file_reloading$setup_path__GT_name_BANG_(){
var nameToPath = goog.object.filter(goog.dependencies_.nameToPath,(function (v,k,o){
return goog.string.startsWith(v,"../");
}));
return goog.object.forEach(nameToPath,((function (nameToPath){
return (function (v,k,o){
return figwheel.client.file_reloading.path_to_name_BANG_.call(null,v,k);
});})(nameToPath))
);
});
/**
 * returns a set of namespaces defined by a path
 */
figwheel.client.file_reloading.path__GT_name = (function figwheel$client$file_reloading$path__GT_name(path){
return cljs.core.get_in.call(null,cljs.core.deref.call(null,figwheel.client.file_reloading.dependency_data),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"pathToName","pathToName",-1236616181),path], null));
});
figwheel.client.file_reloading.name_to_parent_BANG_ = (function figwheel$client$file_reloading$name_to_parent_BANG_(ns,parent_ns){
return cljs.core.swap_BANG_.call(null,figwheel.client.file_reloading.dependency_data,cljs.core.update_in,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"dependents","dependents",136812837),ns], null),cljs.core.fnil.call(null,clojure.set.union,cljs.core.PersistentHashSet.EMPTY),cljs.core.PersistentHashSet.createAsIfByAssoc([parent_ns]));
});
/**
 * This reverses the goog.dependencies_.requires for looking up ns-dependents.
 */
figwheel.client.file_reloading.setup_ns__GT_dependents_BANG_ = (function figwheel$client$file_reloading$setup_ns__GT_dependents_BANG_(){
var requires = goog.object.filter(goog.dependencies_.requires,(function (v,k,o){
return goog.string.startsWith(k,"../");
}));
return goog.object.forEach(requires,((function (requires){
return (function (v,k,_){
return goog.object.forEach(v,((function (requires){
return (function (v_SINGLEQUOTE_,k_SINGLEQUOTE_,___$1){
var seq__44567 = cljs.core.seq.call(null,figwheel.client.file_reloading.path__GT_name.call(null,k));
var chunk__44568 = null;
var count__44569 = (0);
var i__44570 = (0);
while(true){
if((i__44570 < count__44569)){
var n = cljs.core._nth.call(null,chunk__44568,i__44570);
figwheel.client.file_reloading.name_to_parent_BANG_.call(null,k_SINGLEQUOTE_,n);

var G__44571 = seq__44567;
var G__44572 = chunk__44568;
var G__44573 = count__44569;
var G__44574 = (i__44570 + (1));
seq__44567 = G__44571;
chunk__44568 = G__44572;
count__44569 = G__44573;
i__44570 = G__44574;
continue;
} else {
var temp__5278__auto__ = cljs.core.seq.call(null,seq__44567);
if(temp__5278__auto__){
var seq__44567__$1 = temp__5278__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__44567__$1)){
var c__31113__auto__ = cljs.core.chunk_first.call(null,seq__44567__$1);
var G__44575 = cljs.core.chunk_rest.call(null,seq__44567__$1);
var G__44576 = c__31113__auto__;
var G__44577 = cljs.core.count.call(null,c__31113__auto__);
var G__44578 = (0);
seq__44567 = G__44575;
chunk__44568 = G__44576;
count__44569 = G__44577;
i__44570 = G__44578;
continue;
} else {
var n = cljs.core.first.call(null,seq__44567__$1);
figwheel.client.file_reloading.name_to_parent_BANG_.call(null,k_SINGLEQUOTE_,n);

var G__44579 = cljs.core.next.call(null,seq__44567__$1);
var G__44580 = null;
var G__44581 = (0);
var G__44582 = (0);
seq__44567 = G__44579;
chunk__44568 = G__44580;
count__44569 = G__44581;
i__44570 = G__44582;
continue;
}
} else {
return null;
}
}
break;
}
});})(requires))
);
});})(requires))
);
});
figwheel.client.file_reloading.ns__GT_dependents = (function figwheel$client$file_reloading$ns__GT_dependents(ns){
return cljs.core.get_in.call(null,cljs.core.deref.call(null,figwheel.client.file_reloading.dependency_data),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"dependents","dependents",136812837),ns], null));
});
figwheel.client.file_reloading.build_topo_sort = (function figwheel$client$file_reloading$build_topo_sort(get_deps){
var get_deps__$1 = cljs.core.memoize.call(null,get_deps);
var topo_sort_helper_STAR_ = ((function (get_deps__$1){
return (function figwheel$client$file_reloading$build_topo_sort_$_topo_sort_helper_STAR_(x,depth,state){
var deps = get_deps__$1.call(null,x);
if(cljs.core.empty_QMARK_.call(null,deps)){
return null;
} else {
return topo_sort_STAR_.call(null,deps,depth,state);
}
});})(get_deps__$1))
;
var topo_sort_STAR_ = ((function (get_deps__$1){
return (function() {
var figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR_ = null;
var figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR___1 = (function (deps){
return figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR_.call(null,deps,(0),cljs.core.atom.call(null,cljs.core.sorted_map.call(null)));
});
var figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR___3 = (function (deps,depth,state){
cljs.core.swap_BANG_.call(null,state,cljs.core.update_in,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [depth], null),cljs.core.fnil.call(null,cljs.core.into,cljs.core.PersistentHashSet.EMPTY),deps);

var seq__44592_44600 = cljs.core.seq.call(null,deps);
var chunk__44593_44601 = null;
var count__44594_44602 = (0);
var i__44595_44603 = (0);
while(true){
if((i__44595_44603 < count__44594_44602)){
var dep_44604 = cljs.core._nth.call(null,chunk__44593_44601,i__44595_44603);
topo_sort_helper_STAR_.call(null,dep_44604,(depth + (1)),state);

var G__44605 = seq__44592_44600;
var G__44606 = chunk__44593_44601;
var G__44607 = count__44594_44602;
var G__44608 = (i__44595_44603 + (1));
seq__44592_44600 = G__44605;
chunk__44593_44601 = G__44606;
count__44594_44602 = G__44607;
i__44595_44603 = G__44608;
continue;
} else {
var temp__5278__auto___44609 = cljs.core.seq.call(null,seq__44592_44600);
if(temp__5278__auto___44609){
var seq__44592_44610__$1 = temp__5278__auto___44609;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__44592_44610__$1)){
var c__31113__auto___44611 = cljs.core.chunk_first.call(null,seq__44592_44610__$1);
var G__44612 = cljs.core.chunk_rest.call(null,seq__44592_44610__$1);
var G__44613 = c__31113__auto___44611;
var G__44614 = cljs.core.count.call(null,c__31113__auto___44611);
var G__44615 = (0);
seq__44592_44600 = G__44612;
chunk__44593_44601 = G__44613;
count__44594_44602 = G__44614;
i__44595_44603 = G__44615;
continue;
} else {
var dep_44616 = cljs.core.first.call(null,seq__44592_44610__$1);
topo_sort_helper_STAR_.call(null,dep_44616,(depth + (1)),state);

var G__44617 = cljs.core.next.call(null,seq__44592_44610__$1);
var G__44618 = null;
var G__44619 = (0);
var G__44620 = (0);
seq__44592_44600 = G__44617;
chunk__44593_44601 = G__44618;
count__44594_44602 = G__44619;
i__44595_44603 = G__44620;
continue;
}
} else {
}
}
break;
}

if(cljs.core._EQ_.call(null,depth,(0))){
return elim_dups_STAR_.call(null,cljs.core.reverse.call(null,cljs.core.vals.call(null,cljs.core.deref.call(null,state))));
} else {
return null;
}
});
figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR_ = function(deps,depth,state){
switch(arguments.length){
case 1:
return figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR___1.call(this,deps);
case 3:
return figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR___3.call(this,deps,depth,state);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR_.cljs$core$IFn$_invoke$arity$1 = figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR___1;
figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR_.cljs$core$IFn$_invoke$arity$3 = figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR___3;
return figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR_;
})()
;})(get_deps__$1))
;
var elim_dups_STAR_ = ((function (get_deps__$1){
return (function figwheel$client$file_reloading$build_topo_sort_$_elim_dups_STAR_(p__44596){
var vec__44597 = p__44596;
var seq__44598 = cljs.core.seq.call(null,vec__44597);
var first__44599 = cljs.core.first.call(null,seq__44598);
var seq__44598__$1 = cljs.core.next.call(null,seq__44598);
var x = first__44599;
var xs = seq__44598__$1;
if((x == null)){
return cljs.core.List.EMPTY;
} else {
return cljs.core.cons.call(null,x,figwheel$client$file_reloading$build_topo_sort_$_elim_dups_STAR_.call(null,cljs.core.map.call(null,((function (vec__44597,seq__44598,first__44599,seq__44598__$1,x,xs,get_deps__$1){
return (function (p1__44583_SHARP_){
return clojure.set.difference.call(null,p1__44583_SHARP_,x);
});})(vec__44597,seq__44598,first__44599,seq__44598__$1,x,xs,get_deps__$1))
,xs)));
}
});})(get_deps__$1))
;
return topo_sort_STAR_;
});
figwheel.client.file_reloading.get_all_dependencies = (function figwheel$client$file_reloading$get_all_dependencies(ns){
var topo_sort_SINGLEQUOTE_ = figwheel.client.file_reloading.build_topo_sort.call(null,figwheel.client.file_reloading.get_requires);
return cljs.core.apply.call(null,cljs.core.concat,topo_sort_SINGLEQUOTE_.call(null,cljs.core.set.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [ns], null))));
});
figwheel.client.file_reloading.get_all_dependents = (function figwheel$client$file_reloading$get_all_dependents(nss){
var topo_sort_SINGLEQUOTE_ = figwheel.client.file_reloading.build_topo_sort.call(null,figwheel.client.file_reloading.ns__GT_dependents);
return cljs.core.filter.call(null,cljs.core.comp.call(null,cljs.core.not,figwheel.client.file_reloading.immutable_ns_QMARK_),cljs.core.reverse.call(null,cljs.core.apply.call(null,cljs.core.concat,topo_sort_SINGLEQUOTE_.call(null,cljs.core.set.call(null,nss)))));
});
figwheel.client.file_reloading.unprovide_BANG_ = (function figwheel$client$file_reloading$unprovide_BANG_(ns){
var path = figwheel.client.file_reloading.name__GT_path.call(null,ns);
goog.object.remove(goog.dependencies_.visited,path);

goog.object.remove(goog.dependencies_.written,path);

return goog.object.remove(goog.dependencies_.written,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(goog.basePath),cljs.core.str.cljs$core$IFn$_invoke$arity$1(path)].join(''));
});
figwheel.client.file_reloading.resolve_ns = (function figwheel$client$file_reloading$resolve_ns(ns){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(goog.basePath),cljs.core.str.cljs$core$IFn$_invoke$arity$1(figwheel.client.file_reloading.name__GT_path.call(null,ns))].join('');
});
figwheel.client.file_reloading.addDependency = (function figwheel$client$file_reloading$addDependency(path,provides,requires){
var seq__44621 = cljs.core.seq.call(null,provides);
var chunk__44622 = null;
var count__44623 = (0);
var i__44624 = (0);
while(true){
if((i__44624 < count__44623)){
var prov = cljs.core._nth.call(null,chunk__44622,i__44624);
figwheel.client.file_reloading.path_to_name_BANG_.call(null,path,prov);

var seq__44625_44633 = cljs.core.seq.call(null,requires);
var chunk__44626_44634 = null;
var count__44627_44635 = (0);
var i__44628_44636 = (0);
while(true){
if((i__44628_44636 < count__44627_44635)){
var req_44637 = cljs.core._nth.call(null,chunk__44626_44634,i__44628_44636);
figwheel.client.file_reloading.name_to_parent_BANG_.call(null,req_44637,prov);

var G__44638 = seq__44625_44633;
var G__44639 = chunk__44626_44634;
var G__44640 = count__44627_44635;
var G__44641 = (i__44628_44636 + (1));
seq__44625_44633 = G__44638;
chunk__44626_44634 = G__44639;
count__44627_44635 = G__44640;
i__44628_44636 = G__44641;
continue;
} else {
var temp__5278__auto___44642 = cljs.core.seq.call(null,seq__44625_44633);
if(temp__5278__auto___44642){
var seq__44625_44643__$1 = temp__5278__auto___44642;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__44625_44643__$1)){
var c__31113__auto___44644 = cljs.core.chunk_first.call(null,seq__44625_44643__$1);
var G__44645 = cljs.core.chunk_rest.call(null,seq__44625_44643__$1);
var G__44646 = c__31113__auto___44644;
var G__44647 = cljs.core.count.call(null,c__31113__auto___44644);
var G__44648 = (0);
seq__44625_44633 = G__44645;
chunk__44626_44634 = G__44646;
count__44627_44635 = G__44647;
i__44628_44636 = G__44648;
continue;
} else {
var req_44649 = cljs.core.first.call(null,seq__44625_44643__$1);
figwheel.client.file_reloading.name_to_parent_BANG_.call(null,req_44649,prov);

var G__44650 = cljs.core.next.call(null,seq__44625_44643__$1);
var G__44651 = null;
var G__44652 = (0);
var G__44653 = (0);
seq__44625_44633 = G__44650;
chunk__44626_44634 = G__44651;
count__44627_44635 = G__44652;
i__44628_44636 = G__44653;
continue;
}
} else {
}
}
break;
}

var G__44654 = seq__44621;
var G__44655 = chunk__44622;
var G__44656 = count__44623;
var G__44657 = (i__44624 + (1));
seq__44621 = G__44654;
chunk__44622 = G__44655;
count__44623 = G__44656;
i__44624 = G__44657;
continue;
} else {
var temp__5278__auto__ = cljs.core.seq.call(null,seq__44621);
if(temp__5278__auto__){
var seq__44621__$1 = temp__5278__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__44621__$1)){
var c__31113__auto__ = cljs.core.chunk_first.call(null,seq__44621__$1);
var G__44658 = cljs.core.chunk_rest.call(null,seq__44621__$1);
var G__44659 = c__31113__auto__;
var G__44660 = cljs.core.count.call(null,c__31113__auto__);
var G__44661 = (0);
seq__44621 = G__44658;
chunk__44622 = G__44659;
count__44623 = G__44660;
i__44624 = G__44661;
continue;
} else {
var prov = cljs.core.first.call(null,seq__44621__$1);
figwheel.client.file_reloading.path_to_name_BANG_.call(null,path,prov);

var seq__44629_44662 = cljs.core.seq.call(null,requires);
var chunk__44630_44663 = null;
var count__44631_44664 = (0);
var i__44632_44665 = (0);
while(true){
if((i__44632_44665 < count__44631_44664)){
var req_44666 = cljs.core._nth.call(null,chunk__44630_44663,i__44632_44665);
figwheel.client.file_reloading.name_to_parent_BANG_.call(null,req_44666,prov);

var G__44667 = seq__44629_44662;
var G__44668 = chunk__44630_44663;
var G__44669 = count__44631_44664;
var G__44670 = (i__44632_44665 + (1));
seq__44629_44662 = G__44667;
chunk__44630_44663 = G__44668;
count__44631_44664 = G__44669;
i__44632_44665 = G__44670;
continue;
} else {
var temp__5278__auto___44671__$1 = cljs.core.seq.call(null,seq__44629_44662);
if(temp__5278__auto___44671__$1){
var seq__44629_44672__$1 = temp__5278__auto___44671__$1;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__44629_44672__$1)){
var c__31113__auto___44673 = cljs.core.chunk_first.call(null,seq__44629_44672__$1);
var G__44674 = cljs.core.chunk_rest.call(null,seq__44629_44672__$1);
var G__44675 = c__31113__auto___44673;
var G__44676 = cljs.core.count.call(null,c__31113__auto___44673);
var G__44677 = (0);
seq__44629_44662 = G__44674;
chunk__44630_44663 = G__44675;
count__44631_44664 = G__44676;
i__44632_44665 = G__44677;
continue;
} else {
var req_44678 = cljs.core.first.call(null,seq__44629_44672__$1);
figwheel.client.file_reloading.name_to_parent_BANG_.call(null,req_44678,prov);

var G__44679 = cljs.core.next.call(null,seq__44629_44672__$1);
var G__44680 = null;
var G__44681 = (0);
var G__44682 = (0);
seq__44629_44662 = G__44679;
chunk__44630_44663 = G__44680;
count__44631_44664 = G__44681;
i__44632_44665 = G__44682;
continue;
}
} else {
}
}
break;
}

var G__44683 = cljs.core.next.call(null,seq__44621__$1);
var G__44684 = null;
var G__44685 = (0);
var G__44686 = (0);
seq__44621 = G__44683;
chunk__44622 = G__44684;
count__44623 = G__44685;
i__44624 = G__44686;
continue;
}
} else {
return null;
}
}
break;
}
});
figwheel.client.file_reloading.figwheel_require = (function figwheel$client$file_reloading$figwheel_require(src,reload){
goog.require = figwheel.client.file_reloading.figwheel_require;

if(cljs.core._EQ_.call(null,reload,"reload-all")){
var seq__44687_44691 = cljs.core.seq.call(null,figwheel.client.file_reloading.get_all_dependencies.call(null,src));
var chunk__44688_44692 = null;
var count__44689_44693 = (0);
var i__44690_44694 = (0);
while(true){
if((i__44690_44694 < count__44689_44693)){
var ns_44695 = cljs.core._nth.call(null,chunk__44688_44692,i__44690_44694);
figwheel.client.file_reloading.unprovide_BANG_.call(null,ns_44695);

var G__44696 = seq__44687_44691;
var G__44697 = chunk__44688_44692;
var G__44698 = count__44689_44693;
var G__44699 = (i__44690_44694 + (1));
seq__44687_44691 = G__44696;
chunk__44688_44692 = G__44697;
count__44689_44693 = G__44698;
i__44690_44694 = G__44699;
continue;
} else {
var temp__5278__auto___44700 = cljs.core.seq.call(null,seq__44687_44691);
if(temp__5278__auto___44700){
var seq__44687_44701__$1 = temp__5278__auto___44700;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__44687_44701__$1)){
var c__31113__auto___44702 = cljs.core.chunk_first.call(null,seq__44687_44701__$1);
var G__44703 = cljs.core.chunk_rest.call(null,seq__44687_44701__$1);
var G__44704 = c__31113__auto___44702;
var G__44705 = cljs.core.count.call(null,c__31113__auto___44702);
var G__44706 = (0);
seq__44687_44691 = G__44703;
chunk__44688_44692 = G__44704;
count__44689_44693 = G__44705;
i__44690_44694 = G__44706;
continue;
} else {
var ns_44707 = cljs.core.first.call(null,seq__44687_44701__$1);
figwheel.client.file_reloading.unprovide_BANG_.call(null,ns_44707);

var G__44708 = cljs.core.next.call(null,seq__44687_44701__$1);
var G__44709 = null;
var G__44710 = (0);
var G__44711 = (0);
seq__44687_44691 = G__44708;
chunk__44688_44692 = G__44709;
count__44689_44693 = G__44710;
i__44690_44694 = G__44711;
continue;
}
} else {
}
}
break;
}
} else {
}

if(cljs.core.truth_(reload)){
figwheel.client.file_reloading.unprovide_BANG_.call(null,src);
} else {
}

return goog.require_figwheel_backup_(src);
});
/**
 * Reusable browser REPL bootstrapping. Patches the essential functions
 *   in goog.base to support re-loading of namespaces after page load.
 */
figwheel.client.file_reloading.bootstrap_goog_base = (function figwheel$client$file_reloading$bootstrap_goog_base(){
if(cljs.core.truth_(COMPILED)){
return null;
} else {
goog.require_figwheel_backup_ = (function (){var or__30182__auto__ = goog.require__;
if(cljs.core.truth_(or__30182__auto__)){
return or__30182__auto__;
} else {
return goog.require;
}
})();

goog.isProvided_ = (function (name){
return false;
});

figwheel.client.file_reloading.setup_path__GT_name_BANG_.call(null);

figwheel.client.file_reloading.setup_ns__GT_dependents_BANG_.call(null);

goog.addDependency_figwheel_backup_ = goog.addDependency;

goog.addDependency = (function() { 
var G__44712__delegate = function (args){
cljs.core.apply.call(null,figwheel.client.file_reloading.addDependency,args);

return cljs.core.apply.call(null,goog.addDependency_figwheel_backup_,args);
};
var G__44712 = function (var_args){
var args = null;
if (arguments.length > 0) {
var G__44713__i = 0, G__44713__a = new Array(arguments.length -  0);
while (G__44713__i < G__44713__a.length) {G__44713__a[G__44713__i] = arguments[G__44713__i + 0]; ++G__44713__i;}
  args = new cljs.core.IndexedSeq(G__44713__a,0,null);
} 
return G__44712__delegate.call(this,args);};
G__44712.cljs$lang$maxFixedArity = 0;
G__44712.cljs$lang$applyTo = (function (arglist__44714){
var args = cljs.core.seq(arglist__44714);
return G__44712__delegate(args);
});
G__44712.cljs$core$IFn$_invoke$arity$variadic = G__44712__delegate;
return G__44712;
})()
;

goog.constructNamespace_("cljs.user");

goog.global.CLOSURE_IMPORT_SCRIPT = figwheel.client.file_reloading.queued_file_reload;

return goog.require = figwheel.client.file_reloading.figwheel_require;
}
});
figwheel.client.file_reloading.patch_goog_base = (function figwheel$client$file_reloading$patch_goog_base(){
if(typeof figwheel.client.file_reloading.bootstrapped_cljs !== 'undefined'){
return null;
} else {
return (
figwheel.client.file_reloading.bootstrapped_cljs = (function (){
figwheel.client.file_reloading.bootstrap_goog_base.call(null);

return true;
})()
)
;
}
});
figwheel.client.file_reloading.gloader = ((typeof goog.net.jsloader.safeLoad !== 'undefined')?(function (p1__44715_SHARP_,p2__44716_SHARP_){
return goog.net.jsloader.safeLoad(goog.html.legacyconversions.trustedResourceUrlFromString([cljs.core.str.cljs$core$IFn$_invoke$arity$1(p1__44715_SHARP_)].join('')),p2__44716_SHARP_);
}):((typeof goog.net.jsloader.load !== 'undefined')?(function (p1__44717_SHARP_,p2__44718_SHARP_){
return goog.net.jsloader.load([cljs.core.str.cljs$core$IFn$_invoke$arity$1(p1__44717_SHARP_)].join(''),p2__44718_SHARP_);
}):(function(){throw cljs.core.ex_info.call(null,"No remote script loading function found.",cljs.core.PersistentArrayMap.EMPTY)})()
));
figwheel.client.file_reloading.reload_file_in_html_env = (function figwheel$client$file_reloading$reload_file_in_html_env(request_url,callback){

var G__44719 = figwheel.client.file_reloading.gloader.call(null,figwheel.client.file_reloading.add_cache_buster.call(null,request_url),({"cleanupWhenDone": true}));
G__44719.addCallback(((function (G__44719){
return (function (){
return cljs.core.apply.call(null,callback,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [true], null));
});})(G__44719))
);

G__44719.addErrback(((function (G__44719){
return (function (){
return cljs.core.apply.call(null,callback,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [false], null));
});})(G__44719))
);

return G__44719;
});
figwheel.client.file_reloading.reload_file_STAR_ = (function (){var pred__44720 = cljs.core._EQ_;
var expr__44721 = figwheel.client.utils.host_env_QMARK_.call(null);
if(cljs.core.truth_(pred__44720.call(null,new cljs.core.Keyword(null,"node","node",581201198),expr__44721))){
var node_path_lib = require("path");
var util_pattern = [cljs.core.str.cljs$core$IFn$_invoke$arity$1(node_path_lib.sep),cljs.core.str.cljs$core$IFn$_invoke$arity$1(node_path_lib.join("goog","bootstrap","nodejs.js"))].join('');
var util_path = goog.object.findKey(require.cache,((function (node_path_lib,util_pattern,pred__44720,expr__44721){
return (function (v,k,o){
return goog.string.endsWith(k,util_pattern);
});})(node_path_lib,util_pattern,pred__44720,expr__44721))
);
var parts = cljs.core.pop.call(null,cljs.core.pop.call(null,clojure.string.split.call(null,util_path,/[\/\\]/)));
var root_path = clojure.string.join.call(null,node_path_lib.sep,parts);
return ((function (node_path_lib,util_pattern,util_path,parts,root_path,pred__44720,expr__44721){
return (function (request_url,callback){

var cache_path = node_path_lib.resolve(root_path,request_url);
goog.object.remove(require.cache,cache_path);

return callback.call(null,(function (){try{return require(cache_path);
}catch (e44723){if((e44723 instanceof Error)){
var e = e44723;
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"error","error",-978969032),["Figwheel: Error loading file ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cache_path)].join(''));

figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"error","error",-978969032),e.stack);

return false;
} else {
throw e44723;

}
}})());
});
;})(node_path_lib,util_pattern,util_path,parts,root_path,pred__44720,expr__44721))
} else {
if(cljs.core.truth_(pred__44720.call(null,new cljs.core.Keyword(null,"html","html",-998796897),expr__44721))){
return figwheel.client.file_reloading.reload_file_in_html_env;
} else {
if(cljs.core.truth_(pred__44720.call(null,new cljs.core.Keyword(null,"react-native","react-native",-1543085138),expr__44721))){
return figwheel.client.file_reloading.reload_file_in_html_env;
} else {
if(cljs.core.truth_(pred__44720.call(null,new cljs.core.Keyword(null,"worker","worker",938239996),expr__44721))){
return ((function (pred__44720,expr__44721){
return (function (request_url,callback){

return callback.call(null,(function (){try{self.importScripts(figwheel.client.file_reloading.add_cache_buster.call(null,request_url));

return true;
}catch (e44724){if((e44724 instanceof Error)){
var e = e44724;
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"error","error",-978969032),["Figwheel: Error loading file ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(request_url)].join(''));

figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"error","error",-978969032),e.stack);

return false;
} else {
throw e44724;

}
}})());
});
;})(pred__44720,expr__44721))
} else {
return ((function (pred__44720,expr__44721){
return (function (a,b){
throw "Reload not defined for this platform";
});
;})(pred__44720,expr__44721))
}
}
}
}
})();
figwheel.client.file_reloading.reload_file = (function figwheel$client$file_reloading$reload_file(p__44725,callback){
var map__44726 = p__44725;
var map__44726__$1 = ((((!((map__44726 == null)))?((((map__44726.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__44726.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__44726):map__44726);
var file_msg = map__44726__$1;
var request_url = cljs.core.get.call(null,map__44726__$1,new cljs.core.Keyword(null,"request-url","request-url",2100346596));

figwheel.client.utils.debug_prn.call(null,["FigWheel: Attempting to load ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(request_url)].join(''));

return figwheel.client.file_reloading.reload_file_STAR_.call(null,request_url,((function (map__44726,map__44726__$1,file_msg,request_url){
return (function (success_QMARK_){
if(cljs.core.truth_(success_QMARK_)){
figwheel.client.utils.debug_prn.call(null,["FigWheel: Successfully loaded ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(request_url)].join(''));

return cljs.core.apply.call(null,callback,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.assoc.call(null,file_msg,new cljs.core.Keyword(null,"loaded-file","loaded-file",-168399375),true)], null));
} else {
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"error","error",-978969032),["Figwheel: Error loading file ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(request_url)].join(''));

return cljs.core.apply.call(null,callback,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [file_msg], null));
}
});})(map__44726,map__44726__$1,file_msg,request_url))
);
});
if(typeof figwheel.client.file_reloading.reload_chan !== 'undefined'){
} else {
figwheel.client.file_reloading.reload_chan = cljs.core.async.chan.call(null);
}
if(typeof figwheel.client.file_reloading.on_load_callbacks !== 'undefined'){
} else {
figwheel.client.file_reloading.on_load_callbacks = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
}
if(typeof figwheel.client.file_reloading.dependencies_loaded !== 'undefined'){
} else {
figwheel.client.file_reloading.dependencies_loaded = cljs.core.atom.call(null,cljs.core.PersistentVector.EMPTY);
}
figwheel.client.file_reloading.blocking_load = (function figwheel$client$file_reloading$blocking_load(url){
var out = cljs.core.async.chan.call(null);
figwheel.client.file_reloading.reload_file.call(null,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"request-url","request-url",2100346596),url], null),((function (out){
return (function (file_msg){
cljs.core.async.put_BANG_.call(null,out,file_msg);

return cljs.core.async.close_BANG_.call(null,out);
});})(out))
);

return out;
});
if(typeof figwheel.client.file_reloading.reloader_loop !== 'undefined'){
} else {
figwheel.client.file_reloading.reloader_loop = (function (){var c__35058__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__35058__auto__){
return (function (){
var f__35059__auto__ = (function (){var switch__34968__auto__ = ((function (c__35058__auto__){
return (function (state_44750){
var state_val_44751 = (state_44750[(1)]);
if((state_val_44751 === (7))){
var inst_44746 = (state_44750[(2)]);
var state_44750__$1 = state_44750;
var statearr_44752_44769 = state_44750__$1;
(statearr_44752_44769[(2)] = inst_44746);

(statearr_44752_44769[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44751 === (1))){
var state_44750__$1 = state_44750;
var statearr_44753_44770 = state_44750__$1;
(statearr_44753_44770[(2)] = null);

(statearr_44753_44770[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44751 === (4))){
var inst_44730 = (state_44750[(7)]);
var inst_44730__$1 = (state_44750[(2)]);
var state_44750__$1 = (function (){var statearr_44754 = state_44750;
(statearr_44754[(7)] = inst_44730__$1);

return statearr_44754;
})();
if(cljs.core.truth_(inst_44730__$1)){
var statearr_44755_44771 = state_44750__$1;
(statearr_44755_44771[(1)] = (5));

} else {
var statearr_44756_44772 = state_44750__$1;
(statearr_44756_44772[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44751 === (6))){
var state_44750__$1 = state_44750;
var statearr_44757_44773 = state_44750__$1;
(statearr_44757_44773[(2)] = null);

(statearr_44757_44773[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44751 === (3))){
var inst_44748 = (state_44750[(2)]);
var state_44750__$1 = state_44750;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_44750__$1,inst_44748);
} else {
if((state_val_44751 === (2))){
var state_44750__$1 = state_44750;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_44750__$1,(4),figwheel.client.file_reloading.reload_chan);
} else {
if((state_val_44751 === (11))){
var inst_44742 = (state_44750[(2)]);
var state_44750__$1 = (function (){var statearr_44758 = state_44750;
(statearr_44758[(8)] = inst_44742);

return statearr_44758;
})();
var statearr_44759_44774 = state_44750__$1;
(statearr_44759_44774[(2)] = null);

(statearr_44759_44774[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44751 === (9))){
var inst_44736 = (state_44750[(9)]);
var inst_44734 = (state_44750[(10)]);
var inst_44738 = inst_44736.call(null,inst_44734);
var state_44750__$1 = state_44750;
var statearr_44760_44775 = state_44750__$1;
(statearr_44760_44775[(2)] = inst_44738);

(statearr_44760_44775[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44751 === (5))){
var inst_44730 = (state_44750[(7)]);
var inst_44732 = figwheel.client.file_reloading.blocking_load.call(null,inst_44730);
var state_44750__$1 = state_44750;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_44750__$1,(8),inst_44732);
} else {
if((state_val_44751 === (10))){
var inst_44734 = (state_44750[(10)]);
var inst_44740 = cljs.core.swap_BANG_.call(null,figwheel.client.file_reloading.dependencies_loaded,cljs.core.conj,inst_44734);
var state_44750__$1 = state_44750;
var statearr_44761_44776 = state_44750__$1;
(statearr_44761_44776[(2)] = inst_44740);

(statearr_44761_44776[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44751 === (8))){
var inst_44730 = (state_44750[(7)]);
var inst_44736 = (state_44750[(9)]);
var inst_44734 = (state_44750[(2)]);
var inst_44735 = cljs.core.deref.call(null,figwheel.client.file_reloading.on_load_callbacks);
var inst_44736__$1 = cljs.core.get.call(null,inst_44735,inst_44730);
var state_44750__$1 = (function (){var statearr_44762 = state_44750;
(statearr_44762[(9)] = inst_44736__$1);

(statearr_44762[(10)] = inst_44734);

return statearr_44762;
})();
if(cljs.core.truth_(inst_44736__$1)){
var statearr_44763_44777 = state_44750__$1;
(statearr_44763_44777[(1)] = (9));

} else {
var statearr_44764_44778 = state_44750__$1;
(statearr_44764_44778[(1)] = (10));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__35058__auto__))
;
return ((function (switch__34968__auto__,c__35058__auto__){
return (function() {
var figwheel$client$file_reloading$state_machine__34969__auto__ = null;
var figwheel$client$file_reloading$state_machine__34969__auto____0 = (function (){
var statearr_44765 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_44765[(0)] = figwheel$client$file_reloading$state_machine__34969__auto__);

(statearr_44765[(1)] = (1));

return statearr_44765;
});
var figwheel$client$file_reloading$state_machine__34969__auto____1 = (function (state_44750){
while(true){
var ret_value__34970__auto__ = (function (){try{while(true){
var result__34971__auto__ = switch__34968__auto__.call(null,state_44750);
if(cljs.core.keyword_identical_QMARK_.call(null,result__34971__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__34971__auto__;
}
break;
}
}catch (e44766){if((e44766 instanceof Object)){
var ex__34972__auto__ = e44766;
var statearr_44767_44779 = state_44750;
(statearr_44767_44779[(5)] = ex__34972__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_44750);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e44766;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__34970__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__44780 = state_44750;
state_44750 = G__44780;
continue;
} else {
return ret_value__34970__auto__;
}
break;
}
});
figwheel$client$file_reloading$state_machine__34969__auto__ = function(state_44750){
switch(arguments.length){
case 0:
return figwheel$client$file_reloading$state_machine__34969__auto____0.call(this);
case 1:
return figwheel$client$file_reloading$state_machine__34969__auto____1.call(this,state_44750);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
figwheel$client$file_reloading$state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$file_reloading$state_machine__34969__auto____0;
figwheel$client$file_reloading$state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$file_reloading$state_machine__34969__auto____1;
return figwheel$client$file_reloading$state_machine__34969__auto__;
})()
;})(switch__34968__auto__,c__35058__auto__))
})();
var state__35060__auto__ = (function (){var statearr_44768 = f__35059__auto__.call(null);
(statearr_44768[(6)] = c__35058__auto__);

return statearr_44768;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__35060__auto__);
});})(c__35058__auto__))
);

return c__35058__auto__;
})();
}
figwheel.client.file_reloading.queued_file_reload = (function figwheel$client$file_reloading$queued_file_reload(url){
return cljs.core.async.put_BANG_.call(null,figwheel.client.file_reloading.reload_chan,url);
});
figwheel.client.file_reloading.require_with_callback = (function figwheel$client$file_reloading$require_with_callback(p__44781,callback){
var map__44782 = p__44781;
var map__44782__$1 = ((((!((map__44782 == null)))?((((map__44782.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__44782.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__44782):map__44782);
var file_msg = map__44782__$1;
var namespace = cljs.core.get.call(null,map__44782__$1,new cljs.core.Keyword(null,"namespace","namespace",-377510372));
var request_url = figwheel.client.file_reloading.resolve_ns.call(null,namespace);
cljs.core.swap_BANG_.call(null,figwheel.client.file_reloading.on_load_callbacks,cljs.core.assoc,request_url,((function (request_url,map__44782,map__44782__$1,file_msg,namespace){
return (function (file_msg_SINGLEQUOTE_){
cljs.core.swap_BANG_.call(null,figwheel.client.file_reloading.on_load_callbacks,cljs.core.dissoc,request_url);

return cljs.core.apply.call(null,callback,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.merge.call(null,file_msg,cljs.core.select_keys.call(null,file_msg_SINGLEQUOTE_,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"loaded-file","loaded-file",-168399375)], null)))], null));
});})(request_url,map__44782,map__44782__$1,file_msg,namespace))
);

return figwheel.client.file_reloading.figwheel_require.call(null,cljs.core.name.call(null,namespace),true);
});
figwheel.client.file_reloading.figwheel_no_load_QMARK_ = (function figwheel$client$file_reloading$figwheel_no_load_QMARK_(p__44784){
var map__44785 = p__44784;
var map__44785__$1 = ((((!((map__44785 == null)))?((((map__44785.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__44785.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__44785):map__44785);
var file_msg = map__44785__$1;
var namespace = cljs.core.get.call(null,map__44785__$1,new cljs.core.Keyword(null,"namespace","namespace",-377510372));
var meta_pragmas = cljs.core.get.call(null,cljs.core.deref.call(null,figwheel.client.file_reloading.figwheel_meta_pragmas),cljs.core.name.call(null,namespace));
return new cljs.core.Keyword(null,"figwheel-no-load","figwheel-no-load",-555840179).cljs$core$IFn$_invoke$arity$1(meta_pragmas);
});
figwheel.client.file_reloading.reload_file_QMARK_ = (function figwheel$client$file_reloading$reload_file_QMARK_(p__44787){
var map__44788 = p__44787;
var map__44788__$1 = ((((!((map__44788 == null)))?((((map__44788.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__44788.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__44788):map__44788);
var file_msg = map__44788__$1;
var namespace = cljs.core.get.call(null,map__44788__$1,new cljs.core.Keyword(null,"namespace","namespace",-377510372));

var meta_pragmas = cljs.core.get.call(null,cljs.core.deref.call(null,figwheel.client.file_reloading.figwheel_meta_pragmas),cljs.core.name.call(null,namespace));
var and__30170__auto__ = cljs.core.not.call(null,figwheel.client.file_reloading.figwheel_no_load_QMARK_.call(null,file_msg));
if(and__30170__auto__){
var or__30182__auto__ = new cljs.core.Keyword(null,"figwheel-always","figwheel-always",799819691).cljs$core$IFn$_invoke$arity$1(meta_pragmas);
if(cljs.core.truth_(or__30182__auto__)){
return or__30182__auto__;
} else {
var or__30182__auto____$1 = new cljs.core.Keyword(null,"figwheel-load","figwheel-load",1316089175).cljs$core$IFn$_invoke$arity$1(meta_pragmas);
if(cljs.core.truth_(or__30182__auto____$1)){
return or__30182__auto____$1;
} else {
return figwheel.client.file_reloading.provided_QMARK_.call(null,cljs.core.name.call(null,namespace));
}
}
} else {
return and__30170__auto__;
}
});
figwheel.client.file_reloading.js_reload = (function figwheel$client$file_reloading$js_reload(p__44790,callback){
var map__44791 = p__44790;
var map__44791__$1 = ((((!((map__44791 == null)))?((((map__44791.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__44791.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__44791):map__44791);
var file_msg = map__44791__$1;
var request_url = cljs.core.get.call(null,map__44791__$1,new cljs.core.Keyword(null,"request-url","request-url",2100346596));
var namespace = cljs.core.get.call(null,map__44791__$1,new cljs.core.Keyword(null,"namespace","namespace",-377510372));

if(cljs.core.truth_(figwheel.client.file_reloading.reload_file_QMARK_.call(null,file_msg))){
return figwheel.client.file_reloading.require_with_callback.call(null,file_msg,callback);
} else {
figwheel.client.utils.debug_prn.call(null,["Figwheel: Not trying to load file ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(request_url)].join(''));

return cljs.core.apply.call(null,callback,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [file_msg], null));
}
});
figwheel.client.file_reloading.reload_js_file = (function figwheel$client$file_reloading$reload_js_file(file_msg){
var out = cljs.core.async.chan.call(null);
figwheel.client.file_reloading.js_reload.call(null,file_msg,((function (out){
return (function (url){
cljs.core.async.put_BANG_.call(null,out,url);

return cljs.core.async.close_BANG_.call(null,out);
});})(out))
);

return out;
});
/**
 * Returns a chanel with one collection of loaded filenames on it.
 */
figwheel.client.file_reloading.load_all_js_files = (function figwheel$client$file_reloading$load_all_js_files(files){
var out = cljs.core.async.chan.call(null);
var c__35058__auto___44841 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__35058__auto___44841,out){
return (function (){
var f__35059__auto__ = (function (){var switch__34968__auto__ = ((function (c__35058__auto___44841,out){
return (function (state_44826){
var state_val_44827 = (state_44826[(1)]);
if((state_val_44827 === (1))){
var inst_44800 = cljs.core.seq.call(null,files);
var inst_44801 = cljs.core.first.call(null,inst_44800);
var inst_44802 = cljs.core.next.call(null,inst_44800);
var inst_44803 = files;
var state_44826__$1 = (function (){var statearr_44828 = state_44826;
(statearr_44828[(7)] = inst_44802);

(statearr_44828[(8)] = inst_44801);

(statearr_44828[(9)] = inst_44803);

return statearr_44828;
})();
var statearr_44829_44842 = state_44826__$1;
(statearr_44829_44842[(2)] = null);

(statearr_44829_44842[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44827 === (2))){
var inst_44809 = (state_44826[(10)]);
var inst_44803 = (state_44826[(9)]);
var inst_44808 = cljs.core.seq.call(null,inst_44803);
var inst_44809__$1 = cljs.core.first.call(null,inst_44808);
var inst_44810 = cljs.core.next.call(null,inst_44808);
var inst_44811 = (inst_44809__$1 == null);
var inst_44812 = cljs.core.not.call(null,inst_44811);
var state_44826__$1 = (function (){var statearr_44830 = state_44826;
(statearr_44830[(11)] = inst_44810);

(statearr_44830[(10)] = inst_44809__$1);

return statearr_44830;
})();
if(inst_44812){
var statearr_44831_44843 = state_44826__$1;
(statearr_44831_44843[(1)] = (4));

} else {
var statearr_44832_44844 = state_44826__$1;
(statearr_44832_44844[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44827 === (3))){
var inst_44824 = (state_44826[(2)]);
var state_44826__$1 = state_44826;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_44826__$1,inst_44824);
} else {
if((state_val_44827 === (4))){
var inst_44809 = (state_44826[(10)]);
var inst_44814 = figwheel.client.file_reloading.reload_js_file.call(null,inst_44809);
var state_44826__$1 = state_44826;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_44826__$1,(7),inst_44814);
} else {
if((state_val_44827 === (5))){
var inst_44820 = cljs.core.async.close_BANG_.call(null,out);
var state_44826__$1 = state_44826;
var statearr_44833_44845 = state_44826__$1;
(statearr_44833_44845[(2)] = inst_44820);

(statearr_44833_44845[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44827 === (6))){
var inst_44822 = (state_44826[(2)]);
var state_44826__$1 = state_44826;
var statearr_44834_44846 = state_44826__$1;
(statearr_44834_44846[(2)] = inst_44822);

(statearr_44834_44846[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44827 === (7))){
var inst_44810 = (state_44826[(11)]);
var inst_44816 = (state_44826[(2)]);
var inst_44817 = cljs.core.async.put_BANG_.call(null,out,inst_44816);
var inst_44803 = inst_44810;
var state_44826__$1 = (function (){var statearr_44835 = state_44826;
(statearr_44835[(9)] = inst_44803);

(statearr_44835[(12)] = inst_44817);

return statearr_44835;
})();
var statearr_44836_44847 = state_44826__$1;
(statearr_44836_44847[(2)] = null);

(statearr_44836_44847[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(c__35058__auto___44841,out))
;
return ((function (switch__34968__auto__,c__35058__auto___44841,out){
return (function() {
var figwheel$client$file_reloading$load_all_js_files_$_state_machine__34969__auto__ = null;
var figwheel$client$file_reloading$load_all_js_files_$_state_machine__34969__auto____0 = (function (){
var statearr_44837 = [null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_44837[(0)] = figwheel$client$file_reloading$load_all_js_files_$_state_machine__34969__auto__);

(statearr_44837[(1)] = (1));

return statearr_44837;
});
var figwheel$client$file_reloading$load_all_js_files_$_state_machine__34969__auto____1 = (function (state_44826){
while(true){
var ret_value__34970__auto__ = (function (){try{while(true){
var result__34971__auto__ = switch__34968__auto__.call(null,state_44826);
if(cljs.core.keyword_identical_QMARK_.call(null,result__34971__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__34971__auto__;
}
break;
}
}catch (e44838){if((e44838 instanceof Object)){
var ex__34972__auto__ = e44838;
var statearr_44839_44848 = state_44826;
(statearr_44839_44848[(5)] = ex__34972__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_44826);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e44838;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__34970__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__44849 = state_44826;
state_44826 = G__44849;
continue;
} else {
return ret_value__34970__auto__;
}
break;
}
});
figwheel$client$file_reloading$load_all_js_files_$_state_machine__34969__auto__ = function(state_44826){
switch(arguments.length){
case 0:
return figwheel$client$file_reloading$load_all_js_files_$_state_machine__34969__auto____0.call(this);
case 1:
return figwheel$client$file_reloading$load_all_js_files_$_state_machine__34969__auto____1.call(this,state_44826);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
figwheel$client$file_reloading$load_all_js_files_$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$file_reloading$load_all_js_files_$_state_machine__34969__auto____0;
figwheel$client$file_reloading$load_all_js_files_$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$file_reloading$load_all_js_files_$_state_machine__34969__auto____1;
return figwheel$client$file_reloading$load_all_js_files_$_state_machine__34969__auto__;
})()
;})(switch__34968__auto__,c__35058__auto___44841,out))
})();
var state__35060__auto__ = (function (){var statearr_44840 = f__35059__auto__.call(null);
(statearr_44840[(6)] = c__35058__auto___44841);

return statearr_44840;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__35060__auto__);
});})(c__35058__auto___44841,out))
);


return cljs.core.async.into.call(null,cljs.core.PersistentVector.EMPTY,out);
});
figwheel.client.file_reloading.eval_body = (function figwheel$client$file_reloading$eval_body(p__44850,opts){
var map__44851 = p__44850;
var map__44851__$1 = ((((!((map__44851 == null)))?((((map__44851.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__44851.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__44851):map__44851);
var eval_body = cljs.core.get.call(null,map__44851__$1,new cljs.core.Keyword(null,"eval-body","eval-body",-907279883));
var file = cljs.core.get.call(null,map__44851__$1,new cljs.core.Keyword(null,"file","file",-1269645878));
if(cljs.core.truth_((function (){var and__30170__auto__ = eval_body;
if(cljs.core.truth_(and__30170__auto__)){
return typeof eval_body === 'string';
} else {
return and__30170__auto__;
}
})())){
var code = eval_body;
try{figwheel.client.utils.debug_prn.call(null,["Evaling file ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(file)].join(''));

return figwheel.client.utils.eval_helper.call(null,code,opts);
}catch (e44853){var e = e44853;
return figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"error","error",-978969032),["Unable to evaluate ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(file)].join(''));
}} else {
return null;
}
});
figwheel.client.file_reloading.expand_files = (function figwheel$client$file_reloading$expand_files(files){
var deps = figwheel.client.file_reloading.get_all_dependents.call(null,cljs.core.map.call(null,new cljs.core.Keyword(null,"namespace","namespace",-377510372),files));
return cljs.core.filter.call(null,cljs.core.comp.call(null,cljs.core.not,cljs.core.partial.call(null,cljs.core.re_matches,/figwheel\.connect.*/),new cljs.core.Keyword(null,"namespace","namespace",-377510372)),cljs.core.map.call(null,((function (deps){
return (function (n){
var temp__5276__auto__ = cljs.core.first.call(null,cljs.core.filter.call(null,((function (deps){
return (function (p1__44854_SHARP_){
return cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"namespace","namespace",-377510372).cljs$core$IFn$_invoke$arity$1(p1__44854_SHARP_),n);
});})(deps))
,files));
if(cljs.core.truth_(temp__5276__auto__)){
var file_msg = temp__5276__auto__;
return file_msg;
} else {
return new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"namespace","namespace",-377510372),new cljs.core.Keyword(null,"namespace","namespace",-377510372),n], null);
}
});})(deps))
,deps));
});
figwheel.client.file_reloading.sort_files = (function figwheel$client$file_reloading$sort_files(files){
if((cljs.core.count.call(null,files) <= (1))){
return files;
} else {
var keep_files = cljs.core.set.call(null,cljs.core.keep.call(null,new cljs.core.Keyword(null,"namespace","namespace",-377510372),files));
return cljs.core.filter.call(null,cljs.core.comp.call(null,keep_files,new cljs.core.Keyword(null,"namespace","namespace",-377510372)),figwheel.client.file_reloading.expand_files.call(null,files));
}
});
figwheel.client.file_reloading.get_figwheel_always = (function figwheel$client$file_reloading$get_figwheel_always(){
return cljs.core.map.call(null,(function (p__44855){
var vec__44856 = p__44855;
var k = cljs.core.nth.call(null,vec__44856,(0),null);
var v = cljs.core.nth.call(null,vec__44856,(1),null);
return new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"namespace","namespace",-377510372),k,new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"namespace","namespace",-377510372)], null);
}),cljs.core.filter.call(null,(function (p__44859){
var vec__44860 = p__44859;
var k = cljs.core.nth.call(null,vec__44860,(0),null);
var v = cljs.core.nth.call(null,vec__44860,(1),null);
return new cljs.core.Keyword(null,"figwheel-always","figwheel-always",799819691).cljs$core$IFn$_invoke$arity$1(v);
}),cljs.core.deref.call(null,figwheel.client.file_reloading.figwheel_meta_pragmas)));
});
figwheel.client.file_reloading.reload_js_files = (function figwheel$client$file_reloading$reload_js_files(p__44866,p__44867){
var map__44868 = p__44866;
var map__44868__$1 = ((((!((map__44868 == null)))?((((map__44868.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__44868.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__44868):map__44868);
var opts = map__44868__$1;
var before_jsload = cljs.core.get.call(null,map__44868__$1,new cljs.core.Keyword(null,"before-jsload","before-jsload",-847513128));
var on_jsload = cljs.core.get.call(null,map__44868__$1,new cljs.core.Keyword(null,"on-jsload","on-jsload",-395756602));
var reload_dependents = cljs.core.get.call(null,map__44868__$1,new cljs.core.Keyword(null,"reload-dependents","reload-dependents",-956865430));
var map__44869 = p__44867;
var map__44869__$1 = ((((!((map__44869 == null)))?((((map__44869.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__44869.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__44869):map__44869);
var msg = map__44869__$1;
var files = cljs.core.get.call(null,map__44869__$1,new cljs.core.Keyword(null,"files","files",-472457450));
var figwheel_meta = cljs.core.get.call(null,map__44869__$1,new cljs.core.Keyword(null,"figwheel-meta","figwheel-meta",-225970237));
var recompile_dependents = cljs.core.get.call(null,map__44869__$1,new cljs.core.Keyword(null,"recompile-dependents","recompile-dependents",523804171));
if(cljs.core.empty_QMARK_.call(null,figwheel_meta)){
} else {
cljs.core.reset_BANG_.call(null,figwheel.client.file_reloading.figwheel_meta_pragmas,figwheel_meta);
}

var c__35058__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__35058__auto__,map__44868,map__44868__$1,opts,before_jsload,on_jsload,reload_dependents,map__44869,map__44869__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (){
var f__35059__auto__ = (function (){var switch__34968__auto__ = ((function (c__35058__auto__,map__44868,map__44868__$1,opts,before_jsload,on_jsload,reload_dependents,map__44869,map__44869__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (state_45023){
var state_val_45024 = (state_45023[(1)]);
if((state_val_45024 === (7))){
var inst_44885 = (state_45023[(7)]);
var inst_44883 = (state_45023[(8)]);
var inst_44886 = (state_45023[(9)]);
var inst_44884 = (state_45023[(10)]);
var inst_44891 = cljs.core._nth.call(null,inst_44884,inst_44886);
var inst_44892 = figwheel.client.file_reloading.eval_body.call(null,inst_44891,opts);
var inst_44893 = (inst_44886 + (1));
var tmp45025 = inst_44885;
var tmp45026 = inst_44883;
var tmp45027 = inst_44884;
var inst_44883__$1 = tmp45026;
var inst_44884__$1 = tmp45027;
var inst_44885__$1 = tmp45025;
var inst_44886__$1 = inst_44893;
var state_45023__$1 = (function (){var statearr_45028 = state_45023;
(statearr_45028[(11)] = inst_44892);

(statearr_45028[(7)] = inst_44885__$1);

(statearr_45028[(8)] = inst_44883__$1);

(statearr_45028[(9)] = inst_44886__$1);

(statearr_45028[(10)] = inst_44884__$1);

return statearr_45028;
})();
var statearr_45029_45112 = state_45023__$1;
(statearr_45029_45112[(2)] = null);

(statearr_45029_45112[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_45024 === (20))){
var inst_44926 = (state_45023[(12)]);
var inst_44934 = figwheel.client.file_reloading.sort_files.call(null,inst_44926);
var state_45023__$1 = state_45023;
var statearr_45030_45113 = state_45023__$1;
(statearr_45030_45113[(2)] = inst_44934);

(statearr_45030_45113[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_45024 === (27))){
var state_45023__$1 = state_45023;
var statearr_45031_45114 = state_45023__$1;
(statearr_45031_45114[(2)] = null);

(statearr_45031_45114[(1)] = (28));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_45024 === (1))){
var inst_44875 = (state_45023[(13)]);
var inst_44872 = before_jsload.call(null,files);
var inst_44873 = figwheel.client.file_reloading.before_jsload_custom_event.call(null,files);
var inst_44874 = (function (){return ((function (inst_44875,inst_44872,inst_44873,state_val_45024,c__35058__auto__,map__44868,map__44868__$1,opts,before_jsload,on_jsload,reload_dependents,map__44869,map__44869__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (p1__44863_SHARP_){
return new cljs.core.Keyword(null,"eval-body","eval-body",-907279883).cljs$core$IFn$_invoke$arity$1(p1__44863_SHARP_);
});
;})(inst_44875,inst_44872,inst_44873,state_val_45024,c__35058__auto__,map__44868,map__44868__$1,opts,before_jsload,on_jsload,reload_dependents,map__44869,map__44869__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_44875__$1 = cljs.core.filter.call(null,inst_44874,files);
var inst_44876 = cljs.core.not_empty.call(null,inst_44875__$1);
var state_45023__$1 = (function (){var statearr_45032 = state_45023;
(statearr_45032[(13)] = inst_44875__$1);

(statearr_45032[(14)] = inst_44872);

(statearr_45032[(15)] = inst_44873);

return statearr_45032;
})();
if(cljs.core.truth_(inst_44876)){
var statearr_45033_45115 = state_45023__$1;
(statearr_45033_45115[(1)] = (2));

} else {
var statearr_45034_45116 = state_45023__$1;
(statearr_45034_45116[(1)] = (3));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_45024 === (24))){
var state_45023__$1 = state_45023;
var statearr_45035_45117 = state_45023__$1;
(statearr_45035_45117[(2)] = null);

(statearr_45035_45117[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_45024 === (39))){
var inst_44976 = (state_45023[(16)]);
var state_45023__$1 = state_45023;
var statearr_45036_45118 = state_45023__$1;
(statearr_45036_45118[(2)] = inst_44976);

(statearr_45036_45118[(1)] = (40));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_45024 === (46))){
var inst_45018 = (state_45023[(2)]);
var state_45023__$1 = state_45023;
var statearr_45037_45119 = state_45023__$1;
(statearr_45037_45119[(2)] = inst_45018);

(statearr_45037_45119[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_45024 === (4))){
var inst_44920 = (state_45023[(2)]);
var inst_44921 = cljs.core.List.EMPTY;
var inst_44922 = cljs.core.reset_BANG_.call(null,figwheel.client.file_reloading.dependencies_loaded,inst_44921);
var inst_44923 = (function (){return ((function (inst_44920,inst_44921,inst_44922,state_val_45024,c__35058__auto__,map__44868,map__44868__$1,opts,before_jsload,on_jsload,reload_dependents,map__44869,map__44869__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (p1__44864_SHARP_){
var and__30170__auto__ = new cljs.core.Keyword(null,"namespace","namespace",-377510372).cljs$core$IFn$_invoke$arity$1(p1__44864_SHARP_);
if(cljs.core.truth_(and__30170__auto__)){
return (cljs.core.not.call(null,new cljs.core.Keyword(null,"eval-body","eval-body",-907279883).cljs$core$IFn$_invoke$arity$1(p1__44864_SHARP_))) && (cljs.core.not.call(null,figwheel.client.file_reloading.figwheel_no_load_QMARK_.call(null,p1__44864_SHARP_)));
} else {
return and__30170__auto__;
}
});
;})(inst_44920,inst_44921,inst_44922,state_val_45024,c__35058__auto__,map__44868,map__44868__$1,opts,before_jsload,on_jsload,reload_dependents,map__44869,map__44869__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_44924 = cljs.core.filter.call(null,inst_44923,files);
var inst_44925 = figwheel.client.file_reloading.get_figwheel_always.call(null);
var inst_44926 = cljs.core.concat.call(null,inst_44924,inst_44925);
var state_45023__$1 = (function (){var statearr_45038 = state_45023;
(statearr_45038[(12)] = inst_44926);

(statearr_45038[(17)] = inst_44922);

(statearr_45038[(18)] = inst_44920);

return statearr_45038;
})();
if(cljs.core.truth_(reload_dependents)){
var statearr_45039_45120 = state_45023__$1;
(statearr_45039_45120[(1)] = (16));

} else {
var statearr_45040_45121 = state_45023__$1;
(statearr_45040_45121[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_45024 === (15))){
var inst_44910 = (state_45023[(2)]);
var state_45023__$1 = state_45023;
var statearr_45041_45122 = state_45023__$1;
(statearr_45041_45122[(2)] = inst_44910);

(statearr_45041_45122[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_45024 === (21))){
var inst_44936 = (state_45023[(19)]);
var inst_44936__$1 = (state_45023[(2)]);
var inst_44937 = figwheel.client.file_reloading.load_all_js_files.call(null,inst_44936__$1);
var state_45023__$1 = (function (){var statearr_45042 = state_45023;
(statearr_45042[(19)] = inst_44936__$1);

return statearr_45042;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_45023__$1,(22),inst_44937);
} else {
if((state_val_45024 === (31))){
var inst_45021 = (state_45023[(2)]);
var state_45023__$1 = state_45023;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_45023__$1,inst_45021);
} else {
if((state_val_45024 === (32))){
var inst_44976 = (state_45023[(16)]);
var inst_44981 = inst_44976.cljs$lang$protocol_mask$partition0$;
var inst_44982 = (inst_44981 & (64));
var inst_44983 = inst_44976.cljs$core$ISeq$;
var inst_44984 = (cljs.core.PROTOCOL_SENTINEL === inst_44983);
var inst_44985 = (inst_44982) || (inst_44984);
var state_45023__$1 = state_45023;
if(cljs.core.truth_(inst_44985)){
var statearr_45043_45123 = state_45023__$1;
(statearr_45043_45123[(1)] = (35));

} else {
var statearr_45044_45124 = state_45023__$1;
(statearr_45044_45124[(1)] = (36));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_45024 === (40))){
var inst_44998 = (state_45023[(20)]);
var inst_44997 = (state_45023[(2)]);
var inst_44998__$1 = cljs.core.get.call(null,inst_44997,new cljs.core.Keyword(null,"figwheel-no-load","figwheel-no-load",-555840179));
var inst_44999 = cljs.core.get.call(null,inst_44997,new cljs.core.Keyword(null,"not-required","not-required",-950359114));
var inst_45000 = cljs.core.not_empty.call(null,inst_44998__$1);
var state_45023__$1 = (function (){var statearr_45045 = state_45023;
(statearr_45045[(21)] = inst_44999);

(statearr_45045[(20)] = inst_44998__$1);

return statearr_45045;
})();
if(cljs.core.truth_(inst_45000)){
var statearr_45046_45125 = state_45023__$1;
(statearr_45046_45125[(1)] = (41));

} else {
var statearr_45047_45126 = state_45023__$1;
(statearr_45047_45126[(1)] = (42));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_45024 === (33))){
var state_45023__$1 = state_45023;
var statearr_45048_45127 = state_45023__$1;
(statearr_45048_45127[(2)] = false);

(statearr_45048_45127[(1)] = (34));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_45024 === (13))){
var inst_44896 = (state_45023[(22)]);
var inst_44900 = cljs.core.chunk_first.call(null,inst_44896);
var inst_44901 = cljs.core.chunk_rest.call(null,inst_44896);
var inst_44902 = cljs.core.count.call(null,inst_44900);
var inst_44883 = inst_44901;
var inst_44884 = inst_44900;
var inst_44885 = inst_44902;
var inst_44886 = (0);
var state_45023__$1 = (function (){var statearr_45049 = state_45023;
(statearr_45049[(7)] = inst_44885);

(statearr_45049[(8)] = inst_44883);

(statearr_45049[(9)] = inst_44886);

(statearr_45049[(10)] = inst_44884);

return statearr_45049;
})();
var statearr_45050_45128 = state_45023__$1;
(statearr_45050_45128[(2)] = null);

(statearr_45050_45128[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_45024 === (22))){
var inst_44940 = (state_45023[(23)]);
var inst_44936 = (state_45023[(19)]);
var inst_44939 = (state_45023[(24)]);
var inst_44944 = (state_45023[(25)]);
var inst_44939__$1 = (state_45023[(2)]);
var inst_44940__$1 = cljs.core.filter.call(null,new cljs.core.Keyword(null,"loaded-file","loaded-file",-168399375),inst_44939__$1);
var inst_44941 = (function (){var all_files = inst_44936;
var res_SINGLEQUOTE_ = inst_44939__$1;
var res = inst_44940__$1;
return ((function (all_files,res_SINGLEQUOTE_,res,inst_44940,inst_44936,inst_44939,inst_44944,inst_44939__$1,inst_44940__$1,state_val_45024,c__35058__auto__,map__44868,map__44868__$1,opts,before_jsload,on_jsload,reload_dependents,map__44869,map__44869__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (p1__44865_SHARP_){
return cljs.core.not.call(null,new cljs.core.Keyword(null,"loaded-file","loaded-file",-168399375).cljs$core$IFn$_invoke$arity$1(p1__44865_SHARP_));
});
;})(all_files,res_SINGLEQUOTE_,res,inst_44940,inst_44936,inst_44939,inst_44944,inst_44939__$1,inst_44940__$1,state_val_45024,c__35058__auto__,map__44868,map__44868__$1,opts,before_jsload,on_jsload,reload_dependents,map__44869,map__44869__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_44942 = cljs.core.filter.call(null,inst_44941,inst_44939__$1);
var inst_44943 = cljs.core.deref.call(null,figwheel.client.file_reloading.dependencies_loaded);
var inst_44944__$1 = cljs.core.filter.call(null,new cljs.core.Keyword(null,"loaded-file","loaded-file",-168399375),inst_44943);
var inst_44945 = cljs.core.not_empty.call(null,inst_44944__$1);
var state_45023__$1 = (function (){var statearr_45051 = state_45023;
(statearr_45051[(23)] = inst_44940__$1);

(statearr_45051[(26)] = inst_44942);

(statearr_45051[(24)] = inst_44939__$1);

(statearr_45051[(25)] = inst_44944__$1);

return statearr_45051;
})();
if(cljs.core.truth_(inst_44945)){
var statearr_45052_45129 = state_45023__$1;
(statearr_45052_45129[(1)] = (23));

} else {
var statearr_45053_45130 = state_45023__$1;
(statearr_45053_45130[(1)] = (24));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_45024 === (36))){
var state_45023__$1 = state_45023;
var statearr_45054_45131 = state_45023__$1;
(statearr_45054_45131[(2)] = false);

(statearr_45054_45131[(1)] = (37));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_45024 === (41))){
var inst_44998 = (state_45023[(20)]);
var inst_45002 = cljs.core.comp.call(null,figwheel.client.file_reloading.name__GT_path,new cljs.core.Keyword(null,"namespace","namespace",-377510372));
var inst_45003 = cljs.core.map.call(null,inst_45002,inst_44998);
var inst_45004 = cljs.core.pr_str.call(null,inst_45003);
var inst_45005 = ["figwheel-no-load meta-data: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(inst_45004)].join('');
var inst_45006 = figwheel.client.utils.log.call(null,inst_45005);
var state_45023__$1 = state_45023;
var statearr_45055_45132 = state_45023__$1;
(statearr_45055_45132[(2)] = inst_45006);

(statearr_45055_45132[(1)] = (43));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_45024 === (43))){
var inst_44999 = (state_45023[(21)]);
var inst_45009 = (state_45023[(2)]);
var inst_45010 = cljs.core.not_empty.call(null,inst_44999);
var state_45023__$1 = (function (){var statearr_45056 = state_45023;
(statearr_45056[(27)] = inst_45009);

return statearr_45056;
})();
if(cljs.core.truth_(inst_45010)){
var statearr_45057_45133 = state_45023__$1;
(statearr_45057_45133[(1)] = (44));

} else {
var statearr_45058_45134 = state_45023__$1;
(statearr_45058_45134[(1)] = (45));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_45024 === (29))){
var inst_44940 = (state_45023[(23)]);
var inst_44976 = (state_45023[(16)]);
var inst_44936 = (state_45023[(19)]);
var inst_44942 = (state_45023[(26)]);
var inst_44939 = (state_45023[(24)]);
var inst_44944 = (state_45023[(25)]);
var inst_44972 = figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"debug","debug",-1608172596),"Figwheel: NOT loading these files ");
var inst_44975 = (function (){var all_files = inst_44936;
var res_SINGLEQUOTE_ = inst_44939;
var res = inst_44940;
var files_not_loaded = inst_44942;
var dependencies_that_loaded = inst_44944;
return ((function (all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_44940,inst_44976,inst_44936,inst_44942,inst_44939,inst_44944,inst_44972,state_val_45024,c__35058__auto__,map__44868,map__44868__$1,opts,before_jsload,on_jsload,reload_dependents,map__44869,map__44869__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (p__44974){
var map__45059 = p__44974;
var map__45059__$1 = ((((!((map__45059 == null)))?((((map__45059.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__45059.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__45059):map__45059);
var namespace = cljs.core.get.call(null,map__45059__$1,new cljs.core.Keyword(null,"namespace","namespace",-377510372));
var meta_data = cljs.core.get.call(null,cljs.core.deref.call(null,figwheel.client.file_reloading.figwheel_meta_pragmas),cljs.core.name.call(null,namespace));
if((meta_data == null)){
return new cljs.core.Keyword(null,"not-required","not-required",-950359114);
} else {
if(cljs.core.truth_(meta_data.call(null,new cljs.core.Keyword(null,"figwheel-no-load","figwheel-no-load",-555840179)))){
return new cljs.core.Keyword(null,"figwheel-no-load","figwheel-no-load",-555840179);
} else {
return new cljs.core.Keyword(null,"not-required","not-required",-950359114);

}
}
});
;})(all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_44940,inst_44976,inst_44936,inst_44942,inst_44939,inst_44944,inst_44972,state_val_45024,c__35058__auto__,map__44868,map__44868__$1,opts,before_jsload,on_jsload,reload_dependents,map__44869,map__44869__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_44976__$1 = cljs.core.group_by.call(null,inst_44975,inst_44942);
var inst_44978 = (inst_44976__$1 == null);
var inst_44979 = cljs.core.not.call(null,inst_44978);
var state_45023__$1 = (function (){var statearr_45061 = state_45023;
(statearr_45061[(16)] = inst_44976__$1);

(statearr_45061[(28)] = inst_44972);

return statearr_45061;
})();
if(inst_44979){
var statearr_45062_45135 = state_45023__$1;
(statearr_45062_45135[(1)] = (32));

} else {
var statearr_45063_45136 = state_45023__$1;
(statearr_45063_45136[(1)] = (33));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_45024 === (44))){
var inst_44999 = (state_45023[(21)]);
var inst_45012 = cljs.core.map.call(null,new cljs.core.Keyword(null,"file","file",-1269645878),inst_44999);
var inst_45013 = cljs.core.pr_str.call(null,inst_45012);
var inst_45014 = ["not required: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(inst_45013)].join('');
var inst_45015 = figwheel.client.utils.log.call(null,inst_45014);
var state_45023__$1 = state_45023;
var statearr_45064_45137 = state_45023__$1;
(statearr_45064_45137[(2)] = inst_45015);

(statearr_45064_45137[(1)] = (46));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_45024 === (6))){
var inst_44917 = (state_45023[(2)]);
var state_45023__$1 = state_45023;
var statearr_45065_45138 = state_45023__$1;
(statearr_45065_45138[(2)] = inst_44917);

(statearr_45065_45138[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_45024 === (28))){
var inst_44942 = (state_45023[(26)]);
var inst_44969 = (state_45023[(2)]);
var inst_44970 = cljs.core.not_empty.call(null,inst_44942);
var state_45023__$1 = (function (){var statearr_45066 = state_45023;
(statearr_45066[(29)] = inst_44969);

return statearr_45066;
})();
if(cljs.core.truth_(inst_44970)){
var statearr_45067_45139 = state_45023__$1;
(statearr_45067_45139[(1)] = (29));

} else {
var statearr_45068_45140 = state_45023__$1;
(statearr_45068_45140[(1)] = (30));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_45024 === (25))){
var inst_44940 = (state_45023[(23)]);
var inst_44956 = (state_45023[(2)]);
var inst_44957 = cljs.core.not_empty.call(null,inst_44940);
var state_45023__$1 = (function (){var statearr_45069 = state_45023;
(statearr_45069[(30)] = inst_44956);

return statearr_45069;
})();
if(cljs.core.truth_(inst_44957)){
var statearr_45070_45141 = state_45023__$1;
(statearr_45070_45141[(1)] = (26));

} else {
var statearr_45071_45142 = state_45023__$1;
(statearr_45071_45142[(1)] = (27));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_45024 === (34))){
var inst_44992 = (state_45023[(2)]);
var state_45023__$1 = state_45023;
if(cljs.core.truth_(inst_44992)){
var statearr_45072_45143 = state_45023__$1;
(statearr_45072_45143[(1)] = (38));

} else {
var statearr_45073_45144 = state_45023__$1;
(statearr_45073_45144[(1)] = (39));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_45024 === (17))){
var state_45023__$1 = state_45023;
var statearr_45074_45145 = state_45023__$1;
(statearr_45074_45145[(2)] = recompile_dependents);

(statearr_45074_45145[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_45024 === (3))){
var state_45023__$1 = state_45023;
var statearr_45075_45146 = state_45023__$1;
(statearr_45075_45146[(2)] = null);

(statearr_45075_45146[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_45024 === (12))){
var inst_44913 = (state_45023[(2)]);
var state_45023__$1 = state_45023;
var statearr_45076_45147 = state_45023__$1;
(statearr_45076_45147[(2)] = inst_44913);

(statearr_45076_45147[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_45024 === (2))){
var inst_44875 = (state_45023[(13)]);
var inst_44882 = cljs.core.seq.call(null,inst_44875);
var inst_44883 = inst_44882;
var inst_44884 = null;
var inst_44885 = (0);
var inst_44886 = (0);
var state_45023__$1 = (function (){var statearr_45077 = state_45023;
(statearr_45077[(7)] = inst_44885);

(statearr_45077[(8)] = inst_44883);

(statearr_45077[(9)] = inst_44886);

(statearr_45077[(10)] = inst_44884);

return statearr_45077;
})();
var statearr_45078_45148 = state_45023__$1;
(statearr_45078_45148[(2)] = null);

(statearr_45078_45148[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_45024 === (23))){
var inst_44940 = (state_45023[(23)]);
var inst_44936 = (state_45023[(19)]);
var inst_44942 = (state_45023[(26)]);
var inst_44939 = (state_45023[(24)]);
var inst_44944 = (state_45023[(25)]);
var inst_44947 = figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"debug","debug",-1608172596),"Figwheel: loaded these dependencies");
var inst_44949 = (function (){var all_files = inst_44936;
var res_SINGLEQUOTE_ = inst_44939;
var res = inst_44940;
var files_not_loaded = inst_44942;
var dependencies_that_loaded = inst_44944;
return ((function (all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_44940,inst_44936,inst_44942,inst_44939,inst_44944,inst_44947,state_val_45024,c__35058__auto__,map__44868,map__44868__$1,opts,before_jsload,on_jsload,reload_dependents,map__44869,map__44869__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (p__44948){
var map__45079 = p__44948;
var map__45079__$1 = ((((!((map__45079 == null)))?((((map__45079.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__45079.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__45079):map__45079);
var request_url = cljs.core.get.call(null,map__45079__$1,new cljs.core.Keyword(null,"request-url","request-url",2100346596));
return clojure.string.replace.call(null,request_url,goog.basePath,"");
});
;})(all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_44940,inst_44936,inst_44942,inst_44939,inst_44944,inst_44947,state_val_45024,c__35058__auto__,map__44868,map__44868__$1,opts,before_jsload,on_jsload,reload_dependents,map__44869,map__44869__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_44950 = cljs.core.reverse.call(null,inst_44944);
var inst_44951 = cljs.core.map.call(null,inst_44949,inst_44950);
var inst_44952 = cljs.core.pr_str.call(null,inst_44951);
var inst_44953 = figwheel.client.utils.log.call(null,inst_44952);
var state_45023__$1 = (function (){var statearr_45081 = state_45023;
(statearr_45081[(31)] = inst_44947);

return statearr_45081;
})();
var statearr_45082_45149 = state_45023__$1;
(statearr_45082_45149[(2)] = inst_44953);

(statearr_45082_45149[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_45024 === (35))){
var state_45023__$1 = state_45023;
var statearr_45083_45150 = state_45023__$1;
(statearr_45083_45150[(2)] = true);

(statearr_45083_45150[(1)] = (37));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_45024 === (19))){
var inst_44926 = (state_45023[(12)]);
var inst_44932 = figwheel.client.file_reloading.expand_files.call(null,inst_44926);
var state_45023__$1 = state_45023;
var statearr_45084_45151 = state_45023__$1;
(statearr_45084_45151[(2)] = inst_44932);

(statearr_45084_45151[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_45024 === (11))){
var state_45023__$1 = state_45023;
var statearr_45085_45152 = state_45023__$1;
(statearr_45085_45152[(2)] = null);

(statearr_45085_45152[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_45024 === (9))){
var inst_44915 = (state_45023[(2)]);
var state_45023__$1 = state_45023;
var statearr_45086_45153 = state_45023__$1;
(statearr_45086_45153[(2)] = inst_44915);

(statearr_45086_45153[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_45024 === (5))){
var inst_44885 = (state_45023[(7)]);
var inst_44886 = (state_45023[(9)]);
var inst_44888 = (inst_44886 < inst_44885);
var inst_44889 = inst_44888;
var state_45023__$1 = state_45023;
if(cljs.core.truth_(inst_44889)){
var statearr_45087_45154 = state_45023__$1;
(statearr_45087_45154[(1)] = (7));

} else {
var statearr_45088_45155 = state_45023__$1;
(statearr_45088_45155[(1)] = (8));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_45024 === (14))){
var inst_44896 = (state_45023[(22)]);
var inst_44905 = cljs.core.first.call(null,inst_44896);
var inst_44906 = figwheel.client.file_reloading.eval_body.call(null,inst_44905,opts);
var inst_44907 = cljs.core.next.call(null,inst_44896);
var inst_44883 = inst_44907;
var inst_44884 = null;
var inst_44885 = (0);
var inst_44886 = (0);
var state_45023__$1 = (function (){var statearr_45089 = state_45023;
(statearr_45089[(32)] = inst_44906);

(statearr_45089[(7)] = inst_44885);

(statearr_45089[(8)] = inst_44883);

(statearr_45089[(9)] = inst_44886);

(statearr_45089[(10)] = inst_44884);

return statearr_45089;
})();
var statearr_45090_45156 = state_45023__$1;
(statearr_45090_45156[(2)] = null);

(statearr_45090_45156[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_45024 === (45))){
var state_45023__$1 = state_45023;
var statearr_45091_45157 = state_45023__$1;
(statearr_45091_45157[(2)] = null);

(statearr_45091_45157[(1)] = (46));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_45024 === (26))){
var inst_44940 = (state_45023[(23)]);
var inst_44936 = (state_45023[(19)]);
var inst_44942 = (state_45023[(26)]);
var inst_44939 = (state_45023[(24)]);
var inst_44944 = (state_45023[(25)]);
var inst_44959 = figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"debug","debug",-1608172596),"Figwheel: loaded these files");
var inst_44961 = (function (){var all_files = inst_44936;
var res_SINGLEQUOTE_ = inst_44939;
var res = inst_44940;
var files_not_loaded = inst_44942;
var dependencies_that_loaded = inst_44944;
return ((function (all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_44940,inst_44936,inst_44942,inst_44939,inst_44944,inst_44959,state_val_45024,c__35058__auto__,map__44868,map__44868__$1,opts,before_jsload,on_jsload,reload_dependents,map__44869,map__44869__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (p__44960){
var map__45092 = p__44960;
var map__45092__$1 = ((((!((map__45092 == null)))?((((map__45092.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__45092.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__45092):map__45092);
var namespace = cljs.core.get.call(null,map__45092__$1,new cljs.core.Keyword(null,"namespace","namespace",-377510372));
var file = cljs.core.get.call(null,map__45092__$1,new cljs.core.Keyword(null,"file","file",-1269645878));
if(cljs.core.truth_(namespace)){
return figwheel.client.file_reloading.name__GT_path.call(null,cljs.core.name.call(null,namespace));
} else {
return file;
}
});
;})(all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_44940,inst_44936,inst_44942,inst_44939,inst_44944,inst_44959,state_val_45024,c__35058__auto__,map__44868,map__44868__$1,opts,before_jsload,on_jsload,reload_dependents,map__44869,map__44869__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_44962 = cljs.core.map.call(null,inst_44961,inst_44940);
var inst_44963 = cljs.core.pr_str.call(null,inst_44962);
var inst_44964 = figwheel.client.utils.log.call(null,inst_44963);
var inst_44965 = (function (){var all_files = inst_44936;
var res_SINGLEQUOTE_ = inst_44939;
var res = inst_44940;
var files_not_loaded = inst_44942;
var dependencies_that_loaded = inst_44944;
return ((function (all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_44940,inst_44936,inst_44942,inst_44939,inst_44944,inst_44959,inst_44961,inst_44962,inst_44963,inst_44964,state_val_45024,c__35058__auto__,map__44868,map__44868__$1,opts,before_jsload,on_jsload,reload_dependents,map__44869,map__44869__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (){
figwheel.client.file_reloading.on_jsload_custom_event.call(null,res);

return cljs.core.apply.call(null,on_jsload,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [res], null));
});
;})(all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_44940,inst_44936,inst_44942,inst_44939,inst_44944,inst_44959,inst_44961,inst_44962,inst_44963,inst_44964,state_val_45024,c__35058__auto__,map__44868,map__44868__$1,opts,before_jsload,on_jsload,reload_dependents,map__44869,map__44869__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_44966 = setTimeout(inst_44965,(10));
var state_45023__$1 = (function (){var statearr_45094 = state_45023;
(statearr_45094[(33)] = inst_44964);

(statearr_45094[(34)] = inst_44959);

return statearr_45094;
})();
var statearr_45095_45158 = state_45023__$1;
(statearr_45095_45158[(2)] = inst_44966);

(statearr_45095_45158[(1)] = (28));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_45024 === (16))){
var state_45023__$1 = state_45023;
var statearr_45096_45159 = state_45023__$1;
(statearr_45096_45159[(2)] = reload_dependents);

(statearr_45096_45159[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_45024 === (38))){
var inst_44976 = (state_45023[(16)]);
var inst_44994 = cljs.core.apply.call(null,cljs.core.hash_map,inst_44976);
var state_45023__$1 = state_45023;
var statearr_45097_45160 = state_45023__$1;
(statearr_45097_45160[(2)] = inst_44994);

(statearr_45097_45160[(1)] = (40));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_45024 === (30))){
var state_45023__$1 = state_45023;
var statearr_45098_45161 = state_45023__$1;
(statearr_45098_45161[(2)] = null);

(statearr_45098_45161[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_45024 === (10))){
var inst_44896 = (state_45023[(22)]);
var inst_44898 = cljs.core.chunked_seq_QMARK_.call(null,inst_44896);
var state_45023__$1 = state_45023;
if(inst_44898){
var statearr_45099_45162 = state_45023__$1;
(statearr_45099_45162[(1)] = (13));

} else {
var statearr_45100_45163 = state_45023__$1;
(statearr_45100_45163[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_45024 === (18))){
var inst_44930 = (state_45023[(2)]);
var state_45023__$1 = state_45023;
if(cljs.core.truth_(inst_44930)){
var statearr_45101_45164 = state_45023__$1;
(statearr_45101_45164[(1)] = (19));

} else {
var statearr_45102_45165 = state_45023__$1;
(statearr_45102_45165[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_45024 === (42))){
var state_45023__$1 = state_45023;
var statearr_45103_45166 = state_45023__$1;
(statearr_45103_45166[(2)] = null);

(statearr_45103_45166[(1)] = (43));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_45024 === (37))){
var inst_44989 = (state_45023[(2)]);
var state_45023__$1 = state_45023;
var statearr_45104_45167 = state_45023__$1;
(statearr_45104_45167[(2)] = inst_44989);

(statearr_45104_45167[(1)] = (34));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_45024 === (8))){
var inst_44896 = (state_45023[(22)]);
var inst_44883 = (state_45023[(8)]);
var inst_44896__$1 = cljs.core.seq.call(null,inst_44883);
var state_45023__$1 = (function (){var statearr_45105 = state_45023;
(statearr_45105[(22)] = inst_44896__$1);

return statearr_45105;
})();
if(inst_44896__$1){
var statearr_45106_45168 = state_45023__$1;
(statearr_45106_45168[(1)] = (10));

} else {
var statearr_45107_45169 = state_45023__$1;
(statearr_45107_45169[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__35058__auto__,map__44868,map__44868__$1,opts,before_jsload,on_jsload,reload_dependents,map__44869,map__44869__$1,msg,files,figwheel_meta,recompile_dependents))
;
return ((function (switch__34968__auto__,c__35058__auto__,map__44868,map__44868__$1,opts,before_jsload,on_jsload,reload_dependents,map__44869,map__44869__$1,msg,files,figwheel_meta,recompile_dependents){
return (function() {
var figwheel$client$file_reloading$reload_js_files_$_state_machine__34969__auto__ = null;
var figwheel$client$file_reloading$reload_js_files_$_state_machine__34969__auto____0 = (function (){
var statearr_45108 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_45108[(0)] = figwheel$client$file_reloading$reload_js_files_$_state_machine__34969__auto__);

(statearr_45108[(1)] = (1));

return statearr_45108;
});
var figwheel$client$file_reloading$reload_js_files_$_state_machine__34969__auto____1 = (function (state_45023){
while(true){
var ret_value__34970__auto__ = (function (){try{while(true){
var result__34971__auto__ = switch__34968__auto__.call(null,state_45023);
if(cljs.core.keyword_identical_QMARK_.call(null,result__34971__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__34971__auto__;
}
break;
}
}catch (e45109){if((e45109 instanceof Object)){
var ex__34972__auto__ = e45109;
var statearr_45110_45170 = state_45023;
(statearr_45110_45170[(5)] = ex__34972__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_45023);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e45109;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__34970__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__45171 = state_45023;
state_45023 = G__45171;
continue;
} else {
return ret_value__34970__auto__;
}
break;
}
});
figwheel$client$file_reloading$reload_js_files_$_state_machine__34969__auto__ = function(state_45023){
switch(arguments.length){
case 0:
return figwheel$client$file_reloading$reload_js_files_$_state_machine__34969__auto____0.call(this);
case 1:
return figwheel$client$file_reloading$reload_js_files_$_state_machine__34969__auto____1.call(this,state_45023);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
figwheel$client$file_reloading$reload_js_files_$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$file_reloading$reload_js_files_$_state_machine__34969__auto____0;
figwheel$client$file_reloading$reload_js_files_$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$file_reloading$reload_js_files_$_state_machine__34969__auto____1;
return figwheel$client$file_reloading$reload_js_files_$_state_machine__34969__auto__;
})()
;})(switch__34968__auto__,c__35058__auto__,map__44868,map__44868__$1,opts,before_jsload,on_jsload,reload_dependents,map__44869,map__44869__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var state__35060__auto__ = (function (){var statearr_45111 = f__35059__auto__.call(null);
(statearr_45111[(6)] = c__35058__auto__);

return statearr_45111;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__35060__auto__);
});})(c__35058__auto__,map__44868,map__44868__$1,opts,before_jsload,on_jsload,reload_dependents,map__44869,map__44869__$1,msg,files,figwheel_meta,recompile_dependents))
);

return c__35058__auto__;
});
figwheel.client.file_reloading.current_links = (function figwheel$client$file_reloading$current_links(){
return Array.prototype.slice.call(document.getElementsByTagName("link"));
});
figwheel.client.file_reloading.truncate_url = (function figwheel$client$file_reloading$truncate_url(url){
return clojure.string.replace_first.call(null,clojure.string.replace_first.call(null,clojure.string.replace_first.call(null,clojure.string.replace_first.call(null,cljs.core.first.call(null,clojure.string.split.call(null,url,/\?/)),[cljs.core.str.cljs$core$IFn$_invoke$arity$1(location.protocol),"//"].join(''),""),".*://",""),/^\/\//,""),/[^\\/]*/,"");
});
figwheel.client.file_reloading.matches_file_QMARK_ = (function figwheel$client$file_reloading$matches_file_QMARK_(p__45174,link){
var map__45175 = p__45174;
var map__45175__$1 = ((((!((map__45175 == null)))?((((map__45175.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__45175.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__45175):map__45175);
var file = cljs.core.get.call(null,map__45175__$1,new cljs.core.Keyword(null,"file","file",-1269645878));
var temp__5278__auto__ = link.href;
if(cljs.core.truth_(temp__5278__auto__)){
var link_href = temp__5278__auto__;
var match = clojure.string.join.call(null,"/",cljs.core.take_while.call(null,cljs.core.identity,cljs.core.map.call(null,((function (link_href,temp__5278__auto__,map__45175,map__45175__$1,file){
return (function (p1__45172_SHARP_,p2__45173_SHARP_){
if(cljs.core._EQ_.call(null,p1__45172_SHARP_,p2__45173_SHARP_)){
return p1__45172_SHARP_;
} else {
return false;
}
});})(link_href,temp__5278__auto__,map__45175,map__45175__$1,file))
,cljs.core.reverse.call(null,clojure.string.split.call(null,file,"/")),cljs.core.reverse.call(null,clojure.string.split.call(null,figwheel.client.file_reloading.truncate_url.call(null,link_href),"/")))));
var match_length = cljs.core.count.call(null,match);
var file_name_length = cljs.core.count.call(null,cljs.core.last.call(null,clojure.string.split.call(null,file,"/")));
if((match_length >= file_name_length)){
return new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"link","link",-1769163468),link,new cljs.core.Keyword(null,"link-href","link-href",-250644450),link_href,new cljs.core.Keyword(null,"match-length","match-length",1101537310),match_length,new cljs.core.Keyword(null,"current-url-length","current-url-length",380404083),cljs.core.count.call(null,figwheel.client.file_reloading.truncate_url.call(null,link_href))], null);
} else {
return null;
}
} else {
return null;
}
});
figwheel.client.file_reloading.get_correct_link = (function figwheel$client$file_reloading$get_correct_link(f_data){
var temp__5278__auto__ = cljs.core.first.call(null,cljs.core.sort_by.call(null,(function (p__45178){
var map__45179 = p__45178;
var map__45179__$1 = ((((!((map__45179 == null)))?((((map__45179.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__45179.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__45179):map__45179);
var match_length = cljs.core.get.call(null,map__45179__$1,new cljs.core.Keyword(null,"match-length","match-length",1101537310));
var current_url_length = cljs.core.get.call(null,map__45179__$1,new cljs.core.Keyword(null,"current-url-length","current-url-length",380404083));
return (current_url_length - match_length);
}),cljs.core.keep.call(null,(function (p1__45177_SHARP_){
return figwheel.client.file_reloading.matches_file_QMARK_.call(null,f_data,p1__45177_SHARP_);
}),figwheel.client.file_reloading.current_links.call(null))));
if(cljs.core.truth_(temp__5278__auto__)){
var res = temp__5278__auto__;
return new cljs.core.Keyword(null,"link","link",-1769163468).cljs$core$IFn$_invoke$arity$1(res);
} else {
return null;
}
});
figwheel.client.file_reloading.clone_link = (function figwheel$client$file_reloading$clone_link(link,url){
var clone = document.createElement("link");
clone.rel = "stylesheet";

clone.media = link.media;

clone.disabled = link.disabled;

clone.href = figwheel.client.file_reloading.add_cache_buster.call(null,url);

return clone;
});
figwheel.client.file_reloading.create_link = (function figwheel$client$file_reloading$create_link(url){
var link = document.createElement("link");
link.rel = "stylesheet";

link.href = figwheel.client.file_reloading.add_cache_buster.call(null,url);

return link;
});
figwheel.client.file_reloading.distinctify = (function figwheel$client$file_reloading$distinctify(key,seqq){
return cljs.core.vals.call(null,cljs.core.reduce.call(null,(function (p1__45181_SHARP_,p2__45182_SHARP_){
return cljs.core.assoc.call(null,p1__45181_SHARP_,cljs.core.get.call(null,p2__45182_SHARP_,key),p2__45182_SHARP_);
}),cljs.core.PersistentArrayMap.EMPTY,seqq));
});
figwheel.client.file_reloading.add_link_to_document = (function figwheel$client$file_reloading$add_link_to_document(orig_link,klone,finished_fn){
var parent = orig_link.parentNode;
if(cljs.core._EQ_.call(null,orig_link,parent.lastChild)){
parent.appendChild(klone);
} else {
parent.insertBefore(klone,orig_link.nextSibling);
}

return setTimeout(((function (parent){
return (function (){
parent.removeChild(orig_link);

return finished_fn.call(null);
});})(parent))
,(300));
});
if(typeof figwheel.client.file_reloading.reload_css_deferred_chain !== 'undefined'){
} else {
figwheel.client.file_reloading.reload_css_deferred_chain = cljs.core.atom.call(null,goog.async.Deferred.succeed());
}
figwheel.client.file_reloading.reload_css_file = (function figwheel$client$file_reloading$reload_css_file(f_data,fin){
var temp__5276__auto__ = figwheel.client.file_reloading.get_correct_link.call(null,f_data);
if(cljs.core.truth_(temp__5276__auto__)){
var link = temp__5276__auto__;
return figwheel.client.file_reloading.add_link_to_document.call(null,link,figwheel.client.file_reloading.clone_link.call(null,link,link.href),((function (link,temp__5276__auto__){
return (function (){
return fin.call(null,cljs.core.assoc.call(null,f_data,new cljs.core.Keyword(null,"loaded","loaded",-1246482293),true));
});})(link,temp__5276__auto__))
);
} else {
return fin.call(null,f_data);
}
});
figwheel.client.file_reloading.reload_css_files_STAR_ = (function figwheel$client$file_reloading$reload_css_files_STAR_(deferred,f_datas,on_cssload){
return figwheel.client.utils.liftContD.call(null,figwheel.client.utils.mapConcatD.call(null,deferred,figwheel.client.file_reloading.reload_css_file,f_datas),(function (f_datas_SINGLEQUOTE_,fin){
var loaded_f_datas_45183 = cljs.core.filter.call(null,new cljs.core.Keyword(null,"loaded","loaded",-1246482293),f_datas_SINGLEQUOTE_);
figwheel.client.file_reloading.on_cssload_custom_event.call(null,loaded_f_datas_45183);

if(cljs.core.fn_QMARK_.call(null,on_cssload)){
on_cssload.call(null,loaded_f_datas_45183);
} else {
}

return fin.call(null);
}));
});
figwheel.client.file_reloading.reload_css_files = (function figwheel$client$file_reloading$reload_css_files(p__45184,p__45185){
var map__45186 = p__45184;
var map__45186__$1 = ((((!((map__45186 == null)))?((((map__45186.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__45186.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__45186):map__45186);
var on_cssload = cljs.core.get.call(null,map__45186__$1,new cljs.core.Keyword(null,"on-cssload","on-cssload",1825432318));
var map__45187 = p__45185;
var map__45187__$1 = ((((!((map__45187 == null)))?((((map__45187.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__45187.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__45187):map__45187);
var files_msg = map__45187__$1;
var files = cljs.core.get.call(null,map__45187__$1,new cljs.core.Keyword(null,"files","files",-472457450));
if(cljs.core.truth_(figwheel.client.utils.html_env_QMARK_.call(null))){
var temp__5278__auto__ = cljs.core.not_empty.call(null,figwheel.client.file_reloading.distinctify.call(null,new cljs.core.Keyword(null,"file","file",-1269645878),files));
if(cljs.core.truth_(temp__5278__auto__)){
var f_datas = temp__5278__auto__;
return cljs.core.swap_BANG_.call(null,figwheel.client.file_reloading.reload_css_deferred_chain,figwheel.client.file_reloading.reload_css_files_STAR_,f_datas,on_cssload);
} else {
return null;
}
} else {
return null;
}
});

//# sourceMappingURL=file_reloading.js.map?rel=1507227568772

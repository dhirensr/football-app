// Compiled by ClojureScript 1.9.908 {}
goog.provide('cljs.repl');
goog.require('cljs.core');
goog.require('cljs.spec.alpha');
cljs.repl.print_doc = (function cljs$repl$print_doc(p__46249){
var map__46250 = p__46249;
var map__46250__$1 = ((((!((map__46250 == null)))?((((map__46250.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__46250.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__46250):map__46250);
var m = map__46250__$1;
var n = cljs.core.get.call(null,map__46250__$1,new cljs.core.Keyword(null,"ns","ns",441598760));
var nm = cljs.core.get.call(null,map__46250__$1,new cljs.core.Keyword(null,"name","name",1843675177));
cljs.core.println.call(null,"-------------------------");

cljs.core.println.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1((function (){var temp__5278__auto__ = new cljs.core.Keyword(null,"ns","ns",441598760).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(temp__5278__auto__)){
var ns = temp__5278__auto__;
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(ns),"/"].join('');
} else {
return null;
}
})()),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join(''));

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"Protocol");
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m))){
var seq__46252_46274 = cljs.core.seq.call(null,new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m));
var chunk__46253_46275 = null;
var count__46254_46276 = (0);
var i__46255_46277 = (0);
while(true){
if((i__46255_46277 < count__46254_46276)){
var f_46278 = cljs.core._nth.call(null,chunk__46253_46275,i__46255_46277);
cljs.core.println.call(null,"  ",f_46278);

var G__46279 = seq__46252_46274;
var G__46280 = chunk__46253_46275;
var G__46281 = count__46254_46276;
var G__46282 = (i__46255_46277 + (1));
seq__46252_46274 = G__46279;
chunk__46253_46275 = G__46280;
count__46254_46276 = G__46281;
i__46255_46277 = G__46282;
continue;
} else {
var temp__5278__auto___46283 = cljs.core.seq.call(null,seq__46252_46274);
if(temp__5278__auto___46283){
var seq__46252_46284__$1 = temp__5278__auto___46283;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__46252_46284__$1)){
var c__31113__auto___46285 = cljs.core.chunk_first.call(null,seq__46252_46284__$1);
var G__46286 = cljs.core.chunk_rest.call(null,seq__46252_46284__$1);
var G__46287 = c__31113__auto___46285;
var G__46288 = cljs.core.count.call(null,c__31113__auto___46285);
var G__46289 = (0);
seq__46252_46274 = G__46286;
chunk__46253_46275 = G__46287;
count__46254_46276 = G__46288;
i__46255_46277 = G__46289;
continue;
} else {
var f_46290 = cljs.core.first.call(null,seq__46252_46284__$1);
cljs.core.println.call(null,"  ",f_46290);

var G__46291 = cljs.core.next.call(null,seq__46252_46284__$1);
var G__46292 = null;
var G__46293 = (0);
var G__46294 = (0);
seq__46252_46274 = G__46291;
chunk__46253_46275 = G__46292;
count__46254_46276 = G__46293;
i__46255_46277 = G__46294;
continue;
}
} else {
}
}
break;
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m))){
var arglists_46295 = new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_((function (){var or__30182__auto__ = new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(or__30182__auto__)){
return or__30182__auto__;
} else {
return new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m);
}
})())){
cljs.core.prn.call(null,arglists_46295);
} else {
cljs.core.prn.call(null,((cljs.core._EQ_.call(null,new cljs.core.Symbol(null,"quote","quote",1377916282,null),cljs.core.first.call(null,arglists_46295)))?cljs.core.second.call(null,arglists_46295):arglists_46295));
}
} else {
}
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"special-form","special-form",-1326536374).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"Special Form");

cljs.core.println.call(null," ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m));

if(cljs.core.contains_QMARK_.call(null,m,new cljs.core.Keyword(null,"url","url",276297046))){
if(cljs.core.truth_(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))){
return cljs.core.println.call(null,["\n  Please see http://clojure.org/",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))].join(''));
} else {
return null;
}
} else {
return cljs.core.println.call(null,["\n  Please see http://clojure.org/special_forms#",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join(''));
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"Macro");
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"REPL Special Function");
} else {
}

cljs.core.println.call(null," ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m));

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
var seq__46256_46296 = cljs.core.seq.call(null,new cljs.core.Keyword(null,"methods","methods",453930866).cljs$core$IFn$_invoke$arity$1(m));
var chunk__46257_46297 = null;
var count__46258_46298 = (0);
var i__46259_46299 = (0);
while(true){
if((i__46259_46299 < count__46258_46298)){
var vec__46260_46300 = cljs.core._nth.call(null,chunk__46257_46297,i__46259_46299);
var name_46301 = cljs.core.nth.call(null,vec__46260_46300,(0),null);
var map__46263_46302 = cljs.core.nth.call(null,vec__46260_46300,(1),null);
var map__46263_46303__$1 = ((((!((map__46263_46302 == null)))?((((map__46263_46302.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__46263_46302.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__46263_46302):map__46263_46302);
var doc_46304 = cljs.core.get.call(null,map__46263_46303__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var arglists_46305 = cljs.core.get.call(null,map__46263_46303__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
cljs.core.println.call(null);

cljs.core.println.call(null," ",name_46301);

cljs.core.println.call(null," ",arglists_46305);

if(cljs.core.truth_(doc_46304)){
cljs.core.println.call(null," ",doc_46304);
} else {
}

var G__46306 = seq__46256_46296;
var G__46307 = chunk__46257_46297;
var G__46308 = count__46258_46298;
var G__46309 = (i__46259_46299 + (1));
seq__46256_46296 = G__46306;
chunk__46257_46297 = G__46307;
count__46258_46298 = G__46308;
i__46259_46299 = G__46309;
continue;
} else {
var temp__5278__auto___46310 = cljs.core.seq.call(null,seq__46256_46296);
if(temp__5278__auto___46310){
var seq__46256_46311__$1 = temp__5278__auto___46310;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__46256_46311__$1)){
var c__31113__auto___46312 = cljs.core.chunk_first.call(null,seq__46256_46311__$1);
var G__46313 = cljs.core.chunk_rest.call(null,seq__46256_46311__$1);
var G__46314 = c__31113__auto___46312;
var G__46315 = cljs.core.count.call(null,c__31113__auto___46312);
var G__46316 = (0);
seq__46256_46296 = G__46313;
chunk__46257_46297 = G__46314;
count__46258_46298 = G__46315;
i__46259_46299 = G__46316;
continue;
} else {
var vec__46265_46317 = cljs.core.first.call(null,seq__46256_46311__$1);
var name_46318 = cljs.core.nth.call(null,vec__46265_46317,(0),null);
var map__46268_46319 = cljs.core.nth.call(null,vec__46265_46317,(1),null);
var map__46268_46320__$1 = ((((!((map__46268_46319 == null)))?((((map__46268_46319.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__46268_46319.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__46268_46319):map__46268_46319);
var doc_46321 = cljs.core.get.call(null,map__46268_46320__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var arglists_46322 = cljs.core.get.call(null,map__46268_46320__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
cljs.core.println.call(null);

cljs.core.println.call(null," ",name_46318);

cljs.core.println.call(null," ",arglists_46322);

if(cljs.core.truth_(doc_46321)){
cljs.core.println.call(null," ",doc_46321);
} else {
}

var G__46323 = cljs.core.next.call(null,seq__46256_46311__$1);
var G__46324 = null;
var G__46325 = (0);
var G__46326 = (0);
seq__46256_46296 = G__46323;
chunk__46257_46297 = G__46324;
count__46258_46298 = G__46325;
i__46259_46299 = G__46326;
continue;
}
} else {
}
}
break;
}
} else {
}

if(cljs.core.truth_(n)){
var temp__5278__auto__ = cljs.spec.alpha.get_spec.call(null,cljs.core.symbol.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.ns_name.call(null,n))].join(''),cljs.core.name.call(null,nm)));
if(cljs.core.truth_(temp__5278__auto__)){
var fnspec = temp__5278__auto__;
cljs.core.print.call(null,"Spec");

var seq__46270 = cljs.core.seq.call(null,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"args","args",1315556576),new cljs.core.Keyword(null,"ret","ret",-468222814),new cljs.core.Keyword(null,"fn","fn",-1175266204)], null));
var chunk__46271 = null;
var count__46272 = (0);
var i__46273 = (0);
while(true){
if((i__46273 < count__46272)){
var role = cljs.core._nth.call(null,chunk__46271,i__46273);
var temp__5278__auto___46327__$1 = cljs.core.get.call(null,fnspec,role);
if(cljs.core.truth_(temp__5278__auto___46327__$1)){
var spec_46328 = temp__5278__auto___46327__$1;
cljs.core.print.call(null,["\n ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.name.call(null,role)),":"].join(''),cljs.spec.alpha.describe.call(null,spec_46328));
} else {
}

var G__46329 = seq__46270;
var G__46330 = chunk__46271;
var G__46331 = count__46272;
var G__46332 = (i__46273 + (1));
seq__46270 = G__46329;
chunk__46271 = G__46330;
count__46272 = G__46331;
i__46273 = G__46332;
continue;
} else {
var temp__5278__auto____$1 = cljs.core.seq.call(null,seq__46270);
if(temp__5278__auto____$1){
var seq__46270__$1 = temp__5278__auto____$1;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__46270__$1)){
var c__31113__auto__ = cljs.core.chunk_first.call(null,seq__46270__$1);
var G__46333 = cljs.core.chunk_rest.call(null,seq__46270__$1);
var G__46334 = c__31113__auto__;
var G__46335 = cljs.core.count.call(null,c__31113__auto__);
var G__46336 = (0);
seq__46270 = G__46333;
chunk__46271 = G__46334;
count__46272 = G__46335;
i__46273 = G__46336;
continue;
} else {
var role = cljs.core.first.call(null,seq__46270__$1);
var temp__5278__auto___46337__$2 = cljs.core.get.call(null,fnspec,role);
if(cljs.core.truth_(temp__5278__auto___46337__$2)){
var spec_46338 = temp__5278__auto___46337__$2;
cljs.core.print.call(null,["\n ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.name.call(null,role)),":"].join(''),cljs.spec.alpha.describe.call(null,spec_46338));
} else {
}

var G__46339 = cljs.core.next.call(null,seq__46270__$1);
var G__46340 = null;
var G__46341 = (0);
var G__46342 = (0);
seq__46270 = G__46339;
chunk__46271 = G__46340;
count__46272 = G__46341;
i__46273 = G__46342;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return null;
}
} else {
return null;
}
}
});

//# sourceMappingURL=repl.js.map?rel=1507227570332

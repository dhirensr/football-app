// Compiled by ClojureScript 1.9.908 {}
goog.provide('cljs.core.async');
goog.require('cljs.core');
goog.require('cljs.core.async.impl.protocols');
goog.require('cljs.core.async.impl.channels');
goog.require('cljs.core.async.impl.buffers');
goog.require('cljs.core.async.impl.timers');
goog.require('cljs.core.async.impl.dispatch');
goog.require('cljs.core.async.impl.ioc_helpers');
cljs.core.async.fn_handler = (function cljs$core$async$fn_handler(var_args){
var G__41584 = arguments.length;
switch (G__41584) {
case 1:
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1 = (function (f){
return cljs.core.async.fn_handler.call(null,f,true);
});

cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2 = (function (f,blockable){
if(typeof cljs.core.async.t_cljs$core$async41585 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async41585 = (function (f,blockable,meta41586){
this.f = f;
this.blockable = blockable;
this.meta41586 = meta41586;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async41585.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_41587,meta41586__$1){
var self__ = this;
var _41587__$1 = this;
return (new cljs.core.async.t_cljs$core$async41585(self__.f,self__.blockable,meta41586__$1));
});

cljs.core.async.t_cljs$core$async41585.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_41587){
var self__ = this;
var _41587__$1 = this;
return self__.meta41586;
});

cljs.core.async.t_cljs$core$async41585.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async41585.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
});

cljs.core.async.t_cljs$core$async41585.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.blockable;
});

cljs.core.async.t_cljs$core$async41585.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.f;
});

cljs.core.async.t_cljs$core$async41585.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"blockable","blockable",-28395259,null),new cljs.core.Symbol(null,"meta41586","meta41586",869099455,null)], null);
});

cljs.core.async.t_cljs$core$async41585.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async41585.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async41585";

cljs.core.async.t_cljs$core$async41585.cljs$lang$ctorPrWriter = (function (this__30853__auto__,writer__30854__auto__,opt__30855__auto__){
return cljs.core._write.call(null,writer__30854__auto__,"cljs.core.async/t_cljs$core$async41585");
});

cljs.core.async.__GT_t_cljs$core$async41585 = (function cljs$core$async$__GT_t_cljs$core$async41585(f__$1,blockable__$1,meta41586){
return (new cljs.core.async.t_cljs$core$async41585(f__$1,blockable__$1,meta41586));
});

}

return (new cljs.core.async.t_cljs$core$async41585(f,blockable,cljs.core.PersistentArrayMap.EMPTY));
});

cljs.core.async.fn_handler.cljs$lang$maxFixedArity = 2;

/**
 * Returns a fixed buffer of size n. When full, puts will block/park.
 */
cljs.core.async.buffer = (function cljs$core$async$buffer(n){
return cljs.core.async.impl.buffers.fixed_buffer.call(null,n);
});
/**
 * Returns a buffer of size n. When full, puts will complete but
 *   val will be dropped (no transfer).
 */
cljs.core.async.dropping_buffer = (function cljs$core$async$dropping_buffer(n){
return cljs.core.async.impl.buffers.dropping_buffer.call(null,n);
});
/**
 * Returns a buffer of size n. When full, puts will complete, and be
 *   buffered, but oldest elements in buffer will be dropped (not
 *   transferred).
 */
cljs.core.async.sliding_buffer = (function cljs$core$async$sliding_buffer(n){
return cljs.core.async.impl.buffers.sliding_buffer.call(null,n);
});
/**
 * Returns true if a channel created with buff will never block. That is to say,
 * puts into this buffer will never cause the buffer to be full. 
 */
cljs.core.async.unblocking_buffer_QMARK_ = (function cljs$core$async$unblocking_buffer_QMARK_(buff){
if(!((buff == null))){
if((false) || ((cljs.core.PROTOCOL_SENTINEL === buff.cljs$core$async$impl$protocols$UnblockingBuffer$))){
return true;
} else {
if((!buff.cljs$lang$protocol_mask$partition$)){
return cljs.core.native_satisfies_QMARK_.call(null,cljs.core.async.impl.protocols.UnblockingBuffer,buff);
} else {
return false;
}
}
} else {
return cljs.core.native_satisfies_QMARK_.call(null,cljs.core.async.impl.protocols.UnblockingBuffer,buff);
}
});
/**
 * Creates a channel with an optional buffer, an optional transducer (like (map f),
 *   (filter p) etc or a composition thereof), and an optional exception handler.
 *   If buf-or-n is a number, will create and use a fixed buffer of that size. If a
 *   transducer is supplied a buffer must be specified. ex-handler must be a
 *   fn of one argument - if an exception occurs during transformation it will be called
 *   with the thrown value as an argument, and any non-nil return value will be placed
 *   in the channel.
 */
cljs.core.async.chan = (function cljs$core$async$chan(var_args){
var G__41591 = arguments.length;
switch (G__41591) {
case 0:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0 = (function (){
return cljs.core.async.chan.call(null,null);
});

cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1 = (function (buf_or_n){
return cljs.core.async.chan.call(null,buf_or_n,null,null);
});

cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2 = (function (buf_or_n,xform){
return cljs.core.async.chan.call(null,buf_or_n,xform,null);
});

cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3 = (function (buf_or_n,xform,ex_handler){
var buf_or_n__$1 = ((cljs.core._EQ_.call(null,buf_or_n,(0)))?null:buf_or_n);
if(cljs.core.truth_(xform)){
if(cljs.core.truth_(buf_or_n__$1)){
} else {
throw (new Error(["Assert failed: ","buffer must be supplied when transducer is","\n","buf-or-n"].join('')));
}
} else {
}

return cljs.core.async.impl.channels.chan.call(null,((typeof buf_or_n__$1 === 'number')?cljs.core.async.buffer.call(null,buf_or_n__$1):buf_or_n__$1),xform,ex_handler);
});

cljs.core.async.chan.cljs$lang$maxFixedArity = 3;

/**
 * Creates a promise channel with an optional transducer, and an optional
 *   exception-handler. A promise channel can take exactly one value that consumers
 *   will receive. Once full, puts complete but val is dropped (no transfer).
 *   Consumers will block until either a value is placed in the channel or the
 *   channel is closed. See chan for the semantics of xform and ex-handler.
 */
cljs.core.async.promise_chan = (function cljs$core$async$promise_chan(var_args){
var G__41594 = arguments.length;
switch (G__41594) {
case 0:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0 = (function (){
return cljs.core.async.promise_chan.call(null,null);
});

cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1 = (function (xform){
return cljs.core.async.promise_chan.call(null,xform,null);
});

cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2 = (function (xform,ex_handler){
return cljs.core.async.chan.call(null,cljs.core.async.impl.buffers.promise_buffer.call(null),xform,ex_handler);
});

cljs.core.async.promise_chan.cljs$lang$maxFixedArity = 2;

/**
 * Returns a channel that will close after msecs
 */
cljs.core.async.timeout = (function cljs$core$async$timeout(msecs){
return cljs.core.async.impl.timers.timeout.call(null,msecs);
});
/**
 * takes a val from port. Must be called inside a (go ...) block. Will
 *   return nil if closed. Will park if nothing is available.
 *   Returns true unless port is already closed
 */
cljs.core.async._LT__BANG_ = (function cljs$core$async$_LT__BANG_(port){
throw (new Error("<! used not in (go ...) block"));
});
/**
 * Asynchronously takes a val from port, passing to fn1. Will pass nil
 * if closed. If on-caller? (default true) is true, and value is
 * immediately available, will call fn1 on calling thread.
 * Returns nil.
 */
cljs.core.async.take_BANG_ = (function cljs$core$async$take_BANG_(var_args){
var G__41597 = arguments.length;
switch (G__41597) {
case 2:
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (port,fn1){
return cljs.core.async.take_BANG_.call(null,port,fn1,true);
});

cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (port,fn1,on_caller_QMARK_){
var ret = cljs.core.async.impl.protocols.take_BANG_.call(null,port,cljs.core.async.fn_handler.call(null,fn1));
if(cljs.core.truth_(ret)){
var val_41599 = cljs.core.deref.call(null,ret);
if(cljs.core.truth_(on_caller_QMARK_)){
fn1.call(null,val_41599);
} else {
cljs.core.async.impl.dispatch.run.call(null,((function (val_41599,ret){
return (function (){
return fn1.call(null,val_41599);
});})(val_41599,ret))
);
}
} else {
}

return null;
});

cljs.core.async.take_BANG_.cljs$lang$maxFixedArity = 3;

cljs.core.async.nop = (function cljs$core$async$nop(_){
return null;
});
cljs.core.async.fhnop = cljs.core.async.fn_handler.call(null,cljs.core.async.nop);
/**
 * puts a val into port. nil values are not allowed. Must be called
 *   inside a (go ...) block. Will park if no buffer space is available.
 *   Returns true unless port is already closed.
 */
cljs.core.async._GT__BANG_ = (function cljs$core$async$_GT__BANG_(port,val){
throw (new Error(">! used not in (go ...) block"));
});
/**
 * Asynchronously puts a val into port, calling fn0 (if supplied) when
 * complete. nil values are not allowed. Will throw if closed. If
 * on-caller? (default true) is true, and the put is immediately
 * accepted, will call fn0 on calling thread.  Returns nil.
 */
cljs.core.async.put_BANG_ = (function cljs$core$async$put_BANG_(var_args){
var G__41601 = arguments.length;
switch (G__41601) {
case 2:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (port,val){
var temp__5276__auto__ = cljs.core.async.impl.protocols.put_BANG_.call(null,port,val,cljs.core.async.fhnop);
if(cljs.core.truth_(temp__5276__auto__)){
var ret = temp__5276__auto__;
return cljs.core.deref.call(null,ret);
} else {
return true;
}
});

cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (port,val,fn1){
return cljs.core.async.put_BANG_.call(null,port,val,fn1,true);
});

cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4 = (function (port,val,fn1,on_caller_QMARK_){
var temp__5276__auto__ = cljs.core.async.impl.protocols.put_BANG_.call(null,port,val,cljs.core.async.fn_handler.call(null,fn1));
if(cljs.core.truth_(temp__5276__auto__)){
var retb = temp__5276__auto__;
var ret = cljs.core.deref.call(null,retb);
if(cljs.core.truth_(on_caller_QMARK_)){
fn1.call(null,ret);
} else {
cljs.core.async.impl.dispatch.run.call(null,((function (ret,retb,temp__5276__auto__){
return (function (){
return fn1.call(null,ret);
});})(ret,retb,temp__5276__auto__))
);
}

return ret;
} else {
return true;
}
});

cljs.core.async.put_BANG_.cljs$lang$maxFixedArity = 4;

cljs.core.async.close_BANG_ = (function cljs$core$async$close_BANG_(port){
return cljs.core.async.impl.protocols.close_BANG_.call(null,port);
});
cljs.core.async.random_array = (function cljs$core$async$random_array(n){
var a = (new Array(n));
var n__31225__auto___41603 = n;
var x_41604 = (0);
while(true){
if((x_41604 < n__31225__auto___41603)){
(a[x_41604] = (0));

var G__41605 = (x_41604 + (1));
x_41604 = G__41605;
continue;
} else {
}
break;
}

var i = (1);
while(true){
if(cljs.core._EQ_.call(null,i,n)){
return a;
} else {
var j = cljs.core.rand_int.call(null,i);
(a[i] = (a[j]));

(a[j] = i);

var G__41606 = (i + (1));
i = G__41606;
continue;
}
break;
}
});
cljs.core.async.alt_flag = (function cljs$core$async$alt_flag(){
var flag = cljs.core.atom.call(null,true);
if(typeof cljs.core.async.t_cljs$core$async41607 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async41607 = (function (flag,meta41608){
this.flag = flag;
this.meta41608 = meta41608;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async41607.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (flag){
return (function (_41609,meta41608__$1){
var self__ = this;
var _41609__$1 = this;
return (new cljs.core.async.t_cljs$core$async41607(self__.flag,meta41608__$1));
});})(flag))
;

cljs.core.async.t_cljs$core$async41607.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (flag){
return (function (_41609){
var self__ = this;
var _41609__$1 = this;
return self__.meta41608;
});})(flag))
;

cljs.core.async.t_cljs$core$async41607.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async41607.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = ((function (flag){
return (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.deref.call(null,self__.flag);
});})(flag))
;

cljs.core.async.t_cljs$core$async41607.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = ((function (flag){
return (function (_){
var self__ = this;
var ___$1 = this;
return true;
});})(flag))
;

cljs.core.async.t_cljs$core$async41607.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = ((function (flag){
return (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_.call(null,self__.flag,null);

return true;
});})(flag))
;

cljs.core.async.t_cljs$core$async41607.getBasis = ((function (flag){
return (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"flag","flag",-1565787888,null),new cljs.core.Symbol(null,"meta41608","meta41608",-760173445,null)], null);
});})(flag))
;

cljs.core.async.t_cljs$core$async41607.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async41607.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async41607";

cljs.core.async.t_cljs$core$async41607.cljs$lang$ctorPrWriter = ((function (flag){
return (function (this__30853__auto__,writer__30854__auto__,opt__30855__auto__){
return cljs.core._write.call(null,writer__30854__auto__,"cljs.core.async/t_cljs$core$async41607");
});})(flag))
;

cljs.core.async.__GT_t_cljs$core$async41607 = ((function (flag){
return (function cljs$core$async$alt_flag_$___GT_t_cljs$core$async41607(flag__$1,meta41608){
return (new cljs.core.async.t_cljs$core$async41607(flag__$1,meta41608));
});})(flag))
;

}

return (new cljs.core.async.t_cljs$core$async41607(flag,cljs.core.PersistentArrayMap.EMPTY));
});
cljs.core.async.alt_handler = (function cljs$core$async$alt_handler(flag,cb){
if(typeof cljs.core.async.t_cljs$core$async41610 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async41610 = (function (flag,cb,meta41611){
this.flag = flag;
this.cb = cb;
this.meta41611 = meta41611;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async41610.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_41612,meta41611__$1){
var self__ = this;
var _41612__$1 = this;
return (new cljs.core.async.t_cljs$core$async41610(self__.flag,self__.cb,meta41611__$1));
});

cljs.core.async.t_cljs$core$async41610.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_41612){
var self__ = this;
var _41612__$1 = this;
return self__.meta41611;
});

cljs.core.async.t_cljs$core$async41610.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async41610.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.active_QMARK_.call(null,self__.flag);
});

cljs.core.async.t_cljs$core$async41610.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
});

cljs.core.async.t_cljs$core$async41610.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.async.impl.protocols.commit.call(null,self__.flag);

return self__.cb;
});

cljs.core.async.t_cljs$core$async41610.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"flag","flag",-1565787888,null),new cljs.core.Symbol(null,"cb","cb",-2064487928,null),new cljs.core.Symbol(null,"meta41611","meta41611",565515602,null)], null);
});

cljs.core.async.t_cljs$core$async41610.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async41610.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async41610";

cljs.core.async.t_cljs$core$async41610.cljs$lang$ctorPrWriter = (function (this__30853__auto__,writer__30854__auto__,opt__30855__auto__){
return cljs.core._write.call(null,writer__30854__auto__,"cljs.core.async/t_cljs$core$async41610");
});

cljs.core.async.__GT_t_cljs$core$async41610 = (function cljs$core$async$alt_handler_$___GT_t_cljs$core$async41610(flag__$1,cb__$1,meta41611){
return (new cljs.core.async.t_cljs$core$async41610(flag__$1,cb__$1,meta41611));
});

}

return (new cljs.core.async.t_cljs$core$async41610(flag,cb,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * returns derefable [val port] if immediate, nil if enqueued
 */
cljs.core.async.do_alts = (function cljs$core$async$do_alts(fret,ports,opts){
var flag = cljs.core.async.alt_flag.call(null);
var n = cljs.core.count.call(null,ports);
var idxs = cljs.core.async.random_array.call(null,n);
var priority = new cljs.core.Keyword(null,"priority","priority",1431093715).cljs$core$IFn$_invoke$arity$1(opts);
var ret = (function (){var i = (0);
while(true){
if((i < n)){
var idx = (cljs.core.truth_(priority)?i:(idxs[i]));
var port = cljs.core.nth.call(null,ports,idx);
var wport = ((cljs.core.vector_QMARK_.call(null,port))?port.call(null,(0)):null);
var vbox = (cljs.core.truth_(wport)?(function (){var val = port.call(null,(1));
return cljs.core.async.impl.protocols.put_BANG_.call(null,wport,val,cljs.core.async.alt_handler.call(null,flag,((function (i,val,idx,port,wport,flag,n,idxs,priority){
return (function (p1__41613_SHARP_){
return fret.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p1__41613_SHARP_,wport], null));
});})(i,val,idx,port,wport,flag,n,idxs,priority))
));
})():cljs.core.async.impl.protocols.take_BANG_.call(null,port,cljs.core.async.alt_handler.call(null,flag,((function (i,idx,port,wport,flag,n,idxs,priority){
return (function (p1__41614_SHARP_){
return fret.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p1__41614_SHARP_,port], null));
});})(i,idx,port,wport,flag,n,idxs,priority))
)));
if(cljs.core.truth_(vbox)){
return cljs.core.async.impl.channels.box.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.deref.call(null,vbox),(function (){var or__30182__auto__ = wport;
if(cljs.core.truth_(or__30182__auto__)){
return or__30182__auto__;
} else {
return port;
}
})()], null));
} else {
var G__41615 = (i + (1));
i = G__41615;
continue;
}
} else {
return null;
}
break;
}
})();
var or__30182__auto__ = ret;
if(cljs.core.truth_(or__30182__auto__)){
return or__30182__auto__;
} else {
if(cljs.core.contains_QMARK_.call(null,opts,new cljs.core.Keyword(null,"default","default",-1987822328))){
var temp__5278__auto__ = (function (){var and__30170__auto__ = cljs.core.async.impl.protocols.active_QMARK_.call(null,flag);
if(cljs.core.truth_(and__30170__auto__)){
return cljs.core.async.impl.protocols.commit.call(null,flag);
} else {
return and__30170__auto__;
}
})();
if(cljs.core.truth_(temp__5278__auto__)){
var got = temp__5278__auto__;
return cljs.core.async.impl.channels.box.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"default","default",-1987822328).cljs$core$IFn$_invoke$arity$1(opts),new cljs.core.Keyword(null,"default","default",-1987822328)], null));
} else {
return null;
}
} else {
return null;
}
}
});
/**
 * Completes at most one of several channel operations. Must be called
 * inside a (go ...) block. ports is a vector of channel endpoints,
 * which can be either a channel to take from or a vector of
 *   [channel-to-put-to val-to-put], in any combination. Takes will be
 *   made as if by <!, and puts will be made as if by >!. Unless
 *   the :priority option is true, if more than one port operation is
 *   ready a non-deterministic choice will be made. If no operation is
 *   ready and a :default value is supplied, [default-val :default] will
 *   be returned, otherwise alts! will park until the first operation to
 *   become ready completes. Returns [val port] of the completed
 *   operation, where val is the value taken for takes, and a
 *   boolean (true unless already closed, as per put!) for puts.
 * 
 *   opts are passed as :key val ... Supported options:
 * 
 *   :default val - the value to use if none of the operations are immediately ready
 *   :priority true - (default nil) when true, the operations will be tried in order.
 * 
 *   Note: there is no guarantee that the port exps or val exprs will be
 *   used, nor in what order should they be, so they should not be
 *   depended upon for side effects.
 */
cljs.core.async.alts_BANG_ = (function cljs$core$async$alts_BANG_(var_args){
var args__31466__auto__ = [];
var len__31459__auto___41621 = arguments.length;
var i__31460__auto___41622 = (0);
while(true){
if((i__31460__auto___41622 < len__31459__auto___41621)){
args__31466__auto__.push((arguments[i__31460__auto___41622]));

var G__41623 = (i__31460__auto___41622 + (1));
i__31460__auto___41622 = G__41623;
continue;
} else {
}
break;
}

var argseq__31467__auto__ = ((((1) < args__31466__auto__.length))?(new cljs.core.IndexedSeq(args__31466__auto__.slice((1)),(0),null)):null);
return cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__31467__auto__);
});

cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (ports,p__41618){
var map__41619 = p__41618;
var map__41619__$1 = ((((!((map__41619 == null)))?((((map__41619.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__41619.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__41619):map__41619);
var opts = map__41619__$1;
throw (new Error("alts! used not in (go ...) block"));
});

cljs.core.async.alts_BANG_.cljs$lang$maxFixedArity = (1);

cljs.core.async.alts_BANG_.cljs$lang$applyTo = (function (seq41616){
var G__41617 = cljs.core.first.call(null,seq41616);
var seq41616__$1 = cljs.core.next.call(null,seq41616);
return cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic(G__41617,seq41616__$1);
});

/**
 * Puts a val into port if it's possible to do so immediately.
 *   nil values are not allowed. Never blocks. Returns true if offer succeeds.
 */
cljs.core.async.offer_BANG_ = (function cljs$core$async$offer_BANG_(port,val){
var ret = cljs.core.async.impl.protocols.put_BANG_.call(null,port,val,cljs.core.async.fn_handler.call(null,cljs.core.async.nop,false));
if(cljs.core.truth_(ret)){
return cljs.core.deref.call(null,ret);
} else {
return null;
}
});
/**
 * Takes a val from port if it's possible to do so immediately.
 *   Never blocks. Returns value if successful, nil otherwise.
 */
cljs.core.async.poll_BANG_ = (function cljs$core$async$poll_BANG_(port){
var ret = cljs.core.async.impl.protocols.take_BANG_.call(null,port,cljs.core.async.fn_handler.call(null,cljs.core.async.nop,false));
if(cljs.core.truth_(ret)){
return cljs.core.deref.call(null,ret);
} else {
return null;
}
});
/**
 * Takes elements from the from channel and supplies them to the to
 * channel. By default, the to channel will be closed when the from
 * channel closes, but can be determined by the close?  parameter. Will
 * stop consuming the from channel if the to channel closes
 */
cljs.core.async.pipe = (function cljs$core$async$pipe(var_args){
var G__41625 = arguments.length;
switch (G__41625) {
case 2:
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$2 = (function (from,to){
return cljs.core.async.pipe.call(null,from,to,true);
});

cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3 = (function (from,to,close_QMARK_){
var c__35058__auto___41671 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__35058__auto___41671){
return (function (){
var f__35059__auto__ = (function (){var switch__34968__auto__ = ((function (c__35058__auto___41671){
return (function (state_41649){
var state_val_41650 = (state_41649[(1)]);
if((state_val_41650 === (7))){
var inst_41645 = (state_41649[(2)]);
var state_41649__$1 = state_41649;
var statearr_41651_41672 = state_41649__$1;
(statearr_41651_41672[(2)] = inst_41645);

(statearr_41651_41672[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41650 === (1))){
var state_41649__$1 = state_41649;
var statearr_41652_41673 = state_41649__$1;
(statearr_41652_41673[(2)] = null);

(statearr_41652_41673[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41650 === (4))){
var inst_41628 = (state_41649[(7)]);
var inst_41628__$1 = (state_41649[(2)]);
var inst_41629 = (inst_41628__$1 == null);
var state_41649__$1 = (function (){var statearr_41653 = state_41649;
(statearr_41653[(7)] = inst_41628__$1);

return statearr_41653;
})();
if(cljs.core.truth_(inst_41629)){
var statearr_41654_41674 = state_41649__$1;
(statearr_41654_41674[(1)] = (5));

} else {
var statearr_41655_41675 = state_41649__$1;
(statearr_41655_41675[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41650 === (13))){
var state_41649__$1 = state_41649;
var statearr_41656_41676 = state_41649__$1;
(statearr_41656_41676[(2)] = null);

(statearr_41656_41676[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41650 === (6))){
var inst_41628 = (state_41649[(7)]);
var state_41649__$1 = state_41649;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_41649__$1,(11),to,inst_41628);
} else {
if((state_val_41650 === (3))){
var inst_41647 = (state_41649[(2)]);
var state_41649__$1 = state_41649;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_41649__$1,inst_41647);
} else {
if((state_val_41650 === (12))){
var state_41649__$1 = state_41649;
var statearr_41657_41677 = state_41649__$1;
(statearr_41657_41677[(2)] = null);

(statearr_41657_41677[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41650 === (2))){
var state_41649__$1 = state_41649;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_41649__$1,(4),from);
} else {
if((state_val_41650 === (11))){
var inst_41638 = (state_41649[(2)]);
var state_41649__$1 = state_41649;
if(cljs.core.truth_(inst_41638)){
var statearr_41658_41678 = state_41649__$1;
(statearr_41658_41678[(1)] = (12));

} else {
var statearr_41659_41679 = state_41649__$1;
(statearr_41659_41679[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41650 === (9))){
var state_41649__$1 = state_41649;
var statearr_41660_41680 = state_41649__$1;
(statearr_41660_41680[(2)] = null);

(statearr_41660_41680[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41650 === (5))){
var state_41649__$1 = state_41649;
if(cljs.core.truth_(close_QMARK_)){
var statearr_41661_41681 = state_41649__$1;
(statearr_41661_41681[(1)] = (8));

} else {
var statearr_41662_41682 = state_41649__$1;
(statearr_41662_41682[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41650 === (14))){
var inst_41643 = (state_41649[(2)]);
var state_41649__$1 = state_41649;
var statearr_41663_41683 = state_41649__$1;
(statearr_41663_41683[(2)] = inst_41643);

(statearr_41663_41683[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41650 === (10))){
var inst_41635 = (state_41649[(2)]);
var state_41649__$1 = state_41649;
var statearr_41664_41684 = state_41649__$1;
(statearr_41664_41684[(2)] = inst_41635);

(statearr_41664_41684[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41650 === (8))){
var inst_41632 = cljs.core.async.close_BANG_.call(null,to);
var state_41649__$1 = state_41649;
var statearr_41665_41685 = state_41649__$1;
(statearr_41665_41685[(2)] = inst_41632);

(statearr_41665_41685[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__35058__auto___41671))
;
return ((function (switch__34968__auto__,c__35058__auto___41671){
return (function() {
var cljs$core$async$state_machine__34969__auto__ = null;
var cljs$core$async$state_machine__34969__auto____0 = (function (){
var statearr_41666 = [null,null,null,null,null,null,null,null];
(statearr_41666[(0)] = cljs$core$async$state_machine__34969__auto__);

(statearr_41666[(1)] = (1));

return statearr_41666;
});
var cljs$core$async$state_machine__34969__auto____1 = (function (state_41649){
while(true){
var ret_value__34970__auto__ = (function (){try{while(true){
var result__34971__auto__ = switch__34968__auto__.call(null,state_41649);
if(cljs.core.keyword_identical_QMARK_.call(null,result__34971__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__34971__auto__;
}
break;
}
}catch (e41667){if((e41667 instanceof Object)){
var ex__34972__auto__ = e41667;
var statearr_41668_41686 = state_41649;
(statearr_41668_41686[(5)] = ex__34972__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_41649);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e41667;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__34970__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__41687 = state_41649;
state_41649 = G__41687;
continue;
} else {
return ret_value__34970__auto__;
}
break;
}
});
cljs$core$async$state_machine__34969__auto__ = function(state_41649){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__34969__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__34969__auto____1.call(this,state_41649);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__34969__auto____0;
cljs$core$async$state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__34969__auto____1;
return cljs$core$async$state_machine__34969__auto__;
})()
;})(switch__34968__auto__,c__35058__auto___41671))
})();
var state__35060__auto__ = (function (){var statearr_41669 = f__35059__auto__.call(null);
(statearr_41669[(6)] = c__35058__auto___41671);

return statearr_41669;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__35060__auto__);
});})(c__35058__auto___41671))
);


return to;
});

cljs.core.async.pipe.cljs$lang$maxFixedArity = 3;

cljs.core.async.pipeline_STAR_ = (function cljs$core$async$pipeline_STAR_(n,to,xf,from,close_QMARK_,ex_handler,type){
if((n > (0))){
} else {
throw (new Error("Assert failed: (pos? n)"));
}

var jobs = cljs.core.async.chan.call(null,n);
var results = cljs.core.async.chan.call(null,n);
var process__$1 = ((function (jobs,results){
return (function (p__41688){
var vec__41689 = p__41688;
var v = cljs.core.nth.call(null,vec__41689,(0),null);
var p = cljs.core.nth.call(null,vec__41689,(1),null);
var job = vec__41689;
if((job == null)){
cljs.core.async.close_BANG_.call(null,results);

return null;
} else {
var res = cljs.core.async.chan.call(null,(1),xf,ex_handler);
var c__35058__auto___41860 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__35058__auto___41860,res,vec__41689,v,p,job,jobs,results){
return (function (){
var f__35059__auto__ = (function (){var switch__34968__auto__ = ((function (c__35058__auto___41860,res,vec__41689,v,p,job,jobs,results){
return (function (state_41696){
var state_val_41697 = (state_41696[(1)]);
if((state_val_41697 === (1))){
var state_41696__$1 = state_41696;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_41696__$1,(2),res,v);
} else {
if((state_val_41697 === (2))){
var inst_41693 = (state_41696[(2)]);
var inst_41694 = cljs.core.async.close_BANG_.call(null,res);
var state_41696__$1 = (function (){var statearr_41698 = state_41696;
(statearr_41698[(7)] = inst_41693);

return statearr_41698;
})();
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_41696__$1,inst_41694);
} else {
return null;
}
}
});})(c__35058__auto___41860,res,vec__41689,v,p,job,jobs,results))
;
return ((function (switch__34968__auto__,c__35058__auto___41860,res,vec__41689,v,p,job,jobs,results){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____0 = (function (){
var statearr_41699 = [null,null,null,null,null,null,null,null];
(statearr_41699[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__);

(statearr_41699[(1)] = (1));

return statearr_41699;
});
var cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____1 = (function (state_41696){
while(true){
var ret_value__34970__auto__ = (function (){try{while(true){
var result__34971__auto__ = switch__34968__auto__.call(null,state_41696);
if(cljs.core.keyword_identical_QMARK_.call(null,result__34971__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__34971__auto__;
}
break;
}
}catch (e41700){if((e41700 instanceof Object)){
var ex__34972__auto__ = e41700;
var statearr_41701_41861 = state_41696;
(statearr_41701_41861[(5)] = ex__34972__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_41696);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e41700;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__34970__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__41862 = state_41696;
state_41696 = G__41862;
continue;
} else {
return ret_value__34970__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__ = function(state_41696){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____1.call(this,state_41696);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__;
})()
;})(switch__34968__auto__,c__35058__auto___41860,res,vec__41689,v,p,job,jobs,results))
})();
var state__35060__auto__ = (function (){var statearr_41702 = f__35059__auto__.call(null);
(statearr_41702[(6)] = c__35058__auto___41860);

return statearr_41702;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__35060__auto__);
});})(c__35058__auto___41860,res,vec__41689,v,p,job,jobs,results))
);


cljs.core.async.put_BANG_.call(null,p,res);

return true;
}
});})(jobs,results))
;
var async = ((function (jobs,results,process__$1){
return (function (p__41703){
var vec__41704 = p__41703;
var v = cljs.core.nth.call(null,vec__41704,(0),null);
var p = cljs.core.nth.call(null,vec__41704,(1),null);
var job = vec__41704;
if((job == null)){
cljs.core.async.close_BANG_.call(null,results);

return null;
} else {
var res = cljs.core.async.chan.call(null,(1));
xf.call(null,v,res);

cljs.core.async.put_BANG_.call(null,p,res);

return true;
}
});})(jobs,results,process__$1))
;
var n__31225__auto___41863 = n;
var __41864 = (0);
while(true){
if((__41864 < n__31225__auto___41863)){
var G__41707_41865 = type;
var G__41707_41866__$1 = (((G__41707_41865 instanceof cljs.core.Keyword))?G__41707_41865.fqn:null);
switch (G__41707_41866__$1) {
case "compute":
var c__35058__auto___41868 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (__41864,c__35058__auto___41868,G__41707_41865,G__41707_41866__$1,n__31225__auto___41863,jobs,results,process__$1,async){
return (function (){
var f__35059__auto__ = (function (){var switch__34968__auto__ = ((function (__41864,c__35058__auto___41868,G__41707_41865,G__41707_41866__$1,n__31225__auto___41863,jobs,results,process__$1,async){
return (function (state_41720){
var state_val_41721 = (state_41720[(1)]);
if((state_val_41721 === (1))){
var state_41720__$1 = state_41720;
var statearr_41722_41869 = state_41720__$1;
(statearr_41722_41869[(2)] = null);

(statearr_41722_41869[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41721 === (2))){
var state_41720__$1 = state_41720;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_41720__$1,(4),jobs);
} else {
if((state_val_41721 === (3))){
var inst_41718 = (state_41720[(2)]);
var state_41720__$1 = state_41720;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_41720__$1,inst_41718);
} else {
if((state_val_41721 === (4))){
var inst_41710 = (state_41720[(2)]);
var inst_41711 = process__$1.call(null,inst_41710);
var state_41720__$1 = state_41720;
if(cljs.core.truth_(inst_41711)){
var statearr_41723_41870 = state_41720__$1;
(statearr_41723_41870[(1)] = (5));

} else {
var statearr_41724_41871 = state_41720__$1;
(statearr_41724_41871[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41721 === (5))){
var state_41720__$1 = state_41720;
var statearr_41725_41872 = state_41720__$1;
(statearr_41725_41872[(2)] = null);

(statearr_41725_41872[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41721 === (6))){
var state_41720__$1 = state_41720;
var statearr_41726_41873 = state_41720__$1;
(statearr_41726_41873[(2)] = null);

(statearr_41726_41873[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41721 === (7))){
var inst_41716 = (state_41720[(2)]);
var state_41720__$1 = state_41720;
var statearr_41727_41874 = state_41720__$1;
(statearr_41727_41874[(2)] = inst_41716);

(statearr_41727_41874[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(__41864,c__35058__auto___41868,G__41707_41865,G__41707_41866__$1,n__31225__auto___41863,jobs,results,process__$1,async))
;
return ((function (__41864,switch__34968__auto__,c__35058__auto___41868,G__41707_41865,G__41707_41866__$1,n__31225__auto___41863,jobs,results,process__$1,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____0 = (function (){
var statearr_41728 = [null,null,null,null,null,null,null];
(statearr_41728[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__);

(statearr_41728[(1)] = (1));

return statearr_41728;
});
var cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____1 = (function (state_41720){
while(true){
var ret_value__34970__auto__ = (function (){try{while(true){
var result__34971__auto__ = switch__34968__auto__.call(null,state_41720);
if(cljs.core.keyword_identical_QMARK_.call(null,result__34971__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__34971__auto__;
}
break;
}
}catch (e41729){if((e41729 instanceof Object)){
var ex__34972__auto__ = e41729;
var statearr_41730_41875 = state_41720;
(statearr_41730_41875[(5)] = ex__34972__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_41720);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e41729;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__34970__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__41876 = state_41720;
state_41720 = G__41876;
continue;
} else {
return ret_value__34970__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__ = function(state_41720){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____1.call(this,state_41720);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__;
})()
;})(__41864,switch__34968__auto__,c__35058__auto___41868,G__41707_41865,G__41707_41866__$1,n__31225__auto___41863,jobs,results,process__$1,async))
})();
var state__35060__auto__ = (function (){var statearr_41731 = f__35059__auto__.call(null);
(statearr_41731[(6)] = c__35058__auto___41868);

return statearr_41731;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__35060__auto__);
});})(__41864,c__35058__auto___41868,G__41707_41865,G__41707_41866__$1,n__31225__auto___41863,jobs,results,process__$1,async))
);


break;
case "async":
var c__35058__auto___41877 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (__41864,c__35058__auto___41877,G__41707_41865,G__41707_41866__$1,n__31225__auto___41863,jobs,results,process__$1,async){
return (function (){
var f__35059__auto__ = (function (){var switch__34968__auto__ = ((function (__41864,c__35058__auto___41877,G__41707_41865,G__41707_41866__$1,n__31225__auto___41863,jobs,results,process__$1,async){
return (function (state_41744){
var state_val_41745 = (state_41744[(1)]);
if((state_val_41745 === (1))){
var state_41744__$1 = state_41744;
var statearr_41746_41878 = state_41744__$1;
(statearr_41746_41878[(2)] = null);

(statearr_41746_41878[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41745 === (2))){
var state_41744__$1 = state_41744;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_41744__$1,(4),jobs);
} else {
if((state_val_41745 === (3))){
var inst_41742 = (state_41744[(2)]);
var state_41744__$1 = state_41744;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_41744__$1,inst_41742);
} else {
if((state_val_41745 === (4))){
var inst_41734 = (state_41744[(2)]);
var inst_41735 = async.call(null,inst_41734);
var state_41744__$1 = state_41744;
if(cljs.core.truth_(inst_41735)){
var statearr_41747_41879 = state_41744__$1;
(statearr_41747_41879[(1)] = (5));

} else {
var statearr_41748_41880 = state_41744__$1;
(statearr_41748_41880[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41745 === (5))){
var state_41744__$1 = state_41744;
var statearr_41749_41881 = state_41744__$1;
(statearr_41749_41881[(2)] = null);

(statearr_41749_41881[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41745 === (6))){
var state_41744__$1 = state_41744;
var statearr_41750_41882 = state_41744__$1;
(statearr_41750_41882[(2)] = null);

(statearr_41750_41882[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41745 === (7))){
var inst_41740 = (state_41744[(2)]);
var state_41744__$1 = state_41744;
var statearr_41751_41883 = state_41744__$1;
(statearr_41751_41883[(2)] = inst_41740);

(statearr_41751_41883[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(__41864,c__35058__auto___41877,G__41707_41865,G__41707_41866__$1,n__31225__auto___41863,jobs,results,process__$1,async))
;
return ((function (__41864,switch__34968__auto__,c__35058__auto___41877,G__41707_41865,G__41707_41866__$1,n__31225__auto___41863,jobs,results,process__$1,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____0 = (function (){
var statearr_41752 = [null,null,null,null,null,null,null];
(statearr_41752[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__);

(statearr_41752[(1)] = (1));

return statearr_41752;
});
var cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____1 = (function (state_41744){
while(true){
var ret_value__34970__auto__ = (function (){try{while(true){
var result__34971__auto__ = switch__34968__auto__.call(null,state_41744);
if(cljs.core.keyword_identical_QMARK_.call(null,result__34971__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__34971__auto__;
}
break;
}
}catch (e41753){if((e41753 instanceof Object)){
var ex__34972__auto__ = e41753;
var statearr_41754_41884 = state_41744;
(statearr_41754_41884[(5)] = ex__34972__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_41744);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e41753;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__34970__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__41885 = state_41744;
state_41744 = G__41885;
continue;
} else {
return ret_value__34970__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__ = function(state_41744){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____1.call(this,state_41744);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__;
})()
;})(__41864,switch__34968__auto__,c__35058__auto___41877,G__41707_41865,G__41707_41866__$1,n__31225__auto___41863,jobs,results,process__$1,async))
})();
var state__35060__auto__ = (function (){var statearr_41755 = f__35059__auto__.call(null);
(statearr_41755[(6)] = c__35058__auto___41877);

return statearr_41755;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__35060__auto__);
});})(__41864,c__35058__auto___41877,G__41707_41865,G__41707_41866__$1,n__31225__auto___41863,jobs,results,process__$1,async))
);


break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__41707_41866__$1)].join('')));

}

var G__41886 = (__41864 + (1));
__41864 = G__41886;
continue;
} else {
}
break;
}

var c__35058__auto___41887 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__35058__auto___41887,jobs,results,process__$1,async){
return (function (){
var f__35059__auto__ = (function (){var switch__34968__auto__ = ((function (c__35058__auto___41887,jobs,results,process__$1,async){
return (function (state_41777){
var state_val_41778 = (state_41777[(1)]);
if((state_val_41778 === (1))){
var state_41777__$1 = state_41777;
var statearr_41779_41888 = state_41777__$1;
(statearr_41779_41888[(2)] = null);

(statearr_41779_41888[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41778 === (2))){
var state_41777__$1 = state_41777;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_41777__$1,(4),from);
} else {
if((state_val_41778 === (3))){
var inst_41775 = (state_41777[(2)]);
var state_41777__$1 = state_41777;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_41777__$1,inst_41775);
} else {
if((state_val_41778 === (4))){
var inst_41758 = (state_41777[(7)]);
var inst_41758__$1 = (state_41777[(2)]);
var inst_41759 = (inst_41758__$1 == null);
var state_41777__$1 = (function (){var statearr_41780 = state_41777;
(statearr_41780[(7)] = inst_41758__$1);

return statearr_41780;
})();
if(cljs.core.truth_(inst_41759)){
var statearr_41781_41889 = state_41777__$1;
(statearr_41781_41889[(1)] = (5));

} else {
var statearr_41782_41890 = state_41777__$1;
(statearr_41782_41890[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41778 === (5))){
var inst_41761 = cljs.core.async.close_BANG_.call(null,jobs);
var state_41777__$1 = state_41777;
var statearr_41783_41891 = state_41777__$1;
(statearr_41783_41891[(2)] = inst_41761);

(statearr_41783_41891[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41778 === (6))){
var inst_41763 = (state_41777[(8)]);
var inst_41758 = (state_41777[(7)]);
var inst_41763__$1 = cljs.core.async.chan.call(null,(1));
var inst_41764 = cljs.core.PersistentVector.EMPTY_NODE;
var inst_41765 = [inst_41758,inst_41763__$1];
var inst_41766 = (new cljs.core.PersistentVector(null,2,(5),inst_41764,inst_41765,null));
var state_41777__$1 = (function (){var statearr_41784 = state_41777;
(statearr_41784[(8)] = inst_41763__$1);

return statearr_41784;
})();
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_41777__$1,(8),jobs,inst_41766);
} else {
if((state_val_41778 === (7))){
var inst_41773 = (state_41777[(2)]);
var state_41777__$1 = state_41777;
var statearr_41785_41892 = state_41777__$1;
(statearr_41785_41892[(2)] = inst_41773);

(statearr_41785_41892[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41778 === (8))){
var inst_41763 = (state_41777[(8)]);
var inst_41768 = (state_41777[(2)]);
var state_41777__$1 = (function (){var statearr_41786 = state_41777;
(statearr_41786[(9)] = inst_41768);

return statearr_41786;
})();
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_41777__$1,(9),results,inst_41763);
} else {
if((state_val_41778 === (9))){
var inst_41770 = (state_41777[(2)]);
var state_41777__$1 = (function (){var statearr_41787 = state_41777;
(statearr_41787[(10)] = inst_41770);

return statearr_41787;
})();
var statearr_41788_41893 = state_41777__$1;
(statearr_41788_41893[(2)] = null);

(statearr_41788_41893[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
});})(c__35058__auto___41887,jobs,results,process__$1,async))
;
return ((function (switch__34968__auto__,c__35058__auto___41887,jobs,results,process__$1,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____0 = (function (){
var statearr_41789 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_41789[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__);

(statearr_41789[(1)] = (1));

return statearr_41789;
});
var cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____1 = (function (state_41777){
while(true){
var ret_value__34970__auto__ = (function (){try{while(true){
var result__34971__auto__ = switch__34968__auto__.call(null,state_41777);
if(cljs.core.keyword_identical_QMARK_.call(null,result__34971__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__34971__auto__;
}
break;
}
}catch (e41790){if((e41790 instanceof Object)){
var ex__34972__auto__ = e41790;
var statearr_41791_41894 = state_41777;
(statearr_41791_41894[(5)] = ex__34972__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_41777);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e41790;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__34970__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__41895 = state_41777;
state_41777 = G__41895;
continue;
} else {
return ret_value__34970__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__ = function(state_41777){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____1.call(this,state_41777);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__;
})()
;})(switch__34968__auto__,c__35058__auto___41887,jobs,results,process__$1,async))
})();
var state__35060__auto__ = (function (){var statearr_41792 = f__35059__auto__.call(null);
(statearr_41792[(6)] = c__35058__auto___41887);

return statearr_41792;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__35060__auto__);
});})(c__35058__auto___41887,jobs,results,process__$1,async))
);


var c__35058__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__35058__auto__,jobs,results,process__$1,async){
return (function (){
var f__35059__auto__ = (function (){var switch__34968__auto__ = ((function (c__35058__auto__,jobs,results,process__$1,async){
return (function (state_41830){
var state_val_41831 = (state_41830[(1)]);
if((state_val_41831 === (7))){
var inst_41826 = (state_41830[(2)]);
var state_41830__$1 = state_41830;
var statearr_41832_41896 = state_41830__$1;
(statearr_41832_41896[(2)] = inst_41826);

(statearr_41832_41896[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41831 === (20))){
var state_41830__$1 = state_41830;
var statearr_41833_41897 = state_41830__$1;
(statearr_41833_41897[(2)] = null);

(statearr_41833_41897[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41831 === (1))){
var state_41830__$1 = state_41830;
var statearr_41834_41898 = state_41830__$1;
(statearr_41834_41898[(2)] = null);

(statearr_41834_41898[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41831 === (4))){
var inst_41795 = (state_41830[(7)]);
var inst_41795__$1 = (state_41830[(2)]);
var inst_41796 = (inst_41795__$1 == null);
var state_41830__$1 = (function (){var statearr_41835 = state_41830;
(statearr_41835[(7)] = inst_41795__$1);

return statearr_41835;
})();
if(cljs.core.truth_(inst_41796)){
var statearr_41836_41899 = state_41830__$1;
(statearr_41836_41899[(1)] = (5));

} else {
var statearr_41837_41900 = state_41830__$1;
(statearr_41837_41900[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41831 === (15))){
var inst_41808 = (state_41830[(8)]);
var state_41830__$1 = state_41830;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_41830__$1,(18),to,inst_41808);
} else {
if((state_val_41831 === (21))){
var inst_41821 = (state_41830[(2)]);
var state_41830__$1 = state_41830;
var statearr_41838_41901 = state_41830__$1;
(statearr_41838_41901[(2)] = inst_41821);

(statearr_41838_41901[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41831 === (13))){
var inst_41823 = (state_41830[(2)]);
var state_41830__$1 = (function (){var statearr_41839 = state_41830;
(statearr_41839[(9)] = inst_41823);

return statearr_41839;
})();
var statearr_41840_41902 = state_41830__$1;
(statearr_41840_41902[(2)] = null);

(statearr_41840_41902[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41831 === (6))){
var inst_41795 = (state_41830[(7)]);
var state_41830__$1 = state_41830;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_41830__$1,(11),inst_41795);
} else {
if((state_val_41831 === (17))){
var inst_41816 = (state_41830[(2)]);
var state_41830__$1 = state_41830;
if(cljs.core.truth_(inst_41816)){
var statearr_41841_41903 = state_41830__$1;
(statearr_41841_41903[(1)] = (19));

} else {
var statearr_41842_41904 = state_41830__$1;
(statearr_41842_41904[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41831 === (3))){
var inst_41828 = (state_41830[(2)]);
var state_41830__$1 = state_41830;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_41830__$1,inst_41828);
} else {
if((state_val_41831 === (12))){
var inst_41805 = (state_41830[(10)]);
var state_41830__$1 = state_41830;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_41830__$1,(14),inst_41805);
} else {
if((state_val_41831 === (2))){
var state_41830__$1 = state_41830;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_41830__$1,(4),results);
} else {
if((state_val_41831 === (19))){
var state_41830__$1 = state_41830;
var statearr_41843_41905 = state_41830__$1;
(statearr_41843_41905[(2)] = null);

(statearr_41843_41905[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41831 === (11))){
var inst_41805 = (state_41830[(2)]);
var state_41830__$1 = (function (){var statearr_41844 = state_41830;
(statearr_41844[(10)] = inst_41805);

return statearr_41844;
})();
var statearr_41845_41906 = state_41830__$1;
(statearr_41845_41906[(2)] = null);

(statearr_41845_41906[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41831 === (9))){
var state_41830__$1 = state_41830;
var statearr_41846_41907 = state_41830__$1;
(statearr_41846_41907[(2)] = null);

(statearr_41846_41907[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41831 === (5))){
var state_41830__$1 = state_41830;
if(cljs.core.truth_(close_QMARK_)){
var statearr_41847_41908 = state_41830__$1;
(statearr_41847_41908[(1)] = (8));

} else {
var statearr_41848_41909 = state_41830__$1;
(statearr_41848_41909[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41831 === (14))){
var inst_41810 = (state_41830[(11)]);
var inst_41808 = (state_41830[(8)]);
var inst_41808__$1 = (state_41830[(2)]);
var inst_41809 = (inst_41808__$1 == null);
var inst_41810__$1 = cljs.core.not.call(null,inst_41809);
var state_41830__$1 = (function (){var statearr_41849 = state_41830;
(statearr_41849[(11)] = inst_41810__$1);

(statearr_41849[(8)] = inst_41808__$1);

return statearr_41849;
})();
if(inst_41810__$1){
var statearr_41850_41910 = state_41830__$1;
(statearr_41850_41910[(1)] = (15));

} else {
var statearr_41851_41911 = state_41830__$1;
(statearr_41851_41911[(1)] = (16));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41831 === (16))){
var inst_41810 = (state_41830[(11)]);
var state_41830__$1 = state_41830;
var statearr_41852_41912 = state_41830__$1;
(statearr_41852_41912[(2)] = inst_41810);

(statearr_41852_41912[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41831 === (10))){
var inst_41802 = (state_41830[(2)]);
var state_41830__$1 = state_41830;
var statearr_41853_41913 = state_41830__$1;
(statearr_41853_41913[(2)] = inst_41802);

(statearr_41853_41913[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41831 === (18))){
var inst_41813 = (state_41830[(2)]);
var state_41830__$1 = state_41830;
var statearr_41854_41914 = state_41830__$1;
(statearr_41854_41914[(2)] = inst_41813);

(statearr_41854_41914[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41831 === (8))){
var inst_41799 = cljs.core.async.close_BANG_.call(null,to);
var state_41830__$1 = state_41830;
var statearr_41855_41915 = state_41830__$1;
(statearr_41855_41915[(2)] = inst_41799);

(statearr_41855_41915[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__35058__auto__,jobs,results,process__$1,async))
;
return ((function (switch__34968__auto__,c__35058__auto__,jobs,results,process__$1,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____0 = (function (){
var statearr_41856 = [null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_41856[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__);

(statearr_41856[(1)] = (1));

return statearr_41856;
});
var cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____1 = (function (state_41830){
while(true){
var ret_value__34970__auto__ = (function (){try{while(true){
var result__34971__auto__ = switch__34968__auto__.call(null,state_41830);
if(cljs.core.keyword_identical_QMARK_.call(null,result__34971__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__34971__auto__;
}
break;
}
}catch (e41857){if((e41857 instanceof Object)){
var ex__34972__auto__ = e41857;
var statearr_41858_41916 = state_41830;
(statearr_41858_41916[(5)] = ex__34972__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_41830);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e41857;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__34970__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__41917 = state_41830;
state_41830 = G__41917;
continue;
} else {
return ret_value__34970__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__ = function(state_41830){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____1.call(this,state_41830);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__;
})()
;})(switch__34968__auto__,c__35058__auto__,jobs,results,process__$1,async))
})();
var state__35060__auto__ = (function (){var statearr_41859 = f__35059__auto__.call(null);
(statearr_41859[(6)] = c__35058__auto__);

return statearr_41859;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__35060__auto__);
});})(c__35058__auto__,jobs,results,process__$1,async))
);

return c__35058__auto__;
});
/**
 * Takes elements from the from channel and supplies them to the to
 *   channel, subject to the async function af, with parallelism n. af
 *   must be a function of two arguments, the first an input value and
 *   the second a channel on which to place the result(s). af must close!
 *   the channel before returning.  The presumption is that af will
 *   return immediately, having launched some asynchronous operation
 *   whose completion/callback will manipulate the result channel. Outputs
 *   will be returned in order relative to  the inputs. By default, the to
 *   channel will be closed when the from channel closes, but can be
 *   determined by the close?  parameter. Will stop consuming the from
 *   channel if the to channel closes.
 */
cljs.core.async.pipeline_async = (function cljs$core$async$pipeline_async(var_args){
var G__41919 = arguments.length;
switch (G__41919) {
case 4:
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
case 5:
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$4 = (function (n,to,af,from){
return cljs.core.async.pipeline_async.call(null,n,to,af,from,true);
});

cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5 = (function (n,to,af,from,close_QMARK_){
return cljs.core.async.pipeline_STAR_.call(null,n,to,af,from,close_QMARK_,null,new cljs.core.Keyword(null,"async","async",1050769601));
});

cljs.core.async.pipeline_async.cljs$lang$maxFixedArity = 5;

/**
 * Takes elements from the from channel and supplies them to the to
 *   channel, subject to the transducer xf, with parallelism n. Because
 *   it is parallel, the transducer will be applied independently to each
 *   element, not across elements, and may produce zero or more outputs
 *   per input.  Outputs will be returned in order relative to the
 *   inputs. By default, the to channel will be closed when the from
 *   channel closes, but can be determined by the close?  parameter. Will
 *   stop consuming the from channel if the to channel closes.
 * 
 *   Note this is supplied for API compatibility with the Clojure version.
 *   Values of N > 1 will not result in actual concurrency in a
 *   single-threaded runtime.
 */
cljs.core.async.pipeline = (function cljs$core$async$pipeline(var_args){
var G__41922 = arguments.length;
switch (G__41922) {
case 4:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
case 5:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]));

break;
case 6:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]),(arguments[(5)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$4 = (function (n,to,xf,from){
return cljs.core.async.pipeline.call(null,n,to,xf,from,true);
});

cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5 = (function (n,to,xf,from,close_QMARK_){
return cljs.core.async.pipeline.call(null,n,to,xf,from,close_QMARK_,null);
});

cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6 = (function (n,to,xf,from,close_QMARK_,ex_handler){
return cljs.core.async.pipeline_STAR_.call(null,n,to,xf,from,close_QMARK_,ex_handler,new cljs.core.Keyword(null,"compute","compute",1555393130));
});

cljs.core.async.pipeline.cljs$lang$maxFixedArity = 6;

/**
 * Takes a predicate and a source channel and returns a vector of two
 *   channels, the first of which will contain the values for which the
 *   predicate returned true, the second those for which it returned
 *   false.
 * 
 *   The out channels will be unbuffered by default, or two buf-or-ns can
 *   be supplied. The channels will close after the source channel has
 *   closed.
 */
cljs.core.async.split = (function cljs$core$async$split(var_args){
var G__41925 = arguments.length;
switch (G__41925) {
case 2:
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 4:
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.split.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.split.call(null,p,ch,null,null);
});

cljs.core.async.split.cljs$core$IFn$_invoke$arity$4 = (function (p,ch,t_buf_or_n,f_buf_or_n){
var tc = cljs.core.async.chan.call(null,t_buf_or_n);
var fc = cljs.core.async.chan.call(null,f_buf_or_n);
var c__35058__auto___41974 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__35058__auto___41974,tc,fc){
return (function (){
var f__35059__auto__ = (function (){var switch__34968__auto__ = ((function (c__35058__auto___41974,tc,fc){
return (function (state_41951){
var state_val_41952 = (state_41951[(1)]);
if((state_val_41952 === (7))){
var inst_41947 = (state_41951[(2)]);
var state_41951__$1 = state_41951;
var statearr_41953_41975 = state_41951__$1;
(statearr_41953_41975[(2)] = inst_41947);

(statearr_41953_41975[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41952 === (1))){
var state_41951__$1 = state_41951;
var statearr_41954_41976 = state_41951__$1;
(statearr_41954_41976[(2)] = null);

(statearr_41954_41976[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41952 === (4))){
var inst_41928 = (state_41951[(7)]);
var inst_41928__$1 = (state_41951[(2)]);
var inst_41929 = (inst_41928__$1 == null);
var state_41951__$1 = (function (){var statearr_41955 = state_41951;
(statearr_41955[(7)] = inst_41928__$1);

return statearr_41955;
})();
if(cljs.core.truth_(inst_41929)){
var statearr_41956_41977 = state_41951__$1;
(statearr_41956_41977[(1)] = (5));

} else {
var statearr_41957_41978 = state_41951__$1;
(statearr_41957_41978[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41952 === (13))){
var state_41951__$1 = state_41951;
var statearr_41958_41979 = state_41951__$1;
(statearr_41958_41979[(2)] = null);

(statearr_41958_41979[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41952 === (6))){
var inst_41928 = (state_41951[(7)]);
var inst_41934 = p.call(null,inst_41928);
var state_41951__$1 = state_41951;
if(cljs.core.truth_(inst_41934)){
var statearr_41959_41980 = state_41951__$1;
(statearr_41959_41980[(1)] = (9));

} else {
var statearr_41960_41981 = state_41951__$1;
(statearr_41960_41981[(1)] = (10));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41952 === (3))){
var inst_41949 = (state_41951[(2)]);
var state_41951__$1 = state_41951;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_41951__$1,inst_41949);
} else {
if((state_val_41952 === (12))){
var state_41951__$1 = state_41951;
var statearr_41961_41982 = state_41951__$1;
(statearr_41961_41982[(2)] = null);

(statearr_41961_41982[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41952 === (2))){
var state_41951__$1 = state_41951;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_41951__$1,(4),ch);
} else {
if((state_val_41952 === (11))){
var inst_41928 = (state_41951[(7)]);
var inst_41938 = (state_41951[(2)]);
var state_41951__$1 = state_41951;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_41951__$1,(8),inst_41938,inst_41928);
} else {
if((state_val_41952 === (9))){
var state_41951__$1 = state_41951;
var statearr_41962_41983 = state_41951__$1;
(statearr_41962_41983[(2)] = tc);

(statearr_41962_41983[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41952 === (5))){
var inst_41931 = cljs.core.async.close_BANG_.call(null,tc);
var inst_41932 = cljs.core.async.close_BANG_.call(null,fc);
var state_41951__$1 = (function (){var statearr_41963 = state_41951;
(statearr_41963[(8)] = inst_41931);

return statearr_41963;
})();
var statearr_41964_41984 = state_41951__$1;
(statearr_41964_41984[(2)] = inst_41932);

(statearr_41964_41984[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41952 === (14))){
var inst_41945 = (state_41951[(2)]);
var state_41951__$1 = state_41951;
var statearr_41965_41985 = state_41951__$1;
(statearr_41965_41985[(2)] = inst_41945);

(statearr_41965_41985[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41952 === (10))){
var state_41951__$1 = state_41951;
var statearr_41966_41986 = state_41951__$1;
(statearr_41966_41986[(2)] = fc);

(statearr_41966_41986[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41952 === (8))){
var inst_41940 = (state_41951[(2)]);
var state_41951__$1 = state_41951;
if(cljs.core.truth_(inst_41940)){
var statearr_41967_41987 = state_41951__$1;
(statearr_41967_41987[(1)] = (12));

} else {
var statearr_41968_41988 = state_41951__$1;
(statearr_41968_41988[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__35058__auto___41974,tc,fc))
;
return ((function (switch__34968__auto__,c__35058__auto___41974,tc,fc){
return (function() {
var cljs$core$async$state_machine__34969__auto__ = null;
var cljs$core$async$state_machine__34969__auto____0 = (function (){
var statearr_41969 = [null,null,null,null,null,null,null,null,null];
(statearr_41969[(0)] = cljs$core$async$state_machine__34969__auto__);

(statearr_41969[(1)] = (1));

return statearr_41969;
});
var cljs$core$async$state_machine__34969__auto____1 = (function (state_41951){
while(true){
var ret_value__34970__auto__ = (function (){try{while(true){
var result__34971__auto__ = switch__34968__auto__.call(null,state_41951);
if(cljs.core.keyword_identical_QMARK_.call(null,result__34971__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__34971__auto__;
}
break;
}
}catch (e41970){if((e41970 instanceof Object)){
var ex__34972__auto__ = e41970;
var statearr_41971_41989 = state_41951;
(statearr_41971_41989[(5)] = ex__34972__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_41951);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e41970;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__34970__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__41990 = state_41951;
state_41951 = G__41990;
continue;
} else {
return ret_value__34970__auto__;
}
break;
}
});
cljs$core$async$state_machine__34969__auto__ = function(state_41951){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__34969__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__34969__auto____1.call(this,state_41951);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__34969__auto____0;
cljs$core$async$state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__34969__auto____1;
return cljs$core$async$state_machine__34969__auto__;
})()
;})(switch__34968__auto__,c__35058__auto___41974,tc,fc))
})();
var state__35060__auto__ = (function (){var statearr_41972 = f__35059__auto__.call(null);
(statearr_41972[(6)] = c__35058__auto___41974);

return statearr_41972;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__35060__auto__);
});})(c__35058__auto___41974,tc,fc))
);


return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [tc,fc], null);
});

cljs.core.async.split.cljs$lang$maxFixedArity = 4;

/**
 * f should be a function of 2 arguments. Returns a channel containing
 *   the single result of applying f to init and the first item from the
 *   channel, then applying f to that result and the 2nd item, etc. If
 *   the channel closes without yielding items, returns init and f is not
 *   called. ch must close before reduce produces a result.
 */
cljs.core.async.reduce = (function cljs$core$async$reduce(f,init,ch){
var c__35058__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__35058__auto__){
return (function (){
var f__35059__auto__ = (function (){var switch__34968__auto__ = ((function (c__35058__auto__){
return (function (state_42011){
var state_val_42012 = (state_42011[(1)]);
if((state_val_42012 === (7))){
var inst_42007 = (state_42011[(2)]);
var state_42011__$1 = state_42011;
var statearr_42013_42031 = state_42011__$1;
(statearr_42013_42031[(2)] = inst_42007);

(statearr_42013_42031[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42012 === (1))){
var inst_41991 = init;
var state_42011__$1 = (function (){var statearr_42014 = state_42011;
(statearr_42014[(7)] = inst_41991);

return statearr_42014;
})();
var statearr_42015_42032 = state_42011__$1;
(statearr_42015_42032[(2)] = null);

(statearr_42015_42032[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42012 === (4))){
var inst_41994 = (state_42011[(8)]);
var inst_41994__$1 = (state_42011[(2)]);
var inst_41995 = (inst_41994__$1 == null);
var state_42011__$1 = (function (){var statearr_42016 = state_42011;
(statearr_42016[(8)] = inst_41994__$1);

return statearr_42016;
})();
if(cljs.core.truth_(inst_41995)){
var statearr_42017_42033 = state_42011__$1;
(statearr_42017_42033[(1)] = (5));

} else {
var statearr_42018_42034 = state_42011__$1;
(statearr_42018_42034[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42012 === (6))){
var inst_41994 = (state_42011[(8)]);
var inst_41991 = (state_42011[(7)]);
var inst_41998 = (state_42011[(9)]);
var inst_41998__$1 = f.call(null,inst_41991,inst_41994);
var inst_41999 = cljs.core.reduced_QMARK_.call(null,inst_41998__$1);
var state_42011__$1 = (function (){var statearr_42019 = state_42011;
(statearr_42019[(9)] = inst_41998__$1);

return statearr_42019;
})();
if(inst_41999){
var statearr_42020_42035 = state_42011__$1;
(statearr_42020_42035[(1)] = (8));

} else {
var statearr_42021_42036 = state_42011__$1;
(statearr_42021_42036[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42012 === (3))){
var inst_42009 = (state_42011[(2)]);
var state_42011__$1 = state_42011;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_42011__$1,inst_42009);
} else {
if((state_val_42012 === (2))){
var state_42011__$1 = state_42011;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_42011__$1,(4),ch);
} else {
if((state_val_42012 === (9))){
var inst_41998 = (state_42011[(9)]);
var inst_41991 = inst_41998;
var state_42011__$1 = (function (){var statearr_42022 = state_42011;
(statearr_42022[(7)] = inst_41991);

return statearr_42022;
})();
var statearr_42023_42037 = state_42011__$1;
(statearr_42023_42037[(2)] = null);

(statearr_42023_42037[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42012 === (5))){
var inst_41991 = (state_42011[(7)]);
var state_42011__$1 = state_42011;
var statearr_42024_42038 = state_42011__$1;
(statearr_42024_42038[(2)] = inst_41991);

(statearr_42024_42038[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42012 === (10))){
var inst_42005 = (state_42011[(2)]);
var state_42011__$1 = state_42011;
var statearr_42025_42039 = state_42011__$1;
(statearr_42025_42039[(2)] = inst_42005);

(statearr_42025_42039[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42012 === (8))){
var inst_41998 = (state_42011[(9)]);
var inst_42001 = cljs.core.deref.call(null,inst_41998);
var state_42011__$1 = state_42011;
var statearr_42026_42040 = state_42011__$1;
(statearr_42026_42040[(2)] = inst_42001);

(statearr_42026_42040[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
});})(c__35058__auto__))
;
return ((function (switch__34968__auto__,c__35058__auto__){
return (function() {
var cljs$core$async$reduce_$_state_machine__34969__auto__ = null;
var cljs$core$async$reduce_$_state_machine__34969__auto____0 = (function (){
var statearr_42027 = [null,null,null,null,null,null,null,null,null,null];
(statearr_42027[(0)] = cljs$core$async$reduce_$_state_machine__34969__auto__);

(statearr_42027[(1)] = (1));

return statearr_42027;
});
var cljs$core$async$reduce_$_state_machine__34969__auto____1 = (function (state_42011){
while(true){
var ret_value__34970__auto__ = (function (){try{while(true){
var result__34971__auto__ = switch__34968__auto__.call(null,state_42011);
if(cljs.core.keyword_identical_QMARK_.call(null,result__34971__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__34971__auto__;
}
break;
}
}catch (e42028){if((e42028 instanceof Object)){
var ex__34972__auto__ = e42028;
var statearr_42029_42041 = state_42011;
(statearr_42029_42041[(5)] = ex__34972__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_42011);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e42028;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__34970__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__42042 = state_42011;
state_42011 = G__42042;
continue;
} else {
return ret_value__34970__auto__;
}
break;
}
});
cljs$core$async$reduce_$_state_machine__34969__auto__ = function(state_42011){
switch(arguments.length){
case 0:
return cljs$core$async$reduce_$_state_machine__34969__auto____0.call(this);
case 1:
return cljs$core$async$reduce_$_state_machine__34969__auto____1.call(this,state_42011);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$reduce_$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$reduce_$_state_machine__34969__auto____0;
cljs$core$async$reduce_$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$reduce_$_state_machine__34969__auto____1;
return cljs$core$async$reduce_$_state_machine__34969__auto__;
})()
;})(switch__34968__auto__,c__35058__auto__))
})();
var state__35060__auto__ = (function (){var statearr_42030 = f__35059__auto__.call(null);
(statearr_42030[(6)] = c__35058__auto__);

return statearr_42030;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__35060__auto__);
});})(c__35058__auto__))
);

return c__35058__auto__;
});
/**
 * async/reduces a channel with a transformation (xform f).
 *   Returns a channel containing the result.  ch must close before
 *   transduce produces a result.
 */
cljs.core.async.transduce = (function cljs$core$async$transduce(xform,f,init,ch){
var f__$1 = xform.call(null,f);
var c__35058__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__35058__auto__,f__$1){
return (function (){
var f__35059__auto__ = (function (){var switch__34968__auto__ = ((function (c__35058__auto__,f__$1){
return (function (state_42048){
var state_val_42049 = (state_42048[(1)]);
if((state_val_42049 === (1))){
var inst_42043 = cljs.core.async.reduce.call(null,f__$1,init,ch);
var state_42048__$1 = state_42048;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_42048__$1,(2),inst_42043);
} else {
if((state_val_42049 === (2))){
var inst_42045 = (state_42048[(2)]);
var inst_42046 = f__$1.call(null,inst_42045);
var state_42048__$1 = state_42048;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_42048__$1,inst_42046);
} else {
return null;
}
}
});})(c__35058__auto__,f__$1))
;
return ((function (switch__34968__auto__,c__35058__auto__,f__$1){
return (function() {
var cljs$core$async$transduce_$_state_machine__34969__auto__ = null;
var cljs$core$async$transduce_$_state_machine__34969__auto____0 = (function (){
var statearr_42050 = [null,null,null,null,null,null,null];
(statearr_42050[(0)] = cljs$core$async$transduce_$_state_machine__34969__auto__);

(statearr_42050[(1)] = (1));

return statearr_42050;
});
var cljs$core$async$transduce_$_state_machine__34969__auto____1 = (function (state_42048){
while(true){
var ret_value__34970__auto__ = (function (){try{while(true){
var result__34971__auto__ = switch__34968__auto__.call(null,state_42048);
if(cljs.core.keyword_identical_QMARK_.call(null,result__34971__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__34971__auto__;
}
break;
}
}catch (e42051){if((e42051 instanceof Object)){
var ex__34972__auto__ = e42051;
var statearr_42052_42054 = state_42048;
(statearr_42052_42054[(5)] = ex__34972__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_42048);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e42051;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__34970__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__42055 = state_42048;
state_42048 = G__42055;
continue;
} else {
return ret_value__34970__auto__;
}
break;
}
});
cljs$core$async$transduce_$_state_machine__34969__auto__ = function(state_42048){
switch(arguments.length){
case 0:
return cljs$core$async$transduce_$_state_machine__34969__auto____0.call(this);
case 1:
return cljs$core$async$transduce_$_state_machine__34969__auto____1.call(this,state_42048);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$transduce_$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$transduce_$_state_machine__34969__auto____0;
cljs$core$async$transduce_$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$transduce_$_state_machine__34969__auto____1;
return cljs$core$async$transduce_$_state_machine__34969__auto__;
})()
;})(switch__34968__auto__,c__35058__auto__,f__$1))
})();
var state__35060__auto__ = (function (){var statearr_42053 = f__35059__auto__.call(null);
(statearr_42053[(6)] = c__35058__auto__);

return statearr_42053;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__35060__auto__);
});})(c__35058__auto__,f__$1))
);

return c__35058__auto__;
});
/**
 * Puts the contents of coll into the supplied channel.
 * 
 *   By default the channel will be closed after the items are copied,
 *   but can be determined by the close? parameter.
 * 
 *   Returns a channel which will close after the items are copied.
 */
cljs.core.async.onto_chan = (function cljs$core$async$onto_chan(var_args){
var G__42057 = arguments.length;
switch (G__42057) {
case 2:
return cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$2 = (function (ch,coll){
return cljs.core.async.onto_chan.call(null,ch,coll,true);
});

cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$3 = (function (ch,coll,close_QMARK_){
var c__35058__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__35058__auto__){
return (function (){
var f__35059__auto__ = (function (){var switch__34968__auto__ = ((function (c__35058__auto__){
return (function (state_42082){
var state_val_42083 = (state_42082[(1)]);
if((state_val_42083 === (7))){
var inst_42064 = (state_42082[(2)]);
var state_42082__$1 = state_42082;
var statearr_42084_42105 = state_42082__$1;
(statearr_42084_42105[(2)] = inst_42064);

(statearr_42084_42105[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42083 === (1))){
var inst_42058 = cljs.core.seq.call(null,coll);
var inst_42059 = inst_42058;
var state_42082__$1 = (function (){var statearr_42085 = state_42082;
(statearr_42085[(7)] = inst_42059);

return statearr_42085;
})();
var statearr_42086_42106 = state_42082__$1;
(statearr_42086_42106[(2)] = null);

(statearr_42086_42106[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42083 === (4))){
var inst_42059 = (state_42082[(7)]);
var inst_42062 = cljs.core.first.call(null,inst_42059);
var state_42082__$1 = state_42082;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_42082__$1,(7),ch,inst_42062);
} else {
if((state_val_42083 === (13))){
var inst_42076 = (state_42082[(2)]);
var state_42082__$1 = state_42082;
var statearr_42087_42107 = state_42082__$1;
(statearr_42087_42107[(2)] = inst_42076);

(statearr_42087_42107[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42083 === (6))){
var inst_42067 = (state_42082[(2)]);
var state_42082__$1 = state_42082;
if(cljs.core.truth_(inst_42067)){
var statearr_42088_42108 = state_42082__$1;
(statearr_42088_42108[(1)] = (8));

} else {
var statearr_42089_42109 = state_42082__$1;
(statearr_42089_42109[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42083 === (3))){
var inst_42080 = (state_42082[(2)]);
var state_42082__$1 = state_42082;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_42082__$1,inst_42080);
} else {
if((state_val_42083 === (12))){
var state_42082__$1 = state_42082;
var statearr_42090_42110 = state_42082__$1;
(statearr_42090_42110[(2)] = null);

(statearr_42090_42110[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42083 === (2))){
var inst_42059 = (state_42082[(7)]);
var state_42082__$1 = state_42082;
if(cljs.core.truth_(inst_42059)){
var statearr_42091_42111 = state_42082__$1;
(statearr_42091_42111[(1)] = (4));

} else {
var statearr_42092_42112 = state_42082__$1;
(statearr_42092_42112[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42083 === (11))){
var inst_42073 = cljs.core.async.close_BANG_.call(null,ch);
var state_42082__$1 = state_42082;
var statearr_42093_42113 = state_42082__$1;
(statearr_42093_42113[(2)] = inst_42073);

(statearr_42093_42113[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42083 === (9))){
var state_42082__$1 = state_42082;
if(cljs.core.truth_(close_QMARK_)){
var statearr_42094_42114 = state_42082__$1;
(statearr_42094_42114[(1)] = (11));

} else {
var statearr_42095_42115 = state_42082__$1;
(statearr_42095_42115[(1)] = (12));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42083 === (5))){
var inst_42059 = (state_42082[(7)]);
var state_42082__$1 = state_42082;
var statearr_42096_42116 = state_42082__$1;
(statearr_42096_42116[(2)] = inst_42059);

(statearr_42096_42116[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42083 === (10))){
var inst_42078 = (state_42082[(2)]);
var state_42082__$1 = state_42082;
var statearr_42097_42117 = state_42082__$1;
(statearr_42097_42117[(2)] = inst_42078);

(statearr_42097_42117[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42083 === (8))){
var inst_42059 = (state_42082[(7)]);
var inst_42069 = cljs.core.next.call(null,inst_42059);
var inst_42059__$1 = inst_42069;
var state_42082__$1 = (function (){var statearr_42098 = state_42082;
(statearr_42098[(7)] = inst_42059__$1);

return statearr_42098;
})();
var statearr_42099_42118 = state_42082__$1;
(statearr_42099_42118[(2)] = null);

(statearr_42099_42118[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__35058__auto__))
;
return ((function (switch__34968__auto__,c__35058__auto__){
return (function() {
var cljs$core$async$state_machine__34969__auto__ = null;
var cljs$core$async$state_machine__34969__auto____0 = (function (){
var statearr_42100 = [null,null,null,null,null,null,null,null];
(statearr_42100[(0)] = cljs$core$async$state_machine__34969__auto__);

(statearr_42100[(1)] = (1));

return statearr_42100;
});
var cljs$core$async$state_machine__34969__auto____1 = (function (state_42082){
while(true){
var ret_value__34970__auto__ = (function (){try{while(true){
var result__34971__auto__ = switch__34968__auto__.call(null,state_42082);
if(cljs.core.keyword_identical_QMARK_.call(null,result__34971__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__34971__auto__;
}
break;
}
}catch (e42101){if((e42101 instanceof Object)){
var ex__34972__auto__ = e42101;
var statearr_42102_42119 = state_42082;
(statearr_42102_42119[(5)] = ex__34972__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_42082);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e42101;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__34970__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__42120 = state_42082;
state_42082 = G__42120;
continue;
} else {
return ret_value__34970__auto__;
}
break;
}
});
cljs$core$async$state_machine__34969__auto__ = function(state_42082){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__34969__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__34969__auto____1.call(this,state_42082);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__34969__auto____0;
cljs$core$async$state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__34969__auto____1;
return cljs$core$async$state_machine__34969__auto__;
})()
;})(switch__34968__auto__,c__35058__auto__))
})();
var state__35060__auto__ = (function (){var statearr_42103 = f__35059__auto__.call(null);
(statearr_42103[(6)] = c__35058__auto__);

return statearr_42103;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__35060__auto__);
});})(c__35058__auto__))
);

return c__35058__auto__;
});

cljs.core.async.onto_chan.cljs$lang$maxFixedArity = 3;

/**
 * Creates and returns a channel which contains the contents of coll,
 *   closing when exhausted.
 */
cljs.core.async.to_chan = (function cljs$core$async$to_chan(coll){
var ch = cljs.core.async.chan.call(null,cljs.core.bounded_count.call(null,(100),coll));
cljs.core.async.onto_chan.call(null,ch,coll);

return ch;
});

/**
 * @interface
 */
cljs.core.async.Mux = function(){};

cljs.core.async.muxch_STAR_ = (function cljs$core$async$muxch_STAR_(_){
if((!((_ == null))) && (!((_.cljs$core$async$Mux$muxch_STAR_$arity$1 == null)))){
return _.cljs$core$async$Mux$muxch_STAR_$arity$1(_);
} else {
var x__30915__auto__ = (((_ == null))?null:_);
var m__30916__auto__ = (cljs.core.async.muxch_STAR_[goog.typeOf(x__30915__auto__)]);
if(!((m__30916__auto__ == null))){
return m__30916__auto__.call(null,_);
} else {
var m__30916__auto____$1 = (cljs.core.async.muxch_STAR_["_"]);
if(!((m__30916__auto____$1 == null))){
return m__30916__auto____$1.call(null,_);
} else {
throw cljs.core.missing_protocol.call(null,"Mux.muxch*",_);
}
}
}
});


/**
 * @interface
 */
cljs.core.async.Mult = function(){};

cljs.core.async.tap_STAR_ = (function cljs$core$async$tap_STAR_(m,ch,close_QMARK_){
if((!((m == null))) && (!((m.cljs$core$async$Mult$tap_STAR_$arity$3 == null)))){
return m.cljs$core$async$Mult$tap_STAR_$arity$3(m,ch,close_QMARK_);
} else {
var x__30915__auto__ = (((m == null))?null:m);
var m__30916__auto__ = (cljs.core.async.tap_STAR_[goog.typeOf(x__30915__auto__)]);
if(!((m__30916__auto__ == null))){
return m__30916__auto__.call(null,m,ch,close_QMARK_);
} else {
var m__30916__auto____$1 = (cljs.core.async.tap_STAR_["_"]);
if(!((m__30916__auto____$1 == null))){
return m__30916__auto____$1.call(null,m,ch,close_QMARK_);
} else {
throw cljs.core.missing_protocol.call(null,"Mult.tap*",m);
}
}
}
});

cljs.core.async.untap_STAR_ = (function cljs$core$async$untap_STAR_(m,ch){
if((!((m == null))) && (!((m.cljs$core$async$Mult$untap_STAR_$arity$2 == null)))){
return m.cljs$core$async$Mult$untap_STAR_$arity$2(m,ch);
} else {
var x__30915__auto__ = (((m == null))?null:m);
var m__30916__auto__ = (cljs.core.async.untap_STAR_[goog.typeOf(x__30915__auto__)]);
if(!((m__30916__auto__ == null))){
return m__30916__auto__.call(null,m,ch);
} else {
var m__30916__auto____$1 = (cljs.core.async.untap_STAR_["_"]);
if(!((m__30916__auto____$1 == null))){
return m__30916__auto____$1.call(null,m,ch);
} else {
throw cljs.core.missing_protocol.call(null,"Mult.untap*",m);
}
}
}
});

cljs.core.async.untap_all_STAR_ = (function cljs$core$async$untap_all_STAR_(m){
if((!((m == null))) && (!((m.cljs$core$async$Mult$untap_all_STAR_$arity$1 == null)))){
return m.cljs$core$async$Mult$untap_all_STAR_$arity$1(m);
} else {
var x__30915__auto__ = (((m == null))?null:m);
var m__30916__auto__ = (cljs.core.async.untap_all_STAR_[goog.typeOf(x__30915__auto__)]);
if(!((m__30916__auto__ == null))){
return m__30916__auto__.call(null,m);
} else {
var m__30916__auto____$1 = (cljs.core.async.untap_all_STAR_["_"]);
if(!((m__30916__auto____$1 == null))){
return m__30916__auto____$1.call(null,m);
} else {
throw cljs.core.missing_protocol.call(null,"Mult.untap-all*",m);
}
}
}
});

/**
 * Creates and returns a mult(iple) of the supplied channel. Channels
 *   containing copies of the channel can be created with 'tap', and
 *   detached with 'untap'.
 * 
 *   Each item is distributed to all taps in parallel and synchronously,
 *   i.e. each tap must accept before the next item is distributed. Use
 *   buffering/windowing to prevent slow taps from holding up the mult.
 * 
 *   Items received when there are no taps get dropped.
 * 
 *   If a tap puts to a closed channel, it will be removed from the mult.
 */
cljs.core.async.mult = (function cljs$core$async$mult(ch){
var cs = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var m = (function (){
if(typeof cljs.core.async.t_cljs$core$async42121 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.Mult}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async42121 = (function (ch,cs,meta42122){
this.ch = ch;
this.cs = cs;
this.meta42122 = meta42122;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async42121.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (cs){
return (function (_42123,meta42122__$1){
var self__ = this;
var _42123__$1 = this;
return (new cljs.core.async.t_cljs$core$async42121(self__.ch,self__.cs,meta42122__$1));
});})(cs))
;

cljs.core.async.t_cljs$core$async42121.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (cs){
return (function (_42123){
var self__ = this;
var _42123__$1 = this;
return self__.meta42122;
});})(cs))
;

cljs.core.async.t_cljs$core$async42121.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async42121.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = ((function (cs){
return (function (_){
var self__ = this;
var ___$1 = this;
return self__.ch;
});})(cs))
;

cljs.core.async.t_cljs$core$async42121.prototype.cljs$core$async$Mult$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async42121.prototype.cljs$core$async$Mult$tap_STAR_$arity$3 = ((function (cs){
return (function (_,ch__$1,close_QMARK_){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.assoc,ch__$1,close_QMARK_);

return null;
});})(cs))
;

cljs.core.async.t_cljs$core$async42121.prototype.cljs$core$async$Mult$untap_STAR_$arity$2 = ((function (cs){
return (function (_,ch__$1){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.dissoc,ch__$1);

return null;
});})(cs))
;

cljs.core.async.t_cljs$core$async42121.prototype.cljs$core$async$Mult$untap_all_STAR_$arity$1 = ((function (cs){
return (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_.call(null,self__.cs,cljs.core.PersistentArrayMap.EMPTY);

return null;
});})(cs))
;

cljs.core.async.t_cljs$core$async42121.getBasis = ((function (cs){
return (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"cs","cs",-117024463,null),new cljs.core.Symbol(null,"meta42122","meta42122",-1520173393,null)], null);
});})(cs))
;

cljs.core.async.t_cljs$core$async42121.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async42121.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async42121";

cljs.core.async.t_cljs$core$async42121.cljs$lang$ctorPrWriter = ((function (cs){
return (function (this__30853__auto__,writer__30854__auto__,opt__30855__auto__){
return cljs.core._write.call(null,writer__30854__auto__,"cljs.core.async/t_cljs$core$async42121");
});})(cs))
;

cljs.core.async.__GT_t_cljs$core$async42121 = ((function (cs){
return (function cljs$core$async$mult_$___GT_t_cljs$core$async42121(ch__$1,cs__$1,meta42122){
return (new cljs.core.async.t_cljs$core$async42121(ch__$1,cs__$1,meta42122));
});})(cs))
;

}

return (new cljs.core.async.t_cljs$core$async42121(ch,cs,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var dchan = cljs.core.async.chan.call(null,(1));
var dctr = cljs.core.atom.call(null,null);
var done = ((function (cs,m,dchan,dctr){
return (function (_){
if((cljs.core.swap_BANG_.call(null,dctr,cljs.core.dec) === (0))){
return cljs.core.async.put_BANG_.call(null,dchan,true);
} else {
return null;
}
});})(cs,m,dchan,dctr))
;
var c__35058__auto___42343 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__35058__auto___42343,cs,m,dchan,dctr,done){
return (function (){
var f__35059__auto__ = (function (){var switch__34968__auto__ = ((function (c__35058__auto___42343,cs,m,dchan,dctr,done){
return (function (state_42258){
var state_val_42259 = (state_42258[(1)]);
if((state_val_42259 === (7))){
var inst_42254 = (state_42258[(2)]);
var state_42258__$1 = state_42258;
var statearr_42260_42344 = state_42258__$1;
(statearr_42260_42344[(2)] = inst_42254);

(statearr_42260_42344[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42259 === (20))){
var inst_42157 = (state_42258[(7)]);
var inst_42169 = cljs.core.first.call(null,inst_42157);
var inst_42170 = cljs.core.nth.call(null,inst_42169,(0),null);
var inst_42171 = cljs.core.nth.call(null,inst_42169,(1),null);
var state_42258__$1 = (function (){var statearr_42261 = state_42258;
(statearr_42261[(8)] = inst_42170);

return statearr_42261;
})();
if(cljs.core.truth_(inst_42171)){
var statearr_42262_42345 = state_42258__$1;
(statearr_42262_42345[(1)] = (22));

} else {
var statearr_42263_42346 = state_42258__$1;
(statearr_42263_42346[(1)] = (23));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42259 === (27))){
var inst_42201 = (state_42258[(9)]);
var inst_42126 = (state_42258[(10)]);
var inst_42199 = (state_42258[(11)]);
var inst_42206 = (state_42258[(12)]);
var inst_42206__$1 = cljs.core._nth.call(null,inst_42199,inst_42201);
var inst_42207 = cljs.core.async.put_BANG_.call(null,inst_42206__$1,inst_42126,done);
var state_42258__$1 = (function (){var statearr_42264 = state_42258;
(statearr_42264[(12)] = inst_42206__$1);

return statearr_42264;
})();
if(cljs.core.truth_(inst_42207)){
var statearr_42265_42347 = state_42258__$1;
(statearr_42265_42347[(1)] = (30));

} else {
var statearr_42266_42348 = state_42258__$1;
(statearr_42266_42348[(1)] = (31));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42259 === (1))){
var state_42258__$1 = state_42258;
var statearr_42267_42349 = state_42258__$1;
(statearr_42267_42349[(2)] = null);

(statearr_42267_42349[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42259 === (24))){
var inst_42157 = (state_42258[(7)]);
var inst_42176 = (state_42258[(2)]);
var inst_42177 = cljs.core.next.call(null,inst_42157);
var inst_42135 = inst_42177;
var inst_42136 = null;
var inst_42137 = (0);
var inst_42138 = (0);
var state_42258__$1 = (function (){var statearr_42268 = state_42258;
(statearr_42268[(13)] = inst_42176);

(statearr_42268[(14)] = inst_42135);

(statearr_42268[(15)] = inst_42138);

(statearr_42268[(16)] = inst_42136);

(statearr_42268[(17)] = inst_42137);

return statearr_42268;
})();
var statearr_42269_42350 = state_42258__$1;
(statearr_42269_42350[(2)] = null);

(statearr_42269_42350[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42259 === (39))){
var state_42258__$1 = state_42258;
var statearr_42273_42351 = state_42258__$1;
(statearr_42273_42351[(2)] = null);

(statearr_42273_42351[(1)] = (41));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42259 === (4))){
var inst_42126 = (state_42258[(10)]);
var inst_42126__$1 = (state_42258[(2)]);
var inst_42127 = (inst_42126__$1 == null);
var state_42258__$1 = (function (){var statearr_42274 = state_42258;
(statearr_42274[(10)] = inst_42126__$1);

return statearr_42274;
})();
if(cljs.core.truth_(inst_42127)){
var statearr_42275_42352 = state_42258__$1;
(statearr_42275_42352[(1)] = (5));

} else {
var statearr_42276_42353 = state_42258__$1;
(statearr_42276_42353[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42259 === (15))){
var inst_42135 = (state_42258[(14)]);
var inst_42138 = (state_42258[(15)]);
var inst_42136 = (state_42258[(16)]);
var inst_42137 = (state_42258[(17)]);
var inst_42153 = (state_42258[(2)]);
var inst_42154 = (inst_42138 + (1));
var tmp42270 = inst_42135;
var tmp42271 = inst_42136;
var tmp42272 = inst_42137;
var inst_42135__$1 = tmp42270;
var inst_42136__$1 = tmp42271;
var inst_42137__$1 = tmp42272;
var inst_42138__$1 = inst_42154;
var state_42258__$1 = (function (){var statearr_42277 = state_42258;
(statearr_42277[(14)] = inst_42135__$1);

(statearr_42277[(15)] = inst_42138__$1);

(statearr_42277[(18)] = inst_42153);

(statearr_42277[(16)] = inst_42136__$1);

(statearr_42277[(17)] = inst_42137__$1);

return statearr_42277;
})();
var statearr_42278_42354 = state_42258__$1;
(statearr_42278_42354[(2)] = null);

(statearr_42278_42354[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42259 === (21))){
var inst_42180 = (state_42258[(2)]);
var state_42258__$1 = state_42258;
var statearr_42282_42355 = state_42258__$1;
(statearr_42282_42355[(2)] = inst_42180);

(statearr_42282_42355[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42259 === (31))){
var inst_42206 = (state_42258[(12)]);
var inst_42210 = done.call(null,null);
var inst_42211 = cljs.core.async.untap_STAR_.call(null,m,inst_42206);
var state_42258__$1 = (function (){var statearr_42283 = state_42258;
(statearr_42283[(19)] = inst_42210);

return statearr_42283;
})();
var statearr_42284_42356 = state_42258__$1;
(statearr_42284_42356[(2)] = inst_42211);

(statearr_42284_42356[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42259 === (32))){
var inst_42201 = (state_42258[(9)]);
var inst_42199 = (state_42258[(11)]);
var inst_42198 = (state_42258[(20)]);
var inst_42200 = (state_42258[(21)]);
var inst_42213 = (state_42258[(2)]);
var inst_42214 = (inst_42201 + (1));
var tmp42279 = inst_42199;
var tmp42280 = inst_42198;
var tmp42281 = inst_42200;
var inst_42198__$1 = tmp42280;
var inst_42199__$1 = tmp42279;
var inst_42200__$1 = tmp42281;
var inst_42201__$1 = inst_42214;
var state_42258__$1 = (function (){var statearr_42285 = state_42258;
(statearr_42285[(9)] = inst_42201__$1);

(statearr_42285[(11)] = inst_42199__$1);

(statearr_42285[(20)] = inst_42198__$1);

(statearr_42285[(22)] = inst_42213);

(statearr_42285[(21)] = inst_42200__$1);

return statearr_42285;
})();
var statearr_42286_42357 = state_42258__$1;
(statearr_42286_42357[(2)] = null);

(statearr_42286_42357[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42259 === (40))){
var inst_42226 = (state_42258[(23)]);
var inst_42230 = done.call(null,null);
var inst_42231 = cljs.core.async.untap_STAR_.call(null,m,inst_42226);
var state_42258__$1 = (function (){var statearr_42287 = state_42258;
(statearr_42287[(24)] = inst_42230);

return statearr_42287;
})();
var statearr_42288_42358 = state_42258__$1;
(statearr_42288_42358[(2)] = inst_42231);

(statearr_42288_42358[(1)] = (41));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42259 === (33))){
var inst_42217 = (state_42258[(25)]);
var inst_42219 = cljs.core.chunked_seq_QMARK_.call(null,inst_42217);
var state_42258__$1 = state_42258;
if(inst_42219){
var statearr_42289_42359 = state_42258__$1;
(statearr_42289_42359[(1)] = (36));

} else {
var statearr_42290_42360 = state_42258__$1;
(statearr_42290_42360[(1)] = (37));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42259 === (13))){
var inst_42147 = (state_42258[(26)]);
var inst_42150 = cljs.core.async.close_BANG_.call(null,inst_42147);
var state_42258__$1 = state_42258;
var statearr_42291_42361 = state_42258__$1;
(statearr_42291_42361[(2)] = inst_42150);

(statearr_42291_42361[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42259 === (22))){
var inst_42170 = (state_42258[(8)]);
var inst_42173 = cljs.core.async.close_BANG_.call(null,inst_42170);
var state_42258__$1 = state_42258;
var statearr_42292_42362 = state_42258__$1;
(statearr_42292_42362[(2)] = inst_42173);

(statearr_42292_42362[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42259 === (36))){
var inst_42217 = (state_42258[(25)]);
var inst_42221 = cljs.core.chunk_first.call(null,inst_42217);
var inst_42222 = cljs.core.chunk_rest.call(null,inst_42217);
var inst_42223 = cljs.core.count.call(null,inst_42221);
var inst_42198 = inst_42222;
var inst_42199 = inst_42221;
var inst_42200 = inst_42223;
var inst_42201 = (0);
var state_42258__$1 = (function (){var statearr_42293 = state_42258;
(statearr_42293[(9)] = inst_42201);

(statearr_42293[(11)] = inst_42199);

(statearr_42293[(20)] = inst_42198);

(statearr_42293[(21)] = inst_42200);

return statearr_42293;
})();
var statearr_42294_42363 = state_42258__$1;
(statearr_42294_42363[(2)] = null);

(statearr_42294_42363[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42259 === (41))){
var inst_42217 = (state_42258[(25)]);
var inst_42233 = (state_42258[(2)]);
var inst_42234 = cljs.core.next.call(null,inst_42217);
var inst_42198 = inst_42234;
var inst_42199 = null;
var inst_42200 = (0);
var inst_42201 = (0);
var state_42258__$1 = (function (){var statearr_42295 = state_42258;
(statearr_42295[(9)] = inst_42201);

(statearr_42295[(11)] = inst_42199);

(statearr_42295[(20)] = inst_42198);

(statearr_42295[(27)] = inst_42233);

(statearr_42295[(21)] = inst_42200);

return statearr_42295;
})();
var statearr_42296_42364 = state_42258__$1;
(statearr_42296_42364[(2)] = null);

(statearr_42296_42364[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42259 === (43))){
var state_42258__$1 = state_42258;
var statearr_42297_42365 = state_42258__$1;
(statearr_42297_42365[(2)] = null);

(statearr_42297_42365[(1)] = (44));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42259 === (29))){
var inst_42242 = (state_42258[(2)]);
var state_42258__$1 = state_42258;
var statearr_42298_42366 = state_42258__$1;
(statearr_42298_42366[(2)] = inst_42242);

(statearr_42298_42366[(1)] = (26));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42259 === (44))){
var inst_42251 = (state_42258[(2)]);
var state_42258__$1 = (function (){var statearr_42299 = state_42258;
(statearr_42299[(28)] = inst_42251);

return statearr_42299;
})();
var statearr_42300_42367 = state_42258__$1;
(statearr_42300_42367[(2)] = null);

(statearr_42300_42367[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42259 === (6))){
var inst_42190 = (state_42258[(29)]);
var inst_42189 = cljs.core.deref.call(null,cs);
var inst_42190__$1 = cljs.core.keys.call(null,inst_42189);
var inst_42191 = cljs.core.count.call(null,inst_42190__$1);
var inst_42192 = cljs.core.reset_BANG_.call(null,dctr,inst_42191);
var inst_42197 = cljs.core.seq.call(null,inst_42190__$1);
var inst_42198 = inst_42197;
var inst_42199 = null;
var inst_42200 = (0);
var inst_42201 = (0);
var state_42258__$1 = (function (){var statearr_42301 = state_42258;
(statearr_42301[(9)] = inst_42201);

(statearr_42301[(11)] = inst_42199);

(statearr_42301[(20)] = inst_42198);

(statearr_42301[(30)] = inst_42192);

(statearr_42301[(29)] = inst_42190__$1);

(statearr_42301[(21)] = inst_42200);

return statearr_42301;
})();
var statearr_42302_42368 = state_42258__$1;
(statearr_42302_42368[(2)] = null);

(statearr_42302_42368[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42259 === (28))){
var inst_42198 = (state_42258[(20)]);
var inst_42217 = (state_42258[(25)]);
var inst_42217__$1 = cljs.core.seq.call(null,inst_42198);
var state_42258__$1 = (function (){var statearr_42303 = state_42258;
(statearr_42303[(25)] = inst_42217__$1);

return statearr_42303;
})();
if(inst_42217__$1){
var statearr_42304_42369 = state_42258__$1;
(statearr_42304_42369[(1)] = (33));

} else {
var statearr_42305_42370 = state_42258__$1;
(statearr_42305_42370[(1)] = (34));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42259 === (25))){
var inst_42201 = (state_42258[(9)]);
var inst_42200 = (state_42258[(21)]);
var inst_42203 = (inst_42201 < inst_42200);
var inst_42204 = inst_42203;
var state_42258__$1 = state_42258;
if(cljs.core.truth_(inst_42204)){
var statearr_42306_42371 = state_42258__$1;
(statearr_42306_42371[(1)] = (27));

} else {
var statearr_42307_42372 = state_42258__$1;
(statearr_42307_42372[(1)] = (28));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42259 === (34))){
var state_42258__$1 = state_42258;
var statearr_42308_42373 = state_42258__$1;
(statearr_42308_42373[(2)] = null);

(statearr_42308_42373[(1)] = (35));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42259 === (17))){
var state_42258__$1 = state_42258;
var statearr_42309_42374 = state_42258__$1;
(statearr_42309_42374[(2)] = null);

(statearr_42309_42374[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42259 === (3))){
var inst_42256 = (state_42258[(2)]);
var state_42258__$1 = state_42258;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_42258__$1,inst_42256);
} else {
if((state_val_42259 === (12))){
var inst_42185 = (state_42258[(2)]);
var state_42258__$1 = state_42258;
var statearr_42310_42375 = state_42258__$1;
(statearr_42310_42375[(2)] = inst_42185);

(statearr_42310_42375[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42259 === (2))){
var state_42258__$1 = state_42258;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_42258__$1,(4),ch);
} else {
if((state_val_42259 === (23))){
var state_42258__$1 = state_42258;
var statearr_42311_42376 = state_42258__$1;
(statearr_42311_42376[(2)] = null);

(statearr_42311_42376[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42259 === (35))){
var inst_42240 = (state_42258[(2)]);
var state_42258__$1 = state_42258;
var statearr_42312_42377 = state_42258__$1;
(statearr_42312_42377[(2)] = inst_42240);

(statearr_42312_42377[(1)] = (29));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42259 === (19))){
var inst_42157 = (state_42258[(7)]);
var inst_42161 = cljs.core.chunk_first.call(null,inst_42157);
var inst_42162 = cljs.core.chunk_rest.call(null,inst_42157);
var inst_42163 = cljs.core.count.call(null,inst_42161);
var inst_42135 = inst_42162;
var inst_42136 = inst_42161;
var inst_42137 = inst_42163;
var inst_42138 = (0);
var state_42258__$1 = (function (){var statearr_42313 = state_42258;
(statearr_42313[(14)] = inst_42135);

(statearr_42313[(15)] = inst_42138);

(statearr_42313[(16)] = inst_42136);

(statearr_42313[(17)] = inst_42137);

return statearr_42313;
})();
var statearr_42314_42378 = state_42258__$1;
(statearr_42314_42378[(2)] = null);

(statearr_42314_42378[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42259 === (11))){
var inst_42135 = (state_42258[(14)]);
var inst_42157 = (state_42258[(7)]);
var inst_42157__$1 = cljs.core.seq.call(null,inst_42135);
var state_42258__$1 = (function (){var statearr_42315 = state_42258;
(statearr_42315[(7)] = inst_42157__$1);

return statearr_42315;
})();
if(inst_42157__$1){
var statearr_42316_42379 = state_42258__$1;
(statearr_42316_42379[(1)] = (16));

} else {
var statearr_42317_42380 = state_42258__$1;
(statearr_42317_42380[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42259 === (9))){
var inst_42187 = (state_42258[(2)]);
var state_42258__$1 = state_42258;
var statearr_42318_42381 = state_42258__$1;
(statearr_42318_42381[(2)] = inst_42187);

(statearr_42318_42381[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42259 === (5))){
var inst_42133 = cljs.core.deref.call(null,cs);
var inst_42134 = cljs.core.seq.call(null,inst_42133);
var inst_42135 = inst_42134;
var inst_42136 = null;
var inst_42137 = (0);
var inst_42138 = (0);
var state_42258__$1 = (function (){var statearr_42319 = state_42258;
(statearr_42319[(14)] = inst_42135);

(statearr_42319[(15)] = inst_42138);

(statearr_42319[(16)] = inst_42136);

(statearr_42319[(17)] = inst_42137);

return statearr_42319;
})();
var statearr_42320_42382 = state_42258__$1;
(statearr_42320_42382[(2)] = null);

(statearr_42320_42382[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42259 === (14))){
var state_42258__$1 = state_42258;
var statearr_42321_42383 = state_42258__$1;
(statearr_42321_42383[(2)] = null);

(statearr_42321_42383[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42259 === (45))){
var inst_42248 = (state_42258[(2)]);
var state_42258__$1 = state_42258;
var statearr_42322_42384 = state_42258__$1;
(statearr_42322_42384[(2)] = inst_42248);

(statearr_42322_42384[(1)] = (44));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42259 === (26))){
var inst_42190 = (state_42258[(29)]);
var inst_42244 = (state_42258[(2)]);
var inst_42245 = cljs.core.seq.call(null,inst_42190);
var state_42258__$1 = (function (){var statearr_42323 = state_42258;
(statearr_42323[(31)] = inst_42244);

return statearr_42323;
})();
if(inst_42245){
var statearr_42324_42385 = state_42258__$1;
(statearr_42324_42385[(1)] = (42));

} else {
var statearr_42325_42386 = state_42258__$1;
(statearr_42325_42386[(1)] = (43));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42259 === (16))){
var inst_42157 = (state_42258[(7)]);
var inst_42159 = cljs.core.chunked_seq_QMARK_.call(null,inst_42157);
var state_42258__$1 = state_42258;
if(inst_42159){
var statearr_42326_42387 = state_42258__$1;
(statearr_42326_42387[(1)] = (19));

} else {
var statearr_42327_42388 = state_42258__$1;
(statearr_42327_42388[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42259 === (38))){
var inst_42237 = (state_42258[(2)]);
var state_42258__$1 = state_42258;
var statearr_42328_42389 = state_42258__$1;
(statearr_42328_42389[(2)] = inst_42237);

(statearr_42328_42389[(1)] = (35));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42259 === (30))){
var state_42258__$1 = state_42258;
var statearr_42329_42390 = state_42258__$1;
(statearr_42329_42390[(2)] = null);

(statearr_42329_42390[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42259 === (10))){
var inst_42138 = (state_42258[(15)]);
var inst_42136 = (state_42258[(16)]);
var inst_42146 = cljs.core._nth.call(null,inst_42136,inst_42138);
var inst_42147 = cljs.core.nth.call(null,inst_42146,(0),null);
var inst_42148 = cljs.core.nth.call(null,inst_42146,(1),null);
var state_42258__$1 = (function (){var statearr_42330 = state_42258;
(statearr_42330[(26)] = inst_42147);

return statearr_42330;
})();
if(cljs.core.truth_(inst_42148)){
var statearr_42331_42391 = state_42258__$1;
(statearr_42331_42391[(1)] = (13));

} else {
var statearr_42332_42392 = state_42258__$1;
(statearr_42332_42392[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42259 === (18))){
var inst_42183 = (state_42258[(2)]);
var state_42258__$1 = state_42258;
var statearr_42333_42393 = state_42258__$1;
(statearr_42333_42393[(2)] = inst_42183);

(statearr_42333_42393[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42259 === (42))){
var state_42258__$1 = state_42258;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_42258__$1,(45),dchan);
} else {
if((state_val_42259 === (37))){
var inst_42126 = (state_42258[(10)]);
var inst_42217 = (state_42258[(25)]);
var inst_42226 = (state_42258[(23)]);
var inst_42226__$1 = cljs.core.first.call(null,inst_42217);
var inst_42227 = cljs.core.async.put_BANG_.call(null,inst_42226__$1,inst_42126,done);
var state_42258__$1 = (function (){var statearr_42334 = state_42258;
(statearr_42334[(23)] = inst_42226__$1);

return statearr_42334;
})();
if(cljs.core.truth_(inst_42227)){
var statearr_42335_42394 = state_42258__$1;
(statearr_42335_42394[(1)] = (39));

} else {
var statearr_42336_42395 = state_42258__$1;
(statearr_42336_42395[(1)] = (40));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42259 === (8))){
var inst_42138 = (state_42258[(15)]);
var inst_42137 = (state_42258[(17)]);
var inst_42140 = (inst_42138 < inst_42137);
var inst_42141 = inst_42140;
var state_42258__$1 = state_42258;
if(cljs.core.truth_(inst_42141)){
var statearr_42337_42396 = state_42258__$1;
(statearr_42337_42396[(1)] = (10));

} else {
var statearr_42338_42397 = state_42258__$1;
(statearr_42338_42397[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__35058__auto___42343,cs,m,dchan,dctr,done))
;
return ((function (switch__34968__auto__,c__35058__auto___42343,cs,m,dchan,dctr,done){
return (function() {
var cljs$core$async$mult_$_state_machine__34969__auto__ = null;
var cljs$core$async$mult_$_state_machine__34969__auto____0 = (function (){
var statearr_42339 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_42339[(0)] = cljs$core$async$mult_$_state_machine__34969__auto__);

(statearr_42339[(1)] = (1));

return statearr_42339;
});
var cljs$core$async$mult_$_state_machine__34969__auto____1 = (function (state_42258){
while(true){
var ret_value__34970__auto__ = (function (){try{while(true){
var result__34971__auto__ = switch__34968__auto__.call(null,state_42258);
if(cljs.core.keyword_identical_QMARK_.call(null,result__34971__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__34971__auto__;
}
break;
}
}catch (e42340){if((e42340 instanceof Object)){
var ex__34972__auto__ = e42340;
var statearr_42341_42398 = state_42258;
(statearr_42341_42398[(5)] = ex__34972__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_42258);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e42340;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__34970__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__42399 = state_42258;
state_42258 = G__42399;
continue;
} else {
return ret_value__34970__auto__;
}
break;
}
});
cljs$core$async$mult_$_state_machine__34969__auto__ = function(state_42258){
switch(arguments.length){
case 0:
return cljs$core$async$mult_$_state_machine__34969__auto____0.call(this);
case 1:
return cljs$core$async$mult_$_state_machine__34969__auto____1.call(this,state_42258);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$mult_$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mult_$_state_machine__34969__auto____0;
cljs$core$async$mult_$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mult_$_state_machine__34969__auto____1;
return cljs$core$async$mult_$_state_machine__34969__auto__;
})()
;})(switch__34968__auto__,c__35058__auto___42343,cs,m,dchan,dctr,done))
})();
var state__35060__auto__ = (function (){var statearr_42342 = f__35059__auto__.call(null);
(statearr_42342[(6)] = c__35058__auto___42343);

return statearr_42342;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__35060__auto__);
});})(c__35058__auto___42343,cs,m,dchan,dctr,done))
);


return m;
});
/**
 * Copies the mult source onto the supplied channel.
 * 
 *   By default the channel will be closed when the source closes,
 *   but can be determined by the close? parameter.
 */
cljs.core.async.tap = (function cljs$core$async$tap(var_args){
var G__42401 = arguments.length;
switch (G__42401) {
case 2:
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.tap.cljs$core$IFn$_invoke$arity$2 = (function (mult,ch){
return cljs.core.async.tap.call(null,mult,ch,true);
});

cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3 = (function (mult,ch,close_QMARK_){
cljs.core.async.tap_STAR_.call(null,mult,ch,close_QMARK_);

return ch;
});

cljs.core.async.tap.cljs$lang$maxFixedArity = 3;

/**
 * Disconnects a target channel from a mult
 */
cljs.core.async.untap = (function cljs$core$async$untap(mult,ch){
return cljs.core.async.untap_STAR_.call(null,mult,ch);
});
/**
 * Disconnects all target channels from a mult
 */
cljs.core.async.untap_all = (function cljs$core$async$untap_all(mult){
return cljs.core.async.untap_all_STAR_.call(null,mult);
});

/**
 * @interface
 */
cljs.core.async.Mix = function(){};

cljs.core.async.admix_STAR_ = (function cljs$core$async$admix_STAR_(m,ch){
if((!((m == null))) && (!((m.cljs$core$async$Mix$admix_STAR_$arity$2 == null)))){
return m.cljs$core$async$Mix$admix_STAR_$arity$2(m,ch);
} else {
var x__30915__auto__ = (((m == null))?null:m);
var m__30916__auto__ = (cljs.core.async.admix_STAR_[goog.typeOf(x__30915__auto__)]);
if(!((m__30916__auto__ == null))){
return m__30916__auto__.call(null,m,ch);
} else {
var m__30916__auto____$1 = (cljs.core.async.admix_STAR_["_"]);
if(!((m__30916__auto____$1 == null))){
return m__30916__auto____$1.call(null,m,ch);
} else {
throw cljs.core.missing_protocol.call(null,"Mix.admix*",m);
}
}
}
});

cljs.core.async.unmix_STAR_ = (function cljs$core$async$unmix_STAR_(m,ch){
if((!((m == null))) && (!((m.cljs$core$async$Mix$unmix_STAR_$arity$2 == null)))){
return m.cljs$core$async$Mix$unmix_STAR_$arity$2(m,ch);
} else {
var x__30915__auto__ = (((m == null))?null:m);
var m__30916__auto__ = (cljs.core.async.unmix_STAR_[goog.typeOf(x__30915__auto__)]);
if(!((m__30916__auto__ == null))){
return m__30916__auto__.call(null,m,ch);
} else {
var m__30916__auto____$1 = (cljs.core.async.unmix_STAR_["_"]);
if(!((m__30916__auto____$1 == null))){
return m__30916__auto____$1.call(null,m,ch);
} else {
throw cljs.core.missing_protocol.call(null,"Mix.unmix*",m);
}
}
}
});

cljs.core.async.unmix_all_STAR_ = (function cljs$core$async$unmix_all_STAR_(m){
if((!((m == null))) && (!((m.cljs$core$async$Mix$unmix_all_STAR_$arity$1 == null)))){
return m.cljs$core$async$Mix$unmix_all_STAR_$arity$1(m);
} else {
var x__30915__auto__ = (((m == null))?null:m);
var m__30916__auto__ = (cljs.core.async.unmix_all_STAR_[goog.typeOf(x__30915__auto__)]);
if(!((m__30916__auto__ == null))){
return m__30916__auto__.call(null,m);
} else {
var m__30916__auto____$1 = (cljs.core.async.unmix_all_STAR_["_"]);
if(!((m__30916__auto____$1 == null))){
return m__30916__auto____$1.call(null,m);
} else {
throw cljs.core.missing_protocol.call(null,"Mix.unmix-all*",m);
}
}
}
});

cljs.core.async.toggle_STAR_ = (function cljs$core$async$toggle_STAR_(m,state_map){
if((!((m == null))) && (!((m.cljs$core$async$Mix$toggle_STAR_$arity$2 == null)))){
return m.cljs$core$async$Mix$toggle_STAR_$arity$2(m,state_map);
} else {
var x__30915__auto__ = (((m == null))?null:m);
var m__30916__auto__ = (cljs.core.async.toggle_STAR_[goog.typeOf(x__30915__auto__)]);
if(!((m__30916__auto__ == null))){
return m__30916__auto__.call(null,m,state_map);
} else {
var m__30916__auto____$1 = (cljs.core.async.toggle_STAR_["_"]);
if(!((m__30916__auto____$1 == null))){
return m__30916__auto____$1.call(null,m,state_map);
} else {
throw cljs.core.missing_protocol.call(null,"Mix.toggle*",m);
}
}
}
});

cljs.core.async.solo_mode_STAR_ = (function cljs$core$async$solo_mode_STAR_(m,mode){
if((!((m == null))) && (!((m.cljs$core$async$Mix$solo_mode_STAR_$arity$2 == null)))){
return m.cljs$core$async$Mix$solo_mode_STAR_$arity$2(m,mode);
} else {
var x__30915__auto__ = (((m == null))?null:m);
var m__30916__auto__ = (cljs.core.async.solo_mode_STAR_[goog.typeOf(x__30915__auto__)]);
if(!((m__30916__auto__ == null))){
return m__30916__auto__.call(null,m,mode);
} else {
var m__30916__auto____$1 = (cljs.core.async.solo_mode_STAR_["_"]);
if(!((m__30916__auto____$1 == null))){
return m__30916__auto____$1.call(null,m,mode);
} else {
throw cljs.core.missing_protocol.call(null,"Mix.solo-mode*",m);
}
}
}
});

cljs.core.async.ioc_alts_BANG_ = (function cljs$core$async$ioc_alts_BANG_(var_args){
var args__31466__auto__ = [];
var len__31459__auto___42413 = arguments.length;
var i__31460__auto___42414 = (0);
while(true){
if((i__31460__auto___42414 < len__31459__auto___42413)){
args__31466__auto__.push((arguments[i__31460__auto___42414]));

var G__42415 = (i__31460__auto___42414 + (1));
i__31460__auto___42414 = G__42415;
continue;
} else {
}
break;
}

var argseq__31467__auto__ = ((((3) < args__31466__auto__.length))?(new cljs.core.IndexedSeq(args__31466__auto__.slice((3)),(0),null)):null);
return cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),argseq__31467__auto__);
});

cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (state,cont_block,ports,p__42407){
var map__42408 = p__42407;
var map__42408__$1 = ((((!((map__42408 == null)))?((((map__42408.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__42408.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__42408):map__42408);
var opts = map__42408__$1;
var statearr_42410_42416 = state;
(statearr_42410_42416[(1)] = cont_block);


var temp__5278__auto__ = cljs.core.async.do_alts.call(null,((function (map__42408,map__42408__$1,opts){
return (function (val){
var statearr_42411_42417 = state;
(statearr_42411_42417[(2)] = val);


return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state);
});})(map__42408,map__42408__$1,opts))
,ports,opts);
if(cljs.core.truth_(temp__5278__auto__)){
var cb = temp__5278__auto__;
var statearr_42412_42418 = state;
(statearr_42412_42418[(2)] = cljs.core.deref.call(null,cb));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
});

cljs.core.async.ioc_alts_BANG_.cljs$lang$maxFixedArity = (3);

cljs.core.async.ioc_alts_BANG_.cljs$lang$applyTo = (function (seq42403){
var G__42404 = cljs.core.first.call(null,seq42403);
var seq42403__$1 = cljs.core.next.call(null,seq42403);
var G__42405 = cljs.core.first.call(null,seq42403__$1);
var seq42403__$2 = cljs.core.next.call(null,seq42403__$1);
var G__42406 = cljs.core.first.call(null,seq42403__$2);
var seq42403__$3 = cljs.core.next.call(null,seq42403__$2);
return cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic(G__42404,G__42405,G__42406,seq42403__$3);
});

/**
 * Creates and returns a mix of one or more input channels which will
 *   be put on the supplied out channel. Input sources can be added to
 *   the mix with 'admix', and removed with 'unmix'. A mix supports
 *   soloing, muting and pausing multiple inputs atomically using
 *   'toggle', and can solo using either muting or pausing as determined
 *   by 'solo-mode'.
 * 
 *   Each channel can have zero or more boolean modes set via 'toggle':
 * 
 *   :solo - when true, only this (ond other soloed) channel(s) will appear
 *        in the mix output channel. :mute and :pause states of soloed
 *        channels are ignored. If solo-mode is :mute, non-soloed
 *        channels are muted, if :pause, non-soloed channels are
 *        paused.
 * 
 *   :mute - muted channels will have their contents consumed but not included in the mix
 *   :pause - paused channels will not have their contents consumed (and thus also not included in the mix)
 */
cljs.core.async.mix = (function cljs$core$async$mix(out){
var cs = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var solo_modes = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"pause","pause",-2095325672),null,new cljs.core.Keyword(null,"mute","mute",1151223646),null], null), null);
var attrs = cljs.core.conj.call(null,solo_modes,new cljs.core.Keyword(null,"solo","solo",-316350075));
var solo_mode = cljs.core.atom.call(null,new cljs.core.Keyword(null,"mute","mute",1151223646));
var change = cljs.core.async.chan.call(null);
var changed = ((function (cs,solo_modes,attrs,solo_mode,change){
return (function (){
return cljs.core.async.put_BANG_.call(null,change,true);
});})(cs,solo_modes,attrs,solo_mode,change))
;
var pick = ((function (cs,solo_modes,attrs,solo_mode,change,changed){
return (function (attr,chs){
return cljs.core.reduce_kv.call(null,((function (cs,solo_modes,attrs,solo_mode,change,changed){
return (function (ret,c,v){
if(cljs.core.truth_(attr.call(null,v))){
return cljs.core.conj.call(null,ret,c);
} else {
return ret;
}
});})(cs,solo_modes,attrs,solo_mode,change,changed))
,cljs.core.PersistentHashSet.EMPTY,chs);
});})(cs,solo_modes,attrs,solo_mode,change,changed))
;
var calc_state = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick){
return (function (){
var chs = cljs.core.deref.call(null,cs);
var mode = cljs.core.deref.call(null,solo_mode);
var solos = pick.call(null,new cljs.core.Keyword(null,"solo","solo",-316350075),chs);
var pauses = pick.call(null,new cljs.core.Keyword(null,"pause","pause",-2095325672),chs);
return new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"solos","solos",1441458643),solos,new cljs.core.Keyword(null,"mutes","mutes",1068806309),pick.call(null,new cljs.core.Keyword(null,"mute","mute",1151223646),chs),new cljs.core.Keyword(null,"reads","reads",-1215067361),cljs.core.conj.call(null,(((cljs.core._EQ_.call(null,mode,new cljs.core.Keyword(null,"pause","pause",-2095325672))) && (!(cljs.core.empty_QMARK_.call(null,solos))))?cljs.core.vec.call(null,solos):cljs.core.vec.call(null,cljs.core.remove.call(null,pauses,cljs.core.keys.call(null,chs)))),change)], null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick))
;
var m = (function (){
if(typeof cljs.core.async.t_cljs$core$async42419 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mix}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async42419 = (function (out,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,meta42420){
this.out = out;
this.cs = cs;
this.solo_modes = solo_modes;
this.attrs = attrs;
this.solo_mode = solo_mode;
this.change = change;
this.changed = changed;
this.pick = pick;
this.calc_state = calc_state;
this.meta42420 = meta42420;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async42419.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_42421,meta42420__$1){
var self__ = this;
var _42421__$1 = this;
return (new cljs.core.async.t_cljs$core$async42419(self__.out,self__.cs,self__.solo_modes,self__.attrs,self__.solo_mode,self__.change,self__.changed,self__.pick,self__.calc_state,meta42420__$1));
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async42419.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_42421){
var self__ = this;
var _42421__$1 = this;
return self__.meta42420;
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async42419.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async42419.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_){
var self__ = this;
var ___$1 = this;
return self__.out;
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async42419.prototype.cljs$core$async$Mix$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async42419.prototype.cljs$core$async$Mix$admix_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,ch){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.assoc,ch,cljs.core.PersistentArrayMap.EMPTY);

return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async42419.prototype.cljs$core$async$Mix$unmix_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,ch){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.dissoc,ch);

return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async42419.prototype.cljs$core$async$Mix$unmix_all_STAR_$arity$1 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_.call(null,self__.cs,cljs.core.PersistentArrayMap.EMPTY);

return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async42419.prototype.cljs$core$async$Mix$toggle_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,state_map){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.partial.call(null,cljs.core.merge_with,cljs.core.merge),state_map);

return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async42419.prototype.cljs$core$async$Mix$solo_mode_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,mode){
var self__ = this;
var ___$1 = this;
if(cljs.core.truth_(self__.solo_modes.call(null,mode))){
} else {
throw (new Error(["Assert failed: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(["mode must be one of: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(self__.solo_modes)].join('')),"\n","(solo-modes mode)"].join('')));
}

cljs.core.reset_BANG_.call(null,self__.solo_mode,mode);

return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async42419.getBasis = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (){
return new cljs.core.PersistentVector(null, 10, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"out","out",729986010,null),new cljs.core.Symbol(null,"cs","cs",-117024463,null),new cljs.core.Symbol(null,"solo-modes","solo-modes",882180540,null),new cljs.core.Symbol(null,"attrs","attrs",-450137186,null),new cljs.core.Symbol(null,"solo-mode","solo-mode",2031788074,null),new cljs.core.Symbol(null,"change","change",477485025,null),new cljs.core.Symbol(null,"changed","changed",-2083710852,null),new cljs.core.Symbol(null,"pick","pick",1300068175,null),new cljs.core.Symbol(null,"calc-state","calc-state",-349968968,null),new cljs.core.Symbol(null,"meta42420","meta42420",-2075113279,null)], null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async42419.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async42419.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async42419";

cljs.core.async.t_cljs$core$async42419.cljs$lang$ctorPrWriter = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (this__30853__auto__,writer__30854__auto__,opt__30855__auto__){
return cljs.core._write.call(null,writer__30854__auto__,"cljs.core.async/t_cljs$core$async42419");
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.__GT_t_cljs$core$async42419 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function cljs$core$async$mix_$___GT_t_cljs$core$async42419(out__$1,cs__$1,solo_modes__$1,attrs__$1,solo_mode__$1,change__$1,changed__$1,pick__$1,calc_state__$1,meta42420){
return (new cljs.core.async.t_cljs$core$async42419(out__$1,cs__$1,solo_modes__$1,attrs__$1,solo_mode__$1,change__$1,changed__$1,pick__$1,calc_state__$1,meta42420));
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

}

return (new cljs.core.async.t_cljs$core$async42419(out,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var c__35058__auto___42583 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__35058__auto___42583,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m){
return (function (){
var f__35059__auto__ = (function (){var switch__34968__auto__ = ((function (c__35058__auto___42583,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m){
return (function (state_42523){
var state_val_42524 = (state_42523[(1)]);
if((state_val_42524 === (7))){
var inst_42438 = (state_42523[(2)]);
var state_42523__$1 = state_42523;
var statearr_42525_42584 = state_42523__$1;
(statearr_42525_42584[(2)] = inst_42438);

(statearr_42525_42584[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42524 === (20))){
var inst_42450 = (state_42523[(7)]);
var state_42523__$1 = state_42523;
var statearr_42526_42585 = state_42523__$1;
(statearr_42526_42585[(2)] = inst_42450);

(statearr_42526_42585[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42524 === (27))){
var state_42523__$1 = state_42523;
var statearr_42527_42586 = state_42523__$1;
(statearr_42527_42586[(2)] = null);

(statearr_42527_42586[(1)] = (28));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42524 === (1))){
var inst_42425 = (state_42523[(8)]);
var inst_42425__$1 = calc_state.call(null);
var inst_42427 = (inst_42425__$1 == null);
var inst_42428 = cljs.core.not.call(null,inst_42427);
var state_42523__$1 = (function (){var statearr_42528 = state_42523;
(statearr_42528[(8)] = inst_42425__$1);

return statearr_42528;
})();
if(inst_42428){
var statearr_42529_42587 = state_42523__$1;
(statearr_42529_42587[(1)] = (2));

} else {
var statearr_42530_42588 = state_42523__$1;
(statearr_42530_42588[(1)] = (3));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42524 === (24))){
var inst_42474 = (state_42523[(9)]);
var inst_42497 = (state_42523[(10)]);
var inst_42483 = (state_42523[(11)]);
var inst_42497__$1 = inst_42474.call(null,inst_42483);
var state_42523__$1 = (function (){var statearr_42531 = state_42523;
(statearr_42531[(10)] = inst_42497__$1);

return statearr_42531;
})();
if(cljs.core.truth_(inst_42497__$1)){
var statearr_42532_42589 = state_42523__$1;
(statearr_42532_42589[(1)] = (29));

} else {
var statearr_42533_42590 = state_42523__$1;
(statearr_42533_42590[(1)] = (30));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42524 === (4))){
var inst_42441 = (state_42523[(2)]);
var state_42523__$1 = state_42523;
if(cljs.core.truth_(inst_42441)){
var statearr_42534_42591 = state_42523__$1;
(statearr_42534_42591[(1)] = (8));

} else {
var statearr_42535_42592 = state_42523__$1;
(statearr_42535_42592[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42524 === (15))){
var inst_42468 = (state_42523[(2)]);
var state_42523__$1 = state_42523;
if(cljs.core.truth_(inst_42468)){
var statearr_42536_42593 = state_42523__$1;
(statearr_42536_42593[(1)] = (19));

} else {
var statearr_42537_42594 = state_42523__$1;
(statearr_42537_42594[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42524 === (21))){
var inst_42473 = (state_42523[(12)]);
var inst_42473__$1 = (state_42523[(2)]);
var inst_42474 = cljs.core.get.call(null,inst_42473__$1,new cljs.core.Keyword(null,"solos","solos",1441458643));
var inst_42475 = cljs.core.get.call(null,inst_42473__$1,new cljs.core.Keyword(null,"mutes","mutes",1068806309));
var inst_42476 = cljs.core.get.call(null,inst_42473__$1,new cljs.core.Keyword(null,"reads","reads",-1215067361));
var state_42523__$1 = (function (){var statearr_42538 = state_42523;
(statearr_42538[(9)] = inst_42474);

(statearr_42538[(12)] = inst_42473__$1);

(statearr_42538[(13)] = inst_42475);

return statearr_42538;
})();
return cljs.core.async.ioc_alts_BANG_.call(null,state_42523__$1,(22),inst_42476);
} else {
if((state_val_42524 === (31))){
var inst_42505 = (state_42523[(2)]);
var state_42523__$1 = state_42523;
if(cljs.core.truth_(inst_42505)){
var statearr_42539_42595 = state_42523__$1;
(statearr_42539_42595[(1)] = (32));

} else {
var statearr_42540_42596 = state_42523__$1;
(statearr_42540_42596[(1)] = (33));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42524 === (32))){
var inst_42482 = (state_42523[(14)]);
var state_42523__$1 = state_42523;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_42523__$1,(35),out,inst_42482);
} else {
if((state_val_42524 === (33))){
var inst_42473 = (state_42523[(12)]);
var inst_42450 = inst_42473;
var state_42523__$1 = (function (){var statearr_42541 = state_42523;
(statearr_42541[(7)] = inst_42450);

return statearr_42541;
})();
var statearr_42542_42597 = state_42523__$1;
(statearr_42542_42597[(2)] = null);

(statearr_42542_42597[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42524 === (13))){
var inst_42450 = (state_42523[(7)]);
var inst_42457 = inst_42450.cljs$lang$protocol_mask$partition0$;
var inst_42458 = (inst_42457 & (64));
var inst_42459 = inst_42450.cljs$core$ISeq$;
var inst_42460 = (cljs.core.PROTOCOL_SENTINEL === inst_42459);
var inst_42461 = (inst_42458) || (inst_42460);
var state_42523__$1 = state_42523;
if(cljs.core.truth_(inst_42461)){
var statearr_42543_42598 = state_42523__$1;
(statearr_42543_42598[(1)] = (16));

} else {
var statearr_42544_42599 = state_42523__$1;
(statearr_42544_42599[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42524 === (22))){
var inst_42482 = (state_42523[(14)]);
var inst_42483 = (state_42523[(11)]);
var inst_42481 = (state_42523[(2)]);
var inst_42482__$1 = cljs.core.nth.call(null,inst_42481,(0),null);
var inst_42483__$1 = cljs.core.nth.call(null,inst_42481,(1),null);
var inst_42484 = (inst_42482__$1 == null);
var inst_42485 = cljs.core._EQ_.call(null,inst_42483__$1,change);
var inst_42486 = (inst_42484) || (inst_42485);
var state_42523__$1 = (function (){var statearr_42545 = state_42523;
(statearr_42545[(14)] = inst_42482__$1);

(statearr_42545[(11)] = inst_42483__$1);

return statearr_42545;
})();
if(cljs.core.truth_(inst_42486)){
var statearr_42546_42600 = state_42523__$1;
(statearr_42546_42600[(1)] = (23));

} else {
var statearr_42547_42601 = state_42523__$1;
(statearr_42547_42601[(1)] = (24));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42524 === (36))){
var inst_42473 = (state_42523[(12)]);
var inst_42450 = inst_42473;
var state_42523__$1 = (function (){var statearr_42548 = state_42523;
(statearr_42548[(7)] = inst_42450);

return statearr_42548;
})();
var statearr_42549_42602 = state_42523__$1;
(statearr_42549_42602[(2)] = null);

(statearr_42549_42602[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42524 === (29))){
var inst_42497 = (state_42523[(10)]);
var state_42523__$1 = state_42523;
var statearr_42550_42603 = state_42523__$1;
(statearr_42550_42603[(2)] = inst_42497);

(statearr_42550_42603[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42524 === (6))){
var state_42523__$1 = state_42523;
var statearr_42551_42604 = state_42523__$1;
(statearr_42551_42604[(2)] = false);

(statearr_42551_42604[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42524 === (28))){
var inst_42493 = (state_42523[(2)]);
var inst_42494 = calc_state.call(null);
var inst_42450 = inst_42494;
var state_42523__$1 = (function (){var statearr_42552 = state_42523;
(statearr_42552[(7)] = inst_42450);

(statearr_42552[(15)] = inst_42493);

return statearr_42552;
})();
var statearr_42553_42605 = state_42523__$1;
(statearr_42553_42605[(2)] = null);

(statearr_42553_42605[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42524 === (25))){
var inst_42519 = (state_42523[(2)]);
var state_42523__$1 = state_42523;
var statearr_42554_42606 = state_42523__$1;
(statearr_42554_42606[(2)] = inst_42519);

(statearr_42554_42606[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42524 === (34))){
var inst_42517 = (state_42523[(2)]);
var state_42523__$1 = state_42523;
var statearr_42555_42607 = state_42523__$1;
(statearr_42555_42607[(2)] = inst_42517);

(statearr_42555_42607[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42524 === (17))){
var state_42523__$1 = state_42523;
var statearr_42556_42608 = state_42523__$1;
(statearr_42556_42608[(2)] = false);

(statearr_42556_42608[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42524 === (3))){
var state_42523__$1 = state_42523;
var statearr_42557_42609 = state_42523__$1;
(statearr_42557_42609[(2)] = false);

(statearr_42557_42609[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42524 === (12))){
var inst_42521 = (state_42523[(2)]);
var state_42523__$1 = state_42523;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_42523__$1,inst_42521);
} else {
if((state_val_42524 === (2))){
var inst_42425 = (state_42523[(8)]);
var inst_42430 = inst_42425.cljs$lang$protocol_mask$partition0$;
var inst_42431 = (inst_42430 & (64));
var inst_42432 = inst_42425.cljs$core$ISeq$;
var inst_42433 = (cljs.core.PROTOCOL_SENTINEL === inst_42432);
var inst_42434 = (inst_42431) || (inst_42433);
var state_42523__$1 = state_42523;
if(cljs.core.truth_(inst_42434)){
var statearr_42558_42610 = state_42523__$1;
(statearr_42558_42610[(1)] = (5));

} else {
var statearr_42559_42611 = state_42523__$1;
(statearr_42559_42611[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42524 === (23))){
var inst_42482 = (state_42523[(14)]);
var inst_42488 = (inst_42482 == null);
var state_42523__$1 = state_42523;
if(cljs.core.truth_(inst_42488)){
var statearr_42560_42612 = state_42523__$1;
(statearr_42560_42612[(1)] = (26));

} else {
var statearr_42561_42613 = state_42523__$1;
(statearr_42561_42613[(1)] = (27));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42524 === (35))){
var inst_42508 = (state_42523[(2)]);
var state_42523__$1 = state_42523;
if(cljs.core.truth_(inst_42508)){
var statearr_42562_42614 = state_42523__$1;
(statearr_42562_42614[(1)] = (36));

} else {
var statearr_42563_42615 = state_42523__$1;
(statearr_42563_42615[(1)] = (37));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42524 === (19))){
var inst_42450 = (state_42523[(7)]);
var inst_42470 = cljs.core.apply.call(null,cljs.core.hash_map,inst_42450);
var state_42523__$1 = state_42523;
var statearr_42564_42616 = state_42523__$1;
(statearr_42564_42616[(2)] = inst_42470);

(statearr_42564_42616[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42524 === (11))){
var inst_42450 = (state_42523[(7)]);
var inst_42454 = (inst_42450 == null);
var inst_42455 = cljs.core.not.call(null,inst_42454);
var state_42523__$1 = state_42523;
if(inst_42455){
var statearr_42565_42617 = state_42523__$1;
(statearr_42565_42617[(1)] = (13));

} else {
var statearr_42566_42618 = state_42523__$1;
(statearr_42566_42618[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42524 === (9))){
var inst_42425 = (state_42523[(8)]);
var state_42523__$1 = state_42523;
var statearr_42567_42619 = state_42523__$1;
(statearr_42567_42619[(2)] = inst_42425);

(statearr_42567_42619[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42524 === (5))){
var state_42523__$1 = state_42523;
var statearr_42568_42620 = state_42523__$1;
(statearr_42568_42620[(2)] = true);

(statearr_42568_42620[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42524 === (14))){
var state_42523__$1 = state_42523;
var statearr_42569_42621 = state_42523__$1;
(statearr_42569_42621[(2)] = false);

(statearr_42569_42621[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42524 === (26))){
var inst_42483 = (state_42523[(11)]);
var inst_42490 = cljs.core.swap_BANG_.call(null,cs,cljs.core.dissoc,inst_42483);
var state_42523__$1 = state_42523;
var statearr_42570_42622 = state_42523__$1;
(statearr_42570_42622[(2)] = inst_42490);

(statearr_42570_42622[(1)] = (28));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42524 === (16))){
var state_42523__$1 = state_42523;
var statearr_42571_42623 = state_42523__$1;
(statearr_42571_42623[(2)] = true);

(statearr_42571_42623[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42524 === (38))){
var inst_42513 = (state_42523[(2)]);
var state_42523__$1 = state_42523;
var statearr_42572_42624 = state_42523__$1;
(statearr_42572_42624[(2)] = inst_42513);

(statearr_42572_42624[(1)] = (34));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42524 === (30))){
var inst_42474 = (state_42523[(9)]);
var inst_42475 = (state_42523[(13)]);
var inst_42483 = (state_42523[(11)]);
var inst_42500 = cljs.core.empty_QMARK_.call(null,inst_42474);
var inst_42501 = inst_42475.call(null,inst_42483);
var inst_42502 = cljs.core.not.call(null,inst_42501);
var inst_42503 = (inst_42500) && (inst_42502);
var state_42523__$1 = state_42523;
var statearr_42573_42625 = state_42523__$1;
(statearr_42573_42625[(2)] = inst_42503);

(statearr_42573_42625[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42524 === (10))){
var inst_42425 = (state_42523[(8)]);
var inst_42446 = (state_42523[(2)]);
var inst_42447 = cljs.core.get.call(null,inst_42446,new cljs.core.Keyword(null,"solos","solos",1441458643));
var inst_42448 = cljs.core.get.call(null,inst_42446,new cljs.core.Keyword(null,"mutes","mutes",1068806309));
var inst_42449 = cljs.core.get.call(null,inst_42446,new cljs.core.Keyword(null,"reads","reads",-1215067361));
var inst_42450 = inst_42425;
var state_42523__$1 = (function (){var statearr_42574 = state_42523;
(statearr_42574[(7)] = inst_42450);

(statearr_42574[(16)] = inst_42449);

(statearr_42574[(17)] = inst_42447);

(statearr_42574[(18)] = inst_42448);

return statearr_42574;
})();
var statearr_42575_42626 = state_42523__$1;
(statearr_42575_42626[(2)] = null);

(statearr_42575_42626[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42524 === (18))){
var inst_42465 = (state_42523[(2)]);
var state_42523__$1 = state_42523;
var statearr_42576_42627 = state_42523__$1;
(statearr_42576_42627[(2)] = inst_42465);

(statearr_42576_42627[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42524 === (37))){
var state_42523__$1 = state_42523;
var statearr_42577_42628 = state_42523__$1;
(statearr_42577_42628[(2)] = null);

(statearr_42577_42628[(1)] = (38));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42524 === (8))){
var inst_42425 = (state_42523[(8)]);
var inst_42443 = cljs.core.apply.call(null,cljs.core.hash_map,inst_42425);
var state_42523__$1 = state_42523;
var statearr_42578_42629 = state_42523__$1;
(statearr_42578_42629[(2)] = inst_42443);

(statearr_42578_42629[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__35058__auto___42583,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m))
;
return ((function (switch__34968__auto__,c__35058__auto___42583,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m){
return (function() {
var cljs$core$async$mix_$_state_machine__34969__auto__ = null;
var cljs$core$async$mix_$_state_machine__34969__auto____0 = (function (){
var statearr_42579 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_42579[(0)] = cljs$core$async$mix_$_state_machine__34969__auto__);

(statearr_42579[(1)] = (1));

return statearr_42579;
});
var cljs$core$async$mix_$_state_machine__34969__auto____1 = (function (state_42523){
while(true){
var ret_value__34970__auto__ = (function (){try{while(true){
var result__34971__auto__ = switch__34968__auto__.call(null,state_42523);
if(cljs.core.keyword_identical_QMARK_.call(null,result__34971__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__34971__auto__;
}
break;
}
}catch (e42580){if((e42580 instanceof Object)){
var ex__34972__auto__ = e42580;
var statearr_42581_42630 = state_42523;
(statearr_42581_42630[(5)] = ex__34972__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_42523);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e42580;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__34970__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__42631 = state_42523;
state_42523 = G__42631;
continue;
} else {
return ret_value__34970__auto__;
}
break;
}
});
cljs$core$async$mix_$_state_machine__34969__auto__ = function(state_42523){
switch(arguments.length){
case 0:
return cljs$core$async$mix_$_state_machine__34969__auto____0.call(this);
case 1:
return cljs$core$async$mix_$_state_machine__34969__auto____1.call(this,state_42523);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$mix_$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mix_$_state_machine__34969__auto____0;
cljs$core$async$mix_$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mix_$_state_machine__34969__auto____1;
return cljs$core$async$mix_$_state_machine__34969__auto__;
})()
;})(switch__34968__auto__,c__35058__auto___42583,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m))
})();
var state__35060__auto__ = (function (){var statearr_42582 = f__35059__auto__.call(null);
(statearr_42582[(6)] = c__35058__auto___42583);

return statearr_42582;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__35060__auto__);
});})(c__35058__auto___42583,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m))
);


return m;
});
/**
 * Adds ch as an input to the mix
 */
cljs.core.async.admix = (function cljs$core$async$admix(mix,ch){
return cljs.core.async.admix_STAR_.call(null,mix,ch);
});
/**
 * Removes ch as an input to the mix
 */
cljs.core.async.unmix = (function cljs$core$async$unmix(mix,ch){
return cljs.core.async.unmix_STAR_.call(null,mix,ch);
});
/**
 * removes all inputs from the mix
 */
cljs.core.async.unmix_all = (function cljs$core$async$unmix_all(mix){
return cljs.core.async.unmix_all_STAR_.call(null,mix);
});
/**
 * Atomically sets the state(s) of one or more channels in a mix. The
 *   state map is a map of channels -> channel-state-map. A
 *   channel-state-map is a map of attrs -> boolean, where attr is one or
 *   more of :mute, :pause or :solo. Any states supplied are merged with
 *   the current state.
 * 
 *   Note that channels can be added to a mix via toggle, which can be
 *   used to add channels in a particular (e.g. paused) state.
 */
cljs.core.async.toggle = (function cljs$core$async$toggle(mix,state_map){
return cljs.core.async.toggle_STAR_.call(null,mix,state_map);
});
/**
 * Sets the solo mode of the mix. mode must be one of :mute or :pause
 */
cljs.core.async.solo_mode = (function cljs$core$async$solo_mode(mix,mode){
return cljs.core.async.solo_mode_STAR_.call(null,mix,mode);
});

/**
 * @interface
 */
cljs.core.async.Pub = function(){};

cljs.core.async.sub_STAR_ = (function cljs$core$async$sub_STAR_(p,v,ch,close_QMARK_){
if((!((p == null))) && (!((p.cljs$core$async$Pub$sub_STAR_$arity$4 == null)))){
return p.cljs$core$async$Pub$sub_STAR_$arity$4(p,v,ch,close_QMARK_);
} else {
var x__30915__auto__ = (((p == null))?null:p);
var m__30916__auto__ = (cljs.core.async.sub_STAR_[goog.typeOf(x__30915__auto__)]);
if(!((m__30916__auto__ == null))){
return m__30916__auto__.call(null,p,v,ch,close_QMARK_);
} else {
var m__30916__auto____$1 = (cljs.core.async.sub_STAR_["_"]);
if(!((m__30916__auto____$1 == null))){
return m__30916__auto____$1.call(null,p,v,ch,close_QMARK_);
} else {
throw cljs.core.missing_protocol.call(null,"Pub.sub*",p);
}
}
}
});

cljs.core.async.unsub_STAR_ = (function cljs$core$async$unsub_STAR_(p,v,ch){
if((!((p == null))) && (!((p.cljs$core$async$Pub$unsub_STAR_$arity$3 == null)))){
return p.cljs$core$async$Pub$unsub_STAR_$arity$3(p,v,ch);
} else {
var x__30915__auto__ = (((p == null))?null:p);
var m__30916__auto__ = (cljs.core.async.unsub_STAR_[goog.typeOf(x__30915__auto__)]);
if(!((m__30916__auto__ == null))){
return m__30916__auto__.call(null,p,v,ch);
} else {
var m__30916__auto____$1 = (cljs.core.async.unsub_STAR_["_"]);
if(!((m__30916__auto____$1 == null))){
return m__30916__auto____$1.call(null,p,v,ch);
} else {
throw cljs.core.missing_protocol.call(null,"Pub.unsub*",p);
}
}
}
});

cljs.core.async.unsub_all_STAR_ = (function cljs$core$async$unsub_all_STAR_(var_args){
var G__42633 = arguments.length;
switch (G__42633) {
case 1:
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1 = (function (p){
if((!((p == null))) && (!((p.cljs$core$async$Pub$unsub_all_STAR_$arity$1 == null)))){
return p.cljs$core$async$Pub$unsub_all_STAR_$arity$1(p);
} else {
var x__30915__auto__ = (((p == null))?null:p);
var m__30916__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__30915__auto__)]);
if(!((m__30916__auto__ == null))){
return m__30916__auto__.call(null,p);
} else {
var m__30916__auto____$1 = (cljs.core.async.unsub_all_STAR_["_"]);
if(!((m__30916__auto____$1 == null))){
return m__30916__auto____$1.call(null,p);
} else {
throw cljs.core.missing_protocol.call(null,"Pub.unsub-all*",p);
}
}
}
});

cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2 = (function (p,v){
if((!((p == null))) && (!((p.cljs$core$async$Pub$unsub_all_STAR_$arity$2 == null)))){
return p.cljs$core$async$Pub$unsub_all_STAR_$arity$2(p,v);
} else {
var x__30915__auto__ = (((p == null))?null:p);
var m__30916__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__30915__auto__)]);
if(!((m__30916__auto__ == null))){
return m__30916__auto__.call(null,p,v);
} else {
var m__30916__auto____$1 = (cljs.core.async.unsub_all_STAR_["_"]);
if(!((m__30916__auto____$1 == null))){
return m__30916__auto____$1.call(null,p,v);
} else {
throw cljs.core.missing_protocol.call(null,"Pub.unsub-all*",p);
}
}
}
});

cljs.core.async.unsub_all_STAR_.cljs$lang$maxFixedArity = 2;


/**
 * Creates and returns a pub(lication) of the supplied channel,
 *   partitioned into topics by the topic-fn. topic-fn will be applied to
 *   each value on the channel and the result will determine the 'topic'
 *   on which that value will be put. Channels can be subscribed to
 *   receive copies of topics using 'sub', and unsubscribed using
 *   'unsub'. Each topic will be handled by an internal mult on a
 *   dedicated channel. By default these internal channels are
 *   unbuffered, but a buf-fn can be supplied which, given a topic,
 *   creates a buffer with desired properties.
 * 
 *   Each item is distributed to all subs in parallel and synchronously,
 *   i.e. each sub must accept before the next item is distributed. Use
 *   buffering/windowing to prevent slow subs from holding up the pub.
 * 
 *   Items received when there are no matching subs get dropped.
 * 
 *   Note that if buf-fns are used then each topic is handled
 *   asynchronously, i.e. if a channel is subscribed to more than one
 *   topic it should not expect them to be interleaved identically with
 *   the source.
 */
cljs.core.async.pub = (function cljs$core$async$pub(var_args){
var G__42637 = arguments.length;
switch (G__42637) {
case 2:
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.pub.cljs$core$IFn$_invoke$arity$2 = (function (ch,topic_fn){
return cljs.core.async.pub.call(null,ch,topic_fn,cljs.core.constantly.call(null,null));
});

cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3 = (function (ch,topic_fn,buf_fn){
var mults = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var ensure_mult = ((function (mults){
return (function (topic){
var or__30182__auto__ = cljs.core.get.call(null,cljs.core.deref.call(null,mults),topic);
if(cljs.core.truth_(or__30182__auto__)){
return or__30182__auto__;
} else {
return cljs.core.get.call(null,cljs.core.swap_BANG_.call(null,mults,((function (or__30182__auto__,mults){
return (function (p1__42635_SHARP_){
if(cljs.core.truth_(p1__42635_SHARP_.call(null,topic))){
return p1__42635_SHARP_;
} else {
return cljs.core.assoc.call(null,p1__42635_SHARP_,topic,cljs.core.async.mult.call(null,cljs.core.async.chan.call(null,buf_fn.call(null,topic))));
}
});})(or__30182__auto__,mults))
),topic);
}
});})(mults))
;
var p = (function (){
if(typeof cljs.core.async.t_cljs$core$async42638 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.Pub}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async42638 = (function (ch,topic_fn,buf_fn,mults,ensure_mult,meta42639){
this.ch = ch;
this.topic_fn = topic_fn;
this.buf_fn = buf_fn;
this.mults = mults;
this.ensure_mult = ensure_mult;
this.meta42639 = meta42639;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async42638.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (mults,ensure_mult){
return (function (_42640,meta42639__$1){
var self__ = this;
var _42640__$1 = this;
return (new cljs.core.async.t_cljs$core$async42638(self__.ch,self__.topic_fn,self__.buf_fn,self__.mults,self__.ensure_mult,meta42639__$1));
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async42638.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (mults,ensure_mult){
return (function (_42640){
var self__ = this;
var _42640__$1 = this;
return self__.meta42639;
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async42638.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async42638.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = ((function (mults,ensure_mult){
return (function (_){
var self__ = this;
var ___$1 = this;
return self__.ch;
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async42638.prototype.cljs$core$async$Pub$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async42638.prototype.cljs$core$async$Pub$sub_STAR_$arity$4 = ((function (mults,ensure_mult){
return (function (p,topic,ch__$1,close_QMARK_){
var self__ = this;
var p__$1 = this;
var m = self__.ensure_mult.call(null,topic);
return cljs.core.async.tap.call(null,m,ch__$1,close_QMARK_);
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async42638.prototype.cljs$core$async$Pub$unsub_STAR_$arity$3 = ((function (mults,ensure_mult){
return (function (p,topic,ch__$1){
var self__ = this;
var p__$1 = this;
var temp__5278__auto__ = cljs.core.get.call(null,cljs.core.deref.call(null,self__.mults),topic);
if(cljs.core.truth_(temp__5278__auto__)){
var m = temp__5278__auto__;
return cljs.core.async.untap.call(null,m,ch__$1);
} else {
return null;
}
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async42638.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$1 = ((function (mults,ensure_mult){
return (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.reset_BANG_.call(null,self__.mults,cljs.core.PersistentArrayMap.EMPTY);
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async42638.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$2 = ((function (mults,ensure_mult){
return (function (_,topic){
var self__ = this;
var ___$1 = this;
return cljs.core.swap_BANG_.call(null,self__.mults,cljs.core.dissoc,topic);
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async42638.getBasis = ((function (mults,ensure_mult){
return (function (){
return new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"topic-fn","topic-fn",-862449736,null),new cljs.core.Symbol(null,"buf-fn","buf-fn",-1200281591,null),new cljs.core.Symbol(null,"mults","mults",-461114485,null),new cljs.core.Symbol(null,"ensure-mult","ensure-mult",1796584816,null),new cljs.core.Symbol(null,"meta42639","meta42639",228281109,null)], null);
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async42638.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async42638.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async42638";

cljs.core.async.t_cljs$core$async42638.cljs$lang$ctorPrWriter = ((function (mults,ensure_mult){
return (function (this__30853__auto__,writer__30854__auto__,opt__30855__auto__){
return cljs.core._write.call(null,writer__30854__auto__,"cljs.core.async/t_cljs$core$async42638");
});})(mults,ensure_mult))
;

cljs.core.async.__GT_t_cljs$core$async42638 = ((function (mults,ensure_mult){
return (function cljs$core$async$__GT_t_cljs$core$async42638(ch__$1,topic_fn__$1,buf_fn__$1,mults__$1,ensure_mult__$1,meta42639){
return (new cljs.core.async.t_cljs$core$async42638(ch__$1,topic_fn__$1,buf_fn__$1,mults__$1,ensure_mult__$1,meta42639));
});})(mults,ensure_mult))
;

}

return (new cljs.core.async.t_cljs$core$async42638(ch,topic_fn,buf_fn,mults,ensure_mult,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var c__35058__auto___42758 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__35058__auto___42758,mults,ensure_mult,p){
return (function (){
var f__35059__auto__ = (function (){var switch__34968__auto__ = ((function (c__35058__auto___42758,mults,ensure_mult,p){
return (function (state_42712){
var state_val_42713 = (state_42712[(1)]);
if((state_val_42713 === (7))){
var inst_42708 = (state_42712[(2)]);
var state_42712__$1 = state_42712;
var statearr_42714_42759 = state_42712__$1;
(statearr_42714_42759[(2)] = inst_42708);

(statearr_42714_42759[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42713 === (20))){
var state_42712__$1 = state_42712;
var statearr_42715_42760 = state_42712__$1;
(statearr_42715_42760[(2)] = null);

(statearr_42715_42760[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42713 === (1))){
var state_42712__$1 = state_42712;
var statearr_42716_42761 = state_42712__$1;
(statearr_42716_42761[(2)] = null);

(statearr_42716_42761[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42713 === (24))){
var inst_42691 = (state_42712[(7)]);
var inst_42700 = cljs.core.swap_BANG_.call(null,mults,cljs.core.dissoc,inst_42691);
var state_42712__$1 = state_42712;
var statearr_42717_42762 = state_42712__$1;
(statearr_42717_42762[(2)] = inst_42700);

(statearr_42717_42762[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42713 === (4))){
var inst_42643 = (state_42712[(8)]);
var inst_42643__$1 = (state_42712[(2)]);
var inst_42644 = (inst_42643__$1 == null);
var state_42712__$1 = (function (){var statearr_42718 = state_42712;
(statearr_42718[(8)] = inst_42643__$1);

return statearr_42718;
})();
if(cljs.core.truth_(inst_42644)){
var statearr_42719_42763 = state_42712__$1;
(statearr_42719_42763[(1)] = (5));

} else {
var statearr_42720_42764 = state_42712__$1;
(statearr_42720_42764[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42713 === (15))){
var inst_42685 = (state_42712[(2)]);
var state_42712__$1 = state_42712;
var statearr_42721_42765 = state_42712__$1;
(statearr_42721_42765[(2)] = inst_42685);

(statearr_42721_42765[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42713 === (21))){
var inst_42705 = (state_42712[(2)]);
var state_42712__$1 = (function (){var statearr_42722 = state_42712;
(statearr_42722[(9)] = inst_42705);

return statearr_42722;
})();
var statearr_42723_42766 = state_42712__$1;
(statearr_42723_42766[(2)] = null);

(statearr_42723_42766[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42713 === (13))){
var inst_42667 = (state_42712[(10)]);
var inst_42669 = cljs.core.chunked_seq_QMARK_.call(null,inst_42667);
var state_42712__$1 = state_42712;
if(inst_42669){
var statearr_42724_42767 = state_42712__$1;
(statearr_42724_42767[(1)] = (16));

} else {
var statearr_42725_42768 = state_42712__$1;
(statearr_42725_42768[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42713 === (22))){
var inst_42697 = (state_42712[(2)]);
var state_42712__$1 = state_42712;
if(cljs.core.truth_(inst_42697)){
var statearr_42726_42769 = state_42712__$1;
(statearr_42726_42769[(1)] = (23));

} else {
var statearr_42727_42770 = state_42712__$1;
(statearr_42727_42770[(1)] = (24));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42713 === (6))){
var inst_42691 = (state_42712[(7)]);
var inst_42643 = (state_42712[(8)]);
var inst_42693 = (state_42712[(11)]);
var inst_42691__$1 = topic_fn.call(null,inst_42643);
var inst_42692 = cljs.core.deref.call(null,mults);
var inst_42693__$1 = cljs.core.get.call(null,inst_42692,inst_42691__$1);
var state_42712__$1 = (function (){var statearr_42728 = state_42712;
(statearr_42728[(7)] = inst_42691__$1);

(statearr_42728[(11)] = inst_42693__$1);

return statearr_42728;
})();
if(cljs.core.truth_(inst_42693__$1)){
var statearr_42729_42771 = state_42712__$1;
(statearr_42729_42771[(1)] = (19));

} else {
var statearr_42730_42772 = state_42712__$1;
(statearr_42730_42772[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42713 === (25))){
var inst_42702 = (state_42712[(2)]);
var state_42712__$1 = state_42712;
var statearr_42731_42773 = state_42712__$1;
(statearr_42731_42773[(2)] = inst_42702);

(statearr_42731_42773[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42713 === (17))){
var inst_42667 = (state_42712[(10)]);
var inst_42676 = cljs.core.first.call(null,inst_42667);
var inst_42677 = cljs.core.async.muxch_STAR_.call(null,inst_42676);
var inst_42678 = cljs.core.async.close_BANG_.call(null,inst_42677);
var inst_42679 = cljs.core.next.call(null,inst_42667);
var inst_42653 = inst_42679;
var inst_42654 = null;
var inst_42655 = (0);
var inst_42656 = (0);
var state_42712__$1 = (function (){var statearr_42732 = state_42712;
(statearr_42732[(12)] = inst_42656);

(statearr_42732[(13)] = inst_42654);

(statearr_42732[(14)] = inst_42655);

(statearr_42732[(15)] = inst_42653);

(statearr_42732[(16)] = inst_42678);

return statearr_42732;
})();
var statearr_42733_42774 = state_42712__$1;
(statearr_42733_42774[(2)] = null);

(statearr_42733_42774[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42713 === (3))){
var inst_42710 = (state_42712[(2)]);
var state_42712__$1 = state_42712;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_42712__$1,inst_42710);
} else {
if((state_val_42713 === (12))){
var inst_42687 = (state_42712[(2)]);
var state_42712__$1 = state_42712;
var statearr_42734_42775 = state_42712__$1;
(statearr_42734_42775[(2)] = inst_42687);

(statearr_42734_42775[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42713 === (2))){
var state_42712__$1 = state_42712;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_42712__$1,(4),ch);
} else {
if((state_val_42713 === (23))){
var state_42712__$1 = state_42712;
var statearr_42735_42776 = state_42712__$1;
(statearr_42735_42776[(2)] = null);

(statearr_42735_42776[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42713 === (19))){
var inst_42643 = (state_42712[(8)]);
var inst_42693 = (state_42712[(11)]);
var inst_42695 = cljs.core.async.muxch_STAR_.call(null,inst_42693);
var state_42712__$1 = state_42712;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_42712__$1,(22),inst_42695,inst_42643);
} else {
if((state_val_42713 === (11))){
var inst_42653 = (state_42712[(15)]);
var inst_42667 = (state_42712[(10)]);
var inst_42667__$1 = cljs.core.seq.call(null,inst_42653);
var state_42712__$1 = (function (){var statearr_42736 = state_42712;
(statearr_42736[(10)] = inst_42667__$1);

return statearr_42736;
})();
if(inst_42667__$1){
var statearr_42737_42777 = state_42712__$1;
(statearr_42737_42777[(1)] = (13));

} else {
var statearr_42738_42778 = state_42712__$1;
(statearr_42738_42778[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42713 === (9))){
var inst_42689 = (state_42712[(2)]);
var state_42712__$1 = state_42712;
var statearr_42739_42779 = state_42712__$1;
(statearr_42739_42779[(2)] = inst_42689);

(statearr_42739_42779[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42713 === (5))){
var inst_42650 = cljs.core.deref.call(null,mults);
var inst_42651 = cljs.core.vals.call(null,inst_42650);
var inst_42652 = cljs.core.seq.call(null,inst_42651);
var inst_42653 = inst_42652;
var inst_42654 = null;
var inst_42655 = (0);
var inst_42656 = (0);
var state_42712__$1 = (function (){var statearr_42740 = state_42712;
(statearr_42740[(12)] = inst_42656);

(statearr_42740[(13)] = inst_42654);

(statearr_42740[(14)] = inst_42655);

(statearr_42740[(15)] = inst_42653);

return statearr_42740;
})();
var statearr_42741_42780 = state_42712__$1;
(statearr_42741_42780[(2)] = null);

(statearr_42741_42780[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42713 === (14))){
var state_42712__$1 = state_42712;
var statearr_42745_42781 = state_42712__$1;
(statearr_42745_42781[(2)] = null);

(statearr_42745_42781[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42713 === (16))){
var inst_42667 = (state_42712[(10)]);
var inst_42671 = cljs.core.chunk_first.call(null,inst_42667);
var inst_42672 = cljs.core.chunk_rest.call(null,inst_42667);
var inst_42673 = cljs.core.count.call(null,inst_42671);
var inst_42653 = inst_42672;
var inst_42654 = inst_42671;
var inst_42655 = inst_42673;
var inst_42656 = (0);
var state_42712__$1 = (function (){var statearr_42746 = state_42712;
(statearr_42746[(12)] = inst_42656);

(statearr_42746[(13)] = inst_42654);

(statearr_42746[(14)] = inst_42655);

(statearr_42746[(15)] = inst_42653);

return statearr_42746;
})();
var statearr_42747_42782 = state_42712__$1;
(statearr_42747_42782[(2)] = null);

(statearr_42747_42782[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42713 === (10))){
var inst_42656 = (state_42712[(12)]);
var inst_42654 = (state_42712[(13)]);
var inst_42655 = (state_42712[(14)]);
var inst_42653 = (state_42712[(15)]);
var inst_42661 = cljs.core._nth.call(null,inst_42654,inst_42656);
var inst_42662 = cljs.core.async.muxch_STAR_.call(null,inst_42661);
var inst_42663 = cljs.core.async.close_BANG_.call(null,inst_42662);
var inst_42664 = (inst_42656 + (1));
var tmp42742 = inst_42654;
var tmp42743 = inst_42655;
var tmp42744 = inst_42653;
var inst_42653__$1 = tmp42744;
var inst_42654__$1 = tmp42742;
var inst_42655__$1 = tmp42743;
var inst_42656__$1 = inst_42664;
var state_42712__$1 = (function (){var statearr_42748 = state_42712;
(statearr_42748[(12)] = inst_42656__$1);

(statearr_42748[(13)] = inst_42654__$1);

(statearr_42748[(17)] = inst_42663);

(statearr_42748[(14)] = inst_42655__$1);

(statearr_42748[(15)] = inst_42653__$1);

return statearr_42748;
})();
var statearr_42749_42783 = state_42712__$1;
(statearr_42749_42783[(2)] = null);

(statearr_42749_42783[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42713 === (18))){
var inst_42682 = (state_42712[(2)]);
var state_42712__$1 = state_42712;
var statearr_42750_42784 = state_42712__$1;
(statearr_42750_42784[(2)] = inst_42682);

(statearr_42750_42784[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42713 === (8))){
var inst_42656 = (state_42712[(12)]);
var inst_42655 = (state_42712[(14)]);
var inst_42658 = (inst_42656 < inst_42655);
var inst_42659 = inst_42658;
var state_42712__$1 = state_42712;
if(cljs.core.truth_(inst_42659)){
var statearr_42751_42785 = state_42712__$1;
(statearr_42751_42785[(1)] = (10));

} else {
var statearr_42752_42786 = state_42712__$1;
(statearr_42752_42786[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__35058__auto___42758,mults,ensure_mult,p))
;
return ((function (switch__34968__auto__,c__35058__auto___42758,mults,ensure_mult,p){
return (function() {
var cljs$core$async$state_machine__34969__auto__ = null;
var cljs$core$async$state_machine__34969__auto____0 = (function (){
var statearr_42753 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_42753[(0)] = cljs$core$async$state_machine__34969__auto__);

(statearr_42753[(1)] = (1));

return statearr_42753;
});
var cljs$core$async$state_machine__34969__auto____1 = (function (state_42712){
while(true){
var ret_value__34970__auto__ = (function (){try{while(true){
var result__34971__auto__ = switch__34968__auto__.call(null,state_42712);
if(cljs.core.keyword_identical_QMARK_.call(null,result__34971__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__34971__auto__;
}
break;
}
}catch (e42754){if((e42754 instanceof Object)){
var ex__34972__auto__ = e42754;
var statearr_42755_42787 = state_42712;
(statearr_42755_42787[(5)] = ex__34972__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_42712);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e42754;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__34970__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__42788 = state_42712;
state_42712 = G__42788;
continue;
} else {
return ret_value__34970__auto__;
}
break;
}
});
cljs$core$async$state_machine__34969__auto__ = function(state_42712){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__34969__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__34969__auto____1.call(this,state_42712);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__34969__auto____0;
cljs$core$async$state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__34969__auto____1;
return cljs$core$async$state_machine__34969__auto__;
})()
;})(switch__34968__auto__,c__35058__auto___42758,mults,ensure_mult,p))
})();
var state__35060__auto__ = (function (){var statearr_42756 = f__35059__auto__.call(null);
(statearr_42756[(6)] = c__35058__auto___42758);

return statearr_42756;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__35060__auto__);
});})(c__35058__auto___42758,mults,ensure_mult,p))
);


return p;
});

cljs.core.async.pub.cljs$lang$maxFixedArity = 3;

/**
 * Subscribes a channel to a topic of a pub.
 * 
 *   By default the channel will be closed when the source closes,
 *   but can be determined by the close? parameter.
 */
cljs.core.async.sub = (function cljs$core$async$sub(var_args){
var G__42790 = arguments.length;
switch (G__42790) {
case 3:
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.sub.cljs$core$IFn$_invoke$arity$3 = (function (p,topic,ch){
return cljs.core.async.sub.call(null,p,topic,ch,true);
});

cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4 = (function (p,topic,ch,close_QMARK_){
return cljs.core.async.sub_STAR_.call(null,p,topic,ch,close_QMARK_);
});

cljs.core.async.sub.cljs$lang$maxFixedArity = 4;

/**
 * Unsubscribes a channel from a topic of a pub
 */
cljs.core.async.unsub = (function cljs$core$async$unsub(p,topic,ch){
return cljs.core.async.unsub_STAR_.call(null,p,topic,ch);
});
/**
 * Unsubscribes all channels from a pub, or a topic of a pub
 */
cljs.core.async.unsub_all = (function cljs$core$async$unsub_all(var_args){
var G__42793 = arguments.length;
switch (G__42793) {
case 1:
return cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$1 = (function (p){
return cljs.core.async.unsub_all_STAR_.call(null,p);
});

cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$2 = (function (p,topic){
return cljs.core.async.unsub_all_STAR_.call(null,p,topic);
});

cljs.core.async.unsub_all.cljs$lang$maxFixedArity = 2;

/**
 * Takes a function and a collection of source channels, and returns a
 *   channel which contains the values produced by applying f to the set
 *   of first items taken from each source channel, followed by applying
 *   f to the set of second items from each channel, until any one of the
 *   channels is closed, at which point the output channel will be
 *   closed. The returned channel will be unbuffered by default, or a
 *   buf-or-n can be supplied
 */
cljs.core.async.map = (function cljs$core$async$map(var_args){
var G__42796 = arguments.length;
switch (G__42796) {
case 2:
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.map.cljs$core$IFn$_invoke$arity$2 = (function (f,chs){
return cljs.core.async.map.call(null,f,chs,null);
});

cljs.core.async.map.cljs$core$IFn$_invoke$arity$3 = (function (f,chs,buf_or_n){
var chs__$1 = cljs.core.vec.call(null,chs);
var out = cljs.core.async.chan.call(null,buf_or_n);
var cnt = cljs.core.count.call(null,chs__$1);
var rets = cljs.core.object_array.call(null,cnt);
var dchan = cljs.core.async.chan.call(null,(1));
var dctr = cljs.core.atom.call(null,null);
var done = cljs.core.mapv.call(null,((function (chs__$1,out,cnt,rets,dchan,dctr){
return (function (i){
return ((function (chs__$1,out,cnt,rets,dchan,dctr){
return (function (ret){
(rets[i] = ret);

if((cljs.core.swap_BANG_.call(null,dctr,cljs.core.dec) === (0))){
return cljs.core.async.put_BANG_.call(null,dchan,rets.slice((0)));
} else {
return null;
}
});
;})(chs__$1,out,cnt,rets,dchan,dctr))
});})(chs__$1,out,cnt,rets,dchan,dctr))
,cljs.core.range.call(null,cnt));
var c__35058__auto___42863 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__35058__auto___42863,chs__$1,out,cnt,rets,dchan,dctr,done){
return (function (){
var f__35059__auto__ = (function (){var switch__34968__auto__ = ((function (c__35058__auto___42863,chs__$1,out,cnt,rets,dchan,dctr,done){
return (function (state_42835){
var state_val_42836 = (state_42835[(1)]);
if((state_val_42836 === (7))){
var state_42835__$1 = state_42835;
var statearr_42837_42864 = state_42835__$1;
(statearr_42837_42864[(2)] = null);

(statearr_42837_42864[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42836 === (1))){
var state_42835__$1 = state_42835;
var statearr_42838_42865 = state_42835__$1;
(statearr_42838_42865[(2)] = null);

(statearr_42838_42865[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42836 === (4))){
var inst_42799 = (state_42835[(7)]);
var inst_42801 = (inst_42799 < cnt);
var state_42835__$1 = state_42835;
if(cljs.core.truth_(inst_42801)){
var statearr_42839_42866 = state_42835__$1;
(statearr_42839_42866[(1)] = (6));

} else {
var statearr_42840_42867 = state_42835__$1;
(statearr_42840_42867[(1)] = (7));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42836 === (15))){
var inst_42831 = (state_42835[(2)]);
var state_42835__$1 = state_42835;
var statearr_42841_42868 = state_42835__$1;
(statearr_42841_42868[(2)] = inst_42831);

(statearr_42841_42868[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42836 === (13))){
var inst_42824 = cljs.core.async.close_BANG_.call(null,out);
var state_42835__$1 = state_42835;
var statearr_42842_42869 = state_42835__$1;
(statearr_42842_42869[(2)] = inst_42824);

(statearr_42842_42869[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42836 === (6))){
var state_42835__$1 = state_42835;
var statearr_42843_42870 = state_42835__$1;
(statearr_42843_42870[(2)] = null);

(statearr_42843_42870[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42836 === (3))){
var inst_42833 = (state_42835[(2)]);
var state_42835__$1 = state_42835;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_42835__$1,inst_42833);
} else {
if((state_val_42836 === (12))){
var inst_42821 = (state_42835[(8)]);
var inst_42821__$1 = (state_42835[(2)]);
var inst_42822 = cljs.core.some.call(null,cljs.core.nil_QMARK_,inst_42821__$1);
var state_42835__$1 = (function (){var statearr_42844 = state_42835;
(statearr_42844[(8)] = inst_42821__$1);

return statearr_42844;
})();
if(cljs.core.truth_(inst_42822)){
var statearr_42845_42871 = state_42835__$1;
(statearr_42845_42871[(1)] = (13));

} else {
var statearr_42846_42872 = state_42835__$1;
(statearr_42846_42872[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42836 === (2))){
var inst_42798 = cljs.core.reset_BANG_.call(null,dctr,cnt);
var inst_42799 = (0);
var state_42835__$1 = (function (){var statearr_42847 = state_42835;
(statearr_42847[(7)] = inst_42799);

(statearr_42847[(9)] = inst_42798);

return statearr_42847;
})();
var statearr_42848_42873 = state_42835__$1;
(statearr_42848_42873[(2)] = null);

(statearr_42848_42873[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42836 === (11))){
var inst_42799 = (state_42835[(7)]);
var _ = cljs.core.async.impl.ioc_helpers.add_exception_frame.call(null,state_42835,(10),Object,null,(9));
var inst_42808 = chs__$1.call(null,inst_42799);
var inst_42809 = done.call(null,inst_42799);
var inst_42810 = cljs.core.async.take_BANG_.call(null,inst_42808,inst_42809);
var state_42835__$1 = state_42835;
var statearr_42849_42874 = state_42835__$1;
(statearr_42849_42874[(2)] = inst_42810);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_42835__$1);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42836 === (9))){
var inst_42799 = (state_42835[(7)]);
var inst_42812 = (state_42835[(2)]);
var inst_42813 = (inst_42799 + (1));
var inst_42799__$1 = inst_42813;
var state_42835__$1 = (function (){var statearr_42850 = state_42835;
(statearr_42850[(7)] = inst_42799__$1);

(statearr_42850[(10)] = inst_42812);

return statearr_42850;
})();
var statearr_42851_42875 = state_42835__$1;
(statearr_42851_42875[(2)] = null);

(statearr_42851_42875[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42836 === (5))){
var inst_42819 = (state_42835[(2)]);
var state_42835__$1 = (function (){var statearr_42852 = state_42835;
(statearr_42852[(11)] = inst_42819);

return statearr_42852;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_42835__$1,(12),dchan);
} else {
if((state_val_42836 === (14))){
var inst_42821 = (state_42835[(8)]);
var inst_42826 = cljs.core.apply.call(null,f,inst_42821);
var state_42835__$1 = state_42835;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_42835__$1,(16),out,inst_42826);
} else {
if((state_val_42836 === (16))){
var inst_42828 = (state_42835[(2)]);
var state_42835__$1 = (function (){var statearr_42853 = state_42835;
(statearr_42853[(12)] = inst_42828);

return statearr_42853;
})();
var statearr_42854_42876 = state_42835__$1;
(statearr_42854_42876[(2)] = null);

(statearr_42854_42876[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42836 === (10))){
var inst_42803 = (state_42835[(2)]);
var inst_42804 = cljs.core.swap_BANG_.call(null,dctr,cljs.core.dec);
var state_42835__$1 = (function (){var statearr_42855 = state_42835;
(statearr_42855[(13)] = inst_42803);

return statearr_42855;
})();
var statearr_42856_42877 = state_42835__$1;
(statearr_42856_42877[(2)] = inst_42804);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_42835__$1);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42836 === (8))){
var inst_42817 = (state_42835[(2)]);
var state_42835__$1 = state_42835;
var statearr_42857_42878 = state_42835__$1;
(statearr_42857_42878[(2)] = inst_42817);

(statearr_42857_42878[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__35058__auto___42863,chs__$1,out,cnt,rets,dchan,dctr,done))
;
return ((function (switch__34968__auto__,c__35058__auto___42863,chs__$1,out,cnt,rets,dchan,dctr,done){
return (function() {
var cljs$core$async$state_machine__34969__auto__ = null;
var cljs$core$async$state_machine__34969__auto____0 = (function (){
var statearr_42858 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_42858[(0)] = cljs$core$async$state_machine__34969__auto__);

(statearr_42858[(1)] = (1));

return statearr_42858;
});
var cljs$core$async$state_machine__34969__auto____1 = (function (state_42835){
while(true){
var ret_value__34970__auto__ = (function (){try{while(true){
var result__34971__auto__ = switch__34968__auto__.call(null,state_42835);
if(cljs.core.keyword_identical_QMARK_.call(null,result__34971__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__34971__auto__;
}
break;
}
}catch (e42859){if((e42859 instanceof Object)){
var ex__34972__auto__ = e42859;
var statearr_42860_42879 = state_42835;
(statearr_42860_42879[(5)] = ex__34972__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_42835);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e42859;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__34970__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__42880 = state_42835;
state_42835 = G__42880;
continue;
} else {
return ret_value__34970__auto__;
}
break;
}
});
cljs$core$async$state_machine__34969__auto__ = function(state_42835){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__34969__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__34969__auto____1.call(this,state_42835);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__34969__auto____0;
cljs$core$async$state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__34969__auto____1;
return cljs$core$async$state_machine__34969__auto__;
})()
;})(switch__34968__auto__,c__35058__auto___42863,chs__$1,out,cnt,rets,dchan,dctr,done))
})();
var state__35060__auto__ = (function (){var statearr_42861 = f__35059__auto__.call(null);
(statearr_42861[(6)] = c__35058__auto___42863);

return statearr_42861;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__35060__auto__);
});})(c__35058__auto___42863,chs__$1,out,cnt,rets,dchan,dctr,done))
);


return out;
});

cljs.core.async.map.cljs$lang$maxFixedArity = 3;

/**
 * Takes a collection of source channels and returns a channel which
 *   contains all values taken from them. The returned channel will be
 *   unbuffered by default, or a buf-or-n can be supplied. The channel
 *   will close after all the source channels have closed.
 */
cljs.core.async.merge = (function cljs$core$async$merge(var_args){
var G__42883 = arguments.length;
switch (G__42883) {
case 1:
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.merge.cljs$core$IFn$_invoke$arity$1 = (function (chs){
return cljs.core.async.merge.call(null,chs,null);
});

cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2 = (function (chs,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__35058__auto___42937 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__35058__auto___42937,out){
return (function (){
var f__35059__auto__ = (function (){var switch__34968__auto__ = ((function (c__35058__auto___42937,out){
return (function (state_42915){
var state_val_42916 = (state_42915[(1)]);
if((state_val_42916 === (7))){
var inst_42895 = (state_42915[(7)]);
var inst_42894 = (state_42915[(8)]);
var inst_42894__$1 = (state_42915[(2)]);
var inst_42895__$1 = cljs.core.nth.call(null,inst_42894__$1,(0),null);
var inst_42896 = cljs.core.nth.call(null,inst_42894__$1,(1),null);
var inst_42897 = (inst_42895__$1 == null);
var state_42915__$1 = (function (){var statearr_42917 = state_42915;
(statearr_42917[(9)] = inst_42896);

(statearr_42917[(7)] = inst_42895__$1);

(statearr_42917[(8)] = inst_42894__$1);

return statearr_42917;
})();
if(cljs.core.truth_(inst_42897)){
var statearr_42918_42938 = state_42915__$1;
(statearr_42918_42938[(1)] = (8));

} else {
var statearr_42919_42939 = state_42915__$1;
(statearr_42919_42939[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42916 === (1))){
var inst_42884 = cljs.core.vec.call(null,chs);
var inst_42885 = inst_42884;
var state_42915__$1 = (function (){var statearr_42920 = state_42915;
(statearr_42920[(10)] = inst_42885);

return statearr_42920;
})();
var statearr_42921_42940 = state_42915__$1;
(statearr_42921_42940[(2)] = null);

(statearr_42921_42940[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42916 === (4))){
var inst_42885 = (state_42915[(10)]);
var state_42915__$1 = state_42915;
return cljs.core.async.ioc_alts_BANG_.call(null,state_42915__$1,(7),inst_42885);
} else {
if((state_val_42916 === (6))){
var inst_42911 = (state_42915[(2)]);
var state_42915__$1 = state_42915;
var statearr_42922_42941 = state_42915__$1;
(statearr_42922_42941[(2)] = inst_42911);

(statearr_42922_42941[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42916 === (3))){
var inst_42913 = (state_42915[(2)]);
var state_42915__$1 = state_42915;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_42915__$1,inst_42913);
} else {
if((state_val_42916 === (2))){
var inst_42885 = (state_42915[(10)]);
var inst_42887 = cljs.core.count.call(null,inst_42885);
var inst_42888 = (inst_42887 > (0));
var state_42915__$1 = state_42915;
if(cljs.core.truth_(inst_42888)){
var statearr_42924_42942 = state_42915__$1;
(statearr_42924_42942[(1)] = (4));

} else {
var statearr_42925_42943 = state_42915__$1;
(statearr_42925_42943[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42916 === (11))){
var inst_42885 = (state_42915[(10)]);
var inst_42904 = (state_42915[(2)]);
var tmp42923 = inst_42885;
var inst_42885__$1 = tmp42923;
var state_42915__$1 = (function (){var statearr_42926 = state_42915;
(statearr_42926[(10)] = inst_42885__$1);

(statearr_42926[(11)] = inst_42904);

return statearr_42926;
})();
var statearr_42927_42944 = state_42915__$1;
(statearr_42927_42944[(2)] = null);

(statearr_42927_42944[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42916 === (9))){
var inst_42895 = (state_42915[(7)]);
var state_42915__$1 = state_42915;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_42915__$1,(11),out,inst_42895);
} else {
if((state_val_42916 === (5))){
var inst_42909 = cljs.core.async.close_BANG_.call(null,out);
var state_42915__$1 = state_42915;
var statearr_42928_42945 = state_42915__$1;
(statearr_42928_42945[(2)] = inst_42909);

(statearr_42928_42945[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42916 === (10))){
var inst_42907 = (state_42915[(2)]);
var state_42915__$1 = state_42915;
var statearr_42929_42946 = state_42915__$1;
(statearr_42929_42946[(2)] = inst_42907);

(statearr_42929_42946[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42916 === (8))){
var inst_42896 = (state_42915[(9)]);
var inst_42895 = (state_42915[(7)]);
var inst_42885 = (state_42915[(10)]);
var inst_42894 = (state_42915[(8)]);
var inst_42899 = (function (){var cs = inst_42885;
var vec__42890 = inst_42894;
var v = inst_42895;
var c = inst_42896;
return ((function (cs,vec__42890,v,c,inst_42896,inst_42895,inst_42885,inst_42894,state_val_42916,c__35058__auto___42937,out){
return (function (p1__42881_SHARP_){
return cljs.core.not_EQ_.call(null,c,p1__42881_SHARP_);
});
;})(cs,vec__42890,v,c,inst_42896,inst_42895,inst_42885,inst_42894,state_val_42916,c__35058__auto___42937,out))
})();
var inst_42900 = cljs.core.filterv.call(null,inst_42899,inst_42885);
var inst_42885__$1 = inst_42900;
var state_42915__$1 = (function (){var statearr_42930 = state_42915;
(statearr_42930[(10)] = inst_42885__$1);

return statearr_42930;
})();
var statearr_42931_42947 = state_42915__$1;
(statearr_42931_42947[(2)] = null);

(statearr_42931_42947[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__35058__auto___42937,out))
;
return ((function (switch__34968__auto__,c__35058__auto___42937,out){
return (function() {
var cljs$core$async$state_machine__34969__auto__ = null;
var cljs$core$async$state_machine__34969__auto____0 = (function (){
var statearr_42932 = [null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_42932[(0)] = cljs$core$async$state_machine__34969__auto__);

(statearr_42932[(1)] = (1));

return statearr_42932;
});
var cljs$core$async$state_machine__34969__auto____1 = (function (state_42915){
while(true){
var ret_value__34970__auto__ = (function (){try{while(true){
var result__34971__auto__ = switch__34968__auto__.call(null,state_42915);
if(cljs.core.keyword_identical_QMARK_.call(null,result__34971__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__34971__auto__;
}
break;
}
}catch (e42933){if((e42933 instanceof Object)){
var ex__34972__auto__ = e42933;
var statearr_42934_42948 = state_42915;
(statearr_42934_42948[(5)] = ex__34972__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_42915);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e42933;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__34970__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__42949 = state_42915;
state_42915 = G__42949;
continue;
} else {
return ret_value__34970__auto__;
}
break;
}
});
cljs$core$async$state_machine__34969__auto__ = function(state_42915){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__34969__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__34969__auto____1.call(this,state_42915);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__34969__auto____0;
cljs$core$async$state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__34969__auto____1;
return cljs$core$async$state_machine__34969__auto__;
})()
;})(switch__34968__auto__,c__35058__auto___42937,out))
})();
var state__35060__auto__ = (function (){var statearr_42935 = f__35059__auto__.call(null);
(statearr_42935[(6)] = c__35058__auto___42937);

return statearr_42935;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__35060__auto__);
});})(c__35058__auto___42937,out))
);


return out;
});

cljs.core.async.merge.cljs$lang$maxFixedArity = 2;

/**
 * Returns a channel containing the single (collection) result of the
 *   items taken from the channel conjoined to the supplied
 *   collection. ch must close before into produces a result.
 */
cljs.core.async.into = (function cljs$core$async$into(coll,ch){
return cljs.core.async.reduce.call(null,cljs.core.conj,coll,ch);
});
/**
 * Returns a channel that will return, at most, n items from ch. After n items
 * have been returned, or ch has been closed, the return chanel will close.
 * 
 *   The output channel is unbuffered by default, unless buf-or-n is given.
 */
cljs.core.async.take = (function cljs$core$async$take(var_args){
var G__42951 = arguments.length;
switch (G__42951) {
case 2:
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.take.cljs$core$IFn$_invoke$arity$2 = (function (n,ch){
return cljs.core.async.take.call(null,n,ch,null);
});

cljs.core.async.take.cljs$core$IFn$_invoke$arity$3 = (function (n,ch,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__35058__auto___42996 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__35058__auto___42996,out){
return (function (){
var f__35059__auto__ = (function (){var switch__34968__auto__ = ((function (c__35058__auto___42996,out){
return (function (state_42975){
var state_val_42976 = (state_42975[(1)]);
if((state_val_42976 === (7))){
var inst_42957 = (state_42975[(7)]);
var inst_42957__$1 = (state_42975[(2)]);
var inst_42958 = (inst_42957__$1 == null);
var inst_42959 = cljs.core.not.call(null,inst_42958);
var state_42975__$1 = (function (){var statearr_42977 = state_42975;
(statearr_42977[(7)] = inst_42957__$1);

return statearr_42977;
})();
if(inst_42959){
var statearr_42978_42997 = state_42975__$1;
(statearr_42978_42997[(1)] = (8));

} else {
var statearr_42979_42998 = state_42975__$1;
(statearr_42979_42998[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42976 === (1))){
var inst_42952 = (0);
var state_42975__$1 = (function (){var statearr_42980 = state_42975;
(statearr_42980[(8)] = inst_42952);

return statearr_42980;
})();
var statearr_42981_42999 = state_42975__$1;
(statearr_42981_42999[(2)] = null);

(statearr_42981_42999[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42976 === (4))){
var state_42975__$1 = state_42975;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_42975__$1,(7),ch);
} else {
if((state_val_42976 === (6))){
var inst_42970 = (state_42975[(2)]);
var state_42975__$1 = state_42975;
var statearr_42982_43000 = state_42975__$1;
(statearr_42982_43000[(2)] = inst_42970);

(statearr_42982_43000[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42976 === (3))){
var inst_42972 = (state_42975[(2)]);
var inst_42973 = cljs.core.async.close_BANG_.call(null,out);
var state_42975__$1 = (function (){var statearr_42983 = state_42975;
(statearr_42983[(9)] = inst_42972);

return statearr_42983;
})();
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_42975__$1,inst_42973);
} else {
if((state_val_42976 === (2))){
var inst_42952 = (state_42975[(8)]);
var inst_42954 = (inst_42952 < n);
var state_42975__$1 = state_42975;
if(cljs.core.truth_(inst_42954)){
var statearr_42984_43001 = state_42975__$1;
(statearr_42984_43001[(1)] = (4));

} else {
var statearr_42985_43002 = state_42975__$1;
(statearr_42985_43002[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42976 === (11))){
var inst_42952 = (state_42975[(8)]);
var inst_42962 = (state_42975[(2)]);
var inst_42963 = (inst_42952 + (1));
var inst_42952__$1 = inst_42963;
var state_42975__$1 = (function (){var statearr_42986 = state_42975;
(statearr_42986[(10)] = inst_42962);

(statearr_42986[(8)] = inst_42952__$1);

return statearr_42986;
})();
var statearr_42987_43003 = state_42975__$1;
(statearr_42987_43003[(2)] = null);

(statearr_42987_43003[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42976 === (9))){
var state_42975__$1 = state_42975;
var statearr_42988_43004 = state_42975__$1;
(statearr_42988_43004[(2)] = null);

(statearr_42988_43004[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42976 === (5))){
var state_42975__$1 = state_42975;
var statearr_42989_43005 = state_42975__$1;
(statearr_42989_43005[(2)] = null);

(statearr_42989_43005[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42976 === (10))){
var inst_42967 = (state_42975[(2)]);
var state_42975__$1 = state_42975;
var statearr_42990_43006 = state_42975__$1;
(statearr_42990_43006[(2)] = inst_42967);

(statearr_42990_43006[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42976 === (8))){
var inst_42957 = (state_42975[(7)]);
var state_42975__$1 = state_42975;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_42975__$1,(11),out,inst_42957);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__35058__auto___42996,out))
;
return ((function (switch__34968__auto__,c__35058__auto___42996,out){
return (function() {
var cljs$core$async$state_machine__34969__auto__ = null;
var cljs$core$async$state_machine__34969__auto____0 = (function (){
var statearr_42991 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_42991[(0)] = cljs$core$async$state_machine__34969__auto__);

(statearr_42991[(1)] = (1));

return statearr_42991;
});
var cljs$core$async$state_machine__34969__auto____1 = (function (state_42975){
while(true){
var ret_value__34970__auto__ = (function (){try{while(true){
var result__34971__auto__ = switch__34968__auto__.call(null,state_42975);
if(cljs.core.keyword_identical_QMARK_.call(null,result__34971__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__34971__auto__;
}
break;
}
}catch (e42992){if((e42992 instanceof Object)){
var ex__34972__auto__ = e42992;
var statearr_42993_43007 = state_42975;
(statearr_42993_43007[(5)] = ex__34972__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_42975);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e42992;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__34970__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__43008 = state_42975;
state_42975 = G__43008;
continue;
} else {
return ret_value__34970__auto__;
}
break;
}
});
cljs$core$async$state_machine__34969__auto__ = function(state_42975){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__34969__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__34969__auto____1.call(this,state_42975);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__34969__auto____0;
cljs$core$async$state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__34969__auto____1;
return cljs$core$async$state_machine__34969__auto__;
})()
;})(switch__34968__auto__,c__35058__auto___42996,out))
})();
var state__35060__auto__ = (function (){var statearr_42994 = f__35059__auto__.call(null);
(statearr_42994[(6)] = c__35058__auto___42996);

return statearr_42994;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__35060__auto__);
});})(c__35058__auto___42996,out))
);


return out;
});

cljs.core.async.take.cljs$lang$maxFixedArity = 3;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.map_LT_ = (function cljs$core$async$map_LT_(f,ch){
if(typeof cljs.core.async.t_cljs$core$async43010 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async43010 = (function (f,ch,meta43011){
this.f = f;
this.ch = ch;
this.meta43011 = meta43011;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async43010.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_43012,meta43011__$1){
var self__ = this;
var _43012__$1 = this;
return (new cljs.core.async.t_cljs$core$async43010(self__.f,self__.ch,meta43011__$1));
});

cljs.core.async.t_cljs$core$async43010.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_43012){
var self__ = this;
var _43012__$1 = this;
return self__.meta43011;
});

cljs.core.async.t_cljs$core$async43010.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async43010.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_.call(null,self__.ch);
});

cljs.core.async.t_cljs$core$async43010.prototype.cljs$core$async$impl$protocols$Channel$closed_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.closed_QMARK_.call(null,self__.ch);
});

cljs.core.async.t_cljs$core$async43010.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async43010.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
var ret = cljs.core.async.impl.protocols.take_BANG_.call(null,self__.ch,(function (){
if(typeof cljs.core.async.t_cljs$core$async43013 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async43013 = (function (f,ch,meta43011,_,fn1,meta43014){
this.f = f;
this.ch = ch;
this.meta43011 = meta43011;
this._ = _;
this.fn1 = fn1;
this.meta43014 = meta43014;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async43013.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (___$1){
return (function (_43015,meta43014__$1){
var self__ = this;
var _43015__$1 = this;
return (new cljs.core.async.t_cljs$core$async43013(self__.f,self__.ch,self__.meta43011,self__._,self__.fn1,meta43014__$1));
});})(___$1))
;

cljs.core.async.t_cljs$core$async43013.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (___$1){
return (function (_43015){
var self__ = this;
var _43015__$1 = this;
return self__.meta43014;
});})(___$1))
;

cljs.core.async.t_cljs$core$async43013.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async43013.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = ((function (___$1){
return (function (___$1){
var self__ = this;
var ___$2 = this;
return cljs.core.async.impl.protocols.active_QMARK_.call(null,self__.fn1);
});})(___$1))
;

cljs.core.async.t_cljs$core$async43013.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = ((function (___$1){
return (function (___$1){
var self__ = this;
var ___$2 = this;
return true;
});})(___$1))
;

cljs.core.async.t_cljs$core$async43013.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = ((function (___$1){
return (function (___$1){
var self__ = this;
var ___$2 = this;
var f1 = cljs.core.async.impl.protocols.commit.call(null,self__.fn1);
return ((function (f1,___$2,___$1){
return (function (p1__43009_SHARP_){
return f1.call(null,(((p1__43009_SHARP_ == null))?null:self__.f.call(null,p1__43009_SHARP_)));
});
;})(f1,___$2,___$1))
});})(___$1))
;

cljs.core.async.t_cljs$core$async43013.getBasis = ((function (___$1){
return (function (){
return new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta43011","meta43011",41760929,null),cljs.core.with_meta(new cljs.core.Symbol(null,"_","_",-1201019570,null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"tag","tag",-1290361223),new cljs.core.Symbol("cljs.core.async","t_cljs$core$async43010","cljs.core.async/t_cljs$core$async43010",997988620,null)], null)),new cljs.core.Symbol(null,"fn1","fn1",895834444,null),new cljs.core.Symbol(null,"meta43014","meta43014",-1287828469,null)], null);
});})(___$1))
;

cljs.core.async.t_cljs$core$async43013.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async43013.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async43013";

cljs.core.async.t_cljs$core$async43013.cljs$lang$ctorPrWriter = ((function (___$1){
return (function (this__30853__auto__,writer__30854__auto__,opt__30855__auto__){
return cljs.core._write.call(null,writer__30854__auto__,"cljs.core.async/t_cljs$core$async43013");
});})(___$1))
;

cljs.core.async.__GT_t_cljs$core$async43013 = ((function (___$1){
return (function cljs$core$async$map_LT__$___GT_t_cljs$core$async43013(f__$1,ch__$1,meta43011__$1,___$2,fn1__$1,meta43014){
return (new cljs.core.async.t_cljs$core$async43013(f__$1,ch__$1,meta43011__$1,___$2,fn1__$1,meta43014));
});})(___$1))
;

}

return (new cljs.core.async.t_cljs$core$async43013(self__.f,self__.ch,self__.meta43011,___$1,fn1,cljs.core.PersistentArrayMap.EMPTY));
})()
);
if(cljs.core.truth_((function (){var and__30170__auto__ = ret;
if(cljs.core.truth_(and__30170__auto__)){
return !((cljs.core.deref.call(null,ret) == null));
} else {
return and__30170__auto__;
}
})())){
return cljs.core.async.impl.channels.box.call(null,self__.f.call(null,cljs.core.deref.call(null,ret)));
} else {
return ret;
}
});

cljs.core.async.t_cljs$core$async43010.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async43010.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.put_BANG_.call(null,self__.ch,val,fn1);
});

cljs.core.async.t_cljs$core$async43010.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta43011","meta43011",41760929,null)], null);
});

cljs.core.async.t_cljs$core$async43010.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async43010.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async43010";

cljs.core.async.t_cljs$core$async43010.cljs$lang$ctorPrWriter = (function (this__30853__auto__,writer__30854__auto__,opt__30855__auto__){
return cljs.core._write.call(null,writer__30854__auto__,"cljs.core.async/t_cljs$core$async43010");
});

cljs.core.async.__GT_t_cljs$core$async43010 = (function cljs$core$async$map_LT__$___GT_t_cljs$core$async43010(f__$1,ch__$1,meta43011){
return (new cljs.core.async.t_cljs$core$async43010(f__$1,ch__$1,meta43011));
});

}

return (new cljs.core.async.t_cljs$core$async43010(f,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.map_GT_ = (function cljs$core$async$map_GT_(f,ch){
if(typeof cljs.core.async.t_cljs$core$async43016 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async43016 = (function (f,ch,meta43017){
this.f = f;
this.ch = ch;
this.meta43017 = meta43017;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async43016.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_43018,meta43017__$1){
var self__ = this;
var _43018__$1 = this;
return (new cljs.core.async.t_cljs$core$async43016(self__.f,self__.ch,meta43017__$1));
});

cljs.core.async.t_cljs$core$async43016.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_43018){
var self__ = this;
var _43018__$1 = this;
return self__.meta43017;
});

cljs.core.async.t_cljs$core$async43016.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async43016.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_.call(null,self__.ch);
});

cljs.core.async.t_cljs$core$async43016.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async43016.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.take_BANG_.call(null,self__.ch,fn1);
});

cljs.core.async.t_cljs$core$async43016.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async43016.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.put_BANG_.call(null,self__.ch,self__.f.call(null,val),fn1);
});

cljs.core.async.t_cljs$core$async43016.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta43017","meta43017",-1726205421,null)], null);
});

cljs.core.async.t_cljs$core$async43016.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async43016.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async43016";

cljs.core.async.t_cljs$core$async43016.cljs$lang$ctorPrWriter = (function (this__30853__auto__,writer__30854__auto__,opt__30855__auto__){
return cljs.core._write.call(null,writer__30854__auto__,"cljs.core.async/t_cljs$core$async43016");
});

cljs.core.async.__GT_t_cljs$core$async43016 = (function cljs$core$async$map_GT__$___GT_t_cljs$core$async43016(f__$1,ch__$1,meta43017){
return (new cljs.core.async.t_cljs$core$async43016(f__$1,ch__$1,meta43017));
});

}

return (new cljs.core.async.t_cljs$core$async43016(f,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.filter_GT_ = (function cljs$core$async$filter_GT_(p,ch){
if(typeof cljs.core.async.t_cljs$core$async43019 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async43019 = (function (p,ch,meta43020){
this.p = p;
this.ch = ch;
this.meta43020 = meta43020;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async43019.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_43021,meta43020__$1){
var self__ = this;
var _43021__$1 = this;
return (new cljs.core.async.t_cljs$core$async43019(self__.p,self__.ch,meta43020__$1));
});

cljs.core.async.t_cljs$core$async43019.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_43021){
var self__ = this;
var _43021__$1 = this;
return self__.meta43020;
});

cljs.core.async.t_cljs$core$async43019.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async43019.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_.call(null,self__.ch);
});

cljs.core.async.t_cljs$core$async43019.prototype.cljs$core$async$impl$protocols$Channel$closed_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.closed_QMARK_.call(null,self__.ch);
});

cljs.core.async.t_cljs$core$async43019.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async43019.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.take_BANG_.call(null,self__.ch,fn1);
});

cljs.core.async.t_cljs$core$async43019.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async43019.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
if(cljs.core.truth_(self__.p.call(null,val))){
return cljs.core.async.impl.protocols.put_BANG_.call(null,self__.ch,val,fn1);
} else {
return cljs.core.async.impl.channels.box.call(null,cljs.core.not.call(null,cljs.core.async.impl.protocols.closed_QMARK_.call(null,self__.ch)));
}
});

cljs.core.async.t_cljs$core$async43019.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"p","p",1791580836,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta43020","meta43020",-1487179915,null)], null);
});

cljs.core.async.t_cljs$core$async43019.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async43019.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async43019";

cljs.core.async.t_cljs$core$async43019.cljs$lang$ctorPrWriter = (function (this__30853__auto__,writer__30854__auto__,opt__30855__auto__){
return cljs.core._write.call(null,writer__30854__auto__,"cljs.core.async/t_cljs$core$async43019");
});

cljs.core.async.__GT_t_cljs$core$async43019 = (function cljs$core$async$filter_GT__$___GT_t_cljs$core$async43019(p__$1,ch__$1,meta43020){
return (new cljs.core.async.t_cljs$core$async43019(p__$1,ch__$1,meta43020));
});

}

return (new cljs.core.async.t_cljs$core$async43019(p,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.remove_GT_ = (function cljs$core$async$remove_GT_(p,ch){
return cljs.core.async.filter_GT_.call(null,cljs.core.complement.call(null,p),ch);
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.filter_LT_ = (function cljs$core$async$filter_LT_(var_args){
var G__43023 = arguments.length;
switch (G__43023) {
case 2:
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.filter_LT_.call(null,p,ch,null);
});

cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3 = (function (p,ch,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__35058__auto___43063 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__35058__auto___43063,out){
return (function (){
var f__35059__auto__ = (function (){var switch__34968__auto__ = ((function (c__35058__auto___43063,out){
return (function (state_43044){
var state_val_43045 = (state_43044[(1)]);
if((state_val_43045 === (7))){
var inst_43040 = (state_43044[(2)]);
var state_43044__$1 = state_43044;
var statearr_43046_43064 = state_43044__$1;
(statearr_43046_43064[(2)] = inst_43040);

(statearr_43046_43064[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43045 === (1))){
var state_43044__$1 = state_43044;
var statearr_43047_43065 = state_43044__$1;
(statearr_43047_43065[(2)] = null);

(statearr_43047_43065[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43045 === (4))){
var inst_43026 = (state_43044[(7)]);
var inst_43026__$1 = (state_43044[(2)]);
var inst_43027 = (inst_43026__$1 == null);
var state_43044__$1 = (function (){var statearr_43048 = state_43044;
(statearr_43048[(7)] = inst_43026__$1);

return statearr_43048;
})();
if(cljs.core.truth_(inst_43027)){
var statearr_43049_43066 = state_43044__$1;
(statearr_43049_43066[(1)] = (5));

} else {
var statearr_43050_43067 = state_43044__$1;
(statearr_43050_43067[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43045 === (6))){
var inst_43026 = (state_43044[(7)]);
var inst_43031 = p.call(null,inst_43026);
var state_43044__$1 = state_43044;
if(cljs.core.truth_(inst_43031)){
var statearr_43051_43068 = state_43044__$1;
(statearr_43051_43068[(1)] = (8));

} else {
var statearr_43052_43069 = state_43044__$1;
(statearr_43052_43069[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43045 === (3))){
var inst_43042 = (state_43044[(2)]);
var state_43044__$1 = state_43044;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_43044__$1,inst_43042);
} else {
if((state_val_43045 === (2))){
var state_43044__$1 = state_43044;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_43044__$1,(4),ch);
} else {
if((state_val_43045 === (11))){
var inst_43034 = (state_43044[(2)]);
var state_43044__$1 = state_43044;
var statearr_43053_43070 = state_43044__$1;
(statearr_43053_43070[(2)] = inst_43034);

(statearr_43053_43070[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43045 === (9))){
var state_43044__$1 = state_43044;
var statearr_43054_43071 = state_43044__$1;
(statearr_43054_43071[(2)] = null);

(statearr_43054_43071[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43045 === (5))){
var inst_43029 = cljs.core.async.close_BANG_.call(null,out);
var state_43044__$1 = state_43044;
var statearr_43055_43072 = state_43044__$1;
(statearr_43055_43072[(2)] = inst_43029);

(statearr_43055_43072[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43045 === (10))){
var inst_43037 = (state_43044[(2)]);
var state_43044__$1 = (function (){var statearr_43056 = state_43044;
(statearr_43056[(8)] = inst_43037);

return statearr_43056;
})();
var statearr_43057_43073 = state_43044__$1;
(statearr_43057_43073[(2)] = null);

(statearr_43057_43073[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43045 === (8))){
var inst_43026 = (state_43044[(7)]);
var state_43044__$1 = state_43044;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_43044__$1,(11),out,inst_43026);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__35058__auto___43063,out))
;
return ((function (switch__34968__auto__,c__35058__auto___43063,out){
return (function() {
var cljs$core$async$state_machine__34969__auto__ = null;
var cljs$core$async$state_machine__34969__auto____0 = (function (){
var statearr_43058 = [null,null,null,null,null,null,null,null,null];
(statearr_43058[(0)] = cljs$core$async$state_machine__34969__auto__);

(statearr_43058[(1)] = (1));

return statearr_43058;
});
var cljs$core$async$state_machine__34969__auto____1 = (function (state_43044){
while(true){
var ret_value__34970__auto__ = (function (){try{while(true){
var result__34971__auto__ = switch__34968__auto__.call(null,state_43044);
if(cljs.core.keyword_identical_QMARK_.call(null,result__34971__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__34971__auto__;
}
break;
}
}catch (e43059){if((e43059 instanceof Object)){
var ex__34972__auto__ = e43059;
var statearr_43060_43074 = state_43044;
(statearr_43060_43074[(5)] = ex__34972__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_43044);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e43059;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__34970__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__43075 = state_43044;
state_43044 = G__43075;
continue;
} else {
return ret_value__34970__auto__;
}
break;
}
});
cljs$core$async$state_machine__34969__auto__ = function(state_43044){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__34969__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__34969__auto____1.call(this,state_43044);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__34969__auto____0;
cljs$core$async$state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__34969__auto____1;
return cljs$core$async$state_machine__34969__auto__;
})()
;})(switch__34968__auto__,c__35058__auto___43063,out))
})();
var state__35060__auto__ = (function (){var statearr_43061 = f__35059__auto__.call(null);
(statearr_43061[(6)] = c__35058__auto___43063);

return statearr_43061;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__35060__auto__);
});})(c__35058__auto___43063,out))
);


return out;
});

cljs.core.async.filter_LT_.cljs$lang$maxFixedArity = 3;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.remove_LT_ = (function cljs$core$async$remove_LT_(var_args){
var G__43077 = arguments.length;
switch (G__43077) {
case 2:
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.remove_LT_.call(null,p,ch,null);
});

cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3 = (function (p,ch,buf_or_n){
return cljs.core.async.filter_LT_.call(null,cljs.core.complement.call(null,p),ch,buf_or_n);
});

cljs.core.async.remove_LT_.cljs$lang$maxFixedArity = 3;

cljs.core.async.mapcat_STAR_ = (function cljs$core$async$mapcat_STAR_(f,in$,out){
var c__35058__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__35058__auto__){
return (function (){
var f__35059__auto__ = (function (){var switch__34968__auto__ = ((function (c__35058__auto__){
return (function (state_43140){
var state_val_43141 = (state_43140[(1)]);
if((state_val_43141 === (7))){
var inst_43136 = (state_43140[(2)]);
var state_43140__$1 = state_43140;
var statearr_43142_43180 = state_43140__$1;
(statearr_43142_43180[(2)] = inst_43136);

(statearr_43142_43180[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43141 === (20))){
var inst_43106 = (state_43140[(7)]);
var inst_43117 = (state_43140[(2)]);
var inst_43118 = cljs.core.next.call(null,inst_43106);
var inst_43092 = inst_43118;
var inst_43093 = null;
var inst_43094 = (0);
var inst_43095 = (0);
var state_43140__$1 = (function (){var statearr_43143 = state_43140;
(statearr_43143[(8)] = inst_43117);

(statearr_43143[(9)] = inst_43095);

(statearr_43143[(10)] = inst_43093);

(statearr_43143[(11)] = inst_43094);

(statearr_43143[(12)] = inst_43092);

return statearr_43143;
})();
var statearr_43144_43181 = state_43140__$1;
(statearr_43144_43181[(2)] = null);

(statearr_43144_43181[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43141 === (1))){
var state_43140__$1 = state_43140;
var statearr_43145_43182 = state_43140__$1;
(statearr_43145_43182[(2)] = null);

(statearr_43145_43182[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43141 === (4))){
var inst_43081 = (state_43140[(13)]);
var inst_43081__$1 = (state_43140[(2)]);
var inst_43082 = (inst_43081__$1 == null);
var state_43140__$1 = (function (){var statearr_43146 = state_43140;
(statearr_43146[(13)] = inst_43081__$1);

return statearr_43146;
})();
if(cljs.core.truth_(inst_43082)){
var statearr_43147_43183 = state_43140__$1;
(statearr_43147_43183[(1)] = (5));

} else {
var statearr_43148_43184 = state_43140__$1;
(statearr_43148_43184[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43141 === (15))){
var state_43140__$1 = state_43140;
var statearr_43152_43185 = state_43140__$1;
(statearr_43152_43185[(2)] = null);

(statearr_43152_43185[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43141 === (21))){
var state_43140__$1 = state_43140;
var statearr_43153_43186 = state_43140__$1;
(statearr_43153_43186[(2)] = null);

(statearr_43153_43186[(1)] = (23));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43141 === (13))){
var inst_43095 = (state_43140[(9)]);
var inst_43093 = (state_43140[(10)]);
var inst_43094 = (state_43140[(11)]);
var inst_43092 = (state_43140[(12)]);
var inst_43102 = (state_43140[(2)]);
var inst_43103 = (inst_43095 + (1));
var tmp43149 = inst_43093;
var tmp43150 = inst_43094;
var tmp43151 = inst_43092;
var inst_43092__$1 = tmp43151;
var inst_43093__$1 = tmp43149;
var inst_43094__$1 = tmp43150;
var inst_43095__$1 = inst_43103;
var state_43140__$1 = (function (){var statearr_43154 = state_43140;
(statearr_43154[(9)] = inst_43095__$1);

(statearr_43154[(10)] = inst_43093__$1);

(statearr_43154[(11)] = inst_43094__$1);

(statearr_43154[(12)] = inst_43092__$1);

(statearr_43154[(14)] = inst_43102);

return statearr_43154;
})();
var statearr_43155_43187 = state_43140__$1;
(statearr_43155_43187[(2)] = null);

(statearr_43155_43187[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43141 === (22))){
var state_43140__$1 = state_43140;
var statearr_43156_43188 = state_43140__$1;
(statearr_43156_43188[(2)] = null);

(statearr_43156_43188[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43141 === (6))){
var inst_43081 = (state_43140[(13)]);
var inst_43090 = f.call(null,inst_43081);
var inst_43091 = cljs.core.seq.call(null,inst_43090);
var inst_43092 = inst_43091;
var inst_43093 = null;
var inst_43094 = (0);
var inst_43095 = (0);
var state_43140__$1 = (function (){var statearr_43157 = state_43140;
(statearr_43157[(9)] = inst_43095);

(statearr_43157[(10)] = inst_43093);

(statearr_43157[(11)] = inst_43094);

(statearr_43157[(12)] = inst_43092);

return statearr_43157;
})();
var statearr_43158_43189 = state_43140__$1;
(statearr_43158_43189[(2)] = null);

(statearr_43158_43189[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43141 === (17))){
var inst_43106 = (state_43140[(7)]);
var inst_43110 = cljs.core.chunk_first.call(null,inst_43106);
var inst_43111 = cljs.core.chunk_rest.call(null,inst_43106);
var inst_43112 = cljs.core.count.call(null,inst_43110);
var inst_43092 = inst_43111;
var inst_43093 = inst_43110;
var inst_43094 = inst_43112;
var inst_43095 = (0);
var state_43140__$1 = (function (){var statearr_43159 = state_43140;
(statearr_43159[(9)] = inst_43095);

(statearr_43159[(10)] = inst_43093);

(statearr_43159[(11)] = inst_43094);

(statearr_43159[(12)] = inst_43092);

return statearr_43159;
})();
var statearr_43160_43190 = state_43140__$1;
(statearr_43160_43190[(2)] = null);

(statearr_43160_43190[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43141 === (3))){
var inst_43138 = (state_43140[(2)]);
var state_43140__$1 = state_43140;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_43140__$1,inst_43138);
} else {
if((state_val_43141 === (12))){
var inst_43126 = (state_43140[(2)]);
var state_43140__$1 = state_43140;
var statearr_43161_43191 = state_43140__$1;
(statearr_43161_43191[(2)] = inst_43126);

(statearr_43161_43191[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43141 === (2))){
var state_43140__$1 = state_43140;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_43140__$1,(4),in$);
} else {
if((state_val_43141 === (23))){
var inst_43134 = (state_43140[(2)]);
var state_43140__$1 = state_43140;
var statearr_43162_43192 = state_43140__$1;
(statearr_43162_43192[(2)] = inst_43134);

(statearr_43162_43192[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43141 === (19))){
var inst_43121 = (state_43140[(2)]);
var state_43140__$1 = state_43140;
var statearr_43163_43193 = state_43140__$1;
(statearr_43163_43193[(2)] = inst_43121);

(statearr_43163_43193[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43141 === (11))){
var inst_43106 = (state_43140[(7)]);
var inst_43092 = (state_43140[(12)]);
var inst_43106__$1 = cljs.core.seq.call(null,inst_43092);
var state_43140__$1 = (function (){var statearr_43164 = state_43140;
(statearr_43164[(7)] = inst_43106__$1);

return statearr_43164;
})();
if(inst_43106__$1){
var statearr_43165_43194 = state_43140__$1;
(statearr_43165_43194[(1)] = (14));

} else {
var statearr_43166_43195 = state_43140__$1;
(statearr_43166_43195[(1)] = (15));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43141 === (9))){
var inst_43128 = (state_43140[(2)]);
var inst_43129 = cljs.core.async.impl.protocols.closed_QMARK_.call(null,out);
var state_43140__$1 = (function (){var statearr_43167 = state_43140;
(statearr_43167[(15)] = inst_43128);

return statearr_43167;
})();
if(cljs.core.truth_(inst_43129)){
var statearr_43168_43196 = state_43140__$1;
(statearr_43168_43196[(1)] = (21));

} else {
var statearr_43169_43197 = state_43140__$1;
(statearr_43169_43197[(1)] = (22));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43141 === (5))){
var inst_43084 = cljs.core.async.close_BANG_.call(null,out);
var state_43140__$1 = state_43140;
var statearr_43170_43198 = state_43140__$1;
(statearr_43170_43198[(2)] = inst_43084);

(statearr_43170_43198[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43141 === (14))){
var inst_43106 = (state_43140[(7)]);
var inst_43108 = cljs.core.chunked_seq_QMARK_.call(null,inst_43106);
var state_43140__$1 = state_43140;
if(inst_43108){
var statearr_43171_43199 = state_43140__$1;
(statearr_43171_43199[(1)] = (17));

} else {
var statearr_43172_43200 = state_43140__$1;
(statearr_43172_43200[(1)] = (18));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43141 === (16))){
var inst_43124 = (state_43140[(2)]);
var state_43140__$1 = state_43140;
var statearr_43173_43201 = state_43140__$1;
(statearr_43173_43201[(2)] = inst_43124);

(statearr_43173_43201[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43141 === (10))){
var inst_43095 = (state_43140[(9)]);
var inst_43093 = (state_43140[(10)]);
var inst_43100 = cljs.core._nth.call(null,inst_43093,inst_43095);
var state_43140__$1 = state_43140;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_43140__$1,(13),out,inst_43100);
} else {
if((state_val_43141 === (18))){
var inst_43106 = (state_43140[(7)]);
var inst_43115 = cljs.core.first.call(null,inst_43106);
var state_43140__$1 = state_43140;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_43140__$1,(20),out,inst_43115);
} else {
if((state_val_43141 === (8))){
var inst_43095 = (state_43140[(9)]);
var inst_43094 = (state_43140[(11)]);
var inst_43097 = (inst_43095 < inst_43094);
var inst_43098 = inst_43097;
var state_43140__$1 = state_43140;
if(cljs.core.truth_(inst_43098)){
var statearr_43174_43202 = state_43140__$1;
(statearr_43174_43202[(1)] = (10));

} else {
var statearr_43175_43203 = state_43140__$1;
(statearr_43175_43203[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__35058__auto__))
;
return ((function (switch__34968__auto__,c__35058__auto__){
return (function() {
var cljs$core$async$mapcat_STAR__$_state_machine__34969__auto__ = null;
var cljs$core$async$mapcat_STAR__$_state_machine__34969__auto____0 = (function (){
var statearr_43176 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_43176[(0)] = cljs$core$async$mapcat_STAR__$_state_machine__34969__auto__);

(statearr_43176[(1)] = (1));

return statearr_43176;
});
var cljs$core$async$mapcat_STAR__$_state_machine__34969__auto____1 = (function (state_43140){
while(true){
var ret_value__34970__auto__ = (function (){try{while(true){
var result__34971__auto__ = switch__34968__auto__.call(null,state_43140);
if(cljs.core.keyword_identical_QMARK_.call(null,result__34971__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__34971__auto__;
}
break;
}
}catch (e43177){if((e43177 instanceof Object)){
var ex__34972__auto__ = e43177;
var statearr_43178_43204 = state_43140;
(statearr_43178_43204[(5)] = ex__34972__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_43140);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e43177;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__34970__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__43205 = state_43140;
state_43140 = G__43205;
continue;
} else {
return ret_value__34970__auto__;
}
break;
}
});
cljs$core$async$mapcat_STAR__$_state_machine__34969__auto__ = function(state_43140){
switch(arguments.length){
case 0:
return cljs$core$async$mapcat_STAR__$_state_machine__34969__auto____0.call(this);
case 1:
return cljs$core$async$mapcat_STAR__$_state_machine__34969__auto____1.call(this,state_43140);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$mapcat_STAR__$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mapcat_STAR__$_state_machine__34969__auto____0;
cljs$core$async$mapcat_STAR__$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mapcat_STAR__$_state_machine__34969__auto____1;
return cljs$core$async$mapcat_STAR__$_state_machine__34969__auto__;
})()
;})(switch__34968__auto__,c__35058__auto__))
})();
var state__35060__auto__ = (function (){var statearr_43179 = f__35059__auto__.call(null);
(statearr_43179[(6)] = c__35058__auto__);

return statearr_43179;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__35060__auto__);
});})(c__35058__auto__))
);

return c__35058__auto__;
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.mapcat_LT_ = (function cljs$core$async$mapcat_LT_(var_args){
var G__43207 = arguments.length;
switch (G__43207) {
case 2:
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$2 = (function (f,in$){
return cljs.core.async.mapcat_LT_.call(null,f,in$,null);
});

cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3 = (function (f,in$,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
cljs.core.async.mapcat_STAR_.call(null,f,in$,out);

return out;
});

cljs.core.async.mapcat_LT_.cljs$lang$maxFixedArity = 3;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.mapcat_GT_ = (function cljs$core$async$mapcat_GT_(var_args){
var G__43210 = arguments.length;
switch (G__43210) {
case 2:
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$2 = (function (f,out){
return cljs.core.async.mapcat_GT_.call(null,f,out,null);
});

cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3 = (function (f,out,buf_or_n){
var in$ = cljs.core.async.chan.call(null,buf_or_n);
cljs.core.async.mapcat_STAR_.call(null,f,in$,out);

return in$;
});

cljs.core.async.mapcat_GT_.cljs$lang$maxFixedArity = 3;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.unique = (function cljs$core$async$unique(var_args){
var G__43213 = arguments.length;
switch (G__43213) {
case 1:
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.unique.cljs$core$IFn$_invoke$arity$1 = (function (ch){
return cljs.core.async.unique.call(null,ch,null);
});

cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2 = (function (ch,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__35058__auto___43260 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__35058__auto___43260,out){
return (function (){
var f__35059__auto__ = (function (){var switch__34968__auto__ = ((function (c__35058__auto___43260,out){
return (function (state_43237){
var state_val_43238 = (state_43237[(1)]);
if((state_val_43238 === (7))){
var inst_43232 = (state_43237[(2)]);
var state_43237__$1 = state_43237;
var statearr_43239_43261 = state_43237__$1;
(statearr_43239_43261[(2)] = inst_43232);

(statearr_43239_43261[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43238 === (1))){
var inst_43214 = null;
var state_43237__$1 = (function (){var statearr_43240 = state_43237;
(statearr_43240[(7)] = inst_43214);

return statearr_43240;
})();
var statearr_43241_43262 = state_43237__$1;
(statearr_43241_43262[(2)] = null);

(statearr_43241_43262[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43238 === (4))){
var inst_43217 = (state_43237[(8)]);
var inst_43217__$1 = (state_43237[(2)]);
var inst_43218 = (inst_43217__$1 == null);
var inst_43219 = cljs.core.not.call(null,inst_43218);
var state_43237__$1 = (function (){var statearr_43242 = state_43237;
(statearr_43242[(8)] = inst_43217__$1);

return statearr_43242;
})();
if(inst_43219){
var statearr_43243_43263 = state_43237__$1;
(statearr_43243_43263[(1)] = (5));

} else {
var statearr_43244_43264 = state_43237__$1;
(statearr_43244_43264[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43238 === (6))){
var state_43237__$1 = state_43237;
var statearr_43245_43265 = state_43237__$1;
(statearr_43245_43265[(2)] = null);

(statearr_43245_43265[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43238 === (3))){
var inst_43234 = (state_43237[(2)]);
var inst_43235 = cljs.core.async.close_BANG_.call(null,out);
var state_43237__$1 = (function (){var statearr_43246 = state_43237;
(statearr_43246[(9)] = inst_43234);

return statearr_43246;
})();
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_43237__$1,inst_43235);
} else {
if((state_val_43238 === (2))){
var state_43237__$1 = state_43237;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_43237__$1,(4),ch);
} else {
if((state_val_43238 === (11))){
var inst_43217 = (state_43237[(8)]);
var inst_43226 = (state_43237[(2)]);
var inst_43214 = inst_43217;
var state_43237__$1 = (function (){var statearr_43247 = state_43237;
(statearr_43247[(10)] = inst_43226);

(statearr_43247[(7)] = inst_43214);

return statearr_43247;
})();
var statearr_43248_43266 = state_43237__$1;
(statearr_43248_43266[(2)] = null);

(statearr_43248_43266[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43238 === (9))){
var inst_43217 = (state_43237[(8)]);
var state_43237__$1 = state_43237;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_43237__$1,(11),out,inst_43217);
} else {
if((state_val_43238 === (5))){
var inst_43217 = (state_43237[(8)]);
var inst_43214 = (state_43237[(7)]);
var inst_43221 = cljs.core._EQ_.call(null,inst_43217,inst_43214);
var state_43237__$1 = state_43237;
if(inst_43221){
var statearr_43250_43267 = state_43237__$1;
(statearr_43250_43267[(1)] = (8));

} else {
var statearr_43251_43268 = state_43237__$1;
(statearr_43251_43268[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43238 === (10))){
var inst_43229 = (state_43237[(2)]);
var state_43237__$1 = state_43237;
var statearr_43252_43269 = state_43237__$1;
(statearr_43252_43269[(2)] = inst_43229);

(statearr_43252_43269[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43238 === (8))){
var inst_43214 = (state_43237[(7)]);
var tmp43249 = inst_43214;
var inst_43214__$1 = tmp43249;
var state_43237__$1 = (function (){var statearr_43253 = state_43237;
(statearr_43253[(7)] = inst_43214__$1);

return statearr_43253;
})();
var statearr_43254_43270 = state_43237__$1;
(statearr_43254_43270[(2)] = null);

(statearr_43254_43270[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__35058__auto___43260,out))
;
return ((function (switch__34968__auto__,c__35058__auto___43260,out){
return (function() {
var cljs$core$async$state_machine__34969__auto__ = null;
var cljs$core$async$state_machine__34969__auto____0 = (function (){
var statearr_43255 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_43255[(0)] = cljs$core$async$state_machine__34969__auto__);

(statearr_43255[(1)] = (1));

return statearr_43255;
});
var cljs$core$async$state_machine__34969__auto____1 = (function (state_43237){
while(true){
var ret_value__34970__auto__ = (function (){try{while(true){
var result__34971__auto__ = switch__34968__auto__.call(null,state_43237);
if(cljs.core.keyword_identical_QMARK_.call(null,result__34971__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__34971__auto__;
}
break;
}
}catch (e43256){if((e43256 instanceof Object)){
var ex__34972__auto__ = e43256;
var statearr_43257_43271 = state_43237;
(statearr_43257_43271[(5)] = ex__34972__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_43237);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e43256;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__34970__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__43272 = state_43237;
state_43237 = G__43272;
continue;
} else {
return ret_value__34970__auto__;
}
break;
}
});
cljs$core$async$state_machine__34969__auto__ = function(state_43237){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__34969__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__34969__auto____1.call(this,state_43237);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__34969__auto____0;
cljs$core$async$state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__34969__auto____1;
return cljs$core$async$state_machine__34969__auto__;
})()
;})(switch__34968__auto__,c__35058__auto___43260,out))
})();
var state__35060__auto__ = (function (){var statearr_43258 = f__35059__auto__.call(null);
(statearr_43258[(6)] = c__35058__auto___43260);

return statearr_43258;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__35060__auto__);
});})(c__35058__auto___43260,out))
);


return out;
});

cljs.core.async.unique.cljs$lang$maxFixedArity = 2;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.partition = (function cljs$core$async$partition(var_args){
var G__43274 = arguments.length;
switch (G__43274) {
case 2:
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.partition.cljs$core$IFn$_invoke$arity$2 = (function (n,ch){
return cljs.core.async.partition.call(null,n,ch,null);
});

cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3 = (function (n,ch,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__35058__auto___43340 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__35058__auto___43340,out){
return (function (){
var f__35059__auto__ = (function (){var switch__34968__auto__ = ((function (c__35058__auto___43340,out){
return (function (state_43312){
var state_val_43313 = (state_43312[(1)]);
if((state_val_43313 === (7))){
var inst_43308 = (state_43312[(2)]);
var state_43312__$1 = state_43312;
var statearr_43314_43341 = state_43312__$1;
(statearr_43314_43341[(2)] = inst_43308);

(statearr_43314_43341[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43313 === (1))){
var inst_43275 = (new Array(n));
var inst_43276 = inst_43275;
var inst_43277 = (0);
var state_43312__$1 = (function (){var statearr_43315 = state_43312;
(statearr_43315[(7)] = inst_43277);

(statearr_43315[(8)] = inst_43276);

return statearr_43315;
})();
var statearr_43316_43342 = state_43312__$1;
(statearr_43316_43342[(2)] = null);

(statearr_43316_43342[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43313 === (4))){
var inst_43280 = (state_43312[(9)]);
var inst_43280__$1 = (state_43312[(2)]);
var inst_43281 = (inst_43280__$1 == null);
var inst_43282 = cljs.core.not.call(null,inst_43281);
var state_43312__$1 = (function (){var statearr_43317 = state_43312;
(statearr_43317[(9)] = inst_43280__$1);

return statearr_43317;
})();
if(inst_43282){
var statearr_43318_43343 = state_43312__$1;
(statearr_43318_43343[(1)] = (5));

} else {
var statearr_43319_43344 = state_43312__$1;
(statearr_43319_43344[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43313 === (15))){
var inst_43302 = (state_43312[(2)]);
var state_43312__$1 = state_43312;
var statearr_43320_43345 = state_43312__$1;
(statearr_43320_43345[(2)] = inst_43302);

(statearr_43320_43345[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43313 === (13))){
var state_43312__$1 = state_43312;
var statearr_43321_43346 = state_43312__$1;
(statearr_43321_43346[(2)] = null);

(statearr_43321_43346[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43313 === (6))){
var inst_43277 = (state_43312[(7)]);
var inst_43298 = (inst_43277 > (0));
var state_43312__$1 = state_43312;
if(cljs.core.truth_(inst_43298)){
var statearr_43322_43347 = state_43312__$1;
(statearr_43322_43347[(1)] = (12));

} else {
var statearr_43323_43348 = state_43312__$1;
(statearr_43323_43348[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43313 === (3))){
var inst_43310 = (state_43312[(2)]);
var state_43312__$1 = state_43312;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_43312__$1,inst_43310);
} else {
if((state_val_43313 === (12))){
var inst_43276 = (state_43312[(8)]);
var inst_43300 = cljs.core.vec.call(null,inst_43276);
var state_43312__$1 = state_43312;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_43312__$1,(15),out,inst_43300);
} else {
if((state_val_43313 === (2))){
var state_43312__$1 = state_43312;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_43312__$1,(4),ch);
} else {
if((state_val_43313 === (11))){
var inst_43292 = (state_43312[(2)]);
var inst_43293 = (new Array(n));
var inst_43276 = inst_43293;
var inst_43277 = (0);
var state_43312__$1 = (function (){var statearr_43324 = state_43312;
(statearr_43324[(7)] = inst_43277);

(statearr_43324[(10)] = inst_43292);

(statearr_43324[(8)] = inst_43276);

return statearr_43324;
})();
var statearr_43325_43349 = state_43312__$1;
(statearr_43325_43349[(2)] = null);

(statearr_43325_43349[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43313 === (9))){
var inst_43276 = (state_43312[(8)]);
var inst_43290 = cljs.core.vec.call(null,inst_43276);
var state_43312__$1 = state_43312;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_43312__$1,(11),out,inst_43290);
} else {
if((state_val_43313 === (5))){
var inst_43277 = (state_43312[(7)]);
var inst_43285 = (state_43312[(11)]);
var inst_43276 = (state_43312[(8)]);
var inst_43280 = (state_43312[(9)]);
var inst_43284 = (inst_43276[inst_43277] = inst_43280);
var inst_43285__$1 = (inst_43277 + (1));
var inst_43286 = (inst_43285__$1 < n);
var state_43312__$1 = (function (){var statearr_43326 = state_43312;
(statearr_43326[(12)] = inst_43284);

(statearr_43326[(11)] = inst_43285__$1);

return statearr_43326;
})();
if(cljs.core.truth_(inst_43286)){
var statearr_43327_43350 = state_43312__$1;
(statearr_43327_43350[(1)] = (8));

} else {
var statearr_43328_43351 = state_43312__$1;
(statearr_43328_43351[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43313 === (14))){
var inst_43305 = (state_43312[(2)]);
var inst_43306 = cljs.core.async.close_BANG_.call(null,out);
var state_43312__$1 = (function (){var statearr_43330 = state_43312;
(statearr_43330[(13)] = inst_43305);

return statearr_43330;
})();
var statearr_43331_43352 = state_43312__$1;
(statearr_43331_43352[(2)] = inst_43306);

(statearr_43331_43352[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43313 === (10))){
var inst_43296 = (state_43312[(2)]);
var state_43312__$1 = state_43312;
var statearr_43332_43353 = state_43312__$1;
(statearr_43332_43353[(2)] = inst_43296);

(statearr_43332_43353[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43313 === (8))){
var inst_43285 = (state_43312[(11)]);
var inst_43276 = (state_43312[(8)]);
var tmp43329 = inst_43276;
var inst_43276__$1 = tmp43329;
var inst_43277 = inst_43285;
var state_43312__$1 = (function (){var statearr_43333 = state_43312;
(statearr_43333[(7)] = inst_43277);

(statearr_43333[(8)] = inst_43276__$1);

return statearr_43333;
})();
var statearr_43334_43354 = state_43312__$1;
(statearr_43334_43354[(2)] = null);

(statearr_43334_43354[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__35058__auto___43340,out))
;
return ((function (switch__34968__auto__,c__35058__auto___43340,out){
return (function() {
var cljs$core$async$state_machine__34969__auto__ = null;
var cljs$core$async$state_machine__34969__auto____0 = (function (){
var statearr_43335 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_43335[(0)] = cljs$core$async$state_machine__34969__auto__);

(statearr_43335[(1)] = (1));

return statearr_43335;
});
var cljs$core$async$state_machine__34969__auto____1 = (function (state_43312){
while(true){
var ret_value__34970__auto__ = (function (){try{while(true){
var result__34971__auto__ = switch__34968__auto__.call(null,state_43312);
if(cljs.core.keyword_identical_QMARK_.call(null,result__34971__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__34971__auto__;
}
break;
}
}catch (e43336){if((e43336 instanceof Object)){
var ex__34972__auto__ = e43336;
var statearr_43337_43355 = state_43312;
(statearr_43337_43355[(5)] = ex__34972__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_43312);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e43336;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__34970__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__43356 = state_43312;
state_43312 = G__43356;
continue;
} else {
return ret_value__34970__auto__;
}
break;
}
});
cljs$core$async$state_machine__34969__auto__ = function(state_43312){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__34969__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__34969__auto____1.call(this,state_43312);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__34969__auto____0;
cljs$core$async$state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__34969__auto____1;
return cljs$core$async$state_machine__34969__auto__;
})()
;})(switch__34968__auto__,c__35058__auto___43340,out))
})();
var state__35060__auto__ = (function (){var statearr_43338 = f__35059__auto__.call(null);
(statearr_43338[(6)] = c__35058__auto___43340);

return statearr_43338;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__35060__auto__);
});})(c__35058__auto___43340,out))
);


return out;
});

cljs.core.async.partition.cljs$lang$maxFixedArity = 3;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.partition_by = (function cljs$core$async$partition_by(var_args){
var G__43358 = arguments.length;
switch (G__43358) {
case 2:
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$2 = (function (f,ch){
return cljs.core.async.partition_by.call(null,f,ch,null);
});

cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3 = (function (f,ch,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__35058__auto___43428 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__35058__auto___43428,out){
return (function (){
var f__35059__auto__ = (function (){var switch__34968__auto__ = ((function (c__35058__auto___43428,out){
return (function (state_43400){
var state_val_43401 = (state_43400[(1)]);
if((state_val_43401 === (7))){
var inst_43396 = (state_43400[(2)]);
var state_43400__$1 = state_43400;
var statearr_43402_43429 = state_43400__$1;
(statearr_43402_43429[(2)] = inst_43396);

(statearr_43402_43429[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43401 === (1))){
var inst_43359 = [];
var inst_43360 = inst_43359;
var inst_43361 = new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",-69252123);
var state_43400__$1 = (function (){var statearr_43403 = state_43400;
(statearr_43403[(7)] = inst_43361);

(statearr_43403[(8)] = inst_43360);

return statearr_43403;
})();
var statearr_43404_43430 = state_43400__$1;
(statearr_43404_43430[(2)] = null);

(statearr_43404_43430[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43401 === (4))){
var inst_43364 = (state_43400[(9)]);
var inst_43364__$1 = (state_43400[(2)]);
var inst_43365 = (inst_43364__$1 == null);
var inst_43366 = cljs.core.not.call(null,inst_43365);
var state_43400__$1 = (function (){var statearr_43405 = state_43400;
(statearr_43405[(9)] = inst_43364__$1);

return statearr_43405;
})();
if(inst_43366){
var statearr_43406_43431 = state_43400__$1;
(statearr_43406_43431[(1)] = (5));

} else {
var statearr_43407_43432 = state_43400__$1;
(statearr_43407_43432[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43401 === (15))){
var inst_43390 = (state_43400[(2)]);
var state_43400__$1 = state_43400;
var statearr_43408_43433 = state_43400__$1;
(statearr_43408_43433[(2)] = inst_43390);

(statearr_43408_43433[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43401 === (13))){
var state_43400__$1 = state_43400;
var statearr_43409_43434 = state_43400__$1;
(statearr_43409_43434[(2)] = null);

(statearr_43409_43434[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43401 === (6))){
var inst_43360 = (state_43400[(8)]);
var inst_43385 = inst_43360.length;
var inst_43386 = (inst_43385 > (0));
var state_43400__$1 = state_43400;
if(cljs.core.truth_(inst_43386)){
var statearr_43410_43435 = state_43400__$1;
(statearr_43410_43435[(1)] = (12));

} else {
var statearr_43411_43436 = state_43400__$1;
(statearr_43411_43436[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43401 === (3))){
var inst_43398 = (state_43400[(2)]);
var state_43400__$1 = state_43400;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_43400__$1,inst_43398);
} else {
if((state_val_43401 === (12))){
var inst_43360 = (state_43400[(8)]);
var inst_43388 = cljs.core.vec.call(null,inst_43360);
var state_43400__$1 = state_43400;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_43400__$1,(15),out,inst_43388);
} else {
if((state_val_43401 === (2))){
var state_43400__$1 = state_43400;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_43400__$1,(4),ch);
} else {
if((state_val_43401 === (11))){
var inst_43368 = (state_43400[(10)]);
var inst_43364 = (state_43400[(9)]);
var inst_43378 = (state_43400[(2)]);
var inst_43379 = [];
var inst_43380 = inst_43379.push(inst_43364);
var inst_43360 = inst_43379;
var inst_43361 = inst_43368;
var state_43400__$1 = (function (){var statearr_43412 = state_43400;
(statearr_43412[(11)] = inst_43380);

(statearr_43412[(12)] = inst_43378);

(statearr_43412[(7)] = inst_43361);

(statearr_43412[(8)] = inst_43360);

return statearr_43412;
})();
var statearr_43413_43437 = state_43400__$1;
(statearr_43413_43437[(2)] = null);

(statearr_43413_43437[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43401 === (9))){
var inst_43360 = (state_43400[(8)]);
var inst_43376 = cljs.core.vec.call(null,inst_43360);
var state_43400__$1 = state_43400;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_43400__$1,(11),out,inst_43376);
} else {
if((state_val_43401 === (5))){
var inst_43368 = (state_43400[(10)]);
var inst_43364 = (state_43400[(9)]);
var inst_43361 = (state_43400[(7)]);
var inst_43368__$1 = f.call(null,inst_43364);
var inst_43369 = cljs.core._EQ_.call(null,inst_43368__$1,inst_43361);
var inst_43370 = cljs.core.keyword_identical_QMARK_.call(null,inst_43361,new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",-69252123));
var inst_43371 = (inst_43369) || (inst_43370);
var state_43400__$1 = (function (){var statearr_43414 = state_43400;
(statearr_43414[(10)] = inst_43368__$1);

return statearr_43414;
})();
if(cljs.core.truth_(inst_43371)){
var statearr_43415_43438 = state_43400__$1;
(statearr_43415_43438[(1)] = (8));

} else {
var statearr_43416_43439 = state_43400__$1;
(statearr_43416_43439[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43401 === (14))){
var inst_43393 = (state_43400[(2)]);
var inst_43394 = cljs.core.async.close_BANG_.call(null,out);
var state_43400__$1 = (function (){var statearr_43418 = state_43400;
(statearr_43418[(13)] = inst_43393);

return statearr_43418;
})();
var statearr_43419_43440 = state_43400__$1;
(statearr_43419_43440[(2)] = inst_43394);

(statearr_43419_43440[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43401 === (10))){
var inst_43383 = (state_43400[(2)]);
var state_43400__$1 = state_43400;
var statearr_43420_43441 = state_43400__$1;
(statearr_43420_43441[(2)] = inst_43383);

(statearr_43420_43441[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43401 === (8))){
var inst_43368 = (state_43400[(10)]);
var inst_43364 = (state_43400[(9)]);
var inst_43360 = (state_43400[(8)]);
var inst_43373 = inst_43360.push(inst_43364);
var tmp43417 = inst_43360;
var inst_43360__$1 = tmp43417;
var inst_43361 = inst_43368;
var state_43400__$1 = (function (){var statearr_43421 = state_43400;
(statearr_43421[(7)] = inst_43361);

(statearr_43421[(14)] = inst_43373);

(statearr_43421[(8)] = inst_43360__$1);

return statearr_43421;
})();
var statearr_43422_43442 = state_43400__$1;
(statearr_43422_43442[(2)] = null);

(statearr_43422_43442[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__35058__auto___43428,out))
;
return ((function (switch__34968__auto__,c__35058__auto___43428,out){
return (function() {
var cljs$core$async$state_machine__34969__auto__ = null;
var cljs$core$async$state_machine__34969__auto____0 = (function (){
var statearr_43423 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_43423[(0)] = cljs$core$async$state_machine__34969__auto__);

(statearr_43423[(1)] = (1));

return statearr_43423;
});
var cljs$core$async$state_machine__34969__auto____1 = (function (state_43400){
while(true){
var ret_value__34970__auto__ = (function (){try{while(true){
var result__34971__auto__ = switch__34968__auto__.call(null,state_43400);
if(cljs.core.keyword_identical_QMARK_.call(null,result__34971__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__34971__auto__;
}
break;
}
}catch (e43424){if((e43424 instanceof Object)){
var ex__34972__auto__ = e43424;
var statearr_43425_43443 = state_43400;
(statearr_43425_43443[(5)] = ex__34972__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_43400);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e43424;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__34970__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__43444 = state_43400;
state_43400 = G__43444;
continue;
} else {
return ret_value__34970__auto__;
}
break;
}
});
cljs$core$async$state_machine__34969__auto__ = function(state_43400){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__34969__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__34969__auto____1.call(this,state_43400);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__34969__auto____0;
cljs$core$async$state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__34969__auto____1;
return cljs$core$async$state_machine__34969__auto__;
})()
;})(switch__34968__auto__,c__35058__auto___43428,out))
})();
var state__35060__auto__ = (function (){var statearr_43426 = f__35059__auto__.call(null);
(statearr_43426[(6)] = c__35058__auto___43428);

return statearr_43426;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__35060__auto__);
});})(c__35058__auto___43428,out))
);


return out;
});

cljs.core.async.partition_by.cljs$lang$maxFixedArity = 3;


//# sourceMappingURL=async.js.map?rel=1507227566751

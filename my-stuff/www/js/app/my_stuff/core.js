// Compiled by ClojureScript 1.9.908 {}
goog.provide('my_stuff.core');
goog.require('cljs.core');
goog.require('reagent.core');
goog.require('my_stuff.states');
goog.require('jayq.core');
cljs.core.enable_console_print_BANG_.call(null);
my_stuff.core.by_id = (function my_stuff$core$by_id(id){
return document.getElementById(id);
});
my_stuff.core.log = (function my_stuff$core$log(var_args){
var args__31466__auto__ = [];
var len__31459__auto___47411 = arguments.length;
var i__31460__auto___47412 = (0);
while(true){
if((i__31460__auto___47412 < len__31459__auto___47411)){
args__31466__auto__.push((arguments[i__31460__auto___47412]));

var G__47413 = (i__31460__auto___47412 + (1));
i__31460__auto___47412 = G__47413;
continue;
} else {
}
break;
}

var argseq__31467__auto__ = ((((0) < args__31466__auto__.length))?(new cljs.core.IndexedSeq(args__31466__auto__.slice((0)),(0),null)):null);
return my_stuff.core.log.cljs$core$IFn$_invoke$arity$variadic(argseq__31467__auto__);
});

my_stuff.core.log.cljs$core$IFn$_invoke$arity$variadic = (function (msgs){
return cljs.core.doall.call(null,(function (){var iter__31064__auto__ = (function my_stuff$core$iter__47407(s__47408){
return (new cljs.core.LazySeq(null,(function (){
var s__47408__$1 = s__47408;
while(true){
var temp__5278__auto__ = cljs.core.seq.call(null,s__47408__$1);
if(temp__5278__auto__){
var s__47408__$2 = temp__5278__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,s__47408__$2)){
var c__31062__auto__ = cljs.core.chunk_first.call(null,s__47408__$2);
var size__31063__auto__ = cljs.core.count.call(null,c__31062__auto__);
var b__47410 = cljs.core.chunk_buffer.call(null,size__31063__auto__);
if((function (){var i__47409 = (0);
while(true){
if((i__47409 < size__31063__auto__)){
var msg = cljs.core._nth.call(null,c__31062__auto__,i__47409);
cljs.core.chunk_append.call(null,b__47410,console.log(msg));

var G__47414 = (i__47409 + (1));
i__47409 = G__47414;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__47410),my_stuff$core$iter__47407.call(null,cljs.core.chunk_rest.call(null,s__47408__$2)));
} else {
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__47410),null);
}
} else {
var msg = cljs.core.first.call(null,s__47408__$2);
return cljs.core.cons.call(null,console.log(msg),my_stuff$core$iter__47407.call(null,cljs.core.rest.call(null,s__47408__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__31064__auto__.call(null,msgs);
})());
});

my_stuff.core.log.cljs$lang$maxFixedArity = (0);

my_stuff.core.log.cljs$lang$applyTo = (function (seq47406){
return my_stuff.core.log.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq47406));
});

my_stuff.core.click_nav = (function my_stuff$core$click_nav(var_args){
var G__47416 = arguments.length;
switch (G__47416) {
case 0:
return my_stuff.core.click_nav.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return my_stuff.core.click_nav.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return my_stuff.core.click_nav.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

my_stuff.core.click_nav.cljs$core$IFn$_invoke$arity$0 = (function (){
return null;
});

my_stuff.core.click_nav.cljs$core$IFn$_invoke$arity$1 = (function (v){
return my_stuff.core.click_nav.call(null,new cljs.core.Keyword(null,"page","page",849072397),v);
});

my_stuff.core.click_nav.cljs$core$IFn$_invoke$arity$2 = (function (k,v){
my_stuff.core.click_nav.call(null);

return cljs.core.swap_BANG_.call(null,my_stuff.states.app_state,cljs.core.assoc,k,v);
});

my_stuff.core.click_nav.cljs$lang$maxFixedArity = 2;

my_stuff.core.nav = (function my_stuff$core$nav(){
return new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ul.side-nav","ul.side-nav",1381687621),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"id","id",-1388402092),"nav-ul"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",723558921),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",-2123407586),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"href","href",-793805698),"#",new cljs.core.Keyword(null,"on-click","on-click",1632826543),(function (e){
return my_stuff.core.click_nav.call(null,"home");
})], null),"Home"], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",723558921),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",-2123407586),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"href","href",-793805698),"#",new cljs.core.Keyword(null,"on-click","on-click",1632826543),(function (e){
return my_stuff.core.click_nav.call(null,"activities");
})], null),"Activities"], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",723558921),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",-2123407586),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"href","href",-793805698),"#",new cljs.core.Keyword(null,"on-click","on-click",1632826543),(function (e){
return my_stuff.core.click_nav.call(null,"contact-us");
})], null),"Contact us"], null)], null)], null);
});
my_stuff.core.cb_nav = cljs.core.with_meta.call(null,my_stuff.core.nav,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"component-did-mount","component-did-mount",-1126910518),(function (this$){
return jayq.core.$.call(null,".button-collapse").sideNav(cljs.core.clj__GT_js.call(null,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"closeOnClick","closeOnClick",174981882),true,new cljs.core.Keyword(null,"draggable","draggable",1676206163),true], null)));
})], null));
my_stuff.core.menu_bar = (function my_stuff$core$menu_bar(child){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.navbar-fixed","div.navbar-fixed",-404547015),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"nav","nav",719540477),new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.nav-wrapper","div.nav-wrapper",2090748862),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"className","className",-1983287057),"nav-bar-bg light-blue darken-3"], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a.brand-logo","a.brand-logo",1920204378),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"href","href",-793805698),"#!"], null),"Football app"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",-2123407586),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"data-activates","data-activates",1521953804),"mobile-demo",new cljs.core.Keyword(null,"className","className",-1983287057),"button-collapse",new cljs.core.Keyword(null,"href","href",-793805698),"#"], null)], null),child], null)], null)], null);
});
my_stuff.core.container = (function my_stuff$core$container(){
return new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [my_stuff.core.menu_bar], null);
});
my_stuff.core.mount_root = (function my_stuff$core$mount_root(){
return reagent.core.render.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [my_stuff.core.container], null),my_stuff.core.by_id.call(null,"container"));
});
document.addEventListener("deviceready",my_stuff.core.mount_root,false);
my_stuff.core.reg_default = (function my_stuff$core$reg_default(){
var pred__47418 = cljs.core._EQ_;
var expr__47419 = new cljs.core.Keyword(null,"page","page",849072397).cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"reg-info","reg-info",-809443695).cljs$core$IFn$_invoke$arity$1(cljs.core.deref.call(null,my_stuff.states.app_state)));
return "start";
});

//# sourceMappingURL=core.js.map?rel=1507228485158

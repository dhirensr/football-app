(ns my-stuff.states
  (:require [reagent.core :as r]))

(defonce profile "dev")

(defonce default-app-state {:reg-info {:page "splash-screen"
                                       :bg-image "splash-screen"}})

(defonce app-state (r/atom default-app-state))

(defn set-state [params]
  (swap! app-state merge params))

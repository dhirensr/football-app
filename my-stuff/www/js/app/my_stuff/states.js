// Compiled by ClojureScript 1.9.908 {}
goog.provide('my_stuff.states');
goog.require('cljs.core');
goog.require('reagent.core');
if(typeof my_stuff.states.profile !== 'undefined'){
} else {
my_stuff.states.profile = "dev";
}
if(typeof my_stuff.states.default_app_state !== 'undefined'){
} else {
my_stuff.states.default_app_state = new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"reg-info","reg-info",-809443695),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"page","page",849072397),"splash-screen",new cljs.core.Keyword(null,"bg-image","bg-image",-1707022052),"splash-screen"], null)], null);
}
if(typeof my_stuff.states.app_state !== 'undefined'){
} else {
my_stuff.states.app_state = reagent.core.atom.call(null,my_stuff.states.default_app_state);
}
my_stuff.states.set_state = (function my_stuff$states$set_state(params){
return cljs.core.swap_BANG_.call(null,my_stuff.states.app_state,cljs.core.merge,params);
});

//# sourceMappingURL=states.js.map?rel=1507227565763

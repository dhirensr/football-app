(ns my-stuff.core
  (:require [reagent.core :as r]
            [my-stuff.states :as s]
            [jayq.core :as jq]))

(enable-console-print!)


(defn by-id [id] (.getElementById js/document id))

(defn log [& msgs] (doall (for [msg msgs] (.log js/console msg))))


#_(defn splash-screen []
  (r/create-class
   {:component-did-mount
    (fn []
      (js/setTimeout #(swap! s/app-state update-in [:reg-info]
                             merge {:page "start"})
                     2000))
    :reagent-render
    (fn []
      [:div.splash-screen])}))

(defn click-nav
  ([] nil)
  ([v] (click-nav :page v))
  ([k v]
   (click-nav)
   (swap! s/app-state assoc k v)))

(defn nav []
  [:ul.side-nav {:id "nav-ul"}
   [:li [:a {:href "#" :on-click (fn [e] (click-nav "home"))}
         "Home"]]
   [:li [:a {:href "#" :on-click (fn [e] (click-nav "activities"))}
         "Activities"]]
   [:li [:a {:href "#" :on-click (fn [e] (click-nav "contact-us"))}
         "Contact us"]]])

(def cb-nav
  (with-meta nav
    {:component-did-mount
     (fn [this]
       (.sideNav (jq/$ ".button-collapse") (clj->js {:closeOnClick true
                                                     :draggable true})))}))

(defn menu-bar [child]
  ;;http://materializecss.com/navbar.html
  [:div.navbar-fixed
     [:nav
      [:div.nav-wrapper {:className "nav-bar-bg light-blue darken-3"}
       [:a.brand-logo {:href "#!"} "Football app"]
       [:a {:data-activates "mobile-demo"
            :className "button-collapse"
            :href "#"}]
       child]]])



#_(defn start-page []
  (r/create-class
   {:component-did-mount #(.slick (js/$ ".single-item") #js{:dots true
                                                            :centered false})
    :reagent-render
    (fn []
      [:div.container
       [:div.single-item
        [:div.size [:h3 "1"]]
        [:div  [:h3 "2"]]
        [:div  [:h3 "3"]]
        [:div  [:h3 "4"]]]])}))

(defn container []
  [menu-bar])

(defn mount-root
  ;;;Start of the application
  []
  (r/render [container] (by-id "container")))

(.addEventListener js/document "deviceready"
                   mount-root
                   false)



(defn reg-default []
  (condp = (:page (:reg-info @s/app-state))
    "start"  #_[start-page]))

// Compiled by ClojureScript 1.9.908 {}
goog.provide('figwheel.client');
goog.require('cljs.core');
goog.require('goog.Uri');
goog.require('goog.userAgent.product');
goog.require('goog.object');
goog.require('cljs.reader');
goog.require('cljs.core.async');
goog.require('figwheel.client.socket');
goog.require('figwheel.client.utils');
goog.require('figwheel.client.heads_up');
goog.require('figwheel.client.file_reloading');
goog.require('clojure.string');
goog.require('cljs.repl');
figwheel.client._figwheel_version_ = "0.5.13";
figwheel.client.js_stringify = (((typeof JSON !== 'undefined') && (!((JSON.stringify == null))))?(function (x){
return ["#js ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(JSON.stringify(x,null," "))].join('');
}):(function (x){
try{return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(x)].join('');
}catch (e46125){if((e46125 instanceof Error)){
var e = e46125;
return "Error: Unable to stringify";
} else {
throw e46125;

}
}}));
figwheel.client.figwheel_repl_print = (function figwheel$client$figwheel_repl_print(var_args){
var G__46128 = arguments.length;
switch (G__46128) {
case 2:
return figwheel.client.figwheel_repl_print.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 1:
return figwheel.client.figwheel_repl_print.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

figwheel.client.figwheel_repl_print.cljs$core$IFn$_invoke$arity$2 = (function (stream,args){
figwheel.client.socket.send_BANG_.call(null,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"figwheel-event","figwheel-event",519570592),"callback",new cljs.core.Keyword(null,"callback-name","callback-name",336964714),"figwheel-repl-print",new cljs.core.Keyword(null,"content","content",15833224),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"stream","stream",1534941648),stream,new cljs.core.Keyword(null,"args","args",1315556576),cljs.core.mapv.call(null,(function (p1__46126_SHARP_){
if(typeof p1__46126_SHARP_ === 'string'){
return p1__46126_SHARP_;
} else {
return figwheel.client.js_stringify.call(null,p1__46126_SHARP_);
}
}),args)], null)], null));

return null;
});

figwheel.client.figwheel_repl_print.cljs$core$IFn$_invoke$arity$1 = (function (args){
return figwheel.client.figwheel_repl_print.call(null,new cljs.core.Keyword(null,"out","out",-910545517),args);
});

figwheel.client.figwheel_repl_print.cljs$lang$maxFixedArity = 2;

figwheel.client.console_out_print = (function figwheel$client$console_out_print(args){
return console.log.apply(console,cljs.core.into_array.call(null,args));
});
figwheel.client.console_err_print = (function figwheel$client$console_err_print(args){
return console.error.apply(console,cljs.core.into_array.call(null,args));
});
figwheel.client.repl_out_print_fn = (function figwheel$client$repl_out_print_fn(var_args){
var args__31466__auto__ = [];
var len__31459__auto___46131 = arguments.length;
var i__31460__auto___46132 = (0);
while(true){
if((i__31460__auto___46132 < len__31459__auto___46131)){
args__31466__auto__.push((arguments[i__31460__auto___46132]));

var G__46133 = (i__31460__auto___46132 + (1));
i__31460__auto___46132 = G__46133;
continue;
} else {
}
break;
}

var argseq__31467__auto__ = ((((0) < args__31466__auto__.length))?(new cljs.core.IndexedSeq(args__31466__auto__.slice((0)),(0),null)):null);
return figwheel.client.repl_out_print_fn.cljs$core$IFn$_invoke$arity$variadic(argseq__31467__auto__);
});

figwheel.client.repl_out_print_fn.cljs$core$IFn$_invoke$arity$variadic = (function (args){
figwheel.client.console_out_print.call(null,args);

figwheel.client.figwheel_repl_print.call(null,new cljs.core.Keyword(null,"out","out",-910545517),args);

return null;
});

figwheel.client.repl_out_print_fn.cljs$lang$maxFixedArity = (0);

figwheel.client.repl_out_print_fn.cljs$lang$applyTo = (function (seq46130){
return figwheel.client.repl_out_print_fn.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq46130));
});

figwheel.client.repl_err_print_fn = (function figwheel$client$repl_err_print_fn(var_args){
var args__31466__auto__ = [];
var len__31459__auto___46135 = arguments.length;
var i__31460__auto___46136 = (0);
while(true){
if((i__31460__auto___46136 < len__31459__auto___46135)){
args__31466__auto__.push((arguments[i__31460__auto___46136]));

var G__46137 = (i__31460__auto___46136 + (1));
i__31460__auto___46136 = G__46137;
continue;
} else {
}
break;
}

var argseq__31467__auto__ = ((((0) < args__31466__auto__.length))?(new cljs.core.IndexedSeq(args__31466__auto__.slice((0)),(0),null)):null);
return figwheel.client.repl_err_print_fn.cljs$core$IFn$_invoke$arity$variadic(argseq__31467__auto__);
});

figwheel.client.repl_err_print_fn.cljs$core$IFn$_invoke$arity$variadic = (function (args){
figwheel.client.console_err_print.call(null,args);

figwheel.client.figwheel_repl_print.call(null,new cljs.core.Keyword(null,"err","err",-2089457205),args);

return null;
});

figwheel.client.repl_err_print_fn.cljs$lang$maxFixedArity = (0);

figwheel.client.repl_err_print_fn.cljs$lang$applyTo = (function (seq46134){
return figwheel.client.repl_err_print_fn.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq46134));
});

figwheel.client.enable_repl_print_BANG_ = (function figwheel$client$enable_repl_print_BANG_(){
cljs.core._STAR_print_newline_STAR_ = false;

cljs.core.set_print_fn_BANG_.call(null,figwheel.client.repl_out_print_fn);

cljs.core.set_print_err_fn_BANG_.call(null,figwheel.client.repl_err_print_fn);

return null;
});
figwheel.client.autoload_QMARK_ = (function figwheel$client$autoload_QMARK_(){
return figwheel.client.utils.persistent_config_get.call(null,new cljs.core.Keyword(null,"figwheel-autoload","figwheel-autoload",-2044741728),true);
});
figwheel.client.toggle_autoload = (function figwheel$client$toggle_autoload(){
var res = figwheel.client.utils.persistent_config_set_BANG_.call(null,new cljs.core.Keyword(null,"figwheel-autoload","figwheel-autoload",-2044741728),cljs.core.not.call(null,figwheel.client.autoload_QMARK_.call(null)));
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"info","info",-317069002),["Toggle autoload deprecated! Use (figwheel.client/set-autoload! false)"].join(''));

figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"info","info",-317069002),["Figwheel autoloading ",cljs.core.str.cljs$core$IFn$_invoke$arity$1((cljs.core.truth_(figwheel.client.autoload_QMARK_.call(null))?"ON":"OFF"))].join(''));

return res;
});
goog.exportSymbol('figwheel.client.toggle_autoload', figwheel.client.toggle_autoload);
/**
 * Figwheel by default loads code changes as you work. Sometimes you
 *   just want to work on your code without the ramifications of
 *   autoloading and simply load your code piecemeal in the REPL. You can
 *   turn autoloading on and of with this method.
 * 
 *   (figwheel.client/set-autoload false)
 * 
 *   NOTE: This is a persistent setting, meaning that it will persist
 *   through browser reloads.
 */
figwheel.client.set_autoload = (function figwheel$client$set_autoload(b){
if((b === true) || (b === false)){
} else {
throw (new Error("Assert failed: (or (true? b) (false? b))"));
}

return figwheel.client.utils.persistent_config_set_BANG_.call(null,new cljs.core.Keyword(null,"figwheel-autoload","figwheel-autoload",-2044741728),b);
});
goog.exportSymbol('figwheel.client.set_autoload', figwheel.client.set_autoload);
figwheel.client.repl_pprint = (function figwheel$client$repl_pprint(){
return figwheel.client.utils.persistent_config_get.call(null,new cljs.core.Keyword(null,"figwheel-repl-pprint","figwheel-repl-pprint",1076150873),true);
});
goog.exportSymbol('figwheel.client.repl_pprint', figwheel.client.repl_pprint);
/**
 * This method gives you the ability to turn the pretty printing of
 *   the REPL's return value on and off.
 * 
 *   (figwheel.client/set-repl-pprint false)
 * 
 *   NOTE: This is a persistent setting, meaning that it will persist
 *   through browser reloads.
 */
figwheel.client.set_repl_pprint = (function figwheel$client$set_repl_pprint(b){
if((b === true) || (b === false)){
} else {
throw (new Error("Assert failed: (or (true? b) (false? b))"));
}

return figwheel.client.utils.persistent_config_set_BANG_.call(null,new cljs.core.Keyword(null,"figwheel-repl-pprint","figwheel-repl-pprint",1076150873),b);
});
goog.exportSymbol('figwheel.client.set_repl_pprint', figwheel.client.set_repl_pprint);
figwheel.client.repl_result_pr_str = (function figwheel$client$repl_result_pr_str(v){
if(cljs.core.truth_(figwheel.client.repl_pprint.call(null))){
return figwheel.client.utils.pprint_to_string.call(null,v);
} else {
return cljs.core.pr_str.call(null,v);
}
});
goog.exportSymbol('figwheel.client.repl_result_pr_str', figwheel.client.repl_result_pr_str);
figwheel.client.get_essential_messages = (function figwheel$client$get_essential_messages(ed){
if(cljs.core.truth_(ed)){
return cljs.core.cons.call(null,cljs.core.select_keys.call(null,ed,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"message","message",-406056002),new cljs.core.Keyword(null,"class","class",-2030961996)], null)),figwheel.client.get_essential_messages.call(null,new cljs.core.Keyword(null,"cause","cause",231901252).cljs$core$IFn$_invoke$arity$1(ed)));
} else {
return null;
}
});
figwheel.client.error_msg_format = (function figwheel$client$error_msg_format(p__46138){
var map__46139 = p__46138;
var map__46139__$1 = ((((!((map__46139 == null)))?((((map__46139.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__46139.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__46139):map__46139);
var message = cljs.core.get.call(null,map__46139__$1,new cljs.core.Keyword(null,"message","message",-406056002));
var class$ = cljs.core.get.call(null,map__46139__$1,new cljs.core.Keyword(null,"class","class",-2030961996));
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(class$)," : ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(message)].join('');
});
figwheel.client.format_messages = cljs.core.comp.call(null,cljs.core.partial.call(null,cljs.core.map,figwheel.client.error_msg_format),figwheel.client.get_essential_messages);
figwheel.client.focus_msgs = (function figwheel$client$focus_msgs(name_set,msg_hist){
return cljs.core.cons.call(null,cljs.core.first.call(null,msg_hist),cljs.core.filter.call(null,cljs.core.comp.call(null,name_set,new cljs.core.Keyword(null,"msg-name","msg-name",-353709863)),cljs.core.rest.call(null,msg_hist)));
});
figwheel.client.reload_file_QMARK__STAR_ = (function figwheel$client$reload_file_QMARK__STAR_(msg_name,opts){
var or__30182__auto__ = new cljs.core.Keyword(null,"load-warninged-code","load-warninged-code",-2030345223).cljs$core$IFn$_invoke$arity$1(opts);
if(cljs.core.truth_(or__30182__auto__)){
return or__30182__auto__;
} else {
return cljs.core.not_EQ_.call(null,msg_name,new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356));
}
});
figwheel.client.reload_file_state_QMARK_ = (function figwheel$client$reload_file_state_QMARK_(msg_names,opts){
var and__30170__auto__ = cljs.core._EQ_.call(null,cljs.core.first.call(null,msg_names),new cljs.core.Keyword(null,"files-changed","files-changed",-1418200563));
if(and__30170__auto__){
return figwheel.client.reload_file_QMARK__STAR_.call(null,cljs.core.second.call(null,msg_names),opts);
} else {
return and__30170__auto__;
}
});
figwheel.client.block_reload_file_state_QMARK_ = (function figwheel$client$block_reload_file_state_QMARK_(msg_names,opts){
return (cljs.core._EQ_.call(null,cljs.core.first.call(null,msg_names),new cljs.core.Keyword(null,"files-changed","files-changed",-1418200563))) && (cljs.core.not.call(null,figwheel.client.reload_file_QMARK__STAR_.call(null,cljs.core.second.call(null,msg_names),opts)));
});
figwheel.client.warning_append_state_QMARK_ = (function figwheel$client$warning_append_state_QMARK_(msg_names){
return cljs.core._EQ_.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356),new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356)], null),cljs.core.take.call(null,(2),msg_names));
});
figwheel.client.warning_state_QMARK_ = (function figwheel$client$warning_state_QMARK_(msg_names){
return cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356),cljs.core.first.call(null,msg_names));
});
figwheel.client.rewarning_state_QMARK_ = (function figwheel$client$rewarning_state_QMARK_(msg_names){
return cljs.core._EQ_.call(null,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356),new cljs.core.Keyword(null,"files-changed","files-changed",-1418200563),new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356)], null),cljs.core.take.call(null,(3),msg_names));
});
figwheel.client.compile_fail_state_QMARK_ = (function figwheel$client$compile_fail_state_QMARK_(msg_names){
return cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"compile-failed","compile-failed",-477639289),cljs.core.first.call(null,msg_names));
});
figwheel.client.compile_refail_state_QMARK_ = (function figwheel$client$compile_refail_state_QMARK_(msg_names){
return cljs.core._EQ_.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"compile-failed","compile-failed",-477639289),new cljs.core.Keyword(null,"compile-failed","compile-failed",-477639289)], null),cljs.core.take.call(null,(2),msg_names));
});
figwheel.client.css_loaded_state_QMARK_ = (function figwheel$client$css_loaded_state_QMARK_(msg_names){
return cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"css-files-changed","css-files-changed",720773874),cljs.core.first.call(null,msg_names));
});
figwheel.client.file_reloader_plugin = (function figwheel$client$file_reloader_plugin(opts){
var ch = cljs.core.async.chan.call(null);
var c__35058__auto___46218 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__35058__auto___46218,ch){
return (function (){
var f__35059__auto__ = (function (){var switch__34968__auto__ = ((function (c__35058__auto___46218,ch){
return (function (state_46190){
var state_val_46191 = (state_46190[(1)]);
if((state_val_46191 === (7))){
var inst_46186 = (state_46190[(2)]);
var state_46190__$1 = state_46190;
var statearr_46192_46219 = state_46190__$1;
(statearr_46192_46219[(2)] = inst_46186);

(statearr_46192_46219[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46191 === (1))){
var state_46190__$1 = state_46190;
var statearr_46193_46220 = state_46190__$1;
(statearr_46193_46220[(2)] = null);

(statearr_46193_46220[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46191 === (4))){
var inst_46143 = (state_46190[(7)]);
var inst_46143__$1 = (state_46190[(2)]);
var state_46190__$1 = (function (){var statearr_46194 = state_46190;
(statearr_46194[(7)] = inst_46143__$1);

return statearr_46194;
})();
if(cljs.core.truth_(inst_46143__$1)){
var statearr_46195_46221 = state_46190__$1;
(statearr_46195_46221[(1)] = (5));

} else {
var statearr_46196_46222 = state_46190__$1;
(statearr_46196_46222[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46191 === (15))){
var inst_46150 = (state_46190[(8)]);
var inst_46165 = new cljs.core.Keyword(null,"files","files",-472457450).cljs$core$IFn$_invoke$arity$1(inst_46150);
var inst_46166 = cljs.core.first.call(null,inst_46165);
var inst_46167 = new cljs.core.Keyword(null,"file","file",-1269645878).cljs$core$IFn$_invoke$arity$1(inst_46166);
var inst_46168 = ["Figwheel: Not loading code with warnings - ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(inst_46167)].join('');
var inst_46169 = figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"warn","warn",-436710552),inst_46168);
var state_46190__$1 = state_46190;
var statearr_46197_46223 = state_46190__$1;
(statearr_46197_46223[(2)] = inst_46169);

(statearr_46197_46223[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46191 === (13))){
var inst_46174 = (state_46190[(2)]);
var state_46190__$1 = state_46190;
var statearr_46198_46224 = state_46190__$1;
(statearr_46198_46224[(2)] = inst_46174);

(statearr_46198_46224[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46191 === (6))){
var state_46190__$1 = state_46190;
var statearr_46199_46225 = state_46190__$1;
(statearr_46199_46225[(2)] = null);

(statearr_46199_46225[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46191 === (17))){
var inst_46172 = (state_46190[(2)]);
var state_46190__$1 = state_46190;
var statearr_46200_46226 = state_46190__$1;
(statearr_46200_46226[(2)] = inst_46172);

(statearr_46200_46226[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46191 === (3))){
var inst_46188 = (state_46190[(2)]);
var state_46190__$1 = state_46190;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_46190__$1,inst_46188);
} else {
if((state_val_46191 === (12))){
var inst_46149 = (state_46190[(9)]);
var inst_46163 = figwheel.client.block_reload_file_state_QMARK_.call(null,inst_46149,opts);
var state_46190__$1 = state_46190;
if(cljs.core.truth_(inst_46163)){
var statearr_46201_46227 = state_46190__$1;
(statearr_46201_46227[(1)] = (15));

} else {
var statearr_46202_46228 = state_46190__$1;
(statearr_46202_46228[(1)] = (16));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46191 === (2))){
var state_46190__$1 = state_46190;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_46190__$1,(4),ch);
} else {
if((state_val_46191 === (11))){
var inst_46150 = (state_46190[(8)]);
var inst_46155 = cljs.core.PersistentVector.EMPTY_NODE;
var inst_46156 = figwheel.client.file_reloading.reload_js_files.call(null,opts,inst_46150);
var inst_46157 = cljs.core.async.timeout.call(null,(1000));
var inst_46158 = [inst_46156,inst_46157];
var inst_46159 = (new cljs.core.PersistentVector(null,2,(5),inst_46155,inst_46158,null));
var state_46190__$1 = state_46190;
return cljs.core.async.ioc_alts_BANG_.call(null,state_46190__$1,(14),inst_46159);
} else {
if((state_val_46191 === (9))){
var inst_46150 = (state_46190[(8)]);
var inst_46176 = figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"warn","warn",-436710552),"Figwheel: code autoloading is OFF");
var inst_46177 = new cljs.core.Keyword(null,"files","files",-472457450).cljs$core$IFn$_invoke$arity$1(inst_46150);
var inst_46178 = cljs.core.map.call(null,new cljs.core.Keyword(null,"file","file",-1269645878),inst_46177);
var inst_46179 = ["Not loading: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(inst_46178)].join('');
var inst_46180 = figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"info","info",-317069002),inst_46179);
var state_46190__$1 = (function (){var statearr_46203 = state_46190;
(statearr_46203[(10)] = inst_46176);

return statearr_46203;
})();
var statearr_46204_46229 = state_46190__$1;
(statearr_46204_46229[(2)] = inst_46180);

(statearr_46204_46229[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46191 === (5))){
var inst_46143 = (state_46190[(7)]);
var inst_46145 = [new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356),null,new cljs.core.Keyword(null,"files-changed","files-changed",-1418200563),null];
var inst_46146 = (new cljs.core.PersistentArrayMap(null,2,inst_46145,null));
var inst_46147 = (new cljs.core.PersistentHashSet(null,inst_46146,null));
var inst_46148 = figwheel.client.focus_msgs.call(null,inst_46147,inst_46143);
var inst_46149 = cljs.core.map.call(null,new cljs.core.Keyword(null,"msg-name","msg-name",-353709863),inst_46148);
var inst_46150 = cljs.core.first.call(null,inst_46148);
var inst_46151 = figwheel.client.autoload_QMARK_.call(null);
var state_46190__$1 = (function (){var statearr_46205 = state_46190;
(statearr_46205[(8)] = inst_46150);

(statearr_46205[(9)] = inst_46149);

return statearr_46205;
})();
if(cljs.core.truth_(inst_46151)){
var statearr_46206_46230 = state_46190__$1;
(statearr_46206_46230[(1)] = (8));

} else {
var statearr_46207_46231 = state_46190__$1;
(statearr_46207_46231[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46191 === (14))){
var inst_46161 = (state_46190[(2)]);
var state_46190__$1 = state_46190;
var statearr_46208_46232 = state_46190__$1;
(statearr_46208_46232[(2)] = inst_46161);

(statearr_46208_46232[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46191 === (16))){
var state_46190__$1 = state_46190;
var statearr_46209_46233 = state_46190__$1;
(statearr_46209_46233[(2)] = null);

(statearr_46209_46233[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46191 === (10))){
var inst_46182 = (state_46190[(2)]);
var state_46190__$1 = (function (){var statearr_46210 = state_46190;
(statearr_46210[(11)] = inst_46182);

return statearr_46210;
})();
var statearr_46211_46234 = state_46190__$1;
(statearr_46211_46234[(2)] = null);

(statearr_46211_46234[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46191 === (8))){
var inst_46149 = (state_46190[(9)]);
var inst_46153 = figwheel.client.reload_file_state_QMARK_.call(null,inst_46149,opts);
var state_46190__$1 = state_46190;
if(cljs.core.truth_(inst_46153)){
var statearr_46212_46235 = state_46190__$1;
(statearr_46212_46235[(1)] = (11));

} else {
var statearr_46213_46236 = state_46190__$1;
(statearr_46213_46236[(1)] = (12));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__35058__auto___46218,ch))
;
return ((function (switch__34968__auto__,c__35058__auto___46218,ch){
return (function() {
var figwheel$client$file_reloader_plugin_$_state_machine__34969__auto__ = null;
var figwheel$client$file_reloader_plugin_$_state_machine__34969__auto____0 = (function (){
var statearr_46214 = [null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_46214[(0)] = figwheel$client$file_reloader_plugin_$_state_machine__34969__auto__);

(statearr_46214[(1)] = (1));

return statearr_46214;
});
var figwheel$client$file_reloader_plugin_$_state_machine__34969__auto____1 = (function (state_46190){
while(true){
var ret_value__34970__auto__ = (function (){try{while(true){
var result__34971__auto__ = switch__34968__auto__.call(null,state_46190);
if(cljs.core.keyword_identical_QMARK_.call(null,result__34971__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__34971__auto__;
}
break;
}
}catch (e46215){if((e46215 instanceof Object)){
var ex__34972__auto__ = e46215;
var statearr_46216_46237 = state_46190;
(statearr_46216_46237[(5)] = ex__34972__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_46190);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e46215;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__34970__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__46238 = state_46190;
state_46190 = G__46238;
continue;
} else {
return ret_value__34970__auto__;
}
break;
}
});
figwheel$client$file_reloader_plugin_$_state_machine__34969__auto__ = function(state_46190){
switch(arguments.length){
case 0:
return figwheel$client$file_reloader_plugin_$_state_machine__34969__auto____0.call(this);
case 1:
return figwheel$client$file_reloader_plugin_$_state_machine__34969__auto____1.call(this,state_46190);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
figwheel$client$file_reloader_plugin_$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$file_reloader_plugin_$_state_machine__34969__auto____0;
figwheel$client$file_reloader_plugin_$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$file_reloader_plugin_$_state_machine__34969__auto____1;
return figwheel$client$file_reloader_plugin_$_state_machine__34969__auto__;
})()
;})(switch__34968__auto__,c__35058__auto___46218,ch))
})();
var state__35060__auto__ = (function (){var statearr_46217 = f__35059__auto__.call(null);
(statearr_46217[(6)] = c__35058__auto___46218);

return statearr_46217;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__35060__auto__);
});})(c__35058__auto___46218,ch))
);


return ((function (ch){
return (function (msg_hist){
cljs.core.async.put_BANG_.call(null,ch,msg_hist);

return msg_hist;
});
;})(ch))
});
figwheel.client.truncate_stack_trace = (function figwheel$client$truncate_stack_trace(stack_str){
return cljs.core.take_while.call(null,(function (p1__46239_SHARP_){
return cljs.core.not.call(null,cljs.core.re_matches.call(null,/.*eval_javascript_STAR__STAR_.*/,p1__46239_SHARP_));
}),clojure.string.split_lines.call(null,stack_str));
});
figwheel.client.get_ua_product = (function figwheel$client$get_ua_product(){
if(cljs.core.truth_(figwheel.client.utils.node_env_QMARK_.call(null))){
return new cljs.core.Keyword(null,"chrome","chrome",1718738387);
} else {
if(cljs.core.truth_(goog.userAgent.product.SAFARI)){
return new cljs.core.Keyword(null,"safari","safari",497115653);
} else {
if(cljs.core.truth_(goog.userAgent.product.CHROME)){
return new cljs.core.Keyword(null,"chrome","chrome",1718738387);
} else {
if(cljs.core.truth_(goog.userAgent.product.FIREFOX)){
return new cljs.core.Keyword(null,"firefox","firefox",1283768880);
} else {
if(cljs.core.truth_(goog.userAgent.product.IE)){
return new cljs.core.Keyword(null,"ie","ie",2038473780);
} else {
return null;
}
}
}
}
}
});
var base_path_46241 = figwheel.client.utils.base_url_path.call(null);
figwheel.client.eval_javascript_STAR__STAR_ = ((function (base_path_46241){
return (function figwheel$client$eval_javascript_STAR__STAR_(code,opts,result_handler){
try{figwheel.client.enable_repl_print_BANG_.call(null);

var result_value = figwheel.client.utils.eval_helper.call(null,code,opts);
return result_handler.call(null,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"status","status",-1997798413),new cljs.core.Keyword(null,"success","success",1890645906),new cljs.core.Keyword(null,"ua-product","ua-product",938384227),figwheel.client.get_ua_product.call(null),new cljs.core.Keyword(null,"value","value",305978217),result_value], null));
}catch (e46240){if((e46240 instanceof Error)){
var e = e46240;
return result_handler.call(null,new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"status","status",-1997798413),new cljs.core.Keyword(null,"exception","exception",-335277064),new cljs.core.Keyword(null,"value","value",305978217),cljs.core.pr_str.call(null,e),new cljs.core.Keyword(null,"ua-product","ua-product",938384227),figwheel.client.get_ua_product.call(null),new cljs.core.Keyword(null,"stacktrace","stacktrace",-95588394),clojure.string.join.call(null,"\n",figwheel.client.truncate_stack_trace.call(null,e.stack)),new cljs.core.Keyword(null,"base-path","base-path",495760020),base_path_46241], null));
} else {
var e = e46240;
return result_handler.call(null,new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"status","status",-1997798413),new cljs.core.Keyword(null,"exception","exception",-335277064),new cljs.core.Keyword(null,"ua-product","ua-product",938384227),figwheel.client.get_ua_product.call(null),new cljs.core.Keyword(null,"value","value",305978217),cljs.core.pr_str.call(null,e),new cljs.core.Keyword(null,"stacktrace","stacktrace",-95588394),"No stacktrace available."], null));

}
}finally {figwheel.client.enable_repl_print_BANG_.call(null);
}});})(base_path_46241))
;
/**
 * The REPL can disconnect and reconnect lets ensure cljs.user exists at least.
 */
figwheel.client.ensure_cljs_user = (function figwheel$client$ensure_cljs_user(){
if(cljs.core.truth_(cljs.user)){
return null;
} else {
return cljs.user = ({});
}
});
figwheel.client.repl_plugin = (function figwheel$client$repl_plugin(p__46242){
var map__46243 = p__46242;
var map__46243__$1 = ((((!((map__46243 == null)))?((((map__46243.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__46243.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__46243):map__46243);
var opts = map__46243__$1;
var build_id = cljs.core.get.call(null,map__46243__$1,new cljs.core.Keyword(null,"build-id","build-id",1642831089));
return ((function (map__46243,map__46243__$1,opts,build_id){
return (function (p__46245){
var vec__46246 = p__46245;
var seq__46247 = cljs.core.seq.call(null,vec__46246);
var first__46248 = cljs.core.first.call(null,seq__46247);
var seq__46247__$1 = cljs.core.next.call(null,seq__46247);
var map__46249 = first__46248;
var map__46249__$1 = ((((!((map__46249 == null)))?((((map__46249.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__46249.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__46249):map__46249);
var msg = map__46249__$1;
var msg_name = cljs.core.get.call(null,map__46249__$1,new cljs.core.Keyword(null,"msg-name","msg-name",-353709863));
var _ = seq__46247__$1;
if(cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"repl-eval","repl-eval",-1784727398),msg_name)){
figwheel.client.ensure_cljs_user.call(null);

return figwheel.client.eval_javascript_STAR__STAR_.call(null,new cljs.core.Keyword(null,"code","code",1586293142).cljs$core$IFn$_invoke$arity$1(msg),opts,((function (vec__46246,seq__46247,first__46248,seq__46247__$1,map__46249,map__46249__$1,msg,msg_name,_,map__46243,map__46243__$1,opts,build_id){
return (function (res){
return figwheel.client.socket.send_BANG_.call(null,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"figwheel-event","figwheel-event",519570592),"callback",new cljs.core.Keyword(null,"callback-name","callback-name",336964714),new cljs.core.Keyword(null,"callback-name","callback-name",336964714).cljs$core$IFn$_invoke$arity$1(msg),new cljs.core.Keyword(null,"content","content",15833224),res], null));
});})(vec__46246,seq__46247,first__46248,seq__46247__$1,map__46249,map__46249__$1,msg,msg_name,_,map__46243,map__46243__$1,opts,build_id))
);
} else {
return null;
}
});
;})(map__46243,map__46243__$1,opts,build_id))
});
figwheel.client.css_reloader_plugin = (function figwheel$client$css_reloader_plugin(opts){
return (function (p__46251){
var vec__46252 = p__46251;
var seq__46253 = cljs.core.seq.call(null,vec__46252);
var first__46254 = cljs.core.first.call(null,seq__46253);
var seq__46253__$1 = cljs.core.next.call(null,seq__46253);
var map__46255 = first__46254;
var map__46255__$1 = ((((!((map__46255 == null)))?((((map__46255.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__46255.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__46255):map__46255);
var msg = map__46255__$1;
var msg_name = cljs.core.get.call(null,map__46255__$1,new cljs.core.Keyword(null,"msg-name","msg-name",-353709863));
var _ = seq__46253__$1;
if(cljs.core._EQ_.call(null,msg_name,new cljs.core.Keyword(null,"css-files-changed","css-files-changed",720773874))){
return figwheel.client.file_reloading.reload_css_files.call(null,opts,msg);
} else {
return null;
}
});
});
figwheel.client.compile_fail_warning_plugin = (function figwheel$client$compile_fail_warning_plugin(p__46257){
var map__46258 = p__46257;
var map__46258__$1 = ((((!((map__46258 == null)))?((((map__46258.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__46258.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__46258):map__46258);
var on_compile_warning = cljs.core.get.call(null,map__46258__$1,new cljs.core.Keyword(null,"on-compile-warning","on-compile-warning",-1195585947));
var on_compile_fail = cljs.core.get.call(null,map__46258__$1,new cljs.core.Keyword(null,"on-compile-fail","on-compile-fail",728013036));
return ((function (map__46258,map__46258__$1,on_compile_warning,on_compile_fail){
return (function (p__46260){
var vec__46261 = p__46260;
var seq__46262 = cljs.core.seq.call(null,vec__46261);
var first__46263 = cljs.core.first.call(null,seq__46262);
var seq__46262__$1 = cljs.core.next.call(null,seq__46262);
var map__46264 = first__46263;
var map__46264__$1 = ((((!((map__46264 == null)))?((((map__46264.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__46264.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__46264):map__46264);
var msg = map__46264__$1;
var msg_name = cljs.core.get.call(null,map__46264__$1,new cljs.core.Keyword(null,"msg-name","msg-name",-353709863));
var _ = seq__46262__$1;
var pred__46266 = cljs.core._EQ_;
var expr__46267 = msg_name;
if(cljs.core.truth_(pred__46266.call(null,new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356),expr__46267))){
return on_compile_warning.call(null,msg);
} else {
if(cljs.core.truth_(pred__46266.call(null,new cljs.core.Keyword(null,"compile-failed","compile-failed",-477639289),expr__46267))){
return on_compile_fail.call(null,msg);
} else {
return null;
}
}
});
;})(map__46258,map__46258__$1,on_compile_warning,on_compile_fail))
});
figwheel.client.auto_jump_to_error = (function figwheel$client$auto_jump_to_error(opts,error){
if(cljs.core.truth_(new cljs.core.Keyword(null,"auto-jump-to-source-on-error","auto-jump-to-source-on-error",-960314920).cljs$core$IFn$_invoke$arity$1(opts))){
return figwheel.client.heads_up.auto_notify_source_file_line.call(null,error);
} else {
return null;
}
});
figwheel.client.heads_up_plugin_msg_handler = (function figwheel$client$heads_up_plugin_msg_handler(opts,msg_hist_SINGLEQUOTE_){
var msg_hist = figwheel.client.focus_msgs.call(null,new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"compile-failed","compile-failed",-477639289),null,new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356),null,new cljs.core.Keyword(null,"files-changed","files-changed",-1418200563),null], null), null),msg_hist_SINGLEQUOTE_);
var msg_names = cljs.core.map.call(null,new cljs.core.Keyword(null,"msg-name","msg-name",-353709863),msg_hist);
var msg = cljs.core.first.call(null,msg_hist);
var c__35058__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__35058__auto__,msg_hist,msg_names,msg){
return (function (){
var f__35059__auto__ = (function (){var switch__34968__auto__ = ((function (c__35058__auto__,msg_hist,msg_names,msg){
return (function (state_46356){
var state_val_46357 = (state_46356[(1)]);
if((state_val_46357 === (7))){
var inst_46276 = (state_46356[(2)]);
var state_46356__$1 = state_46356;
if(cljs.core.truth_(inst_46276)){
var statearr_46358_46405 = state_46356__$1;
(statearr_46358_46405[(1)] = (8));

} else {
var statearr_46359_46406 = state_46356__$1;
(statearr_46359_46406[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46357 === (20))){
var inst_46350 = (state_46356[(2)]);
var state_46356__$1 = state_46356;
var statearr_46360_46407 = state_46356__$1;
(statearr_46360_46407[(2)] = inst_46350);

(statearr_46360_46407[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46357 === (27))){
var inst_46346 = (state_46356[(2)]);
var state_46356__$1 = state_46356;
var statearr_46361_46408 = state_46356__$1;
(statearr_46361_46408[(2)] = inst_46346);

(statearr_46361_46408[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46357 === (1))){
var inst_46269 = figwheel.client.reload_file_state_QMARK_.call(null,msg_names,opts);
var state_46356__$1 = state_46356;
if(cljs.core.truth_(inst_46269)){
var statearr_46362_46409 = state_46356__$1;
(statearr_46362_46409[(1)] = (2));

} else {
var statearr_46363_46410 = state_46356__$1;
(statearr_46363_46410[(1)] = (3));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46357 === (24))){
var inst_46348 = (state_46356[(2)]);
var state_46356__$1 = state_46356;
var statearr_46364_46411 = state_46356__$1;
(statearr_46364_46411[(2)] = inst_46348);

(statearr_46364_46411[(1)] = (20));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46357 === (4))){
var inst_46354 = (state_46356[(2)]);
var state_46356__$1 = state_46356;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_46356__$1,inst_46354);
} else {
if((state_val_46357 === (15))){
var inst_46352 = (state_46356[(2)]);
var state_46356__$1 = state_46356;
var statearr_46365_46412 = state_46356__$1;
(statearr_46365_46412[(2)] = inst_46352);

(statearr_46365_46412[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46357 === (21))){
var inst_46305 = (state_46356[(2)]);
var inst_46306 = new cljs.core.Keyword(null,"exception-data","exception-data",-512474886).cljs$core$IFn$_invoke$arity$1(msg);
var inst_46307 = figwheel.client.auto_jump_to_error.call(null,opts,inst_46306);
var state_46356__$1 = (function (){var statearr_46366 = state_46356;
(statearr_46366[(7)] = inst_46305);

return statearr_46366;
})();
var statearr_46367_46413 = state_46356__$1;
(statearr_46367_46413[(2)] = inst_46307);

(statearr_46367_46413[(1)] = (20));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46357 === (31))){
var inst_46335 = figwheel.client.css_loaded_state_QMARK_.call(null,msg_names);
var state_46356__$1 = state_46356;
if(cljs.core.truth_(inst_46335)){
var statearr_46368_46414 = state_46356__$1;
(statearr_46368_46414[(1)] = (34));

} else {
var statearr_46369_46415 = state_46356__$1;
(statearr_46369_46415[(1)] = (35));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46357 === (32))){
var inst_46344 = (state_46356[(2)]);
var state_46356__$1 = state_46356;
var statearr_46370_46416 = state_46356__$1;
(statearr_46370_46416[(2)] = inst_46344);

(statearr_46370_46416[(1)] = (27));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46357 === (33))){
var inst_46331 = (state_46356[(2)]);
var inst_46332 = new cljs.core.Keyword(null,"message","message",-406056002).cljs$core$IFn$_invoke$arity$1(msg);
var inst_46333 = figwheel.client.auto_jump_to_error.call(null,opts,inst_46332);
var state_46356__$1 = (function (){var statearr_46371 = state_46356;
(statearr_46371[(8)] = inst_46331);

return statearr_46371;
})();
var statearr_46372_46417 = state_46356__$1;
(statearr_46372_46417[(2)] = inst_46333);

(statearr_46372_46417[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46357 === (13))){
var inst_46290 = figwheel.client.heads_up.clear.call(null);
var state_46356__$1 = state_46356;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_46356__$1,(16),inst_46290);
} else {
if((state_val_46357 === (22))){
var inst_46311 = new cljs.core.Keyword(null,"message","message",-406056002).cljs$core$IFn$_invoke$arity$1(msg);
var inst_46312 = figwheel.client.heads_up.append_warning_message.call(null,inst_46311);
var state_46356__$1 = state_46356;
var statearr_46373_46418 = state_46356__$1;
(statearr_46373_46418[(2)] = inst_46312);

(statearr_46373_46418[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46357 === (36))){
var inst_46342 = (state_46356[(2)]);
var state_46356__$1 = state_46356;
var statearr_46374_46419 = state_46356__$1;
(statearr_46374_46419[(2)] = inst_46342);

(statearr_46374_46419[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46357 === (29))){
var inst_46322 = (state_46356[(2)]);
var inst_46323 = new cljs.core.Keyword(null,"message","message",-406056002).cljs$core$IFn$_invoke$arity$1(msg);
var inst_46324 = figwheel.client.auto_jump_to_error.call(null,opts,inst_46323);
var state_46356__$1 = (function (){var statearr_46375 = state_46356;
(statearr_46375[(9)] = inst_46322);

return statearr_46375;
})();
var statearr_46376_46420 = state_46356__$1;
(statearr_46376_46420[(2)] = inst_46324);

(statearr_46376_46420[(1)] = (27));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46357 === (6))){
var inst_46271 = (state_46356[(10)]);
var state_46356__$1 = state_46356;
var statearr_46377_46421 = state_46356__$1;
(statearr_46377_46421[(2)] = inst_46271);

(statearr_46377_46421[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46357 === (28))){
var inst_46318 = (state_46356[(2)]);
var inst_46319 = new cljs.core.Keyword(null,"message","message",-406056002).cljs$core$IFn$_invoke$arity$1(msg);
var inst_46320 = figwheel.client.heads_up.display_warning.call(null,inst_46319);
var state_46356__$1 = (function (){var statearr_46378 = state_46356;
(statearr_46378[(11)] = inst_46318);

return statearr_46378;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_46356__$1,(29),inst_46320);
} else {
if((state_val_46357 === (25))){
var inst_46316 = figwheel.client.heads_up.clear.call(null);
var state_46356__$1 = state_46356;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_46356__$1,(28),inst_46316);
} else {
if((state_val_46357 === (34))){
var inst_46337 = figwheel.client.heads_up.flash_loaded.call(null);
var state_46356__$1 = state_46356;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_46356__$1,(37),inst_46337);
} else {
if((state_val_46357 === (17))){
var inst_46296 = (state_46356[(2)]);
var inst_46297 = new cljs.core.Keyword(null,"exception-data","exception-data",-512474886).cljs$core$IFn$_invoke$arity$1(msg);
var inst_46298 = figwheel.client.auto_jump_to_error.call(null,opts,inst_46297);
var state_46356__$1 = (function (){var statearr_46379 = state_46356;
(statearr_46379[(12)] = inst_46296);

return statearr_46379;
})();
var statearr_46380_46422 = state_46356__$1;
(statearr_46380_46422[(2)] = inst_46298);

(statearr_46380_46422[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46357 === (3))){
var inst_46288 = figwheel.client.compile_refail_state_QMARK_.call(null,msg_names);
var state_46356__$1 = state_46356;
if(cljs.core.truth_(inst_46288)){
var statearr_46381_46423 = state_46356__$1;
(statearr_46381_46423[(1)] = (13));

} else {
var statearr_46382_46424 = state_46356__$1;
(statearr_46382_46424[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46357 === (12))){
var inst_46284 = (state_46356[(2)]);
var state_46356__$1 = state_46356;
var statearr_46383_46425 = state_46356__$1;
(statearr_46383_46425[(2)] = inst_46284);

(statearr_46383_46425[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46357 === (2))){
var inst_46271 = (state_46356[(10)]);
var inst_46271__$1 = figwheel.client.autoload_QMARK_.call(null);
var state_46356__$1 = (function (){var statearr_46384 = state_46356;
(statearr_46384[(10)] = inst_46271__$1);

return statearr_46384;
})();
if(cljs.core.truth_(inst_46271__$1)){
var statearr_46385_46426 = state_46356__$1;
(statearr_46385_46426[(1)] = (5));

} else {
var statearr_46386_46427 = state_46356__$1;
(statearr_46386_46427[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46357 === (23))){
var inst_46314 = figwheel.client.rewarning_state_QMARK_.call(null,msg_names);
var state_46356__$1 = state_46356;
if(cljs.core.truth_(inst_46314)){
var statearr_46387_46428 = state_46356__$1;
(statearr_46387_46428[(1)] = (25));

} else {
var statearr_46388_46429 = state_46356__$1;
(statearr_46388_46429[(1)] = (26));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46357 === (35))){
var state_46356__$1 = state_46356;
var statearr_46389_46430 = state_46356__$1;
(statearr_46389_46430[(2)] = null);

(statearr_46389_46430[(1)] = (36));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46357 === (19))){
var inst_46309 = figwheel.client.warning_append_state_QMARK_.call(null,msg_names);
var state_46356__$1 = state_46356;
if(cljs.core.truth_(inst_46309)){
var statearr_46390_46431 = state_46356__$1;
(statearr_46390_46431[(1)] = (22));

} else {
var statearr_46391_46432 = state_46356__$1;
(statearr_46391_46432[(1)] = (23));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46357 === (11))){
var inst_46280 = (state_46356[(2)]);
var state_46356__$1 = state_46356;
var statearr_46392_46433 = state_46356__$1;
(statearr_46392_46433[(2)] = inst_46280);

(statearr_46392_46433[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46357 === (9))){
var inst_46282 = figwheel.client.heads_up.clear.call(null);
var state_46356__$1 = state_46356;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_46356__$1,(12),inst_46282);
} else {
if((state_val_46357 === (5))){
var inst_46273 = new cljs.core.Keyword(null,"autoload","autoload",-354122500).cljs$core$IFn$_invoke$arity$1(opts);
var state_46356__$1 = state_46356;
var statearr_46393_46434 = state_46356__$1;
(statearr_46393_46434[(2)] = inst_46273);

(statearr_46393_46434[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46357 === (14))){
var inst_46300 = figwheel.client.compile_fail_state_QMARK_.call(null,msg_names);
var state_46356__$1 = state_46356;
if(cljs.core.truth_(inst_46300)){
var statearr_46394_46435 = state_46356__$1;
(statearr_46394_46435[(1)] = (18));

} else {
var statearr_46395_46436 = state_46356__$1;
(statearr_46395_46436[(1)] = (19));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46357 === (26))){
var inst_46326 = figwheel.client.warning_state_QMARK_.call(null,msg_names);
var state_46356__$1 = state_46356;
if(cljs.core.truth_(inst_46326)){
var statearr_46396_46437 = state_46356__$1;
(statearr_46396_46437[(1)] = (30));

} else {
var statearr_46397_46438 = state_46356__$1;
(statearr_46397_46438[(1)] = (31));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46357 === (16))){
var inst_46292 = (state_46356[(2)]);
var inst_46293 = new cljs.core.Keyword(null,"exception-data","exception-data",-512474886).cljs$core$IFn$_invoke$arity$1(msg);
var inst_46294 = figwheel.client.heads_up.display_exception.call(null,inst_46293);
var state_46356__$1 = (function (){var statearr_46398 = state_46356;
(statearr_46398[(13)] = inst_46292);

return statearr_46398;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_46356__$1,(17),inst_46294);
} else {
if((state_val_46357 === (30))){
var inst_46328 = new cljs.core.Keyword(null,"message","message",-406056002).cljs$core$IFn$_invoke$arity$1(msg);
var inst_46329 = figwheel.client.heads_up.display_warning.call(null,inst_46328);
var state_46356__$1 = state_46356;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_46356__$1,(33),inst_46329);
} else {
if((state_val_46357 === (10))){
var inst_46286 = (state_46356[(2)]);
var state_46356__$1 = state_46356;
var statearr_46399_46439 = state_46356__$1;
(statearr_46399_46439[(2)] = inst_46286);

(statearr_46399_46439[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46357 === (18))){
var inst_46302 = new cljs.core.Keyword(null,"exception-data","exception-data",-512474886).cljs$core$IFn$_invoke$arity$1(msg);
var inst_46303 = figwheel.client.heads_up.display_exception.call(null,inst_46302);
var state_46356__$1 = state_46356;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_46356__$1,(21),inst_46303);
} else {
if((state_val_46357 === (37))){
var inst_46339 = (state_46356[(2)]);
var state_46356__$1 = state_46356;
var statearr_46400_46440 = state_46356__$1;
(statearr_46400_46440[(2)] = inst_46339);

(statearr_46400_46440[(1)] = (36));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46357 === (8))){
var inst_46278 = figwheel.client.heads_up.flash_loaded.call(null);
var state_46356__$1 = state_46356;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_46356__$1,(11),inst_46278);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__35058__auto__,msg_hist,msg_names,msg))
;
return ((function (switch__34968__auto__,c__35058__auto__,msg_hist,msg_names,msg){
return (function() {
var figwheel$client$heads_up_plugin_msg_handler_$_state_machine__34969__auto__ = null;
var figwheel$client$heads_up_plugin_msg_handler_$_state_machine__34969__auto____0 = (function (){
var statearr_46401 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_46401[(0)] = figwheel$client$heads_up_plugin_msg_handler_$_state_machine__34969__auto__);

(statearr_46401[(1)] = (1));

return statearr_46401;
});
var figwheel$client$heads_up_plugin_msg_handler_$_state_machine__34969__auto____1 = (function (state_46356){
while(true){
var ret_value__34970__auto__ = (function (){try{while(true){
var result__34971__auto__ = switch__34968__auto__.call(null,state_46356);
if(cljs.core.keyword_identical_QMARK_.call(null,result__34971__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__34971__auto__;
}
break;
}
}catch (e46402){if((e46402 instanceof Object)){
var ex__34972__auto__ = e46402;
var statearr_46403_46441 = state_46356;
(statearr_46403_46441[(5)] = ex__34972__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_46356);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e46402;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__34970__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__46442 = state_46356;
state_46356 = G__46442;
continue;
} else {
return ret_value__34970__auto__;
}
break;
}
});
figwheel$client$heads_up_plugin_msg_handler_$_state_machine__34969__auto__ = function(state_46356){
switch(arguments.length){
case 0:
return figwheel$client$heads_up_plugin_msg_handler_$_state_machine__34969__auto____0.call(this);
case 1:
return figwheel$client$heads_up_plugin_msg_handler_$_state_machine__34969__auto____1.call(this,state_46356);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
figwheel$client$heads_up_plugin_msg_handler_$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$heads_up_plugin_msg_handler_$_state_machine__34969__auto____0;
figwheel$client$heads_up_plugin_msg_handler_$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$heads_up_plugin_msg_handler_$_state_machine__34969__auto____1;
return figwheel$client$heads_up_plugin_msg_handler_$_state_machine__34969__auto__;
})()
;})(switch__34968__auto__,c__35058__auto__,msg_hist,msg_names,msg))
})();
var state__35060__auto__ = (function (){var statearr_46404 = f__35059__auto__.call(null);
(statearr_46404[(6)] = c__35058__auto__);

return statearr_46404;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__35060__auto__);
});})(c__35058__auto__,msg_hist,msg_names,msg))
);

return c__35058__auto__;
});
figwheel.client.heads_up_plugin = (function figwheel$client$heads_up_plugin(opts){
var ch = cljs.core.async.chan.call(null);
figwheel.client.heads_up_config_options_STAR__STAR_ = opts;

var c__35058__auto___46471 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__35058__auto___46471,ch){
return (function (){
var f__35059__auto__ = (function (){var switch__34968__auto__ = ((function (c__35058__auto___46471,ch){
return (function (state_46457){
var state_val_46458 = (state_46457[(1)]);
if((state_val_46458 === (1))){
var state_46457__$1 = state_46457;
var statearr_46459_46472 = state_46457__$1;
(statearr_46459_46472[(2)] = null);

(statearr_46459_46472[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46458 === (2))){
var state_46457__$1 = state_46457;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_46457__$1,(4),ch);
} else {
if((state_val_46458 === (3))){
var inst_46455 = (state_46457[(2)]);
var state_46457__$1 = state_46457;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_46457__$1,inst_46455);
} else {
if((state_val_46458 === (4))){
var inst_46445 = (state_46457[(7)]);
var inst_46445__$1 = (state_46457[(2)]);
var state_46457__$1 = (function (){var statearr_46460 = state_46457;
(statearr_46460[(7)] = inst_46445__$1);

return statearr_46460;
})();
if(cljs.core.truth_(inst_46445__$1)){
var statearr_46461_46473 = state_46457__$1;
(statearr_46461_46473[(1)] = (5));

} else {
var statearr_46462_46474 = state_46457__$1;
(statearr_46462_46474[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46458 === (5))){
var inst_46445 = (state_46457[(7)]);
var inst_46447 = figwheel.client.heads_up_plugin_msg_handler.call(null,opts,inst_46445);
var state_46457__$1 = state_46457;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_46457__$1,(8),inst_46447);
} else {
if((state_val_46458 === (6))){
var state_46457__$1 = state_46457;
var statearr_46463_46475 = state_46457__$1;
(statearr_46463_46475[(2)] = null);

(statearr_46463_46475[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46458 === (7))){
var inst_46453 = (state_46457[(2)]);
var state_46457__$1 = state_46457;
var statearr_46464_46476 = state_46457__$1;
(statearr_46464_46476[(2)] = inst_46453);

(statearr_46464_46476[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_46458 === (8))){
var inst_46449 = (state_46457[(2)]);
var state_46457__$1 = (function (){var statearr_46465 = state_46457;
(statearr_46465[(8)] = inst_46449);

return statearr_46465;
})();
var statearr_46466_46477 = state_46457__$1;
(statearr_46466_46477[(2)] = null);

(statearr_46466_46477[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
});})(c__35058__auto___46471,ch))
;
return ((function (switch__34968__auto__,c__35058__auto___46471,ch){
return (function() {
var figwheel$client$heads_up_plugin_$_state_machine__34969__auto__ = null;
var figwheel$client$heads_up_plugin_$_state_machine__34969__auto____0 = (function (){
var statearr_46467 = [null,null,null,null,null,null,null,null,null];
(statearr_46467[(0)] = figwheel$client$heads_up_plugin_$_state_machine__34969__auto__);

(statearr_46467[(1)] = (1));

return statearr_46467;
});
var figwheel$client$heads_up_plugin_$_state_machine__34969__auto____1 = (function (state_46457){
while(true){
var ret_value__34970__auto__ = (function (){try{while(true){
var result__34971__auto__ = switch__34968__auto__.call(null,state_46457);
if(cljs.core.keyword_identical_QMARK_.call(null,result__34971__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__34971__auto__;
}
break;
}
}catch (e46468){if((e46468 instanceof Object)){
var ex__34972__auto__ = e46468;
var statearr_46469_46478 = state_46457;
(statearr_46469_46478[(5)] = ex__34972__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_46457);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e46468;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__34970__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__46479 = state_46457;
state_46457 = G__46479;
continue;
} else {
return ret_value__34970__auto__;
}
break;
}
});
figwheel$client$heads_up_plugin_$_state_machine__34969__auto__ = function(state_46457){
switch(arguments.length){
case 0:
return figwheel$client$heads_up_plugin_$_state_machine__34969__auto____0.call(this);
case 1:
return figwheel$client$heads_up_plugin_$_state_machine__34969__auto____1.call(this,state_46457);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
figwheel$client$heads_up_plugin_$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$heads_up_plugin_$_state_machine__34969__auto____0;
figwheel$client$heads_up_plugin_$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$heads_up_plugin_$_state_machine__34969__auto____1;
return figwheel$client$heads_up_plugin_$_state_machine__34969__auto__;
})()
;})(switch__34968__auto__,c__35058__auto___46471,ch))
})();
var state__35060__auto__ = (function (){var statearr_46470 = f__35059__auto__.call(null);
(statearr_46470[(6)] = c__35058__auto___46471);

return statearr_46470;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__35060__auto__);
});})(c__35058__auto___46471,ch))
);


figwheel.client.heads_up.ensure_container.call(null);

return ((function (ch){
return (function (msg_hist){
cljs.core.async.put_BANG_.call(null,ch,msg_hist);

return msg_hist;
});
;})(ch))
});
figwheel.client.enforce_project_plugin = (function figwheel$client$enforce_project_plugin(opts){
return (function (msg_hist){
if(((1) < cljs.core.count.call(null,cljs.core.set.call(null,cljs.core.keep.call(null,new cljs.core.Keyword(null,"project-id","project-id",206449307),cljs.core.take.call(null,(5),msg_hist)))))){
figwheel.client.socket.close_BANG_.call(null);

console.error("Figwheel: message received from different project. Shutting socket down.");

if(cljs.core.truth_(new cljs.core.Keyword(null,"heads-up-display","heads-up-display",-896577202).cljs$core$IFn$_invoke$arity$1(opts))){
var c__35058__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__35058__auto__){
return (function (){
var f__35059__auto__ = (function (){var switch__34968__auto__ = ((function (c__35058__auto__){
return (function (state_46485){
var state_val_46486 = (state_46485[(1)]);
if((state_val_46486 === (1))){
var inst_46480 = cljs.core.async.timeout.call(null,(3000));
var state_46485__$1 = state_46485;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_46485__$1,(2),inst_46480);
} else {
if((state_val_46486 === (2))){
var inst_46482 = (state_46485[(2)]);
var inst_46483 = figwheel.client.heads_up.display_system_warning.call(null,"Connection from different project","Shutting connection down!!!!!");
var state_46485__$1 = (function (){var statearr_46487 = state_46485;
(statearr_46487[(7)] = inst_46482);

return statearr_46487;
})();
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_46485__$1,inst_46483);
} else {
return null;
}
}
});})(c__35058__auto__))
;
return ((function (switch__34968__auto__,c__35058__auto__){
return (function() {
var figwheel$client$enforce_project_plugin_$_state_machine__34969__auto__ = null;
var figwheel$client$enforce_project_plugin_$_state_machine__34969__auto____0 = (function (){
var statearr_46488 = [null,null,null,null,null,null,null,null];
(statearr_46488[(0)] = figwheel$client$enforce_project_plugin_$_state_machine__34969__auto__);

(statearr_46488[(1)] = (1));

return statearr_46488;
});
var figwheel$client$enforce_project_plugin_$_state_machine__34969__auto____1 = (function (state_46485){
while(true){
var ret_value__34970__auto__ = (function (){try{while(true){
var result__34971__auto__ = switch__34968__auto__.call(null,state_46485);
if(cljs.core.keyword_identical_QMARK_.call(null,result__34971__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__34971__auto__;
}
break;
}
}catch (e46489){if((e46489 instanceof Object)){
var ex__34972__auto__ = e46489;
var statearr_46490_46492 = state_46485;
(statearr_46490_46492[(5)] = ex__34972__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_46485);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e46489;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__34970__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__46493 = state_46485;
state_46485 = G__46493;
continue;
} else {
return ret_value__34970__auto__;
}
break;
}
});
figwheel$client$enforce_project_plugin_$_state_machine__34969__auto__ = function(state_46485){
switch(arguments.length){
case 0:
return figwheel$client$enforce_project_plugin_$_state_machine__34969__auto____0.call(this);
case 1:
return figwheel$client$enforce_project_plugin_$_state_machine__34969__auto____1.call(this,state_46485);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
figwheel$client$enforce_project_plugin_$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$enforce_project_plugin_$_state_machine__34969__auto____0;
figwheel$client$enforce_project_plugin_$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$enforce_project_plugin_$_state_machine__34969__auto____1;
return figwheel$client$enforce_project_plugin_$_state_machine__34969__auto__;
})()
;})(switch__34968__auto__,c__35058__auto__))
})();
var state__35060__auto__ = (function (){var statearr_46491 = f__35059__auto__.call(null);
(statearr_46491[(6)] = c__35058__auto__);

return statearr_46491;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__35060__auto__);
});})(c__35058__auto__))
);

return c__35058__auto__;
} else {
return null;
}
} else {
return null;
}
});
});
figwheel.client.enforce_figwheel_version_plugin = (function figwheel$client$enforce_figwheel_version_plugin(opts){
return (function (msg_hist){
var temp__5278__auto__ = new cljs.core.Keyword(null,"figwheel-version","figwheel-version",1409553832).cljs$core$IFn$_invoke$arity$1(cljs.core.first.call(null,msg_hist));
if(cljs.core.truth_(temp__5278__auto__)){
var figwheel_version = temp__5278__auto__;
if(cljs.core.not_EQ_.call(null,figwheel_version,figwheel.client._figwheel_version_)){
figwheel.client.socket.close_BANG_.call(null);

console.error("Figwheel: message received from different version of Figwheel.");

if(cljs.core.truth_(new cljs.core.Keyword(null,"heads-up-display","heads-up-display",-896577202).cljs$core$IFn$_invoke$arity$1(opts))){
var c__35058__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__35058__auto__,figwheel_version,temp__5278__auto__){
return (function (){
var f__35059__auto__ = (function (){var switch__34968__auto__ = ((function (c__35058__auto__,figwheel_version,temp__5278__auto__){
return (function (state_46500){
var state_val_46501 = (state_46500[(1)]);
if((state_val_46501 === (1))){
var inst_46494 = cljs.core.async.timeout.call(null,(2000));
var state_46500__$1 = state_46500;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_46500__$1,(2),inst_46494);
} else {
if((state_val_46501 === (2))){
var inst_46496 = (state_46500[(2)]);
var inst_46497 = ["Figwheel Client Version <strong>",cljs.core.str.cljs$core$IFn$_invoke$arity$1(figwheel.client._figwheel_version_),"</strong> is not equal to ","Figwheel Sidecar Version <strong>",cljs.core.str.cljs$core$IFn$_invoke$arity$1(figwheel_version),"</strong>",".  Shutting down Websocket Connection!","<h4>To fix try:</h4>","<ol><li>Reload this page and make sure you are not getting a cached version of the client.</li>","<li>You may have to clean (delete compiled assets) and rebuild to make sure that the new client code is being used.</li>","<li>Also, make sure you have consistent Figwheel dependencies.</li></ol>"].join('');
var inst_46498 = figwheel.client.heads_up.display_system_warning.call(null,"Figwheel Client and Server have different versions!!",inst_46497);
var state_46500__$1 = (function (){var statearr_46502 = state_46500;
(statearr_46502[(7)] = inst_46496);

return statearr_46502;
})();
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_46500__$1,inst_46498);
} else {
return null;
}
}
});})(c__35058__auto__,figwheel_version,temp__5278__auto__))
;
return ((function (switch__34968__auto__,c__35058__auto__,figwheel_version,temp__5278__auto__){
return (function() {
var figwheel$client$enforce_figwheel_version_plugin_$_state_machine__34969__auto__ = null;
var figwheel$client$enforce_figwheel_version_plugin_$_state_machine__34969__auto____0 = (function (){
var statearr_46503 = [null,null,null,null,null,null,null,null];
(statearr_46503[(0)] = figwheel$client$enforce_figwheel_version_plugin_$_state_machine__34969__auto__);

(statearr_46503[(1)] = (1));

return statearr_46503;
});
var figwheel$client$enforce_figwheel_version_plugin_$_state_machine__34969__auto____1 = (function (state_46500){
while(true){
var ret_value__34970__auto__ = (function (){try{while(true){
var result__34971__auto__ = switch__34968__auto__.call(null,state_46500);
if(cljs.core.keyword_identical_QMARK_.call(null,result__34971__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__34971__auto__;
}
break;
}
}catch (e46504){if((e46504 instanceof Object)){
var ex__34972__auto__ = e46504;
var statearr_46505_46507 = state_46500;
(statearr_46505_46507[(5)] = ex__34972__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_46500);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e46504;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__34970__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__46508 = state_46500;
state_46500 = G__46508;
continue;
} else {
return ret_value__34970__auto__;
}
break;
}
});
figwheel$client$enforce_figwheel_version_plugin_$_state_machine__34969__auto__ = function(state_46500){
switch(arguments.length){
case 0:
return figwheel$client$enforce_figwheel_version_plugin_$_state_machine__34969__auto____0.call(this);
case 1:
return figwheel$client$enforce_figwheel_version_plugin_$_state_machine__34969__auto____1.call(this,state_46500);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
figwheel$client$enforce_figwheel_version_plugin_$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$enforce_figwheel_version_plugin_$_state_machine__34969__auto____0;
figwheel$client$enforce_figwheel_version_plugin_$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$enforce_figwheel_version_plugin_$_state_machine__34969__auto____1;
return figwheel$client$enforce_figwheel_version_plugin_$_state_machine__34969__auto__;
})()
;})(switch__34968__auto__,c__35058__auto__,figwheel_version,temp__5278__auto__))
})();
var state__35060__auto__ = (function (){var statearr_46506 = f__35059__auto__.call(null);
(statearr_46506[(6)] = c__35058__auto__);

return statearr_46506;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__35060__auto__);
});})(c__35058__auto__,figwheel_version,temp__5278__auto__))
);

return c__35058__auto__;
} else {
return null;
}
} else {
return null;
}
} else {
return null;
}
});
});
figwheel.client.default_on_jsload = cljs.core.identity;
figwheel.client.file_line_column = (function figwheel$client$file_line_column(p__46509){
var map__46510 = p__46509;
var map__46510__$1 = ((((!((map__46510 == null)))?((((map__46510.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__46510.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__46510):map__46510);
var file = cljs.core.get.call(null,map__46510__$1,new cljs.core.Keyword(null,"file","file",-1269645878));
var line = cljs.core.get.call(null,map__46510__$1,new cljs.core.Keyword(null,"line","line",212345235));
var column = cljs.core.get.call(null,map__46510__$1,new cljs.core.Keyword(null,"column","column",2078222095));
var G__46512 = "";
var G__46512__$1 = (cljs.core.truth_(file)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__46512),"file ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(file)].join(''):G__46512);
var G__46512__$2 = (cljs.core.truth_(line)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__46512__$1)," at line ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(line)].join(''):G__46512__$1);
if(cljs.core.truth_((function (){var and__30170__auto__ = line;
if(cljs.core.truth_(and__30170__auto__)){
return column;
} else {
return and__30170__auto__;
}
})())){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__46512__$2),", column ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(column)].join('');
} else {
return G__46512__$2;
}
});
figwheel.client.default_on_compile_fail = (function figwheel$client$default_on_compile_fail(p__46513){
var map__46514 = p__46513;
var map__46514__$1 = ((((!((map__46514 == null)))?((((map__46514.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__46514.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__46514):map__46514);
var ed = map__46514__$1;
var formatted_exception = cljs.core.get.call(null,map__46514__$1,new cljs.core.Keyword(null,"formatted-exception","formatted-exception",-116489026));
var exception_data = cljs.core.get.call(null,map__46514__$1,new cljs.core.Keyword(null,"exception-data","exception-data",-512474886));
var cause = cljs.core.get.call(null,map__46514__$1,new cljs.core.Keyword(null,"cause","cause",231901252));
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"debug","debug",-1608172596),"Figwheel: Compile Exception");

var seq__46516_46520 = cljs.core.seq.call(null,figwheel.client.format_messages.call(null,exception_data));
var chunk__46517_46521 = null;
var count__46518_46522 = (0);
var i__46519_46523 = (0);
while(true){
if((i__46519_46523 < count__46518_46522)){
var msg_46524 = cljs.core._nth.call(null,chunk__46517_46521,i__46519_46523);
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"info","info",-317069002),msg_46524);

var G__46525 = seq__46516_46520;
var G__46526 = chunk__46517_46521;
var G__46527 = count__46518_46522;
var G__46528 = (i__46519_46523 + (1));
seq__46516_46520 = G__46525;
chunk__46517_46521 = G__46526;
count__46518_46522 = G__46527;
i__46519_46523 = G__46528;
continue;
} else {
var temp__5278__auto___46529 = cljs.core.seq.call(null,seq__46516_46520);
if(temp__5278__auto___46529){
var seq__46516_46530__$1 = temp__5278__auto___46529;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__46516_46530__$1)){
var c__31113__auto___46531 = cljs.core.chunk_first.call(null,seq__46516_46530__$1);
var G__46532 = cljs.core.chunk_rest.call(null,seq__46516_46530__$1);
var G__46533 = c__31113__auto___46531;
var G__46534 = cljs.core.count.call(null,c__31113__auto___46531);
var G__46535 = (0);
seq__46516_46520 = G__46532;
chunk__46517_46521 = G__46533;
count__46518_46522 = G__46534;
i__46519_46523 = G__46535;
continue;
} else {
var msg_46536 = cljs.core.first.call(null,seq__46516_46530__$1);
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"info","info",-317069002),msg_46536);

var G__46537 = cljs.core.next.call(null,seq__46516_46530__$1);
var G__46538 = null;
var G__46539 = (0);
var G__46540 = (0);
seq__46516_46520 = G__46537;
chunk__46517_46521 = G__46538;
count__46518_46522 = G__46539;
i__46519_46523 = G__46540;
continue;
}
} else {
}
}
break;
}

if(cljs.core.truth_(cause)){
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"info","info",-317069002),["Error on ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(figwheel.client.file_line_column.call(null,ed))].join(''));
} else {
}

return ed;
});
figwheel.client.default_on_compile_warning = (function figwheel$client$default_on_compile_warning(p__46541){
var map__46542 = p__46541;
var map__46542__$1 = ((((!((map__46542 == null)))?((((map__46542.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__46542.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__46542):map__46542);
var w = map__46542__$1;
var message = cljs.core.get.call(null,map__46542__$1,new cljs.core.Keyword(null,"message","message",-406056002));
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"warn","warn",-436710552),["Figwheel: Compile Warning - ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"message","message",-406056002).cljs$core$IFn$_invoke$arity$1(message))," in ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(figwheel.client.file_line_column.call(null,message))].join(''));

return w;
});
figwheel.client.default_before_load = (function figwheel$client$default_before_load(files){
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"debug","debug",-1608172596),"Figwheel: notified of file changes");

return files;
});
figwheel.client.default_on_cssload = (function figwheel$client$default_on_cssload(files){
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"debug","debug",-1608172596),"Figwheel: loaded CSS files");

figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"info","info",-317069002),cljs.core.pr_str.call(null,cljs.core.map.call(null,new cljs.core.Keyword(null,"file","file",-1269645878),files)));

return files;
});
if(typeof figwheel.client.config_defaults !== 'undefined'){
} else {
figwheel.client.config_defaults = cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"on-compile-warning","on-compile-warning",-1195585947),new cljs.core.Keyword(null,"on-jsload","on-jsload",-395756602),new cljs.core.Keyword(null,"reload-dependents","reload-dependents",-956865430),new cljs.core.Keyword(null,"on-compile-fail","on-compile-fail",728013036),new cljs.core.Keyword(null,"debug","debug",-1608172596),new cljs.core.Keyword(null,"heads-up-display","heads-up-display",-896577202),new cljs.core.Keyword(null,"websocket-url","websocket-url",-490444938),new cljs.core.Keyword(null,"auto-jump-to-source-on-error","auto-jump-to-source-on-error",-960314920),new cljs.core.Keyword(null,"before-jsload","before-jsload",-847513128),new cljs.core.Keyword(null,"load-warninged-code","load-warninged-code",-2030345223),new cljs.core.Keyword(null,"eval-fn","eval-fn",-1111644294),new cljs.core.Keyword(null,"retry-count","retry-count",1936122875),new cljs.core.Keyword(null,"autoload","autoload",-354122500),new cljs.core.Keyword(null,"on-cssload","on-cssload",1825432318)],[new cljs.core.Var(function(){return figwheel.client.default_on_compile_warning;},new cljs.core.Symbol("figwheel.client","default-on-compile-warning","figwheel.client/default-on-compile-warning",584144208,null),cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"ns","ns",441598760),new cljs.core.Keyword(null,"name","name",1843675177),new cljs.core.Keyword(null,"file","file",-1269645878),new cljs.core.Keyword(null,"end-column","end-column",1425389514),new cljs.core.Keyword(null,"column","column",2078222095),new cljs.core.Keyword(null,"line","line",212345235),new cljs.core.Keyword(null,"end-line","end-line",1837326455),new cljs.core.Keyword(null,"arglists","arglists",1661989754),new cljs.core.Keyword(null,"doc","doc",1913296891),new cljs.core.Keyword(null,"test","test",577538877)],[new cljs.core.Symbol(null,"figwheel.client","figwheel.client",-538710252,null),new cljs.core.Symbol(null,"default-on-compile-warning","default-on-compile-warning",-18911586,null),"resources/www/js/app/figwheel/client.cljs",33,1,363,363,cljs.core.list(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"keys","keys",1068423698),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"message","message",1234475525,null)], null),new cljs.core.Keyword(null,"as","as",1148689641),new cljs.core.Symbol(null,"w","w",1994700528,null)], null)], null)),null,(cljs.core.truth_(figwheel.client.default_on_compile_warning)?figwheel.client.default_on_compile_warning.cljs$lang$test:null)])),figwheel.client.default_on_jsload,true,new cljs.core.Var(function(){return figwheel.client.default_on_compile_fail;},new cljs.core.Symbol("figwheel.client","default-on-compile-fail","figwheel.client/default-on-compile-fail",1384826337,null),cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"ns","ns",441598760),new cljs.core.Keyword(null,"name","name",1843675177),new cljs.core.Keyword(null,"file","file",-1269645878),new cljs.core.Keyword(null,"end-column","end-column",1425389514),new cljs.core.Keyword(null,"column","column",2078222095),new cljs.core.Keyword(null,"line","line",212345235),new cljs.core.Keyword(null,"end-line","end-line",1837326455),new cljs.core.Keyword(null,"arglists","arglists",1661989754),new cljs.core.Keyword(null,"doc","doc",1913296891),new cljs.core.Keyword(null,"test","test",577538877)],[new cljs.core.Symbol(null,"figwheel.client","figwheel.client",-538710252,null),new cljs.core.Symbol(null,"default-on-compile-fail","default-on-compile-fail",-158814813,null),"resources/www/js/app/figwheel/client.cljs",30,1,355,355,cljs.core.list(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"keys","keys",1068423698),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"formatted-exception","formatted-exception",1524042501,null),new cljs.core.Symbol(null,"exception-data","exception-data",1128056641,null),new cljs.core.Symbol(null,"cause","cause",1872432779,null)], null),new cljs.core.Keyword(null,"as","as",1148689641),new cljs.core.Symbol(null,"ed","ed",2076825751,null)], null)], null)),null,(cljs.core.truth_(figwheel.client.default_on_compile_fail)?figwheel.client.default_on_compile_fail.cljs$lang$test:null)])),false,true,["ws://",cljs.core.str.cljs$core$IFn$_invoke$arity$1((cljs.core.truth_(figwheel.client.utils.html_env_QMARK_.call(null))?location.host:"localhost:3449")),"/figwheel-ws"].join(''),false,figwheel.client.default_before_load,false,false,(100),true,figwheel.client.default_on_cssload]);
}
figwheel.client.handle_deprecated_jsload_callback = (function figwheel$client$handle_deprecated_jsload_callback(config){
if(cljs.core.truth_(new cljs.core.Keyword(null,"jsload-callback","jsload-callback",-1949628369).cljs$core$IFn$_invoke$arity$1(config))){
return cljs.core.dissoc.call(null,cljs.core.assoc.call(null,config,new cljs.core.Keyword(null,"on-jsload","on-jsload",-395756602),new cljs.core.Keyword(null,"jsload-callback","jsload-callback",-1949628369).cljs$core$IFn$_invoke$arity$1(config)),new cljs.core.Keyword(null,"jsload-callback","jsload-callback",-1949628369));
} else {
return config;
}
});
figwheel.client.fill_url_template = (function figwheel$client$fill_url_template(config){
if(cljs.core.truth_(figwheel.client.utils.html_env_QMARK_.call(null))){
return cljs.core.update_in.call(null,config,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"websocket-url","websocket-url",-490444938)], null),(function (x){
return clojure.string.replace.call(null,clojure.string.replace.call(null,x,"[[client-hostname]]",location.hostname),"[[client-port]]",location.port);
}));
} else {
return config;
}
});
figwheel.client.base_plugins = (function figwheel$client$base_plugins(system_options){
var base = new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"enforce-project-plugin","enforce-project-plugin",959402899),figwheel.client.enforce_project_plugin,new cljs.core.Keyword(null,"enforce-figwheel-version-plugin","enforce-figwheel-version-plugin",-1916185220),figwheel.client.enforce_figwheel_version_plugin,new cljs.core.Keyword(null,"file-reloader-plugin","file-reloader-plugin",-1792964733),figwheel.client.file_reloader_plugin,new cljs.core.Keyword(null,"comp-fail-warning-plugin","comp-fail-warning-plugin",634311),figwheel.client.compile_fail_warning_plugin,new cljs.core.Keyword(null,"css-reloader-plugin","css-reloader-plugin",2002032904),figwheel.client.css_reloader_plugin,new cljs.core.Keyword(null,"repl-plugin","repl-plugin",-1138952371),figwheel.client.repl_plugin], null);
var base__$1 = ((cljs.core.not.call(null,figwheel.client.utils.html_env_QMARK_.call(null)))?cljs.core.select_keys.call(null,base,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"file-reloader-plugin","file-reloader-plugin",-1792964733),new cljs.core.Keyword(null,"comp-fail-warning-plugin","comp-fail-warning-plugin",634311),new cljs.core.Keyword(null,"repl-plugin","repl-plugin",-1138952371)], null)):base);
var base__$2 = ((new cljs.core.Keyword(null,"autoload","autoload",-354122500).cljs$core$IFn$_invoke$arity$1(system_options) === false)?cljs.core.dissoc.call(null,base__$1,new cljs.core.Keyword(null,"file-reloader-plugin","file-reloader-plugin",-1792964733)):base__$1);
if(cljs.core.truth_((function (){var and__30170__auto__ = new cljs.core.Keyword(null,"heads-up-display","heads-up-display",-896577202).cljs$core$IFn$_invoke$arity$1(system_options);
if(cljs.core.truth_(and__30170__auto__)){
return figwheel.client.utils.html_env_QMARK_.call(null);
} else {
return and__30170__auto__;
}
})())){
return cljs.core.assoc.call(null,base__$2,new cljs.core.Keyword(null,"heads-up-display-plugin","heads-up-display-plugin",1745207501),figwheel.client.heads_up_plugin);
} else {
return base__$2;
}
});
figwheel.client.add_message_watch = (function figwheel$client$add_message_watch(key,callback){
return cljs.core.add_watch.call(null,figwheel.client.socket.message_history_atom,key,(function (_,___$1,___$2,msg_hist){
return callback.call(null,cljs.core.first.call(null,msg_hist));
}));
});
figwheel.client.add_plugins = (function figwheel$client$add_plugins(plugins,system_options){
var seq__46544 = cljs.core.seq.call(null,plugins);
var chunk__46545 = null;
var count__46546 = (0);
var i__46547 = (0);
while(true){
if((i__46547 < count__46546)){
var vec__46548 = cljs.core._nth.call(null,chunk__46545,i__46547);
var k = cljs.core.nth.call(null,vec__46548,(0),null);
var plugin = cljs.core.nth.call(null,vec__46548,(1),null);
if(cljs.core.truth_(plugin)){
var pl_46554 = plugin.call(null,system_options);
cljs.core.add_watch.call(null,figwheel.client.socket.message_history_atom,k,((function (seq__46544,chunk__46545,count__46546,i__46547,pl_46554,vec__46548,k,plugin){
return (function (_,___$1,___$2,msg_hist){
return pl_46554.call(null,msg_hist);
});})(seq__46544,chunk__46545,count__46546,i__46547,pl_46554,vec__46548,k,plugin))
);
} else {
}

var G__46555 = seq__46544;
var G__46556 = chunk__46545;
var G__46557 = count__46546;
var G__46558 = (i__46547 + (1));
seq__46544 = G__46555;
chunk__46545 = G__46556;
count__46546 = G__46557;
i__46547 = G__46558;
continue;
} else {
var temp__5278__auto__ = cljs.core.seq.call(null,seq__46544);
if(temp__5278__auto__){
var seq__46544__$1 = temp__5278__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__46544__$1)){
var c__31113__auto__ = cljs.core.chunk_first.call(null,seq__46544__$1);
var G__46559 = cljs.core.chunk_rest.call(null,seq__46544__$1);
var G__46560 = c__31113__auto__;
var G__46561 = cljs.core.count.call(null,c__31113__auto__);
var G__46562 = (0);
seq__46544 = G__46559;
chunk__46545 = G__46560;
count__46546 = G__46561;
i__46547 = G__46562;
continue;
} else {
var vec__46551 = cljs.core.first.call(null,seq__46544__$1);
var k = cljs.core.nth.call(null,vec__46551,(0),null);
var plugin = cljs.core.nth.call(null,vec__46551,(1),null);
if(cljs.core.truth_(plugin)){
var pl_46563 = plugin.call(null,system_options);
cljs.core.add_watch.call(null,figwheel.client.socket.message_history_atom,k,((function (seq__46544,chunk__46545,count__46546,i__46547,pl_46563,vec__46551,k,plugin,seq__46544__$1,temp__5278__auto__){
return (function (_,___$1,___$2,msg_hist){
return pl_46563.call(null,msg_hist);
});})(seq__46544,chunk__46545,count__46546,i__46547,pl_46563,vec__46551,k,plugin,seq__46544__$1,temp__5278__auto__))
);
} else {
}

var G__46564 = cljs.core.next.call(null,seq__46544__$1);
var G__46565 = null;
var G__46566 = (0);
var G__46567 = (0);
seq__46544 = G__46564;
chunk__46545 = G__46565;
count__46546 = G__46566;
i__46547 = G__46567;
continue;
}
} else {
return null;
}
}
break;
}
});
figwheel.client.start = (function figwheel$client$start(var_args){
var G__46569 = arguments.length;
switch (G__46569) {
case 1:
return figwheel.client.start.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 0:
return figwheel.client.start.cljs$core$IFn$_invoke$arity$0();

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

figwheel.client.start.cljs$core$IFn$_invoke$arity$1 = (function (opts){
if((goog.dependencies_ == null)){
return null;
} else {
if(typeof figwheel.client.__figwheel_start_once__ !== 'undefined'){
return null;
} else {
return (
figwheel.client.__figwheel_start_once__ = setTimeout((function (){
var plugins_SINGLEQUOTE_ = new cljs.core.Keyword(null,"plugins","plugins",1900073717).cljs$core$IFn$_invoke$arity$1(opts);
var merge_plugins = new cljs.core.Keyword(null,"merge-plugins","merge-plugins",-1193912370).cljs$core$IFn$_invoke$arity$1(opts);
var system_options = figwheel.client.fill_url_template.call(null,figwheel.client.handle_deprecated_jsload_callback.call(null,cljs.core.merge.call(null,figwheel.client.config_defaults,cljs.core.dissoc.call(null,opts,new cljs.core.Keyword(null,"plugins","plugins",1900073717),new cljs.core.Keyword(null,"merge-plugins","merge-plugins",-1193912370)))));
var plugins = (cljs.core.truth_(plugins_SINGLEQUOTE_)?plugins_SINGLEQUOTE_:cljs.core.merge.call(null,figwheel.client.base_plugins.call(null,system_options),merge_plugins));
figwheel.client.utils._STAR_print_debug_STAR_ = new cljs.core.Keyword(null,"debug","debug",-1608172596).cljs$core$IFn$_invoke$arity$1(opts);

figwheel.client.enable_repl_print_BANG_.call(null);

figwheel.client.add_plugins.call(null,plugins,system_options);

figwheel.client.file_reloading.patch_goog_base.call(null);

var seq__46570_46575 = cljs.core.seq.call(null,new cljs.core.Keyword(null,"initial-messages","initial-messages",2057377771).cljs$core$IFn$_invoke$arity$1(system_options));
var chunk__46571_46576 = null;
var count__46572_46577 = (0);
var i__46573_46578 = (0);
while(true){
if((i__46573_46578 < count__46572_46577)){
var msg_46579 = cljs.core._nth.call(null,chunk__46571_46576,i__46573_46578);
figwheel.client.socket.handle_incoming_message.call(null,msg_46579);

var G__46580 = seq__46570_46575;
var G__46581 = chunk__46571_46576;
var G__46582 = count__46572_46577;
var G__46583 = (i__46573_46578 + (1));
seq__46570_46575 = G__46580;
chunk__46571_46576 = G__46581;
count__46572_46577 = G__46582;
i__46573_46578 = G__46583;
continue;
} else {
var temp__5278__auto___46584 = cljs.core.seq.call(null,seq__46570_46575);
if(temp__5278__auto___46584){
var seq__46570_46585__$1 = temp__5278__auto___46584;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__46570_46585__$1)){
var c__31113__auto___46586 = cljs.core.chunk_first.call(null,seq__46570_46585__$1);
var G__46587 = cljs.core.chunk_rest.call(null,seq__46570_46585__$1);
var G__46588 = c__31113__auto___46586;
var G__46589 = cljs.core.count.call(null,c__31113__auto___46586);
var G__46590 = (0);
seq__46570_46575 = G__46587;
chunk__46571_46576 = G__46588;
count__46572_46577 = G__46589;
i__46573_46578 = G__46590;
continue;
} else {
var msg_46591 = cljs.core.first.call(null,seq__46570_46585__$1);
figwheel.client.socket.handle_incoming_message.call(null,msg_46591);

var G__46592 = cljs.core.next.call(null,seq__46570_46585__$1);
var G__46593 = null;
var G__46594 = (0);
var G__46595 = (0);
seq__46570_46575 = G__46592;
chunk__46571_46576 = G__46593;
count__46572_46577 = G__46594;
i__46573_46578 = G__46595;
continue;
}
} else {
}
}
break;
}

return figwheel.client.socket.open.call(null,system_options);
})))
;
}
}
});

figwheel.client.start.cljs$core$IFn$_invoke$arity$0 = (function (){
return figwheel.client.start.call(null,cljs.core.PersistentArrayMap.EMPTY);
});

figwheel.client.start.cljs$lang$maxFixedArity = 1;

figwheel.client.watch_and_reload_with_opts = figwheel.client.start;
figwheel.client.watch_and_reload = (function figwheel$client$watch_and_reload(var_args){
var args__31466__auto__ = [];
var len__31459__auto___46600 = arguments.length;
var i__31460__auto___46601 = (0);
while(true){
if((i__31460__auto___46601 < len__31459__auto___46600)){
args__31466__auto__.push((arguments[i__31460__auto___46601]));

var G__46602 = (i__31460__auto___46601 + (1));
i__31460__auto___46601 = G__46602;
continue;
} else {
}
break;
}

var argseq__31467__auto__ = ((((0) < args__31466__auto__.length))?(new cljs.core.IndexedSeq(args__31466__auto__.slice((0)),(0),null)):null);
return figwheel.client.watch_and_reload.cljs$core$IFn$_invoke$arity$variadic(argseq__31467__auto__);
});

figwheel.client.watch_and_reload.cljs$core$IFn$_invoke$arity$variadic = (function (p__46597){
var map__46598 = p__46597;
var map__46598__$1 = ((((!((map__46598 == null)))?((((map__46598.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__46598.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__46598):map__46598);
var opts = map__46598__$1;
return figwheel.client.start.call(null,opts);
});

figwheel.client.watch_and_reload.cljs$lang$maxFixedArity = (0);

figwheel.client.watch_and_reload.cljs$lang$applyTo = (function (seq46596){
return figwheel.client.watch_and_reload.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq46596));
});

figwheel.client.fetch_data_from_env = (function figwheel$client$fetch_data_from_env(){
try{return cljs.reader.read_string.call(null,goog.object.get(window,"FIGWHEEL_CLIENT_CONFIGURATION"));
}catch (e46603){if((e46603 instanceof Error)){
var e = e46603;
cljs.core._STAR_print_err_fn_STAR_.call(null,"Unable to load FIGWHEEL_CLIENT_CONFIGURATION from the environment");

return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"autoload","autoload",-354122500),false], null);
} else {
throw e46603;

}
}});
figwheel.client.console_intro_message = "Figwheel has compiled a temporary helper application to your :output-file.\n\nThe code currently in your configured output file does not\nrepresent the code that you are trying to compile.\n\nThis temporary application is intended to help you continue to get\nfeedback from Figwheel until the build you are working on compiles\ncorrectly.\n\nWhen your ClojureScript source code compiles correctly this helper\napplication will auto-reload and pick up your freshly compiled\nClojureScript program.";
figwheel.client.bad_compile_helper_app = (function figwheel$client$bad_compile_helper_app(){
cljs.core.enable_console_print_BANG_.call(null);

var config = figwheel.client.fetch_data_from_env.call(null);
cljs.core.println.call(null,figwheel.client.console_intro_message);

figwheel.client.heads_up.bad_compile_screen.call(null);

if(cljs.core.truth_(goog.dependencies_)){
} else {
goog.dependencies_ = true;
}

figwheel.client.start.call(null,config);

return figwheel.client.add_message_watch.call(null,new cljs.core.Keyword(null,"listen-for-successful-compile","listen-for-successful-compile",-995277603),((function (config){
return (function (p__46604){
var map__46605 = p__46604;
var map__46605__$1 = ((((!((map__46605 == null)))?((((map__46605.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__46605.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__46605):map__46605);
var msg_name = cljs.core.get.call(null,map__46605__$1,new cljs.core.Keyword(null,"msg-name","msg-name",-353709863));
if(cljs.core._EQ_.call(null,msg_name,new cljs.core.Keyword(null,"files-changed","files-changed",-1418200563))){
return location.href = location.href;
} else {
return null;
}
});})(config))
);
});

//# sourceMappingURL=client.js.map?rel=1507225501854

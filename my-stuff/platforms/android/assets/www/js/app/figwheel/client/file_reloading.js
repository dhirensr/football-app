// Compiled by ClojureScript 1.9.908 {}
goog.provide('figwheel.client.file_reloading');
goog.require('cljs.core');
goog.require('figwheel.client.utils');
goog.require('goog.Uri');
goog.require('goog.string');
goog.require('goog.object');
goog.require('goog.net.jsloader');
goog.require('goog.html.legacyconversions');
goog.require('clojure.string');
goog.require('clojure.set');
goog.require('cljs.core.async');
goog.require('goog.async.Deferred');
if(typeof figwheel.client.file_reloading.figwheel_meta_pragmas !== 'undefined'){
} else {
figwheel.client.file_reloading.figwheel_meta_pragmas = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
}
figwheel.client.file_reloading.on_jsload_custom_event = (function figwheel$client$file_reloading$on_jsload_custom_event(url){
return figwheel.client.utils.dispatch_custom_event.call(null,"figwheel.js-reload",url);
});
figwheel.client.file_reloading.before_jsload_custom_event = (function figwheel$client$file_reloading$before_jsload_custom_event(files){
return figwheel.client.utils.dispatch_custom_event.call(null,"figwheel.before-js-reload",files);
});
figwheel.client.file_reloading.on_cssload_custom_event = (function figwheel$client$file_reloading$on_cssload_custom_event(files){
return figwheel.client.utils.dispatch_custom_event.call(null,"figwheel.css-reload",files);
});
figwheel.client.file_reloading.namespace_file_map_QMARK_ = (function figwheel$client$file_reloading$namespace_file_map_QMARK_(m){
var or__30182__auto__ = (cljs.core.map_QMARK_.call(null,m)) && (typeof new cljs.core.Keyword(null,"namespace","namespace",-377510372).cljs$core$IFn$_invoke$arity$1(m) === 'string') && (((new cljs.core.Keyword(null,"file","file",-1269645878).cljs$core$IFn$_invoke$arity$1(m) == null)) || (typeof new cljs.core.Keyword(null,"file","file",-1269645878).cljs$core$IFn$_invoke$arity$1(m) === 'string')) && (cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"type","type",1174270348).cljs$core$IFn$_invoke$arity$1(m),new cljs.core.Keyword(null,"namespace","namespace",-377510372)));
if(or__30182__auto__){
return or__30182__auto__;
} else {
cljs.core.println.call(null,"Error not namespace-file-map",cljs.core.pr_str.call(null,m));

return false;
}
});
figwheel.client.file_reloading.add_cache_buster = (function figwheel$client$file_reloading$add_cache_buster(url){

return goog.Uri.parse(url).makeUnique();
});
figwheel.client.file_reloading.name__GT_path = (function figwheel$client$file_reloading$name__GT_path(ns){

return goog.object.get(goog.dependencies_.nameToPath,ns);
});
figwheel.client.file_reloading.provided_QMARK_ = (function figwheel$client$file_reloading$provided_QMARK_(ns){
return goog.object.get(goog.dependencies_.written,figwheel.client.file_reloading.name__GT_path.call(null,ns));
});
figwheel.client.file_reloading.immutable_ns_QMARK_ = (function figwheel$client$file_reloading$immutable_ns_QMARK_(name){
var or__30182__auto__ = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 3, ["cljs.nodejs",null,"goog",null,"cljs.core",null], null), null).call(null,name);
if(cljs.core.truth_(or__30182__auto__)){
return or__30182__auto__;
} else {
var or__30182__auto____$1 = goog.string.startsWith("clojure.",name);
if(cljs.core.truth_(or__30182__auto____$1)){
return or__30182__auto____$1;
} else {
return goog.string.startsWith("goog.",name);
}
}
});
figwheel.client.file_reloading.get_requires = (function figwheel$client$file_reloading$get_requires(ns){
return cljs.core.set.call(null,cljs.core.filter.call(null,(function (p1__44398_SHARP_){
return cljs.core.not.call(null,figwheel.client.file_reloading.immutable_ns_QMARK_.call(null,p1__44398_SHARP_));
}),goog.object.getKeys(goog.object.get(goog.dependencies_.requires,figwheel.client.file_reloading.name__GT_path.call(null,ns)))));
});
if(typeof figwheel.client.file_reloading.dependency_data !== 'undefined'){
} else {
figwheel.client.file_reloading.dependency_data = cljs.core.atom.call(null,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"pathToName","pathToName",-1236616181),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"dependents","dependents",136812837),cljs.core.PersistentArrayMap.EMPTY], null));
}
figwheel.client.file_reloading.path_to_name_BANG_ = (function figwheel$client$file_reloading$path_to_name_BANG_(path,name){
return cljs.core.swap_BANG_.call(null,figwheel.client.file_reloading.dependency_data,cljs.core.update_in,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"pathToName","pathToName",-1236616181),path], null),cljs.core.fnil.call(null,clojure.set.union,cljs.core.PersistentHashSet.EMPTY),cljs.core.PersistentHashSet.createAsIfByAssoc([name]));
});
/**
 * Setup a path to name dependencies map.
 * That goes from path -> #{ ns-names }
 */
figwheel.client.file_reloading.setup_path__GT_name_BANG_ = (function figwheel$client$file_reloading$setup_path__GT_name_BANG_(){
var nameToPath = goog.object.filter(goog.dependencies_.nameToPath,(function (v,k,o){
return goog.string.startsWith(v,"../");
}));
return goog.object.forEach(nameToPath,((function (nameToPath){
return (function (v,k,o){
return figwheel.client.file_reloading.path_to_name_BANG_.call(null,v,k);
});})(nameToPath))
);
});
/**
 * returns a set of namespaces defined by a path
 */
figwheel.client.file_reloading.path__GT_name = (function figwheel$client$file_reloading$path__GT_name(path){
return cljs.core.get_in.call(null,cljs.core.deref.call(null,figwheel.client.file_reloading.dependency_data),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"pathToName","pathToName",-1236616181),path], null));
});
figwheel.client.file_reloading.name_to_parent_BANG_ = (function figwheel$client$file_reloading$name_to_parent_BANG_(ns,parent_ns){
return cljs.core.swap_BANG_.call(null,figwheel.client.file_reloading.dependency_data,cljs.core.update_in,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"dependents","dependents",136812837),ns], null),cljs.core.fnil.call(null,clojure.set.union,cljs.core.PersistentHashSet.EMPTY),cljs.core.PersistentHashSet.createAsIfByAssoc([parent_ns]));
});
/**
 * This reverses the goog.dependencies_.requires for looking up ns-dependents.
 */
figwheel.client.file_reloading.setup_ns__GT_dependents_BANG_ = (function figwheel$client$file_reloading$setup_ns__GT_dependents_BANG_(){
var requires = goog.object.filter(goog.dependencies_.requires,(function (v,k,o){
return goog.string.startsWith(k,"../");
}));
return goog.object.forEach(requires,((function (requires){
return (function (v,k,_){
return goog.object.forEach(v,((function (requires){
return (function (v_SINGLEQUOTE_,k_SINGLEQUOTE_,___$1){
var seq__44399 = cljs.core.seq.call(null,figwheel.client.file_reloading.path__GT_name.call(null,k));
var chunk__44400 = null;
var count__44401 = (0);
var i__44402 = (0);
while(true){
if((i__44402 < count__44401)){
var n = cljs.core._nth.call(null,chunk__44400,i__44402);
figwheel.client.file_reloading.name_to_parent_BANG_.call(null,k_SINGLEQUOTE_,n);

var G__44403 = seq__44399;
var G__44404 = chunk__44400;
var G__44405 = count__44401;
var G__44406 = (i__44402 + (1));
seq__44399 = G__44403;
chunk__44400 = G__44404;
count__44401 = G__44405;
i__44402 = G__44406;
continue;
} else {
var temp__5278__auto__ = cljs.core.seq.call(null,seq__44399);
if(temp__5278__auto__){
var seq__44399__$1 = temp__5278__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__44399__$1)){
var c__31113__auto__ = cljs.core.chunk_first.call(null,seq__44399__$1);
var G__44407 = cljs.core.chunk_rest.call(null,seq__44399__$1);
var G__44408 = c__31113__auto__;
var G__44409 = cljs.core.count.call(null,c__31113__auto__);
var G__44410 = (0);
seq__44399 = G__44407;
chunk__44400 = G__44408;
count__44401 = G__44409;
i__44402 = G__44410;
continue;
} else {
var n = cljs.core.first.call(null,seq__44399__$1);
figwheel.client.file_reloading.name_to_parent_BANG_.call(null,k_SINGLEQUOTE_,n);

var G__44411 = cljs.core.next.call(null,seq__44399__$1);
var G__44412 = null;
var G__44413 = (0);
var G__44414 = (0);
seq__44399 = G__44411;
chunk__44400 = G__44412;
count__44401 = G__44413;
i__44402 = G__44414;
continue;
}
} else {
return null;
}
}
break;
}
});})(requires))
);
});})(requires))
);
});
figwheel.client.file_reloading.ns__GT_dependents = (function figwheel$client$file_reloading$ns__GT_dependents(ns){
return cljs.core.get_in.call(null,cljs.core.deref.call(null,figwheel.client.file_reloading.dependency_data),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"dependents","dependents",136812837),ns], null));
});
figwheel.client.file_reloading.build_topo_sort = (function figwheel$client$file_reloading$build_topo_sort(get_deps){
var get_deps__$1 = cljs.core.memoize.call(null,get_deps);
var topo_sort_helper_STAR_ = ((function (get_deps__$1){
return (function figwheel$client$file_reloading$build_topo_sort_$_topo_sort_helper_STAR_(x,depth,state){
var deps = get_deps__$1.call(null,x);
if(cljs.core.empty_QMARK_.call(null,deps)){
return null;
} else {
return topo_sort_STAR_.call(null,deps,depth,state);
}
});})(get_deps__$1))
;
var topo_sort_STAR_ = ((function (get_deps__$1){
return (function() {
var figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR_ = null;
var figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR___1 = (function (deps){
return figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR_.call(null,deps,(0),cljs.core.atom.call(null,cljs.core.sorted_map.call(null)));
});
var figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR___3 = (function (deps,depth,state){
cljs.core.swap_BANG_.call(null,state,cljs.core.update_in,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [depth], null),cljs.core.fnil.call(null,cljs.core.into,cljs.core.PersistentHashSet.EMPTY),deps);

var seq__44424_44432 = cljs.core.seq.call(null,deps);
var chunk__44425_44433 = null;
var count__44426_44434 = (0);
var i__44427_44435 = (0);
while(true){
if((i__44427_44435 < count__44426_44434)){
var dep_44436 = cljs.core._nth.call(null,chunk__44425_44433,i__44427_44435);
topo_sort_helper_STAR_.call(null,dep_44436,(depth + (1)),state);

var G__44437 = seq__44424_44432;
var G__44438 = chunk__44425_44433;
var G__44439 = count__44426_44434;
var G__44440 = (i__44427_44435 + (1));
seq__44424_44432 = G__44437;
chunk__44425_44433 = G__44438;
count__44426_44434 = G__44439;
i__44427_44435 = G__44440;
continue;
} else {
var temp__5278__auto___44441 = cljs.core.seq.call(null,seq__44424_44432);
if(temp__5278__auto___44441){
var seq__44424_44442__$1 = temp__5278__auto___44441;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__44424_44442__$1)){
var c__31113__auto___44443 = cljs.core.chunk_first.call(null,seq__44424_44442__$1);
var G__44444 = cljs.core.chunk_rest.call(null,seq__44424_44442__$1);
var G__44445 = c__31113__auto___44443;
var G__44446 = cljs.core.count.call(null,c__31113__auto___44443);
var G__44447 = (0);
seq__44424_44432 = G__44444;
chunk__44425_44433 = G__44445;
count__44426_44434 = G__44446;
i__44427_44435 = G__44447;
continue;
} else {
var dep_44448 = cljs.core.first.call(null,seq__44424_44442__$1);
topo_sort_helper_STAR_.call(null,dep_44448,(depth + (1)),state);

var G__44449 = cljs.core.next.call(null,seq__44424_44442__$1);
var G__44450 = null;
var G__44451 = (0);
var G__44452 = (0);
seq__44424_44432 = G__44449;
chunk__44425_44433 = G__44450;
count__44426_44434 = G__44451;
i__44427_44435 = G__44452;
continue;
}
} else {
}
}
break;
}

if(cljs.core._EQ_.call(null,depth,(0))){
return elim_dups_STAR_.call(null,cljs.core.reverse.call(null,cljs.core.vals.call(null,cljs.core.deref.call(null,state))));
} else {
return null;
}
});
figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR_ = function(deps,depth,state){
switch(arguments.length){
case 1:
return figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR___1.call(this,deps);
case 3:
return figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR___3.call(this,deps,depth,state);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR_.cljs$core$IFn$_invoke$arity$1 = figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR___1;
figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR_.cljs$core$IFn$_invoke$arity$3 = figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR___3;
return figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR_;
})()
;})(get_deps__$1))
;
var elim_dups_STAR_ = ((function (get_deps__$1){
return (function figwheel$client$file_reloading$build_topo_sort_$_elim_dups_STAR_(p__44428){
var vec__44429 = p__44428;
var seq__44430 = cljs.core.seq.call(null,vec__44429);
var first__44431 = cljs.core.first.call(null,seq__44430);
var seq__44430__$1 = cljs.core.next.call(null,seq__44430);
var x = first__44431;
var xs = seq__44430__$1;
if((x == null)){
return cljs.core.List.EMPTY;
} else {
return cljs.core.cons.call(null,x,figwheel$client$file_reloading$build_topo_sort_$_elim_dups_STAR_.call(null,cljs.core.map.call(null,((function (vec__44429,seq__44430,first__44431,seq__44430__$1,x,xs,get_deps__$1){
return (function (p1__44415_SHARP_){
return clojure.set.difference.call(null,p1__44415_SHARP_,x);
});})(vec__44429,seq__44430,first__44431,seq__44430__$1,x,xs,get_deps__$1))
,xs)));
}
});})(get_deps__$1))
;
return topo_sort_STAR_;
});
figwheel.client.file_reloading.get_all_dependencies = (function figwheel$client$file_reloading$get_all_dependencies(ns){
var topo_sort_SINGLEQUOTE_ = figwheel.client.file_reloading.build_topo_sort.call(null,figwheel.client.file_reloading.get_requires);
return cljs.core.apply.call(null,cljs.core.concat,topo_sort_SINGLEQUOTE_.call(null,cljs.core.set.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [ns], null))));
});
figwheel.client.file_reloading.get_all_dependents = (function figwheel$client$file_reloading$get_all_dependents(nss){
var topo_sort_SINGLEQUOTE_ = figwheel.client.file_reloading.build_topo_sort.call(null,figwheel.client.file_reloading.ns__GT_dependents);
return cljs.core.filter.call(null,cljs.core.comp.call(null,cljs.core.not,figwheel.client.file_reloading.immutable_ns_QMARK_),cljs.core.reverse.call(null,cljs.core.apply.call(null,cljs.core.concat,topo_sort_SINGLEQUOTE_.call(null,cljs.core.set.call(null,nss)))));
});
figwheel.client.file_reloading.unprovide_BANG_ = (function figwheel$client$file_reloading$unprovide_BANG_(ns){
var path = figwheel.client.file_reloading.name__GT_path.call(null,ns);
goog.object.remove(goog.dependencies_.visited,path);

goog.object.remove(goog.dependencies_.written,path);

return goog.object.remove(goog.dependencies_.written,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(goog.basePath),cljs.core.str.cljs$core$IFn$_invoke$arity$1(path)].join(''));
});
figwheel.client.file_reloading.resolve_ns = (function figwheel$client$file_reloading$resolve_ns(ns){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(goog.basePath),cljs.core.str.cljs$core$IFn$_invoke$arity$1(figwheel.client.file_reloading.name__GT_path.call(null,ns))].join('');
});
figwheel.client.file_reloading.addDependency = (function figwheel$client$file_reloading$addDependency(path,provides,requires){
var seq__44453 = cljs.core.seq.call(null,provides);
var chunk__44454 = null;
var count__44455 = (0);
var i__44456 = (0);
while(true){
if((i__44456 < count__44455)){
var prov = cljs.core._nth.call(null,chunk__44454,i__44456);
figwheel.client.file_reloading.path_to_name_BANG_.call(null,path,prov);

var seq__44457_44465 = cljs.core.seq.call(null,requires);
var chunk__44458_44466 = null;
var count__44459_44467 = (0);
var i__44460_44468 = (0);
while(true){
if((i__44460_44468 < count__44459_44467)){
var req_44469 = cljs.core._nth.call(null,chunk__44458_44466,i__44460_44468);
figwheel.client.file_reloading.name_to_parent_BANG_.call(null,req_44469,prov);

var G__44470 = seq__44457_44465;
var G__44471 = chunk__44458_44466;
var G__44472 = count__44459_44467;
var G__44473 = (i__44460_44468 + (1));
seq__44457_44465 = G__44470;
chunk__44458_44466 = G__44471;
count__44459_44467 = G__44472;
i__44460_44468 = G__44473;
continue;
} else {
var temp__5278__auto___44474 = cljs.core.seq.call(null,seq__44457_44465);
if(temp__5278__auto___44474){
var seq__44457_44475__$1 = temp__5278__auto___44474;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__44457_44475__$1)){
var c__31113__auto___44476 = cljs.core.chunk_first.call(null,seq__44457_44475__$1);
var G__44477 = cljs.core.chunk_rest.call(null,seq__44457_44475__$1);
var G__44478 = c__31113__auto___44476;
var G__44479 = cljs.core.count.call(null,c__31113__auto___44476);
var G__44480 = (0);
seq__44457_44465 = G__44477;
chunk__44458_44466 = G__44478;
count__44459_44467 = G__44479;
i__44460_44468 = G__44480;
continue;
} else {
var req_44481 = cljs.core.first.call(null,seq__44457_44475__$1);
figwheel.client.file_reloading.name_to_parent_BANG_.call(null,req_44481,prov);

var G__44482 = cljs.core.next.call(null,seq__44457_44475__$1);
var G__44483 = null;
var G__44484 = (0);
var G__44485 = (0);
seq__44457_44465 = G__44482;
chunk__44458_44466 = G__44483;
count__44459_44467 = G__44484;
i__44460_44468 = G__44485;
continue;
}
} else {
}
}
break;
}

var G__44486 = seq__44453;
var G__44487 = chunk__44454;
var G__44488 = count__44455;
var G__44489 = (i__44456 + (1));
seq__44453 = G__44486;
chunk__44454 = G__44487;
count__44455 = G__44488;
i__44456 = G__44489;
continue;
} else {
var temp__5278__auto__ = cljs.core.seq.call(null,seq__44453);
if(temp__5278__auto__){
var seq__44453__$1 = temp__5278__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__44453__$1)){
var c__31113__auto__ = cljs.core.chunk_first.call(null,seq__44453__$1);
var G__44490 = cljs.core.chunk_rest.call(null,seq__44453__$1);
var G__44491 = c__31113__auto__;
var G__44492 = cljs.core.count.call(null,c__31113__auto__);
var G__44493 = (0);
seq__44453 = G__44490;
chunk__44454 = G__44491;
count__44455 = G__44492;
i__44456 = G__44493;
continue;
} else {
var prov = cljs.core.first.call(null,seq__44453__$1);
figwheel.client.file_reloading.path_to_name_BANG_.call(null,path,prov);

var seq__44461_44494 = cljs.core.seq.call(null,requires);
var chunk__44462_44495 = null;
var count__44463_44496 = (0);
var i__44464_44497 = (0);
while(true){
if((i__44464_44497 < count__44463_44496)){
var req_44498 = cljs.core._nth.call(null,chunk__44462_44495,i__44464_44497);
figwheel.client.file_reloading.name_to_parent_BANG_.call(null,req_44498,prov);

var G__44499 = seq__44461_44494;
var G__44500 = chunk__44462_44495;
var G__44501 = count__44463_44496;
var G__44502 = (i__44464_44497 + (1));
seq__44461_44494 = G__44499;
chunk__44462_44495 = G__44500;
count__44463_44496 = G__44501;
i__44464_44497 = G__44502;
continue;
} else {
var temp__5278__auto___44503__$1 = cljs.core.seq.call(null,seq__44461_44494);
if(temp__5278__auto___44503__$1){
var seq__44461_44504__$1 = temp__5278__auto___44503__$1;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__44461_44504__$1)){
var c__31113__auto___44505 = cljs.core.chunk_first.call(null,seq__44461_44504__$1);
var G__44506 = cljs.core.chunk_rest.call(null,seq__44461_44504__$1);
var G__44507 = c__31113__auto___44505;
var G__44508 = cljs.core.count.call(null,c__31113__auto___44505);
var G__44509 = (0);
seq__44461_44494 = G__44506;
chunk__44462_44495 = G__44507;
count__44463_44496 = G__44508;
i__44464_44497 = G__44509;
continue;
} else {
var req_44510 = cljs.core.first.call(null,seq__44461_44504__$1);
figwheel.client.file_reloading.name_to_parent_BANG_.call(null,req_44510,prov);

var G__44511 = cljs.core.next.call(null,seq__44461_44504__$1);
var G__44512 = null;
var G__44513 = (0);
var G__44514 = (0);
seq__44461_44494 = G__44511;
chunk__44462_44495 = G__44512;
count__44463_44496 = G__44513;
i__44464_44497 = G__44514;
continue;
}
} else {
}
}
break;
}

var G__44515 = cljs.core.next.call(null,seq__44453__$1);
var G__44516 = null;
var G__44517 = (0);
var G__44518 = (0);
seq__44453 = G__44515;
chunk__44454 = G__44516;
count__44455 = G__44517;
i__44456 = G__44518;
continue;
}
} else {
return null;
}
}
break;
}
});
figwheel.client.file_reloading.figwheel_require = (function figwheel$client$file_reloading$figwheel_require(src,reload){
goog.require = figwheel.client.file_reloading.figwheel_require;

if(cljs.core._EQ_.call(null,reload,"reload-all")){
var seq__44519_44523 = cljs.core.seq.call(null,figwheel.client.file_reloading.get_all_dependencies.call(null,src));
var chunk__44520_44524 = null;
var count__44521_44525 = (0);
var i__44522_44526 = (0);
while(true){
if((i__44522_44526 < count__44521_44525)){
var ns_44527 = cljs.core._nth.call(null,chunk__44520_44524,i__44522_44526);
figwheel.client.file_reloading.unprovide_BANG_.call(null,ns_44527);

var G__44528 = seq__44519_44523;
var G__44529 = chunk__44520_44524;
var G__44530 = count__44521_44525;
var G__44531 = (i__44522_44526 + (1));
seq__44519_44523 = G__44528;
chunk__44520_44524 = G__44529;
count__44521_44525 = G__44530;
i__44522_44526 = G__44531;
continue;
} else {
var temp__5278__auto___44532 = cljs.core.seq.call(null,seq__44519_44523);
if(temp__5278__auto___44532){
var seq__44519_44533__$1 = temp__5278__auto___44532;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__44519_44533__$1)){
var c__31113__auto___44534 = cljs.core.chunk_first.call(null,seq__44519_44533__$1);
var G__44535 = cljs.core.chunk_rest.call(null,seq__44519_44533__$1);
var G__44536 = c__31113__auto___44534;
var G__44537 = cljs.core.count.call(null,c__31113__auto___44534);
var G__44538 = (0);
seq__44519_44523 = G__44535;
chunk__44520_44524 = G__44536;
count__44521_44525 = G__44537;
i__44522_44526 = G__44538;
continue;
} else {
var ns_44539 = cljs.core.first.call(null,seq__44519_44533__$1);
figwheel.client.file_reloading.unprovide_BANG_.call(null,ns_44539);

var G__44540 = cljs.core.next.call(null,seq__44519_44533__$1);
var G__44541 = null;
var G__44542 = (0);
var G__44543 = (0);
seq__44519_44523 = G__44540;
chunk__44520_44524 = G__44541;
count__44521_44525 = G__44542;
i__44522_44526 = G__44543;
continue;
}
} else {
}
}
break;
}
} else {
}

if(cljs.core.truth_(reload)){
figwheel.client.file_reloading.unprovide_BANG_.call(null,src);
} else {
}

return goog.require_figwheel_backup_(src);
});
/**
 * Reusable browser REPL bootstrapping. Patches the essential functions
 *   in goog.base to support re-loading of namespaces after page load.
 */
figwheel.client.file_reloading.bootstrap_goog_base = (function figwheel$client$file_reloading$bootstrap_goog_base(){
if(cljs.core.truth_(COMPILED)){
return null;
} else {
goog.require_figwheel_backup_ = (function (){var or__30182__auto__ = goog.require__;
if(cljs.core.truth_(or__30182__auto__)){
return or__30182__auto__;
} else {
return goog.require;
}
})();

goog.isProvided_ = (function (name){
return false;
});

figwheel.client.file_reloading.setup_path__GT_name_BANG_.call(null);

figwheel.client.file_reloading.setup_ns__GT_dependents_BANG_.call(null);

goog.addDependency_figwheel_backup_ = goog.addDependency;

goog.addDependency = (function() { 
var G__44544__delegate = function (args){
cljs.core.apply.call(null,figwheel.client.file_reloading.addDependency,args);

return cljs.core.apply.call(null,goog.addDependency_figwheel_backup_,args);
};
var G__44544 = function (var_args){
var args = null;
if (arguments.length > 0) {
var G__44545__i = 0, G__44545__a = new Array(arguments.length -  0);
while (G__44545__i < G__44545__a.length) {G__44545__a[G__44545__i] = arguments[G__44545__i + 0]; ++G__44545__i;}
  args = new cljs.core.IndexedSeq(G__44545__a,0,null);
} 
return G__44544__delegate.call(this,args);};
G__44544.cljs$lang$maxFixedArity = 0;
G__44544.cljs$lang$applyTo = (function (arglist__44546){
var args = cljs.core.seq(arglist__44546);
return G__44544__delegate(args);
});
G__44544.cljs$core$IFn$_invoke$arity$variadic = G__44544__delegate;
return G__44544;
})()
;

goog.constructNamespace_("cljs.user");

goog.global.CLOSURE_IMPORT_SCRIPT = figwheel.client.file_reloading.queued_file_reload;

return goog.require = figwheel.client.file_reloading.figwheel_require;
}
});
figwheel.client.file_reloading.patch_goog_base = (function figwheel$client$file_reloading$patch_goog_base(){
if(typeof figwheel.client.file_reloading.bootstrapped_cljs !== 'undefined'){
return null;
} else {
return (
figwheel.client.file_reloading.bootstrapped_cljs = (function (){
figwheel.client.file_reloading.bootstrap_goog_base.call(null);

return true;
})()
)
;
}
});
figwheel.client.file_reloading.gloader = ((typeof goog.net.jsloader.safeLoad !== 'undefined')?(function (p1__44547_SHARP_,p2__44548_SHARP_){
return goog.net.jsloader.safeLoad(goog.html.legacyconversions.trustedResourceUrlFromString([cljs.core.str.cljs$core$IFn$_invoke$arity$1(p1__44547_SHARP_)].join('')),p2__44548_SHARP_);
}):((typeof goog.net.jsloader.load !== 'undefined')?(function (p1__44549_SHARP_,p2__44550_SHARP_){
return goog.net.jsloader.load([cljs.core.str.cljs$core$IFn$_invoke$arity$1(p1__44549_SHARP_)].join(''),p2__44550_SHARP_);
}):(function(){throw cljs.core.ex_info.call(null,"No remote script loading function found.",cljs.core.PersistentArrayMap.EMPTY)})()
));
figwheel.client.file_reloading.reload_file_in_html_env = (function figwheel$client$file_reloading$reload_file_in_html_env(request_url,callback){

var G__44551 = figwheel.client.file_reloading.gloader.call(null,figwheel.client.file_reloading.add_cache_buster.call(null,request_url),({"cleanupWhenDone": true}));
G__44551.addCallback(((function (G__44551){
return (function (){
return cljs.core.apply.call(null,callback,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [true], null));
});})(G__44551))
);

G__44551.addErrback(((function (G__44551){
return (function (){
return cljs.core.apply.call(null,callback,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [false], null));
});})(G__44551))
);

return G__44551;
});
figwheel.client.file_reloading.reload_file_STAR_ = (function (){var pred__44552 = cljs.core._EQ_;
var expr__44553 = figwheel.client.utils.host_env_QMARK_.call(null);
if(cljs.core.truth_(pred__44552.call(null,new cljs.core.Keyword(null,"node","node",581201198),expr__44553))){
var node_path_lib = require("path");
var util_pattern = [cljs.core.str.cljs$core$IFn$_invoke$arity$1(node_path_lib.sep),cljs.core.str.cljs$core$IFn$_invoke$arity$1(node_path_lib.join("goog","bootstrap","nodejs.js"))].join('');
var util_path = goog.object.findKey(require.cache,((function (node_path_lib,util_pattern,pred__44552,expr__44553){
return (function (v,k,o){
return goog.string.endsWith(k,util_pattern);
});})(node_path_lib,util_pattern,pred__44552,expr__44553))
);
var parts = cljs.core.pop.call(null,cljs.core.pop.call(null,clojure.string.split.call(null,util_path,/[\/\\]/)));
var root_path = clojure.string.join.call(null,node_path_lib.sep,parts);
return ((function (node_path_lib,util_pattern,util_path,parts,root_path,pred__44552,expr__44553){
return (function (request_url,callback){

var cache_path = node_path_lib.resolve(root_path,request_url);
goog.object.remove(require.cache,cache_path);

return callback.call(null,(function (){try{return require(cache_path);
}catch (e44555){if((e44555 instanceof Error)){
var e = e44555;
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"error","error",-978969032),["Figwheel: Error loading file ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cache_path)].join(''));

figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"error","error",-978969032),e.stack);

return false;
} else {
throw e44555;

}
}})());
});
;})(node_path_lib,util_pattern,util_path,parts,root_path,pred__44552,expr__44553))
} else {
if(cljs.core.truth_(pred__44552.call(null,new cljs.core.Keyword(null,"html","html",-998796897),expr__44553))){
return figwheel.client.file_reloading.reload_file_in_html_env;
} else {
if(cljs.core.truth_(pred__44552.call(null,new cljs.core.Keyword(null,"react-native","react-native",-1543085138),expr__44553))){
return figwheel.client.file_reloading.reload_file_in_html_env;
} else {
if(cljs.core.truth_(pred__44552.call(null,new cljs.core.Keyword(null,"worker","worker",938239996),expr__44553))){
return ((function (pred__44552,expr__44553){
return (function (request_url,callback){

return callback.call(null,(function (){try{self.importScripts(figwheel.client.file_reloading.add_cache_buster.call(null,request_url));

return true;
}catch (e44556){if((e44556 instanceof Error)){
var e = e44556;
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"error","error",-978969032),["Figwheel: Error loading file ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(request_url)].join(''));

figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"error","error",-978969032),e.stack);

return false;
} else {
throw e44556;

}
}})());
});
;})(pred__44552,expr__44553))
} else {
return ((function (pred__44552,expr__44553){
return (function (a,b){
throw "Reload not defined for this platform";
});
;})(pred__44552,expr__44553))
}
}
}
}
})();
figwheel.client.file_reloading.reload_file = (function figwheel$client$file_reloading$reload_file(p__44557,callback){
var map__44558 = p__44557;
var map__44558__$1 = ((((!((map__44558 == null)))?((((map__44558.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__44558.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__44558):map__44558);
var file_msg = map__44558__$1;
var request_url = cljs.core.get.call(null,map__44558__$1,new cljs.core.Keyword(null,"request-url","request-url",2100346596));

figwheel.client.utils.debug_prn.call(null,["FigWheel: Attempting to load ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(request_url)].join(''));

return figwheel.client.file_reloading.reload_file_STAR_.call(null,request_url,((function (map__44558,map__44558__$1,file_msg,request_url){
return (function (success_QMARK_){
if(cljs.core.truth_(success_QMARK_)){
figwheel.client.utils.debug_prn.call(null,["FigWheel: Successfully loaded ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(request_url)].join(''));

return cljs.core.apply.call(null,callback,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.assoc.call(null,file_msg,new cljs.core.Keyword(null,"loaded-file","loaded-file",-168399375),true)], null));
} else {
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"error","error",-978969032),["Figwheel: Error loading file ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(request_url)].join(''));

return cljs.core.apply.call(null,callback,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [file_msg], null));
}
});})(map__44558,map__44558__$1,file_msg,request_url))
);
});
if(typeof figwheel.client.file_reloading.reload_chan !== 'undefined'){
} else {
figwheel.client.file_reloading.reload_chan = cljs.core.async.chan.call(null);
}
if(typeof figwheel.client.file_reloading.on_load_callbacks !== 'undefined'){
} else {
figwheel.client.file_reloading.on_load_callbacks = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
}
if(typeof figwheel.client.file_reloading.dependencies_loaded !== 'undefined'){
} else {
figwheel.client.file_reloading.dependencies_loaded = cljs.core.atom.call(null,cljs.core.PersistentVector.EMPTY);
}
figwheel.client.file_reloading.blocking_load = (function figwheel$client$file_reloading$blocking_load(url){
var out = cljs.core.async.chan.call(null);
figwheel.client.file_reloading.reload_file.call(null,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"request-url","request-url",2100346596),url], null),((function (out){
return (function (file_msg){
cljs.core.async.put_BANG_.call(null,out,file_msg);

return cljs.core.async.close_BANG_.call(null,out);
});})(out))
);

return out;
});
if(typeof figwheel.client.file_reloading.reloader_loop !== 'undefined'){
} else {
figwheel.client.file_reloading.reloader_loop = (function (){var c__35058__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__35058__auto__){
return (function (){
var f__35059__auto__ = (function (){var switch__34968__auto__ = ((function (c__35058__auto__){
return (function (state_44582){
var state_val_44583 = (state_44582[(1)]);
if((state_val_44583 === (7))){
var inst_44578 = (state_44582[(2)]);
var state_44582__$1 = state_44582;
var statearr_44584_44601 = state_44582__$1;
(statearr_44584_44601[(2)] = inst_44578);

(statearr_44584_44601[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44583 === (1))){
var state_44582__$1 = state_44582;
var statearr_44585_44602 = state_44582__$1;
(statearr_44585_44602[(2)] = null);

(statearr_44585_44602[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44583 === (4))){
var inst_44562 = (state_44582[(7)]);
var inst_44562__$1 = (state_44582[(2)]);
var state_44582__$1 = (function (){var statearr_44586 = state_44582;
(statearr_44586[(7)] = inst_44562__$1);

return statearr_44586;
})();
if(cljs.core.truth_(inst_44562__$1)){
var statearr_44587_44603 = state_44582__$1;
(statearr_44587_44603[(1)] = (5));

} else {
var statearr_44588_44604 = state_44582__$1;
(statearr_44588_44604[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44583 === (6))){
var state_44582__$1 = state_44582;
var statearr_44589_44605 = state_44582__$1;
(statearr_44589_44605[(2)] = null);

(statearr_44589_44605[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44583 === (3))){
var inst_44580 = (state_44582[(2)]);
var state_44582__$1 = state_44582;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_44582__$1,inst_44580);
} else {
if((state_val_44583 === (2))){
var state_44582__$1 = state_44582;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_44582__$1,(4),figwheel.client.file_reloading.reload_chan);
} else {
if((state_val_44583 === (11))){
var inst_44574 = (state_44582[(2)]);
var state_44582__$1 = (function (){var statearr_44590 = state_44582;
(statearr_44590[(8)] = inst_44574);

return statearr_44590;
})();
var statearr_44591_44606 = state_44582__$1;
(statearr_44591_44606[(2)] = null);

(statearr_44591_44606[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44583 === (9))){
var inst_44568 = (state_44582[(9)]);
var inst_44566 = (state_44582[(10)]);
var inst_44570 = inst_44568.call(null,inst_44566);
var state_44582__$1 = state_44582;
var statearr_44592_44607 = state_44582__$1;
(statearr_44592_44607[(2)] = inst_44570);

(statearr_44592_44607[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44583 === (5))){
var inst_44562 = (state_44582[(7)]);
var inst_44564 = figwheel.client.file_reloading.blocking_load.call(null,inst_44562);
var state_44582__$1 = state_44582;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_44582__$1,(8),inst_44564);
} else {
if((state_val_44583 === (10))){
var inst_44566 = (state_44582[(10)]);
var inst_44572 = cljs.core.swap_BANG_.call(null,figwheel.client.file_reloading.dependencies_loaded,cljs.core.conj,inst_44566);
var state_44582__$1 = state_44582;
var statearr_44593_44608 = state_44582__$1;
(statearr_44593_44608[(2)] = inst_44572);

(statearr_44593_44608[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44583 === (8))){
var inst_44568 = (state_44582[(9)]);
var inst_44562 = (state_44582[(7)]);
var inst_44566 = (state_44582[(2)]);
var inst_44567 = cljs.core.deref.call(null,figwheel.client.file_reloading.on_load_callbacks);
var inst_44568__$1 = cljs.core.get.call(null,inst_44567,inst_44562);
var state_44582__$1 = (function (){var statearr_44594 = state_44582;
(statearr_44594[(9)] = inst_44568__$1);

(statearr_44594[(10)] = inst_44566);

return statearr_44594;
})();
if(cljs.core.truth_(inst_44568__$1)){
var statearr_44595_44609 = state_44582__$1;
(statearr_44595_44609[(1)] = (9));

} else {
var statearr_44596_44610 = state_44582__$1;
(statearr_44596_44610[(1)] = (10));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__35058__auto__))
;
return ((function (switch__34968__auto__,c__35058__auto__){
return (function() {
var figwheel$client$file_reloading$state_machine__34969__auto__ = null;
var figwheel$client$file_reloading$state_machine__34969__auto____0 = (function (){
var statearr_44597 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_44597[(0)] = figwheel$client$file_reloading$state_machine__34969__auto__);

(statearr_44597[(1)] = (1));

return statearr_44597;
});
var figwheel$client$file_reloading$state_machine__34969__auto____1 = (function (state_44582){
while(true){
var ret_value__34970__auto__ = (function (){try{while(true){
var result__34971__auto__ = switch__34968__auto__.call(null,state_44582);
if(cljs.core.keyword_identical_QMARK_.call(null,result__34971__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__34971__auto__;
}
break;
}
}catch (e44598){if((e44598 instanceof Object)){
var ex__34972__auto__ = e44598;
var statearr_44599_44611 = state_44582;
(statearr_44599_44611[(5)] = ex__34972__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_44582);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e44598;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__34970__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__44612 = state_44582;
state_44582 = G__44612;
continue;
} else {
return ret_value__34970__auto__;
}
break;
}
});
figwheel$client$file_reloading$state_machine__34969__auto__ = function(state_44582){
switch(arguments.length){
case 0:
return figwheel$client$file_reloading$state_machine__34969__auto____0.call(this);
case 1:
return figwheel$client$file_reloading$state_machine__34969__auto____1.call(this,state_44582);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
figwheel$client$file_reloading$state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$file_reloading$state_machine__34969__auto____0;
figwheel$client$file_reloading$state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$file_reloading$state_machine__34969__auto____1;
return figwheel$client$file_reloading$state_machine__34969__auto__;
})()
;})(switch__34968__auto__,c__35058__auto__))
})();
var state__35060__auto__ = (function (){var statearr_44600 = f__35059__auto__.call(null);
(statearr_44600[(6)] = c__35058__auto__);

return statearr_44600;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__35060__auto__);
});})(c__35058__auto__))
);

return c__35058__auto__;
})();
}
figwheel.client.file_reloading.queued_file_reload = (function figwheel$client$file_reloading$queued_file_reload(url){
return cljs.core.async.put_BANG_.call(null,figwheel.client.file_reloading.reload_chan,url);
});
figwheel.client.file_reloading.require_with_callback = (function figwheel$client$file_reloading$require_with_callback(p__44613,callback){
var map__44614 = p__44613;
var map__44614__$1 = ((((!((map__44614 == null)))?((((map__44614.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__44614.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__44614):map__44614);
var file_msg = map__44614__$1;
var namespace = cljs.core.get.call(null,map__44614__$1,new cljs.core.Keyword(null,"namespace","namespace",-377510372));
var request_url = figwheel.client.file_reloading.resolve_ns.call(null,namespace);
cljs.core.swap_BANG_.call(null,figwheel.client.file_reloading.on_load_callbacks,cljs.core.assoc,request_url,((function (request_url,map__44614,map__44614__$1,file_msg,namespace){
return (function (file_msg_SINGLEQUOTE_){
cljs.core.swap_BANG_.call(null,figwheel.client.file_reloading.on_load_callbacks,cljs.core.dissoc,request_url);

return cljs.core.apply.call(null,callback,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.merge.call(null,file_msg,cljs.core.select_keys.call(null,file_msg_SINGLEQUOTE_,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"loaded-file","loaded-file",-168399375)], null)))], null));
});})(request_url,map__44614,map__44614__$1,file_msg,namespace))
);

return figwheel.client.file_reloading.figwheel_require.call(null,cljs.core.name.call(null,namespace),true);
});
figwheel.client.file_reloading.figwheel_no_load_QMARK_ = (function figwheel$client$file_reloading$figwheel_no_load_QMARK_(p__44616){
var map__44617 = p__44616;
var map__44617__$1 = ((((!((map__44617 == null)))?((((map__44617.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__44617.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__44617):map__44617);
var file_msg = map__44617__$1;
var namespace = cljs.core.get.call(null,map__44617__$1,new cljs.core.Keyword(null,"namespace","namespace",-377510372));
var meta_pragmas = cljs.core.get.call(null,cljs.core.deref.call(null,figwheel.client.file_reloading.figwheel_meta_pragmas),cljs.core.name.call(null,namespace));
return new cljs.core.Keyword(null,"figwheel-no-load","figwheel-no-load",-555840179).cljs$core$IFn$_invoke$arity$1(meta_pragmas);
});
figwheel.client.file_reloading.reload_file_QMARK_ = (function figwheel$client$file_reloading$reload_file_QMARK_(p__44619){
var map__44620 = p__44619;
var map__44620__$1 = ((((!((map__44620 == null)))?((((map__44620.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__44620.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__44620):map__44620);
var file_msg = map__44620__$1;
var namespace = cljs.core.get.call(null,map__44620__$1,new cljs.core.Keyword(null,"namespace","namespace",-377510372));

var meta_pragmas = cljs.core.get.call(null,cljs.core.deref.call(null,figwheel.client.file_reloading.figwheel_meta_pragmas),cljs.core.name.call(null,namespace));
var and__30170__auto__ = cljs.core.not.call(null,figwheel.client.file_reloading.figwheel_no_load_QMARK_.call(null,file_msg));
if(and__30170__auto__){
var or__30182__auto__ = new cljs.core.Keyword(null,"figwheel-always","figwheel-always",799819691).cljs$core$IFn$_invoke$arity$1(meta_pragmas);
if(cljs.core.truth_(or__30182__auto__)){
return or__30182__auto__;
} else {
var or__30182__auto____$1 = new cljs.core.Keyword(null,"figwheel-load","figwheel-load",1316089175).cljs$core$IFn$_invoke$arity$1(meta_pragmas);
if(cljs.core.truth_(or__30182__auto____$1)){
return or__30182__auto____$1;
} else {
return figwheel.client.file_reloading.provided_QMARK_.call(null,cljs.core.name.call(null,namespace));
}
}
} else {
return and__30170__auto__;
}
});
figwheel.client.file_reloading.js_reload = (function figwheel$client$file_reloading$js_reload(p__44622,callback){
var map__44623 = p__44622;
var map__44623__$1 = ((((!((map__44623 == null)))?((((map__44623.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__44623.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__44623):map__44623);
var file_msg = map__44623__$1;
var request_url = cljs.core.get.call(null,map__44623__$1,new cljs.core.Keyword(null,"request-url","request-url",2100346596));
var namespace = cljs.core.get.call(null,map__44623__$1,new cljs.core.Keyword(null,"namespace","namespace",-377510372));

if(cljs.core.truth_(figwheel.client.file_reloading.reload_file_QMARK_.call(null,file_msg))){
return figwheel.client.file_reloading.require_with_callback.call(null,file_msg,callback);
} else {
figwheel.client.utils.debug_prn.call(null,["Figwheel: Not trying to load file ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(request_url)].join(''));

return cljs.core.apply.call(null,callback,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [file_msg], null));
}
});
figwheel.client.file_reloading.reload_js_file = (function figwheel$client$file_reloading$reload_js_file(file_msg){
var out = cljs.core.async.chan.call(null);
figwheel.client.file_reloading.js_reload.call(null,file_msg,((function (out){
return (function (url){
cljs.core.async.put_BANG_.call(null,out,url);

return cljs.core.async.close_BANG_.call(null,out);
});})(out))
);

return out;
});
/**
 * Returns a chanel with one collection of loaded filenames on it.
 */
figwheel.client.file_reloading.load_all_js_files = (function figwheel$client$file_reloading$load_all_js_files(files){
var out = cljs.core.async.chan.call(null);
var c__35058__auto___44673 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__35058__auto___44673,out){
return (function (){
var f__35059__auto__ = (function (){var switch__34968__auto__ = ((function (c__35058__auto___44673,out){
return (function (state_44658){
var state_val_44659 = (state_44658[(1)]);
if((state_val_44659 === (1))){
var inst_44632 = cljs.core.seq.call(null,files);
var inst_44633 = cljs.core.first.call(null,inst_44632);
var inst_44634 = cljs.core.next.call(null,inst_44632);
var inst_44635 = files;
var state_44658__$1 = (function (){var statearr_44660 = state_44658;
(statearr_44660[(7)] = inst_44634);

(statearr_44660[(8)] = inst_44633);

(statearr_44660[(9)] = inst_44635);

return statearr_44660;
})();
var statearr_44661_44674 = state_44658__$1;
(statearr_44661_44674[(2)] = null);

(statearr_44661_44674[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44659 === (2))){
var inst_44641 = (state_44658[(10)]);
var inst_44635 = (state_44658[(9)]);
var inst_44640 = cljs.core.seq.call(null,inst_44635);
var inst_44641__$1 = cljs.core.first.call(null,inst_44640);
var inst_44642 = cljs.core.next.call(null,inst_44640);
var inst_44643 = (inst_44641__$1 == null);
var inst_44644 = cljs.core.not.call(null,inst_44643);
var state_44658__$1 = (function (){var statearr_44662 = state_44658;
(statearr_44662[(11)] = inst_44642);

(statearr_44662[(10)] = inst_44641__$1);

return statearr_44662;
})();
if(inst_44644){
var statearr_44663_44675 = state_44658__$1;
(statearr_44663_44675[(1)] = (4));

} else {
var statearr_44664_44676 = state_44658__$1;
(statearr_44664_44676[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44659 === (3))){
var inst_44656 = (state_44658[(2)]);
var state_44658__$1 = state_44658;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_44658__$1,inst_44656);
} else {
if((state_val_44659 === (4))){
var inst_44641 = (state_44658[(10)]);
var inst_44646 = figwheel.client.file_reloading.reload_js_file.call(null,inst_44641);
var state_44658__$1 = state_44658;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_44658__$1,(7),inst_44646);
} else {
if((state_val_44659 === (5))){
var inst_44652 = cljs.core.async.close_BANG_.call(null,out);
var state_44658__$1 = state_44658;
var statearr_44665_44677 = state_44658__$1;
(statearr_44665_44677[(2)] = inst_44652);

(statearr_44665_44677[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44659 === (6))){
var inst_44654 = (state_44658[(2)]);
var state_44658__$1 = state_44658;
var statearr_44666_44678 = state_44658__$1;
(statearr_44666_44678[(2)] = inst_44654);

(statearr_44666_44678[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44659 === (7))){
var inst_44642 = (state_44658[(11)]);
var inst_44648 = (state_44658[(2)]);
var inst_44649 = cljs.core.async.put_BANG_.call(null,out,inst_44648);
var inst_44635 = inst_44642;
var state_44658__$1 = (function (){var statearr_44667 = state_44658;
(statearr_44667[(12)] = inst_44649);

(statearr_44667[(9)] = inst_44635);

return statearr_44667;
})();
var statearr_44668_44679 = state_44658__$1;
(statearr_44668_44679[(2)] = null);

(statearr_44668_44679[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(c__35058__auto___44673,out))
;
return ((function (switch__34968__auto__,c__35058__auto___44673,out){
return (function() {
var figwheel$client$file_reloading$load_all_js_files_$_state_machine__34969__auto__ = null;
var figwheel$client$file_reloading$load_all_js_files_$_state_machine__34969__auto____0 = (function (){
var statearr_44669 = [null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_44669[(0)] = figwheel$client$file_reloading$load_all_js_files_$_state_machine__34969__auto__);

(statearr_44669[(1)] = (1));

return statearr_44669;
});
var figwheel$client$file_reloading$load_all_js_files_$_state_machine__34969__auto____1 = (function (state_44658){
while(true){
var ret_value__34970__auto__ = (function (){try{while(true){
var result__34971__auto__ = switch__34968__auto__.call(null,state_44658);
if(cljs.core.keyword_identical_QMARK_.call(null,result__34971__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__34971__auto__;
}
break;
}
}catch (e44670){if((e44670 instanceof Object)){
var ex__34972__auto__ = e44670;
var statearr_44671_44680 = state_44658;
(statearr_44671_44680[(5)] = ex__34972__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_44658);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e44670;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__34970__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__44681 = state_44658;
state_44658 = G__44681;
continue;
} else {
return ret_value__34970__auto__;
}
break;
}
});
figwheel$client$file_reloading$load_all_js_files_$_state_machine__34969__auto__ = function(state_44658){
switch(arguments.length){
case 0:
return figwheel$client$file_reloading$load_all_js_files_$_state_machine__34969__auto____0.call(this);
case 1:
return figwheel$client$file_reloading$load_all_js_files_$_state_machine__34969__auto____1.call(this,state_44658);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
figwheel$client$file_reloading$load_all_js_files_$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$file_reloading$load_all_js_files_$_state_machine__34969__auto____0;
figwheel$client$file_reloading$load_all_js_files_$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$file_reloading$load_all_js_files_$_state_machine__34969__auto____1;
return figwheel$client$file_reloading$load_all_js_files_$_state_machine__34969__auto__;
})()
;})(switch__34968__auto__,c__35058__auto___44673,out))
})();
var state__35060__auto__ = (function (){var statearr_44672 = f__35059__auto__.call(null);
(statearr_44672[(6)] = c__35058__auto___44673);

return statearr_44672;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__35060__auto__);
});})(c__35058__auto___44673,out))
);


return cljs.core.async.into.call(null,cljs.core.PersistentVector.EMPTY,out);
});
figwheel.client.file_reloading.eval_body = (function figwheel$client$file_reloading$eval_body(p__44682,opts){
var map__44683 = p__44682;
var map__44683__$1 = ((((!((map__44683 == null)))?((((map__44683.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__44683.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__44683):map__44683);
var eval_body = cljs.core.get.call(null,map__44683__$1,new cljs.core.Keyword(null,"eval-body","eval-body",-907279883));
var file = cljs.core.get.call(null,map__44683__$1,new cljs.core.Keyword(null,"file","file",-1269645878));
if(cljs.core.truth_((function (){var and__30170__auto__ = eval_body;
if(cljs.core.truth_(and__30170__auto__)){
return typeof eval_body === 'string';
} else {
return and__30170__auto__;
}
})())){
var code = eval_body;
try{figwheel.client.utils.debug_prn.call(null,["Evaling file ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(file)].join(''));

return figwheel.client.utils.eval_helper.call(null,code,opts);
}catch (e44685){var e = e44685;
return figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"error","error",-978969032),["Unable to evaluate ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(file)].join(''));
}} else {
return null;
}
});
figwheel.client.file_reloading.expand_files = (function figwheel$client$file_reloading$expand_files(files){
var deps = figwheel.client.file_reloading.get_all_dependents.call(null,cljs.core.map.call(null,new cljs.core.Keyword(null,"namespace","namespace",-377510372),files));
return cljs.core.filter.call(null,cljs.core.comp.call(null,cljs.core.not,cljs.core.partial.call(null,cljs.core.re_matches,/figwheel\.connect.*/),new cljs.core.Keyword(null,"namespace","namespace",-377510372)),cljs.core.map.call(null,((function (deps){
return (function (n){
var temp__5276__auto__ = cljs.core.first.call(null,cljs.core.filter.call(null,((function (deps){
return (function (p1__44686_SHARP_){
return cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"namespace","namespace",-377510372).cljs$core$IFn$_invoke$arity$1(p1__44686_SHARP_),n);
});})(deps))
,files));
if(cljs.core.truth_(temp__5276__auto__)){
var file_msg = temp__5276__auto__;
return file_msg;
} else {
return new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"namespace","namespace",-377510372),new cljs.core.Keyword(null,"namespace","namespace",-377510372),n], null);
}
});})(deps))
,deps));
});
figwheel.client.file_reloading.sort_files = (function figwheel$client$file_reloading$sort_files(files){
if((cljs.core.count.call(null,files) <= (1))){
return files;
} else {
var keep_files = cljs.core.set.call(null,cljs.core.keep.call(null,new cljs.core.Keyword(null,"namespace","namespace",-377510372),files));
return cljs.core.filter.call(null,cljs.core.comp.call(null,keep_files,new cljs.core.Keyword(null,"namespace","namespace",-377510372)),figwheel.client.file_reloading.expand_files.call(null,files));
}
});
figwheel.client.file_reloading.get_figwheel_always = (function figwheel$client$file_reloading$get_figwheel_always(){
return cljs.core.map.call(null,(function (p__44687){
var vec__44688 = p__44687;
var k = cljs.core.nth.call(null,vec__44688,(0),null);
var v = cljs.core.nth.call(null,vec__44688,(1),null);
return new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"namespace","namespace",-377510372),k,new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"namespace","namespace",-377510372)], null);
}),cljs.core.filter.call(null,(function (p__44691){
var vec__44692 = p__44691;
var k = cljs.core.nth.call(null,vec__44692,(0),null);
var v = cljs.core.nth.call(null,vec__44692,(1),null);
return new cljs.core.Keyword(null,"figwheel-always","figwheel-always",799819691).cljs$core$IFn$_invoke$arity$1(v);
}),cljs.core.deref.call(null,figwheel.client.file_reloading.figwheel_meta_pragmas)));
});
figwheel.client.file_reloading.reload_js_files = (function figwheel$client$file_reloading$reload_js_files(p__44698,p__44699){
var map__44700 = p__44698;
var map__44700__$1 = ((((!((map__44700 == null)))?((((map__44700.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__44700.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__44700):map__44700);
var opts = map__44700__$1;
var before_jsload = cljs.core.get.call(null,map__44700__$1,new cljs.core.Keyword(null,"before-jsload","before-jsload",-847513128));
var on_jsload = cljs.core.get.call(null,map__44700__$1,new cljs.core.Keyword(null,"on-jsload","on-jsload",-395756602));
var reload_dependents = cljs.core.get.call(null,map__44700__$1,new cljs.core.Keyword(null,"reload-dependents","reload-dependents",-956865430));
var map__44701 = p__44699;
var map__44701__$1 = ((((!((map__44701 == null)))?((((map__44701.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__44701.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__44701):map__44701);
var msg = map__44701__$1;
var files = cljs.core.get.call(null,map__44701__$1,new cljs.core.Keyword(null,"files","files",-472457450));
var figwheel_meta = cljs.core.get.call(null,map__44701__$1,new cljs.core.Keyword(null,"figwheel-meta","figwheel-meta",-225970237));
var recompile_dependents = cljs.core.get.call(null,map__44701__$1,new cljs.core.Keyword(null,"recompile-dependents","recompile-dependents",523804171));
if(cljs.core.empty_QMARK_.call(null,figwheel_meta)){
} else {
cljs.core.reset_BANG_.call(null,figwheel.client.file_reloading.figwheel_meta_pragmas,figwheel_meta);
}

var c__35058__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__35058__auto__,map__44700,map__44700__$1,opts,before_jsload,on_jsload,reload_dependents,map__44701,map__44701__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (){
var f__35059__auto__ = (function (){var switch__34968__auto__ = ((function (c__35058__auto__,map__44700,map__44700__$1,opts,before_jsload,on_jsload,reload_dependents,map__44701,map__44701__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (state_44855){
var state_val_44856 = (state_44855[(1)]);
if((state_val_44856 === (7))){
var inst_44717 = (state_44855[(7)]);
var inst_44715 = (state_44855[(8)]);
var inst_44718 = (state_44855[(9)]);
var inst_44716 = (state_44855[(10)]);
var inst_44723 = cljs.core._nth.call(null,inst_44716,inst_44718);
var inst_44724 = figwheel.client.file_reloading.eval_body.call(null,inst_44723,opts);
var inst_44725 = (inst_44718 + (1));
var tmp44857 = inst_44717;
var tmp44858 = inst_44715;
var tmp44859 = inst_44716;
var inst_44715__$1 = tmp44858;
var inst_44716__$1 = tmp44859;
var inst_44717__$1 = tmp44857;
var inst_44718__$1 = inst_44725;
var state_44855__$1 = (function (){var statearr_44860 = state_44855;
(statearr_44860[(7)] = inst_44717__$1);

(statearr_44860[(8)] = inst_44715__$1);

(statearr_44860[(9)] = inst_44718__$1);

(statearr_44860[(11)] = inst_44724);

(statearr_44860[(10)] = inst_44716__$1);

return statearr_44860;
})();
var statearr_44861_44944 = state_44855__$1;
(statearr_44861_44944[(2)] = null);

(statearr_44861_44944[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44856 === (20))){
var inst_44758 = (state_44855[(12)]);
var inst_44766 = figwheel.client.file_reloading.sort_files.call(null,inst_44758);
var state_44855__$1 = state_44855;
var statearr_44862_44945 = state_44855__$1;
(statearr_44862_44945[(2)] = inst_44766);

(statearr_44862_44945[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44856 === (27))){
var state_44855__$1 = state_44855;
var statearr_44863_44946 = state_44855__$1;
(statearr_44863_44946[(2)] = null);

(statearr_44863_44946[(1)] = (28));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44856 === (1))){
var inst_44707 = (state_44855[(13)]);
var inst_44704 = before_jsload.call(null,files);
var inst_44705 = figwheel.client.file_reloading.before_jsload_custom_event.call(null,files);
var inst_44706 = (function (){return ((function (inst_44707,inst_44704,inst_44705,state_val_44856,c__35058__auto__,map__44700,map__44700__$1,opts,before_jsload,on_jsload,reload_dependents,map__44701,map__44701__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (p1__44695_SHARP_){
return new cljs.core.Keyword(null,"eval-body","eval-body",-907279883).cljs$core$IFn$_invoke$arity$1(p1__44695_SHARP_);
});
;})(inst_44707,inst_44704,inst_44705,state_val_44856,c__35058__auto__,map__44700,map__44700__$1,opts,before_jsload,on_jsload,reload_dependents,map__44701,map__44701__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_44707__$1 = cljs.core.filter.call(null,inst_44706,files);
var inst_44708 = cljs.core.not_empty.call(null,inst_44707__$1);
var state_44855__$1 = (function (){var statearr_44864 = state_44855;
(statearr_44864[(14)] = inst_44704);

(statearr_44864[(13)] = inst_44707__$1);

(statearr_44864[(15)] = inst_44705);

return statearr_44864;
})();
if(cljs.core.truth_(inst_44708)){
var statearr_44865_44947 = state_44855__$1;
(statearr_44865_44947[(1)] = (2));

} else {
var statearr_44866_44948 = state_44855__$1;
(statearr_44866_44948[(1)] = (3));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44856 === (24))){
var state_44855__$1 = state_44855;
var statearr_44867_44949 = state_44855__$1;
(statearr_44867_44949[(2)] = null);

(statearr_44867_44949[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44856 === (39))){
var inst_44808 = (state_44855[(16)]);
var state_44855__$1 = state_44855;
var statearr_44868_44950 = state_44855__$1;
(statearr_44868_44950[(2)] = inst_44808);

(statearr_44868_44950[(1)] = (40));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44856 === (46))){
var inst_44850 = (state_44855[(2)]);
var state_44855__$1 = state_44855;
var statearr_44869_44951 = state_44855__$1;
(statearr_44869_44951[(2)] = inst_44850);

(statearr_44869_44951[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44856 === (4))){
var inst_44752 = (state_44855[(2)]);
var inst_44753 = cljs.core.List.EMPTY;
var inst_44754 = cljs.core.reset_BANG_.call(null,figwheel.client.file_reloading.dependencies_loaded,inst_44753);
var inst_44755 = (function (){return ((function (inst_44752,inst_44753,inst_44754,state_val_44856,c__35058__auto__,map__44700,map__44700__$1,opts,before_jsload,on_jsload,reload_dependents,map__44701,map__44701__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (p1__44696_SHARP_){
var and__30170__auto__ = new cljs.core.Keyword(null,"namespace","namespace",-377510372).cljs$core$IFn$_invoke$arity$1(p1__44696_SHARP_);
if(cljs.core.truth_(and__30170__auto__)){
return (cljs.core.not.call(null,new cljs.core.Keyword(null,"eval-body","eval-body",-907279883).cljs$core$IFn$_invoke$arity$1(p1__44696_SHARP_))) && (cljs.core.not.call(null,figwheel.client.file_reloading.figwheel_no_load_QMARK_.call(null,p1__44696_SHARP_)));
} else {
return and__30170__auto__;
}
});
;})(inst_44752,inst_44753,inst_44754,state_val_44856,c__35058__auto__,map__44700,map__44700__$1,opts,before_jsload,on_jsload,reload_dependents,map__44701,map__44701__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_44756 = cljs.core.filter.call(null,inst_44755,files);
var inst_44757 = figwheel.client.file_reloading.get_figwheel_always.call(null);
var inst_44758 = cljs.core.concat.call(null,inst_44756,inst_44757);
var state_44855__$1 = (function (){var statearr_44870 = state_44855;
(statearr_44870[(12)] = inst_44758);

(statearr_44870[(17)] = inst_44754);

(statearr_44870[(18)] = inst_44752);

return statearr_44870;
})();
if(cljs.core.truth_(reload_dependents)){
var statearr_44871_44952 = state_44855__$1;
(statearr_44871_44952[(1)] = (16));

} else {
var statearr_44872_44953 = state_44855__$1;
(statearr_44872_44953[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44856 === (15))){
var inst_44742 = (state_44855[(2)]);
var state_44855__$1 = state_44855;
var statearr_44873_44954 = state_44855__$1;
(statearr_44873_44954[(2)] = inst_44742);

(statearr_44873_44954[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44856 === (21))){
var inst_44768 = (state_44855[(19)]);
var inst_44768__$1 = (state_44855[(2)]);
var inst_44769 = figwheel.client.file_reloading.load_all_js_files.call(null,inst_44768__$1);
var state_44855__$1 = (function (){var statearr_44874 = state_44855;
(statearr_44874[(19)] = inst_44768__$1);

return statearr_44874;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_44855__$1,(22),inst_44769);
} else {
if((state_val_44856 === (31))){
var inst_44853 = (state_44855[(2)]);
var state_44855__$1 = state_44855;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_44855__$1,inst_44853);
} else {
if((state_val_44856 === (32))){
var inst_44808 = (state_44855[(16)]);
var inst_44813 = inst_44808.cljs$lang$protocol_mask$partition0$;
var inst_44814 = (inst_44813 & (64));
var inst_44815 = inst_44808.cljs$core$ISeq$;
var inst_44816 = (cljs.core.PROTOCOL_SENTINEL === inst_44815);
var inst_44817 = (inst_44814) || (inst_44816);
var state_44855__$1 = state_44855;
if(cljs.core.truth_(inst_44817)){
var statearr_44875_44955 = state_44855__$1;
(statearr_44875_44955[(1)] = (35));

} else {
var statearr_44876_44956 = state_44855__$1;
(statearr_44876_44956[(1)] = (36));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44856 === (40))){
var inst_44830 = (state_44855[(20)]);
var inst_44829 = (state_44855[(2)]);
var inst_44830__$1 = cljs.core.get.call(null,inst_44829,new cljs.core.Keyword(null,"figwheel-no-load","figwheel-no-load",-555840179));
var inst_44831 = cljs.core.get.call(null,inst_44829,new cljs.core.Keyword(null,"not-required","not-required",-950359114));
var inst_44832 = cljs.core.not_empty.call(null,inst_44830__$1);
var state_44855__$1 = (function (){var statearr_44877 = state_44855;
(statearr_44877[(21)] = inst_44831);

(statearr_44877[(20)] = inst_44830__$1);

return statearr_44877;
})();
if(cljs.core.truth_(inst_44832)){
var statearr_44878_44957 = state_44855__$1;
(statearr_44878_44957[(1)] = (41));

} else {
var statearr_44879_44958 = state_44855__$1;
(statearr_44879_44958[(1)] = (42));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44856 === (33))){
var state_44855__$1 = state_44855;
var statearr_44880_44959 = state_44855__$1;
(statearr_44880_44959[(2)] = false);

(statearr_44880_44959[(1)] = (34));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44856 === (13))){
var inst_44728 = (state_44855[(22)]);
var inst_44732 = cljs.core.chunk_first.call(null,inst_44728);
var inst_44733 = cljs.core.chunk_rest.call(null,inst_44728);
var inst_44734 = cljs.core.count.call(null,inst_44732);
var inst_44715 = inst_44733;
var inst_44716 = inst_44732;
var inst_44717 = inst_44734;
var inst_44718 = (0);
var state_44855__$1 = (function (){var statearr_44881 = state_44855;
(statearr_44881[(7)] = inst_44717);

(statearr_44881[(8)] = inst_44715);

(statearr_44881[(9)] = inst_44718);

(statearr_44881[(10)] = inst_44716);

return statearr_44881;
})();
var statearr_44882_44960 = state_44855__$1;
(statearr_44882_44960[(2)] = null);

(statearr_44882_44960[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44856 === (22))){
var inst_44776 = (state_44855[(23)]);
var inst_44771 = (state_44855[(24)]);
var inst_44772 = (state_44855[(25)]);
var inst_44768 = (state_44855[(19)]);
var inst_44771__$1 = (state_44855[(2)]);
var inst_44772__$1 = cljs.core.filter.call(null,new cljs.core.Keyword(null,"loaded-file","loaded-file",-168399375),inst_44771__$1);
var inst_44773 = (function (){var all_files = inst_44768;
var res_SINGLEQUOTE_ = inst_44771__$1;
var res = inst_44772__$1;
return ((function (all_files,res_SINGLEQUOTE_,res,inst_44776,inst_44771,inst_44772,inst_44768,inst_44771__$1,inst_44772__$1,state_val_44856,c__35058__auto__,map__44700,map__44700__$1,opts,before_jsload,on_jsload,reload_dependents,map__44701,map__44701__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (p1__44697_SHARP_){
return cljs.core.not.call(null,new cljs.core.Keyword(null,"loaded-file","loaded-file",-168399375).cljs$core$IFn$_invoke$arity$1(p1__44697_SHARP_));
});
;})(all_files,res_SINGLEQUOTE_,res,inst_44776,inst_44771,inst_44772,inst_44768,inst_44771__$1,inst_44772__$1,state_val_44856,c__35058__auto__,map__44700,map__44700__$1,opts,before_jsload,on_jsload,reload_dependents,map__44701,map__44701__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_44774 = cljs.core.filter.call(null,inst_44773,inst_44771__$1);
var inst_44775 = cljs.core.deref.call(null,figwheel.client.file_reloading.dependencies_loaded);
var inst_44776__$1 = cljs.core.filter.call(null,new cljs.core.Keyword(null,"loaded-file","loaded-file",-168399375),inst_44775);
var inst_44777 = cljs.core.not_empty.call(null,inst_44776__$1);
var state_44855__$1 = (function (){var statearr_44883 = state_44855;
(statearr_44883[(23)] = inst_44776__$1);

(statearr_44883[(26)] = inst_44774);

(statearr_44883[(24)] = inst_44771__$1);

(statearr_44883[(25)] = inst_44772__$1);

return statearr_44883;
})();
if(cljs.core.truth_(inst_44777)){
var statearr_44884_44961 = state_44855__$1;
(statearr_44884_44961[(1)] = (23));

} else {
var statearr_44885_44962 = state_44855__$1;
(statearr_44885_44962[(1)] = (24));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44856 === (36))){
var state_44855__$1 = state_44855;
var statearr_44886_44963 = state_44855__$1;
(statearr_44886_44963[(2)] = false);

(statearr_44886_44963[(1)] = (37));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44856 === (41))){
var inst_44830 = (state_44855[(20)]);
var inst_44834 = cljs.core.comp.call(null,figwheel.client.file_reloading.name__GT_path,new cljs.core.Keyword(null,"namespace","namespace",-377510372));
var inst_44835 = cljs.core.map.call(null,inst_44834,inst_44830);
var inst_44836 = cljs.core.pr_str.call(null,inst_44835);
var inst_44837 = ["figwheel-no-load meta-data: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(inst_44836)].join('');
var inst_44838 = figwheel.client.utils.log.call(null,inst_44837);
var state_44855__$1 = state_44855;
var statearr_44887_44964 = state_44855__$1;
(statearr_44887_44964[(2)] = inst_44838);

(statearr_44887_44964[(1)] = (43));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44856 === (43))){
var inst_44831 = (state_44855[(21)]);
var inst_44841 = (state_44855[(2)]);
var inst_44842 = cljs.core.not_empty.call(null,inst_44831);
var state_44855__$1 = (function (){var statearr_44888 = state_44855;
(statearr_44888[(27)] = inst_44841);

return statearr_44888;
})();
if(cljs.core.truth_(inst_44842)){
var statearr_44889_44965 = state_44855__$1;
(statearr_44889_44965[(1)] = (44));

} else {
var statearr_44890_44966 = state_44855__$1;
(statearr_44890_44966[(1)] = (45));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44856 === (29))){
var inst_44808 = (state_44855[(16)]);
var inst_44776 = (state_44855[(23)]);
var inst_44774 = (state_44855[(26)]);
var inst_44771 = (state_44855[(24)]);
var inst_44772 = (state_44855[(25)]);
var inst_44768 = (state_44855[(19)]);
var inst_44804 = figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"debug","debug",-1608172596),"Figwheel: NOT loading these files ");
var inst_44807 = (function (){var all_files = inst_44768;
var res_SINGLEQUOTE_ = inst_44771;
var res = inst_44772;
var files_not_loaded = inst_44774;
var dependencies_that_loaded = inst_44776;
return ((function (all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_44808,inst_44776,inst_44774,inst_44771,inst_44772,inst_44768,inst_44804,state_val_44856,c__35058__auto__,map__44700,map__44700__$1,opts,before_jsload,on_jsload,reload_dependents,map__44701,map__44701__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (p__44806){
var map__44891 = p__44806;
var map__44891__$1 = ((((!((map__44891 == null)))?((((map__44891.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__44891.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__44891):map__44891);
var namespace = cljs.core.get.call(null,map__44891__$1,new cljs.core.Keyword(null,"namespace","namespace",-377510372));
var meta_data = cljs.core.get.call(null,cljs.core.deref.call(null,figwheel.client.file_reloading.figwheel_meta_pragmas),cljs.core.name.call(null,namespace));
if((meta_data == null)){
return new cljs.core.Keyword(null,"not-required","not-required",-950359114);
} else {
if(cljs.core.truth_(meta_data.call(null,new cljs.core.Keyword(null,"figwheel-no-load","figwheel-no-load",-555840179)))){
return new cljs.core.Keyword(null,"figwheel-no-load","figwheel-no-load",-555840179);
} else {
return new cljs.core.Keyword(null,"not-required","not-required",-950359114);

}
}
});
;})(all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_44808,inst_44776,inst_44774,inst_44771,inst_44772,inst_44768,inst_44804,state_val_44856,c__35058__auto__,map__44700,map__44700__$1,opts,before_jsload,on_jsload,reload_dependents,map__44701,map__44701__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_44808__$1 = cljs.core.group_by.call(null,inst_44807,inst_44774);
var inst_44810 = (inst_44808__$1 == null);
var inst_44811 = cljs.core.not.call(null,inst_44810);
var state_44855__$1 = (function (){var statearr_44893 = state_44855;
(statearr_44893[(16)] = inst_44808__$1);

(statearr_44893[(28)] = inst_44804);

return statearr_44893;
})();
if(inst_44811){
var statearr_44894_44967 = state_44855__$1;
(statearr_44894_44967[(1)] = (32));

} else {
var statearr_44895_44968 = state_44855__$1;
(statearr_44895_44968[(1)] = (33));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44856 === (44))){
var inst_44831 = (state_44855[(21)]);
var inst_44844 = cljs.core.map.call(null,new cljs.core.Keyword(null,"file","file",-1269645878),inst_44831);
var inst_44845 = cljs.core.pr_str.call(null,inst_44844);
var inst_44846 = ["not required: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(inst_44845)].join('');
var inst_44847 = figwheel.client.utils.log.call(null,inst_44846);
var state_44855__$1 = state_44855;
var statearr_44896_44969 = state_44855__$1;
(statearr_44896_44969[(2)] = inst_44847);

(statearr_44896_44969[(1)] = (46));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44856 === (6))){
var inst_44749 = (state_44855[(2)]);
var state_44855__$1 = state_44855;
var statearr_44897_44970 = state_44855__$1;
(statearr_44897_44970[(2)] = inst_44749);

(statearr_44897_44970[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44856 === (28))){
var inst_44774 = (state_44855[(26)]);
var inst_44801 = (state_44855[(2)]);
var inst_44802 = cljs.core.not_empty.call(null,inst_44774);
var state_44855__$1 = (function (){var statearr_44898 = state_44855;
(statearr_44898[(29)] = inst_44801);

return statearr_44898;
})();
if(cljs.core.truth_(inst_44802)){
var statearr_44899_44971 = state_44855__$1;
(statearr_44899_44971[(1)] = (29));

} else {
var statearr_44900_44972 = state_44855__$1;
(statearr_44900_44972[(1)] = (30));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44856 === (25))){
var inst_44772 = (state_44855[(25)]);
var inst_44788 = (state_44855[(2)]);
var inst_44789 = cljs.core.not_empty.call(null,inst_44772);
var state_44855__$1 = (function (){var statearr_44901 = state_44855;
(statearr_44901[(30)] = inst_44788);

return statearr_44901;
})();
if(cljs.core.truth_(inst_44789)){
var statearr_44902_44973 = state_44855__$1;
(statearr_44902_44973[(1)] = (26));

} else {
var statearr_44903_44974 = state_44855__$1;
(statearr_44903_44974[(1)] = (27));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44856 === (34))){
var inst_44824 = (state_44855[(2)]);
var state_44855__$1 = state_44855;
if(cljs.core.truth_(inst_44824)){
var statearr_44904_44975 = state_44855__$1;
(statearr_44904_44975[(1)] = (38));

} else {
var statearr_44905_44976 = state_44855__$1;
(statearr_44905_44976[(1)] = (39));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44856 === (17))){
var state_44855__$1 = state_44855;
var statearr_44906_44977 = state_44855__$1;
(statearr_44906_44977[(2)] = recompile_dependents);

(statearr_44906_44977[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44856 === (3))){
var state_44855__$1 = state_44855;
var statearr_44907_44978 = state_44855__$1;
(statearr_44907_44978[(2)] = null);

(statearr_44907_44978[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44856 === (12))){
var inst_44745 = (state_44855[(2)]);
var state_44855__$1 = state_44855;
var statearr_44908_44979 = state_44855__$1;
(statearr_44908_44979[(2)] = inst_44745);

(statearr_44908_44979[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44856 === (2))){
var inst_44707 = (state_44855[(13)]);
var inst_44714 = cljs.core.seq.call(null,inst_44707);
var inst_44715 = inst_44714;
var inst_44716 = null;
var inst_44717 = (0);
var inst_44718 = (0);
var state_44855__$1 = (function (){var statearr_44909 = state_44855;
(statearr_44909[(7)] = inst_44717);

(statearr_44909[(8)] = inst_44715);

(statearr_44909[(9)] = inst_44718);

(statearr_44909[(10)] = inst_44716);

return statearr_44909;
})();
var statearr_44910_44980 = state_44855__$1;
(statearr_44910_44980[(2)] = null);

(statearr_44910_44980[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44856 === (23))){
var inst_44776 = (state_44855[(23)]);
var inst_44774 = (state_44855[(26)]);
var inst_44771 = (state_44855[(24)]);
var inst_44772 = (state_44855[(25)]);
var inst_44768 = (state_44855[(19)]);
var inst_44779 = figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"debug","debug",-1608172596),"Figwheel: loaded these dependencies");
var inst_44781 = (function (){var all_files = inst_44768;
var res_SINGLEQUOTE_ = inst_44771;
var res = inst_44772;
var files_not_loaded = inst_44774;
var dependencies_that_loaded = inst_44776;
return ((function (all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_44776,inst_44774,inst_44771,inst_44772,inst_44768,inst_44779,state_val_44856,c__35058__auto__,map__44700,map__44700__$1,opts,before_jsload,on_jsload,reload_dependents,map__44701,map__44701__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (p__44780){
var map__44911 = p__44780;
var map__44911__$1 = ((((!((map__44911 == null)))?((((map__44911.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__44911.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__44911):map__44911);
var request_url = cljs.core.get.call(null,map__44911__$1,new cljs.core.Keyword(null,"request-url","request-url",2100346596));
return clojure.string.replace.call(null,request_url,goog.basePath,"");
});
;})(all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_44776,inst_44774,inst_44771,inst_44772,inst_44768,inst_44779,state_val_44856,c__35058__auto__,map__44700,map__44700__$1,opts,before_jsload,on_jsload,reload_dependents,map__44701,map__44701__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_44782 = cljs.core.reverse.call(null,inst_44776);
var inst_44783 = cljs.core.map.call(null,inst_44781,inst_44782);
var inst_44784 = cljs.core.pr_str.call(null,inst_44783);
var inst_44785 = figwheel.client.utils.log.call(null,inst_44784);
var state_44855__$1 = (function (){var statearr_44913 = state_44855;
(statearr_44913[(31)] = inst_44779);

return statearr_44913;
})();
var statearr_44914_44981 = state_44855__$1;
(statearr_44914_44981[(2)] = inst_44785);

(statearr_44914_44981[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44856 === (35))){
var state_44855__$1 = state_44855;
var statearr_44915_44982 = state_44855__$1;
(statearr_44915_44982[(2)] = true);

(statearr_44915_44982[(1)] = (37));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44856 === (19))){
var inst_44758 = (state_44855[(12)]);
var inst_44764 = figwheel.client.file_reloading.expand_files.call(null,inst_44758);
var state_44855__$1 = state_44855;
var statearr_44916_44983 = state_44855__$1;
(statearr_44916_44983[(2)] = inst_44764);

(statearr_44916_44983[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44856 === (11))){
var state_44855__$1 = state_44855;
var statearr_44917_44984 = state_44855__$1;
(statearr_44917_44984[(2)] = null);

(statearr_44917_44984[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44856 === (9))){
var inst_44747 = (state_44855[(2)]);
var state_44855__$1 = state_44855;
var statearr_44918_44985 = state_44855__$1;
(statearr_44918_44985[(2)] = inst_44747);

(statearr_44918_44985[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44856 === (5))){
var inst_44717 = (state_44855[(7)]);
var inst_44718 = (state_44855[(9)]);
var inst_44720 = (inst_44718 < inst_44717);
var inst_44721 = inst_44720;
var state_44855__$1 = state_44855;
if(cljs.core.truth_(inst_44721)){
var statearr_44919_44986 = state_44855__$1;
(statearr_44919_44986[(1)] = (7));

} else {
var statearr_44920_44987 = state_44855__$1;
(statearr_44920_44987[(1)] = (8));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44856 === (14))){
var inst_44728 = (state_44855[(22)]);
var inst_44737 = cljs.core.first.call(null,inst_44728);
var inst_44738 = figwheel.client.file_reloading.eval_body.call(null,inst_44737,opts);
var inst_44739 = cljs.core.next.call(null,inst_44728);
var inst_44715 = inst_44739;
var inst_44716 = null;
var inst_44717 = (0);
var inst_44718 = (0);
var state_44855__$1 = (function (){var statearr_44921 = state_44855;
(statearr_44921[(7)] = inst_44717);

(statearr_44921[(8)] = inst_44715);

(statearr_44921[(32)] = inst_44738);

(statearr_44921[(9)] = inst_44718);

(statearr_44921[(10)] = inst_44716);

return statearr_44921;
})();
var statearr_44922_44988 = state_44855__$1;
(statearr_44922_44988[(2)] = null);

(statearr_44922_44988[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44856 === (45))){
var state_44855__$1 = state_44855;
var statearr_44923_44989 = state_44855__$1;
(statearr_44923_44989[(2)] = null);

(statearr_44923_44989[(1)] = (46));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44856 === (26))){
var inst_44776 = (state_44855[(23)]);
var inst_44774 = (state_44855[(26)]);
var inst_44771 = (state_44855[(24)]);
var inst_44772 = (state_44855[(25)]);
var inst_44768 = (state_44855[(19)]);
var inst_44791 = figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"debug","debug",-1608172596),"Figwheel: loaded these files");
var inst_44793 = (function (){var all_files = inst_44768;
var res_SINGLEQUOTE_ = inst_44771;
var res = inst_44772;
var files_not_loaded = inst_44774;
var dependencies_that_loaded = inst_44776;
return ((function (all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_44776,inst_44774,inst_44771,inst_44772,inst_44768,inst_44791,state_val_44856,c__35058__auto__,map__44700,map__44700__$1,opts,before_jsload,on_jsload,reload_dependents,map__44701,map__44701__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (p__44792){
var map__44924 = p__44792;
var map__44924__$1 = ((((!((map__44924 == null)))?((((map__44924.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__44924.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__44924):map__44924);
var namespace = cljs.core.get.call(null,map__44924__$1,new cljs.core.Keyword(null,"namespace","namespace",-377510372));
var file = cljs.core.get.call(null,map__44924__$1,new cljs.core.Keyword(null,"file","file",-1269645878));
if(cljs.core.truth_(namespace)){
return figwheel.client.file_reloading.name__GT_path.call(null,cljs.core.name.call(null,namespace));
} else {
return file;
}
});
;})(all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_44776,inst_44774,inst_44771,inst_44772,inst_44768,inst_44791,state_val_44856,c__35058__auto__,map__44700,map__44700__$1,opts,before_jsload,on_jsload,reload_dependents,map__44701,map__44701__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_44794 = cljs.core.map.call(null,inst_44793,inst_44772);
var inst_44795 = cljs.core.pr_str.call(null,inst_44794);
var inst_44796 = figwheel.client.utils.log.call(null,inst_44795);
var inst_44797 = (function (){var all_files = inst_44768;
var res_SINGLEQUOTE_ = inst_44771;
var res = inst_44772;
var files_not_loaded = inst_44774;
var dependencies_that_loaded = inst_44776;
return ((function (all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_44776,inst_44774,inst_44771,inst_44772,inst_44768,inst_44791,inst_44793,inst_44794,inst_44795,inst_44796,state_val_44856,c__35058__auto__,map__44700,map__44700__$1,opts,before_jsload,on_jsload,reload_dependents,map__44701,map__44701__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (){
figwheel.client.file_reloading.on_jsload_custom_event.call(null,res);

return cljs.core.apply.call(null,on_jsload,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [res], null));
});
;})(all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_44776,inst_44774,inst_44771,inst_44772,inst_44768,inst_44791,inst_44793,inst_44794,inst_44795,inst_44796,state_val_44856,c__35058__auto__,map__44700,map__44700__$1,opts,before_jsload,on_jsload,reload_dependents,map__44701,map__44701__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_44798 = setTimeout(inst_44797,(10));
var state_44855__$1 = (function (){var statearr_44926 = state_44855;
(statearr_44926[(33)] = inst_44791);

(statearr_44926[(34)] = inst_44796);

return statearr_44926;
})();
var statearr_44927_44990 = state_44855__$1;
(statearr_44927_44990[(2)] = inst_44798);

(statearr_44927_44990[(1)] = (28));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44856 === (16))){
var state_44855__$1 = state_44855;
var statearr_44928_44991 = state_44855__$1;
(statearr_44928_44991[(2)] = reload_dependents);

(statearr_44928_44991[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44856 === (38))){
var inst_44808 = (state_44855[(16)]);
var inst_44826 = cljs.core.apply.call(null,cljs.core.hash_map,inst_44808);
var state_44855__$1 = state_44855;
var statearr_44929_44992 = state_44855__$1;
(statearr_44929_44992[(2)] = inst_44826);

(statearr_44929_44992[(1)] = (40));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44856 === (30))){
var state_44855__$1 = state_44855;
var statearr_44930_44993 = state_44855__$1;
(statearr_44930_44993[(2)] = null);

(statearr_44930_44993[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44856 === (10))){
var inst_44728 = (state_44855[(22)]);
var inst_44730 = cljs.core.chunked_seq_QMARK_.call(null,inst_44728);
var state_44855__$1 = state_44855;
if(inst_44730){
var statearr_44931_44994 = state_44855__$1;
(statearr_44931_44994[(1)] = (13));

} else {
var statearr_44932_44995 = state_44855__$1;
(statearr_44932_44995[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44856 === (18))){
var inst_44762 = (state_44855[(2)]);
var state_44855__$1 = state_44855;
if(cljs.core.truth_(inst_44762)){
var statearr_44933_44996 = state_44855__$1;
(statearr_44933_44996[(1)] = (19));

} else {
var statearr_44934_44997 = state_44855__$1;
(statearr_44934_44997[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44856 === (42))){
var state_44855__$1 = state_44855;
var statearr_44935_44998 = state_44855__$1;
(statearr_44935_44998[(2)] = null);

(statearr_44935_44998[(1)] = (43));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44856 === (37))){
var inst_44821 = (state_44855[(2)]);
var state_44855__$1 = state_44855;
var statearr_44936_44999 = state_44855__$1;
(statearr_44936_44999[(2)] = inst_44821);

(statearr_44936_44999[(1)] = (34));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_44856 === (8))){
var inst_44728 = (state_44855[(22)]);
var inst_44715 = (state_44855[(8)]);
var inst_44728__$1 = cljs.core.seq.call(null,inst_44715);
var state_44855__$1 = (function (){var statearr_44937 = state_44855;
(statearr_44937[(22)] = inst_44728__$1);

return statearr_44937;
})();
if(inst_44728__$1){
var statearr_44938_45000 = state_44855__$1;
(statearr_44938_45000[(1)] = (10));

} else {
var statearr_44939_45001 = state_44855__$1;
(statearr_44939_45001[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__35058__auto__,map__44700,map__44700__$1,opts,before_jsload,on_jsload,reload_dependents,map__44701,map__44701__$1,msg,files,figwheel_meta,recompile_dependents))
;
return ((function (switch__34968__auto__,c__35058__auto__,map__44700,map__44700__$1,opts,before_jsload,on_jsload,reload_dependents,map__44701,map__44701__$1,msg,files,figwheel_meta,recompile_dependents){
return (function() {
var figwheel$client$file_reloading$reload_js_files_$_state_machine__34969__auto__ = null;
var figwheel$client$file_reloading$reload_js_files_$_state_machine__34969__auto____0 = (function (){
var statearr_44940 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_44940[(0)] = figwheel$client$file_reloading$reload_js_files_$_state_machine__34969__auto__);

(statearr_44940[(1)] = (1));

return statearr_44940;
});
var figwheel$client$file_reloading$reload_js_files_$_state_machine__34969__auto____1 = (function (state_44855){
while(true){
var ret_value__34970__auto__ = (function (){try{while(true){
var result__34971__auto__ = switch__34968__auto__.call(null,state_44855);
if(cljs.core.keyword_identical_QMARK_.call(null,result__34971__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__34971__auto__;
}
break;
}
}catch (e44941){if((e44941 instanceof Object)){
var ex__34972__auto__ = e44941;
var statearr_44942_45002 = state_44855;
(statearr_44942_45002[(5)] = ex__34972__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_44855);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e44941;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__34970__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__45003 = state_44855;
state_44855 = G__45003;
continue;
} else {
return ret_value__34970__auto__;
}
break;
}
});
figwheel$client$file_reloading$reload_js_files_$_state_machine__34969__auto__ = function(state_44855){
switch(arguments.length){
case 0:
return figwheel$client$file_reloading$reload_js_files_$_state_machine__34969__auto____0.call(this);
case 1:
return figwheel$client$file_reloading$reload_js_files_$_state_machine__34969__auto____1.call(this,state_44855);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
figwheel$client$file_reloading$reload_js_files_$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$file_reloading$reload_js_files_$_state_machine__34969__auto____0;
figwheel$client$file_reloading$reload_js_files_$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$file_reloading$reload_js_files_$_state_machine__34969__auto____1;
return figwheel$client$file_reloading$reload_js_files_$_state_machine__34969__auto__;
})()
;})(switch__34968__auto__,c__35058__auto__,map__44700,map__44700__$1,opts,before_jsload,on_jsload,reload_dependents,map__44701,map__44701__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var state__35060__auto__ = (function (){var statearr_44943 = f__35059__auto__.call(null);
(statearr_44943[(6)] = c__35058__auto__);

return statearr_44943;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__35060__auto__);
});})(c__35058__auto__,map__44700,map__44700__$1,opts,before_jsload,on_jsload,reload_dependents,map__44701,map__44701__$1,msg,files,figwheel_meta,recompile_dependents))
);

return c__35058__auto__;
});
figwheel.client.file_reloading.current_links = (function figwheel$client$file_reloading$current_links(){
return Array.prototype.slice.call(document.getElementsByTagName("link"));
});
figwheel.client.file_reloading.truncate_url = (function figwheel$client$file_reloading$truncate_url(url){
return clojure.string.replace_first.call(null,clojure.string.replace_first.call(null,clojure.string.replace_first.call(null,clojure.string.replace_first.call(null,cljs.core.first.call(null,clojure.string.split.call(null,url,/\?/)),[cljs.core.str.cljs$core$IFn$_invoke$arity$1(location.protocol),"//"].join(''),""),".*://",""),/^\/\//,""),/[^\\/]*/,"");
});
figwheel.client.file_reloading.matches_file_QMARK_ = (function figwheel$client$file_reloading$matches_file_QMARK_(p__45006,link){
var map__45007 = p__45006;
var map__45007__$1 = ((((!((map__45007 == null)))?((((map__45007.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__45007.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__45007):map__45007);
var file = cljs.core.get.call(null,map__45007__$1,new cljs.core.Keyword(null,"file","file",-1269645878));
var temp__5278__auto__ = link.href;
if(cljs.core.truth_(temp__5278__auto__)){
var link_href = temp__5278__auto__;
var match = clojure.string.join.call(null,"/",cljs.core.take_while.call(null,cljs.core.identity,cljs.core.map.call(null,((function (link_href,temp__5278__auto__,map__45007,map__45007__$1,file){
return (function (p1__45004_SHARP_,p2__45005_SHARP_){
if(cljs.core._EQ_.call(null,p1__45004_SHARP_,p2__45005_SHARP_)){
return p1__45004_SHARP_;
} else {
return false;
}
});})(link_href,temp__5278__auto__,map__45007,map__45007__$1,file))
,cljs.core.reverse.call(null,clojure.string.split.call(null,file,"/")),cljs.core.reverse.call(null,clojure.string.split.call(null,figwheel.client.file_reloading.truncate_url.call(null,link_href),"/")))));
var match_length = cljs.core.count.call(null,match);
var file_name_length = cljs.core.count.call(null,cljs.core.last.call(null,clojure.string.split.call(null,file,"/")));
if((match_length >= file_name_length)){
return new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"link","link",-1769163468),link,new cljs.core.Keyword(null,"link-href","link-href",-250644450),link_href,new cljs.core.Keyword(null,"match-length","match-length",1101537310),match_length,new cljs.core.Keyword(null,"current-url-length","current-url-length",380404083),cljs.core.count.call(null,figwheel.client.file_reloading.truncate_url.call(null,link_href))], null);
} else {
return null;
}
} else {
return null;
}
});
figwheel.client.file_reloading.get_correct_link = (function figwheel$client$file_reloading$get_correct_link(f_data){
var temp__5278__auto__ = cljs.core.first.call(null,cljs.core.sort_by.call(null,(function (p__45010){
var map__45011 = p__45010;
var map__45011__$1 = ((((!((map__45011 == null)))?((((map__45011.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__45011.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__45011):map__45011);
var match_length = cljs.core.get.call(null,map__45011__$1,new cljs.core.Keyword(null,"match-length","match-length",1101537310));
var current_url_length = cljs.core.get.call(null,map__45011__$1,new cljs.core.Keyword(null,"current-url-length","current-url-length",380404083));
return (current_url_length - match_length);
}),cljs.core.keep.call(null,(function (p1__45009_SHARP_){
return figwheel.client.file_reloading.matches_file_QMARK_.call(null,f_data,p1__45009_SHARP_);
}),figwheel.client.file_reloading.current_links.call(null))));
if(cljs.core.truth_(temp__5278__auto__)){
var res = temp__5278__auto__;
return new cljs.core.Keyword(null,"link","link",-1769163468).cljs$core$IFn$_invoke$arity$1(res);
} else {
return null;
}
});
figwheel.client.file_reloading.clone_link = (function figwheel$client$file_reloading$clone_link(link,url){
var clone = document.createElement("link");
clone.rel = "stylesheet";

clone.media = link.media;

clone.disabled = link.disabled;

clone.href = figwheel.client.file_reloading.add_cache_buster.call(null,url);

return clone;
});
figwheel.client.file_reloading.create_link = (function figwheel$client$file_reloading$create_link(url){
var link = document.createElement("link");
link.rel = "stylesheet";

link.href = figwheel.client.file_reloading.add_cache_buster.call(null,url);

return link;
});
figwheel.client.file_reloading.distinctify = (function figwheel$client$file_reloading$distinctify(key,seqq){
return cljs.core.vals.call(null,cljs.core.reduce.call(null,(function (p1__45013_SHARP_,p2__45014_SHARP_){
return cljs.core.assoc.call(null,p1__45013_SHARP_,cljs.core.get.call(null,p2__45014_SHARP_,key),p2__45014_SHARP_);
}),cljs.core.PersistentArrayMap.EMPTY,seqq));
});
figwheel.client.file_reloading.add_link_to_document = (function figwheel$client$file_reloading$add_link_to_document(orig_link,klone,finished_fn){
var parent = orig_link.parentNode;
if(cljs.core._EQ_.call(null,orig_link,parent.lastChild)){
parent.appendChild(klone);
} else {
parent.insertBefore(klone,orig_link.nextSibling);
}

return setTimeout(((function (parent){
return (function (){
parent.removeChild(orig_link);

return finished_fn.call(null);
});})(parent))
,(300));
});
if(typeof figwheel.client.file_reloading.reload_css_deferred_chain !== 'undefined'){
} else {
figwheel.client.file_reloading.reload_css_deferred_chain = cljs.core.atom.call(null,goog.async.Deferred.succeed());
}
figwheel.client.file_reloading.reload_css_file = (function figwheel$client$file_reloading$reload_css_file(f_data,fin){
var temp__5276__auto__ = figwheel.client.file_reloading.get_correct_link.call(null,f_data);
if(cljs.core.truth_(temp__5276__auto__)){
var link = temp__5276__auto__;
return figwheel.client.file_reloading.add_link_to_document.call(null,link,figwheel.client.file_reloading.clone_link.call(null,link,link.href),((function (link,temp__5276__auto__){
return (function (){
return fin.call(null,cljs.core.assoc.call(null,f_data,new cljs.core.Keyword(null,"loaded","loaded",-1246482293),true));
});})(link,temp__5276__auto__))
);
} else {
return fin.call(null,f_data);
}
});
figwheel.client.file_reloading.reload_css_files_STAR_ = (function figwheel$client$file_reloading$reload_css_files_STAR_(deferred,f_datas,on_cssload){
return figwheel.client.utils.liftContD.call(null,figwheel.client.utils.mapConcatD.call(null,deferred,figwheel.client.file_reloading.reload_css_file,f_datas),(function (f_datas_SINGLEQUOTE_,fin){
var loaded_f_datas_45015 = cljs.core.filter.call(null,new cljs.core.Keyword(null,"loaded","loaded",-1246482293),f_datas_SINGLEQUOTE_);
figwheel.client.file_reloading.on_cssload_custom_event.call(null,loaded_f_datas_45015);

if(cljs.core.fn_QMARK_.call(null,on_cssload)){
on_cssload.call(null,loaded_f_datas_45015);
} else {
}

return fin.call(null);
}));
});
figwheel.client.file_reloading.reload_css_files = (function figwheel$client$file_reloading$reload_css_files(p__45016,p__45017){
var map__45018 = p__45016;
var map__45018__$1 = ((((!((map__45018 == null)))?((((map__45018.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__45018.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__45018):map__45018);
var on_cssload = cljs.core.get.call(null,map__45018__$1,new cljs.core.Keyword(null,"on-cssload","on-cssload",1825432318));
var map__45019 = p__45017;
var map__45019__$1 = ((((!((map__45019 == null)))?((((map__45019.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__45019.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__45019):map__45019);
var files_msg = map__45019__$1;
var files = cljs.core.get.call(null,map__45019__$1,new cljs.core.Keyword(null,"files","files",-472457450));
if(cljs.core.truth_(figwheel.client.utils.html_env_QMARK_.call(null))){
var temp__5278__auto__ = cljs.core.not_empty.call(null,figwheel.client.file_reloading.distinctify.call(null,new cljs.core.Keyword(null,"file","file",-1269645878),files));
if(cljs.core.truth_(temp__5278__auto__)){
var f_datas = temp__5278__auto__;
return cljs.core.swap_BANG_.call(null,figwheel.client.file_reloading.reload_css_deferred_chain,figwheel.client.file_reloading.reload_css_files_STAR_,f_datas,on_cssload);
} else {
return null;
}
} else {
return null;
}
});

//# sourceMappingURL=file_reloading.js.map?rel=1507225500080

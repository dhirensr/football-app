// Compiled by ClojureScript 1.9.908 {}
goog.provide('reagent.dom');
goog.require('cljs.core');
goog.require('cljsjs.react.dom');
goog.require('reagent.impl.util');
goog.require('reagent.impl.template');
goog.require('reagent.debug');
goog.require('reagent.interop');
if(typeof reagent.dom.dom !== 'undefined'){
} else {
reagent.dom.dom = (function (){var or__30182__auto__ = (function (){var and__30170__auto__ = typeof ReactDOM !== 'undefined';
if(and__30170__auto__){
return ReactDOM;
} else {
return and__30170__auto__;
}
})();
if(cljs.core.truth_(or__30182__auto__)){
return or__30182__auto__;
} else {
var and__30170__auto__ = typeof require !== 'undefined';
if(and__30170__auto__){
return require("react-dom");
} else {
return and__30170__auto__;
}
}
})();
}
if(cljs.core.truth_(reagent.dom.dom)){
} else {
throw (new Error(["Assert failed: ","Could not find ReactDOM","\n","dom"].join('')));
}
if(typeof reagent.dom.roots !== 'undefined'){
} else {
reagent.dom.roots = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
}
reagent.dom.unmount_comp = (function reagent$dom$unmount_comp(container){
cljs.core.swap_BANG_.call(null,reagent.dom.roots,cljs.core.dissoc,container);

return (reagent.dom.dom["unmountComponentAtNode"])(container);
});
reagent.dom.render_comp = (function reagent$dom$render_comp(comp,container,callback){
var _STAR_always_update_STAR_41257 = reagent.impl.util._STAR_always_update_STAR_;
reagent.impl.util._STAR_always_update_STAR_ = true;

try{return (reagent.dom.dom["render"])(comp.call(null),container,((function (_STAR_always_update_STAR_41257){
return (function (){
var _STAR_always_update_STAR_41258 = reagent.impl.util._STAR_always_update_STAR_;
reagent.impl.util._STAR_always_update_STAR_ = false;

try{cljs.core.swap_BANG_.call(null,reagent.dom.roots,cljs.core.assoc,container,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [comp,container], null));

if(!((callback == null))){
return callback.call(null);
} else {
return null;
}
}finally {reagent.impl.util._STAR_always_update_STAR_ = _STAR_always_update_STAR_41258;
}});})(_STAR_always_update_STAR_41257))
);
}finally {reagent.impl.util._STAR_always_update_STAR_ = _STAR_always_update_STAR_41257;
}});
reagent.dom.re_render_component = (function reagent$dom$re_render_component(comp,container){
return reagent.dom.render_comp.call(null,comp,container,null);
});
/**
 * Render a Reagent component into the DOM. The first argument may be
 *   either a vector (using Reagent's Hiccup syntax), or a React element. The second argument should be a DOM node.
 * 
 *   Optionally takes a callback that is called when the component is in place.
 * 
 *   Returns the mounted component instance.
 */
reagent.dom.render = (function reagent$dom$render(var_args){
var G__41260 = arguments.length;
switch (G__41260) {
case 2:
return reagent.dom.render.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return reagent.dom.render.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

reagent.dom.render.cljs$core$IFn$_invoke$arity$2 = (function (comp,container){
return reagent.dom.render.call(null,comp,container,null);
});

reagent.dom.render.cljs$core$IFn$_invoke$arity$3 = (function (comp,container,callback){
var f = (function (){
return reagent.impl.template.as_element.call(null,((cljs.core.fn_QMARK_.call(null,comp))?comp.call(null):comp));
});
return reagent.dom.render_comp.call(null,f,container,callback);
});

reagent.dom.render.cljs$lang$maxFixedArity = 3;

reagent.dom.unmount_component_at_node = (function reagent$dom$unmount_component_at_node(container){
return reagent.dom.unmount_comp.call(null,container);
});
/**
 * Returns the root DOM node of a mounted component.
 */
reagent.dom.dom_node = (function reagent$dom$dom_node(this$){
return (reagent.dom.dom["findDOMNode"])(this$);
});
reagent.impl.template.find_dom_node = reagent.dom.dom_node;
/**
 * Force re-rendering of all mounted Reagent components. This is
 *   probably only useful in a development environment, when you want to
 *   update components in response to some dynamic changes to code.
 * 
 *   Note that force-update-all may not update root components. This
 *   happens if a component 'foo' is mounted with `(render [foo])` (since
 *   functions are passed by value, and not by reference, in
 *   ClojureScript). To get around this you'll have to introduce a layer
 *   of indirection, for example by using `(render [#'foo])` instead.
 */
reagent.dom.force_update_all = (function reagent$dom$force_update_all(){
var seq__41262_41266 = cljs.core.seq.call(null,cljs.core.vals.call(null,cljs.core.deref.call(null,reagent.dom.roots)));
var chunk__41263_41267 = null;
var count__41264_41268 = (0);
var i__41265_41269 = (0);
while(true){
if((i__41265_41269 < count__41264_41268)){
var v_41270 = cljs.core._nth.call(null,chunk__41263_41267,i__41265_41269);
cljs.core.apply.call(null,reagent.dom.re_render_component,v_41270);

var G__41271 = seq__41262_41266;
var G__41272 = chunk__41263_41267;
var G__41273 = count__41264_41268;
var G__41274 = (i__41265_41269 + (1));
seq__41262_41266 = G__41271;
chunk__41263_41267 = G__41272;
count__41264_41268 = G__41273;
i__41265_41269 = G__41274;
continue;
} else {
var temp__5278__auto___41275 = cljs.core.seq.call(null,seq__41262_41266);
if(temp__5278__auto___41275){
var seq__41262_41276__$1 = temp__5278__auto___41275;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__41262_41276__$1)){
var c__31113__auto___41277 = cljs.core.chunk_first.call(null,seq__41262_41276__$1);
var G__41278 = cljs.core.chunk_rest.call(null,seq__41262_41276__$1);
var G__41279 = c__31113__auto___41277;
var G__41280 = cljs.core.count.call(null,c__31113__auto___41277);
var G__41281 = (0);
seq__41262_41266 = G__41278;
chunk__41263_41267 = G__41279;
count__41264_41268 = G__41280;
i__41265_41269 = G__41281;
continue;
} else {
var v_41282 = cljs.core.first.call(null,seq__41262_41276__$1);
cljs.core.apply.call(null,reagent.dom.re_render_component,v_41282);

var G__41283 = cljs.core.next.call(null,seq__41262_41276__$1);
var G__41284 = null;
var G__41285 = (0);
var G__41286 = (0);
seq__41262_41266 = G__41283;
chunk__41263_41267 = G__41284;
count__41264_41268 = G__41285;
i__41265_41269 = G__41286;
continue;
}
} else {
}
}
break;
}

return "Updated";
});

//# sourceMappingURL=dom.js.map?rel=1507225496788

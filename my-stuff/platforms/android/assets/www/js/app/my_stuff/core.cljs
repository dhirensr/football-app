(ns my-stuff.core
  (:require [reagent.core :as r]
            [my-stuff.states :as s]))

(enable-console-print!)


(defn by-id [id] (.getElementById js/document id))

(defn log [& msgs] (doall (for [msg msgs] (.log js/console msg))))


(defn splash-screen []
  (r/create-class
   {:component-did-mount
    (fn []
      (js/setTimeout #(swap! s/app-state update-in [:reg-info]
                             merge {:page "start"})
                     2000))
    :reagent-render
    (fn []
      [:div.splash-screen])}))

(defn click-nav
  ([] nil)
  ([v] (click-nav :page v))
  ([k v]
   (click-nav)
   (swap! s/app-state assoc k v)))


(defn menu-bar []
  [:div.navbar-fixed
     [:nav
      [:div.nav-wrapper {:className "nav-bar-bg light-blue darken-3"}
       [:a.brand-logo {:href "#!"} "Football app"]
       [:a {:data-activates "nav-ul"
            :className "button-collapse"
            :href "#"}
        [:i.material-icons "menu"]]
       [:ul.side-nav {:id "nav-ul"}
        [:li [:a {:href "#" :on-click (fn [e] (click-nav "home"))}
              "Home"]]
        [:li [:a {:href "#" :on-click (fn [e] (click-nav "activities"))}
              "Activities"]]
        [:li [:a {:href "#" :on-click (fn [e] (click-nav "contact-us"))}
              "Contact us"]]]]]])

(defn nav-barr []
  [:nav.navbar.navbar-inverse.navbar-static-top {:role "navigation"}
   [:div.container
    [:div.navbar-header
     [:button.navbar-toggle {:data-toggle "collapse"
                             :type "button"
                             :data-target "#bs-example-navbar-collapse-1"}
      "aff"
      [:span.icon-bar]
      [:span.icon-bar]
      [:span.icon-bar]]
     #_[:a.navbar-brand {:href "#"} "Menu"]]
    [:div.collapse.navbar-collapse {:id "bs-example-navbar-collapse-1"}
     [:ul.nav.navbar-nav
      [:li.active [:a {:href "#"} "Home"]]
      [:li [:a {:href "#"} "About"]]
      [:li [:a {:href "#"
                :data-toggle "collapse"
                :data-target ".navbar-collapse.in"} "Close"]]]]]])


(defn naav-barr []
  [:div.st-container {:id "st-container"}
   [:nav.st-menu.st-effect-1 {:id "menu-1"}]
   [:div.st-pusher
    [:div.st-content
     [:div.st-content-inner]]]])
#_(defn start-page []
  (r/create-class
   {:component-did-mount #(.slick (js/$ ".single-item") #js{:dots true
                                                            :centered false})
    :reagent-render
    (fn []
      [:div.container
       [:div.single-item
        [:div.size [:h3 "1"]]
        [:div  [:h3 "2"]]
        [:div  [:h3 "3"]]
        [:div  [:h3 "4"]]]])}))

(defn container []
  [:div.app-container
   [menu-bar]])

(defn mount-root
  ;;;Start of the application
  []
  (r/render [container] (by-id "container")))

(.addEventListener js/document "deviceready"
                   mount-root
                   false)

#_(defn start-page []
  [:div.container
   [:h1 "fafa"]])

(defn reg-default []
  (condp = (:page (:reg-info @s/app-state))
    "start"  #_[start-page]
    [splash-screen]))

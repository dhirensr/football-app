// Compiled by ClojureScript 1.9.908 {}
goog.provide('my_stuff.core');
goog.require('cljs.core');
goog.require('reagent.core');
goog.require('my_stuff.states');
cljs.core.enable_console_print_BANG_.call(null);
my_stuff.core.by_id = (function my_stuff$core$by_id(id){
return document.getElementById(id);
});
my_stuff.core.log = (function my_stuff$core$log(var_args){
var args__31466__auto__ = [];
var len__31459__auto___45029 = arguments.length;
var i__31460__auto___45030 = (0);
while(true){
if((i__31460__auto___45030 < len__31459__auto___45029)){
args__31466__auto__.push((arguments[i__31460__auto___45030]));

var G__45031 = (i__31460__auto___45030 + (1));
i__31460__auto___45030 = G__45031;
continue;
} else {
}
break;
}

var argseq__31467__auto__ = ((((0) < args__31466__auto__.length))?(new cljs.core.IndexedSeq(args__31466__auto__.slice((0)),(0),null)):null);
return my_stuff.core.log.cljs$core$IFn$_invoke$arity$variadic(argseq__31467__auto__);
});

my_stuff.core.log.cljs$core$IFn$_invoke$arity$variadic = (function (msgs){
return cljs.core.doall.call(null,(function (){var iter__31064__auto__ = (function my_stuff$core$iter__45025(s__45026){
return (new cljs.core.LazySeq(null,(function (){
var s__45026__$1 = s__45026;
while(true){
var temp__5278__auto__ = cljs.core.seq.call(null,s__45026__$1);
if(temp__5278__auto__){
var s__45026__$2 = temp__5278__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,s__45026__$2)){
var c__31062__auto__ = cljs.core.chunk_first.call(null,s__45026__$2);
var size__31063__auto__ = cljs.core.count.call(null,c__31062__auto__);
var b__45028 = cljs.core.chunk_buffer.call(null,size__31063__auto__);
if((function (){var i__45027 = (0);
while(true){
if((i__45027 < size__31063__auto__)){
var msg = cljs.core._nth.call(null,c__31062__auto__,i__45027);
cljs.core.chunk_append.call(null,b__45028,console.log(msg));

var G__45032 = (i__45027 + (1));
i__45027 = G__45032;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__45028),my_stuff$core$iter__45025.call(null,cljs.core.chunk_rest.call(null,s__45026__$2)));
} else {
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__45028),null);
}
} else {
var msg = cljs.core.first.call(null,s__45026__$2);
return cljs.core.cons.call(null,console.log(msg),my_stuff$core$iter__45025.call(null,cljs.core.rest.call(null,s__45026__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__31064__auto__.call(null,msgs);
})());
});

my_stuff.core.log.cljs$lang$maxFixedArity = (0);

my_stuff.core.log.cljs$lang$applyTo = (function (seq45024){
return my_stuff.core.log.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq45024));
});

my_stuff.core.splash_screen = (function my_stuff$core$splash_screen(){
return reagent.core.create_class.call(null,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"component-did-mount","component-did-mount",-1126910518),(function (){
return setTimeout((function (){
return cljs.core.swap_BANG_.call(null,my_stuff.states.app_state,cljs.core.update_in,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"reg-info","reg-info",-809443695)], null),cljs.core.merge,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"page","page",849072397),"start"], null));
}),(2000));
}),new cljs.core.Keyword(null,"reagent-render","reagent-render",-985383853),(function (){
return new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.splash-screen","div.splash-screen",1043954419)], null);
})], null));
});
my_stuff.core.click_nav = (function my_stuff$core$click_nav(var_args){
var G__45034 = arguments.length;
switch (G__45034) {
case 0:
return my_stuff.core.click_nav.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return my_stuff.core.click_nav.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return my_stuff.core.click_nav.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

my_stuff.core.click_nav.cljs$core$IFn$_invoke$arity$0 = (function (){
return null;
});

my_stuff.core.click_nav.cljs$core$IFn$_invoke$arity$1 = (function (v){
return my_stuff.core.click_nav.call(null,new cljs.core.Keyword(null,"page","page",849072397),v);
});

my_stuff.core.click_nav.cljs$core$IFn$_invoke$arity$2 = (function (k,v){
my_stuff.core.click_nav.call(null);

return cljs.core.swap_BANG_.call(null,my_stuff.states.app_state,cljs.core.assoc,k,v);
});

my_stuff.core.click_nav.cljs$lang$maxFixedArity = 2;

my_stuff.core.menu_bar = (function my_stuff$core$menu_bar(){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.navbar-fixed","div.navbar-fixed",-404547015),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"nav","nav",719540477),new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.nav-wrapper","div.nav-wrapper",2090748862),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"className","className",-1983287057),"nav-bar-bg light-blue darken-3"], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a.brand-logo","a.brand-logo",1920204378),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"href","href",-793805698),"#!"], null),"Football app"], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",-2123407586),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"data-activates","data-activates",1521953804),"nav-ul",new cljs.core.Keyword(null,"className","className",-1983287057),"button-collapse",new cljs.core.Keyword(null,"href","href",-793805698),"#"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"i.material-icons","i.material-icons",740058269),"menu"], null)], null),new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ul.side-nav","ul.side-nav",1381687621),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"id","id",-1388402092),"nav-ul"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",723558921),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",-2123407586),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"href","href",-793805698),"#",new cljs.core.Keyword(null,"on-click","on-click",1632826543),(function (e){
return my_stuff.core.click_nav.call(null,"home");
})], null),"Home"], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",723558921),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",-2123407586),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"href","href",-793805698),"#",new cljs.core.Keyword(null,"on-click","on-click",1632826543),(function (e){
return my_stuff.core.click_nav.call(null,"activities");
})], null),"Activities"], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",723558921),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",-2123407586),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"href","href",-793805698),"#",new cljs.core.Keyword(null,"on-click","on-click",1632826543),(function (e){
return my_stuff.core.click_nav.call(null,"contact-us");
})], null),"Contact us"], null)], null)], null)], null)], null)], null);
});
my_stuff.core.nav_barr = (function my_stuff$core$nav_barr(){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"nav.navbar.navbar-inverse.navbar-static-top","nav.navbar.navbar-inverse.navbar-static-top",1275614125),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"role","role",-736691072),"navigation"], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.container","div.container",72419955),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.navbar-header","div.navbar-header",-515823511),new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"button.navbar-toggle","button.navbar-toggle",1737318847),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"data-toggle","data-toggle",436966687),"collapse",new cljs.core.Keyword(null,"type","type",1174270348),"button",new cljs.core.Keyword(null,"data-target","data-target",-113904678),"#bs-example-navbar-collapse-1"], null),"aff",new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span.icon-bar","span.icon-bar",618689172)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span.icon-bar","span.icon-bar",618689172)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span.icon-bar","span.icon-bar",618689172)], null)], null)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.collapse.navbar-collapse","div.collapse.navbar-collapse",-2098143156),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"id","id",-1388402092),"bs-example-navbar-collapse-1"], null),new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ul.nav.navbar-nav","ul.nav.navbar-nav",1805559761),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li.active","li.active",-1051611101),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",-2123407586),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"href","href",-793805698),"#"], null),"Home"], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",723558921),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",-2123407586),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"href","href",-793805698),"#"], null),"About"], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",723558921),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",-2123407586),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"href","href",-793805698),"#",new cljs.core.Keyword(null,"data-toggle","data-toggle",436966687),"collapse",new cljs.core.Keyword(null,"data-target","data-target",-113904678),".navbar-collapse.in"], null),"Close"], null)], null)], null)], null)], null)], null);
});
my_stuff.core.naav_barr = (function my_stuff$core$naav_barr(){
return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.st-container","div.st-container",-896861557),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"id","id",-1388402092),"st-container"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"nav.st-menu.st-effect-1","nav.st-menu.st-effect-1",671915698),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"id","id",-1388402092),"menu-1"], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.st-pusher","div.st-pusher",318484179),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.st-content","div.st-content",-287246799),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.st-content-inner","div.st-content-inner",-2106172865)], null)], null)], null)], null);
});
my_stuff.core.container = (function my_stuff$core$container(){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.app-container","div.app-container",-164087897),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [my_stuff.core.menu_bar], null)], null);
});
my_stuff.core.mount_root = (function my_stuff$core$mount_root(){
return reagent.core.render.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [my_stuff.core.container], null),my_stuff.core.by_id.call(null,"container"));
});
document.addEventListener("deviceready",my_stuff.core.mount_root,false);
my_stuff.core.reg_default = (function my_stuff$core$reg_default(){
var pred__45036 = cljs.core._EQ_;
var expr__45037 = new cljs.core.Keyword(null,"page","page",849072397).cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"reg-info","reg-info",-809443695).cljs$core$IFn$_invoke$arity$1(cljs.core.deref.call(null,my_stuff.states.app_state)));
if(cljs.core.truth_(pred__45036.call(null,"start",expr__45037))){
return new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [my_stuff.core.splash_screen], null);
} else {
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(expr__45037)].join('')));
}
});

//# sourceMappingURL=core.js.map?rel=1507225500148

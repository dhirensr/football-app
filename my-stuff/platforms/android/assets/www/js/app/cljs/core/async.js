// Compiled by ClojureScript 1.9.908 {}
goog.provide('cljs.core.async');
goog.require('cljs.core');
goog.require('cljs.core.async.impl.protocols');
goog.require('cljs.core.async.impl.channels');
goog.require('cljs.core.async.impl.buffers');
goog.require('cljs.core.async.impl.timers');
goog.require('cljs.core.async.impl.dispatch');
goog.require('cljs.core.async.impl.ioc_helpers');
cljs.core.async.fn_handler = (function cljs$core$async$fn_handler(var_args){
var G__41416 = arguments.length;
switch (G__41416) {
case 1:
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1 = (function (f){
return cljs.core.async.fn_handler.call(null,f,true);
});

cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2 = (function (f,blockable){
if(typeof cljs.core.async.t_cljs$core$async41417 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async41417 = (function (f,blockable,meta41418){
this.f = f;
this.blockable = blockable;
this.meta41418 = meta41418;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async41417.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_41419,meta41418__$1){
var self__ = this;
var _41419__$1 = this;
return (new cljs.core.async.t_cljs$core$async41417(self__.f,self__.blockable,meta41418__$1));
});

cljs.core.async.t_cljs$core$async41417.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_41419){
var self__ = this;
var _41419__$1 = this;
return self__.meta41418;
});

cljs.core.async.t_cljs$core$async41417.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async41417.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
});

cljs.core.async.t_cljs$core$async41417.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.blockable;
});

cljs.core.async.t_cljs$core$async41417.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.f;
});

cljs.core.async.t_cljs$core$async41417.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"blockable","blockable",-28395259,null),new cljs.core.Symbol(null,"meta41418","meta41418",1760353138,null)], null);
});

cljs.core.async.t_cljs$core$async41417.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async41417.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async41417";

cljs.core.async.t_cljs$core$async41417.cljs$lang$ctorPrWriter = (function (this__30853__auto__,writer__30854__auto__,opt__30855__auto__){
return cljs.core._write.call(null,writer__30854__auto__,"cljs.core.async/t_cljs$core$async41417");
});

cljs.core.async.__GT_t_cljs$core$async41417 = (function cljs$core$async$__GT_t_cljs$core$async41417(f__$1,blockable__$1,meta41418){
return (new cljs.core.async.t_cljs$core$async41417(f__$1,blockable__$1,meta41418));
});

}

return (new cljs.core.async.t_cljs$core$async41417(f,blockable,cljs.core.PersistentArrayMap.EMPTY));
});

cljs.core.async.fn_handler.cljs$lang$maxFixedArity = 2;

/**
 * Returns a fixed buffer of size n. When full, puts will block/park.
 */
cljs.core.async.buffer = (function cljs$core$async$buffer(n){
return cljs.core.async.impl.buffers.fixed_buffer.call(null,n);
});
/**
 * Returns a buffer of size n. When full, puts will complete but
 *   val will be dropped (no transfer).
 */
cljs.core.async.dropping_buffer = (function cljs$core$async$dropping_buffer(n){
return cljs.core.async.impl.buffers.dropping_buffer.call(null,n);
});
/**
 * Returns a buffer of size n. When full, puts will complete, and be
 *   buffered, but oldest elements in buffer will be dropped (not
 *   transferred).
 */
cljs.core.async.sliding_buffer = (function cljs$core$async$sliding_buffer(n){
return cljs.core.async.impl.buffers.sliding_buffer.call(null,n);
});
/**
 * Returns true if a channel created with buff will never block. That is to say,
 * puts into this buffer will never cause the buffer to be full. 
 */
cljs.core.async.unblocking_buffer_QMARK_ = (function cljs$core$async$unblocking_buffer_QMARK_(buff){
if(!((buff == null))){
if((false) || ((cljs.core.PROTOCOL_SENTINEL === buff.cljs$core$async$impl$protocols$UnblockingBuffer$))){
return true;
} else {
if((!buff.cljs$lang$protocol_mask$partition$)){
return cljs.core.native_satisfies_QMARK_.call(null,cljs.core.async.impl.protocols.UnblockingBuffer,buff);
} else {
return false;
}
}
} else {
return cljs.core.native_satisfies_QMARK_.call(null,cljs.core.async.impl.protocols.UnblockingBuffer,buff);
}
});
/**
 * Creates a channel with an optional buffer, an optional transducer (like (map f),
 *   (filter p) etc or a composition thereof), and an optional exception handler.
 *   If buf-or-n is a number, will create and use a fixed buffer of that size. If a
 *   transducer is supplied a buffer must be specified. ex-handler must be a
 *   fn of one argument - if an exception occurs during transformation it will be called
 *   with the thrown value as an argument, and any non-nil return value will be placed
 *   in the channel.
 */
cljs.core.async.chan = (function cljs$core$async$chan(var_args){
var G__41423 = arguments.length;
switch (G__41423) {
case 0:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0 = (function (){
return cljs.core.async.chan.call(null,null);
});

cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1 = (function (buf_or_n){
return cljs.core.async.chan.call(null,buf_or_n,null,null);
});

cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2 = (function (buf_or_n,xform){
return cljs.core.async.chan.call(null,buf_or_n,xform,null);
});

cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3 = (function (buf_or_n,xform,ex_handler){
var buf_or_n__$1 = ((cljs.core._EQ_.call(null,buf_or_n,(0)))?null:buf_or_n);
if(cljs.core.truth_(xform)){
if(cljs.core.truth_(buf_or_n__$1)){
} else {
throw (new Error(["Assert failed: ","buffer must be supplied when transducer is","\n","buf-or-n"].join('')));
}
} else {
}

return cljs.core.async.impl.channels.chan.call(null,((typeof buf_or_n__$1 === 'number')?cljs.core.async.buffer.call(null,buf_or_n__$1):buf_or_n__$1),xform,ex_handler);
});

cljs.core.async.chan.cljs$lang$maxFixedArity = 3;

/**
 * Creates a promise channel with an optional transducer, and an optional
 *   exception-handler. A promise channel can take exactly one value that consumers
 *   will receive. Once full, puts complete but val is dropped (no transfer).
 *   Consumers will block until either a value is placed in the channel or the
 *   channel is closed. See chan for the semantics of xform and ex-handler.
 */
cljs.core.async.promise_chan = (function cljs$core$async$promise_chan(var_args){
var G__41426 = arguments.length;
switch (G__41426) {
case 0:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0 = (function (){
return cljs.core.async.promise_chan.call(null,null);
});

cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1 = (function (xform){
return cljs.core.async.promise_chan.call(null,xform,null);
});

cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2 = (function (xform,ex_handler){
return cljs.core.async.chan.call(null,cljs.core.async.impl.buffers.promise_buffer.call(null),xform,ex_handler);
});

cljs.core.async.promise_chan.cljs$lang$maxFixedArity = 2;

/**
 * Returns a channel that will close after msecs
 */
cljs.core.async.timeout = (function cljs$core$async$timeout(msecs){
return cljs.core.async.impl.timers.timeout.call(null,msecs);
});
/**
 * takes a val from port. Must be called inside a (go ...) block. Will
 *   return nil if closed. Will park if nothing is available.
 *   Returns true unless port is already closed
 */
cljs.core.async._LT__BANG_ = (function cljs$core$async$_LT__BANG_(port){
throw (new Error("<! used not in (go ...) block"));
});
/**
 * Asynchronously takes a val from port, passing to fn1. Will pass nil
 * if closed. If on-caller? (default true) is true, and value is
 * immediately available, will call fn1 on calling thread.
 * Returns nil.
 */
cljs.core.async.take_BANG_ = (function cljs$core$async$take_BANG_(var_args){
var G__41429 = arguments.length;
switch (G__41429) {
case 2:
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (port,fn1){
return cljs.core.async.take_BANG_.call(null,port,fn1,true);
});

cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (port,fn1,on_caller_QMARK_){
var ret = cljs.core.async.impl.protocols.take_BANG_.call(null,port,cljs.core.async.fn_handler.call(null,fn1));
if(cljs.core.truth_(ret)){
var val_41431 = cljs.core.deref.call(null,ret);
if(cljs.core.truth_(on_caller_QMARK_)){
fn1.call(null,val_41431);
} else {
cljs.core.async.impl.dispatch.run.call(null,((function (val_41431,ret){
return (function (){
return fn1.call(null,val_41431);
});})(val_41431,ret))
);
}
} else {
}

return null;
});

cljs.core.async.take_BANG_.cljs$lang$maxFixedArity = 3;

cljs.core.async.nop = (function cljs$core$async$nop(_){
return null;
});
cljs.core.async.fhnop = cljs.core.async.fn_handler.call(null,cljs.core.async.nop);
/**
 * puts a val into port. nil values are not allowed. Must be called
 *   inside a (go ...) block. Will park if no buffer space is available.
 *   Returns true unless port is already closed.
 */
cljs.core.async._GT__BANG_ = (function cljs$core$async$_GT__BANG_(port,val){
throw (new Error(">! used not in (go ...) block"));
});
/**
 * Asynchronously puts a val into port, calling fn0 (if supplied) when
 * complete. nil values are not allowed. Will throw if closed. If
 * on-caller? (default true) is true, and the put is immediately
 * accepted, will call fn0 on calling thread.  Returns nil.
 */
cljs.core.async.put_BANG_ = (function cljs$core$async$put_BANG_(var_args){
var G__41433 = arguments.length;
switch (G__41433) {
case 2:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (port,val){
var temp__5276__auto__ = cljs.core.async.impl.protocols.put_BANG_.call(null,port,val,cljs.core.async.fhnop);
if(cljs.core.truth_(temp__5276__auto__)){
var ret = temp__5276__auto__;
return cljs.core.deref.call(null,ret);
} else {
return true;
}
});

cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (port,val,fn1){
return cljs.core.async.put_BANG_.call(null,port,val,fn1,true);
});

cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4 = (function (port,val,fn1,on_caller_QMARK_){
var temp__5276__auto__ = cljs.core.async.impl.protocols.put_BANG_.call(null,port,val,cljs.core.async.fn_handler.call(null,fn1));
if(cljs.core.truth_(temp__5276__auto__)){
var retb = temp__5276__auto__;
var ret = cljs.core.deref.call(null,retb);
if(cljs.core.truth_(on_caller_QMARK_)){
fn1.call(null,ret);
} else {
cljs.core.async.impl.dispatch.run.call(null,((function (ret,retb,temp__5276__auto__){
return (function (){
return fn1.call(null,ret);
});})(ret,retb,temp__5276__auto__))
);
}

return ret;
} else {
return true;
}
});

cljs.core.async.put_BANG_.cljs$lang$maxFixedArity = 4;

cljs.core.async.close_BANG_ = (function cljs$core$async$close_BANG_(port){
return cljs.core.async.impl.protocols.close_BANG_.call(null,port);
});
cljs.core.async.random_array = (function cljs$core$async$random_array(n){
var a = (new Array(n));
var n__31225__auto___41435 = n;
var x_41436 = (0);
while(true){
if((x_41436 < n__31225__auto___41435)){
(a[x_41436] = (0));

var G__41437 = (x_41436 + (1));
x_41436 = G__41437;
continue;
} else {
}
break;
}

var i = (1);
while(true){
if(cljs.core._EQ_.call(null,i,n)){
return a;
} else {
var j = cljs.core.rand_int.call(null,i);
(a[i] = (a[j]));

(a[j] = i);

var G__41438 = (i + (1));
i = G__41438;
continue;
}
break;
}
});
cljs.core.async.alt_flag = (function cljs$core$async$alt_flag(){
var flag = cljs.core.atom.call(null,true);
if(typeof cljs.core.async.t_cljs$core$async41439 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async41439 = (function (flag,meta41440){
this.flag = flag;
this.meta41440 = meta41440;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async41439.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (flag){
return (function (_41441,meta41440__$1){
var self__ = this;
var _41441__$1 = this;
return (new cljs.core.async.t_cljs$core$async41439(self__.flag,meta41440__$1));
});})(flag))
;

cljs.core.async.t_cljs$core$async41439.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (flag){
return (function (_41441){
var self__ = this;
var _41441__$1 = this;
return self__.meta41440;
});})(flag))
;

cljs.core.async.t_cljs$core$async41439.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async41439.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = ((function (flag){
return (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.deref.call(null,self__.flag);
});})(flag))
;

cljs.core.async.t_cljs$core$async41439.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = ((function (flag){
return (function (_){
var self__ = this;
var ___$1 = this;
return true;
});})(flag))
;

cljs.core.async.t_cljs$core$async41439.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = ((function (flag){
return (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_.call(null,self__.flag,null);

return true;
});})(flag))
;

cljs.core.async.t_cljs$core$async41439.getBasis = ((function (flag){
return (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"flag","flag",-1565787888,null),new cljs.core.Symbol(null,"meta41440","meta41440",-1945547634,null)], null);
});})(flag))
;

cljs.core.async.t_cljs$core$async41439.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async41439.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async41439";

cljs.core.async.t_cljs$core$async41439.cljs$lang$ctorPrWriter = ((function (flag){
return (function (this__30853__auto__,writer__30854__auto__,opt__30855__auto__){
return cljs.core._write.call(null,writer__30854__auto__,"cljs.core.async/t_cljs$core$async41439");
});})(flag))
;

cljs.core.async.__GT_t_cljs$core$async41439 = ((function (flag){
return (function cljs$core$async$alt_flag_$___GT_t_cljs$core$async41439(flag__$1,meta41440){
return (new cljs.core.async.t_cljs$core$async41439(flag__$1,meta41440));
});})(flag))
;

}

return (new cljs.core.async.t_cljs$core$async41439(flag,cljs.core.PersistentArrayMap.EMPTY));
});
cljs.core.async.alt_handler = (function cljs$core$async$alt_handler(flag,cb){
if(typeof cljs.core.async.t_cljs$core$async41442 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async41442 = (function (flag,cb,meta41443){
this.flag = flag;
this.cb = cb;
this.meta41443 = meta41443;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async41442.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_41444,meta41443__$1){
var self__ = this;
var _41444__$1 = this;
return (new cljs.core.async.t_cljs$core$async41442(self__.flag,self__.cb,meta41443__$1));
});

cljs.core.async.t_cljs$core$async41442.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_41444){
var self__ = this;
var _41444__$1 = this;
return self__.meta41443;
});

cljs.core.async.t_cljs$core$async41442.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async41442.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.active_QMARK_.call(null,self__.flag);
});

cljs.core.async.t_cljs$core$async41442.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
});

cljs.core.async.t_cljs$core$async41442.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.async.impl.protocols.commit.call(null,self__.flag);

return self__.cb;
});

cljs.core.async.t_cljs$core$async41442.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"flag","flag",-1565787888,null),new cljs.core.Symbol(null,"cb","cb",-2064487928,null),new cljs.core.Symbol(null,"meta41443","meta41443",1925181819,null)], null);
});

cljs.core.async.t_cljs$core$async41442.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async41442.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async41442";

cljs.core.async.t_cljs$core$async41442.cljs$lang$ctorPrWriter = (function (this__30853__auto__,writer__30854__auto__,opt__30855__auto__){
return cljs.core._write.call(null,writer__30854__auto__,"cljs.core.async/t_cljs$core$async41442");
});

cljs.core.async.__GT_t_cljs$core$async41442 = (function cljs$core$async$alt_handler_$___GT_t_cljs$core$async41442(flag__$1,cb__$1,meta41443){
return (new cljs.core.async.t_cljs$core$async41442(flag__$1,cb__$1,meta41443));
});

}

return (new cljs.core.async.t_cljs$core$async41442(flag,cb,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * returns derefable [val port] if immediate, nil if enqueued
 */
cljs.core.async.do_alts = (function cljs$core$async$do_alts(fret,ports,opts){
var flag = cljs.core.async.alt_flag.call(null);
var n = cljs.core.count.call(null,ports);
var idxs = cljs.core.async.random_array.call(null,n);
var priority = new cljs.core.Keyword(null,"priority","priority",1431093715).cljs$core$IFn$_invoke$arity$1(opts);
var ret = (function (){var i = (0);
while(true){
if((i < n)){
var idx = (cljs.core.truth_(priority)?i:(idxs[i]));
var port = cljs.core.nth.call(null,ports,idx);
var wport = ((cljs.core.vector_QMARK_.call(null,port))?port.call(null,(0)):null);
var vbox = (cljs.core.truth_(wport)?(function (){var val = port.call(null,(1));
return cljs.core.async.impl.protocols.put_BANG_.call(null,wport,val,cljs.core.async.alt_handler.call(null,flag,((function (i,val,idx,port,wport,flag,n,idxs,priority){
return (function (p1__41445_SHARP_){
return fret.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p1__41445_SHARP_,wport], null));
});})(i,val,idx,port,wport,flag,n,idxs,priority))
));
})():cljs.core.async.impl.protocols.take_BANG_.call(null,port,cljs.core.async.alt_handler.call(null,flag,((function (i,idx,port,wport,flag,n,idxs,priority){
return (function (p1__41446_SHARP_){
return fret.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p1__41446_SHARP_,port], null));
});})(i,idx,port,wport,flag,n,idxs,priority))
)));
if(cljs.core.truth_(vbox)){
return cljs.core.async.impl.channels.box.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.deref.call(null,vbox),(function (){var or__30182__auto__ = wport;
if(cljs.core.truth_(or__30182__auto__)){
return or__30182__auto__;
} else {
return port;
}
})()], null));
} else {
var G__41447 = (i + (1));
i = G__41447;
continue;
}
} else {
return null;
}
break;
}
})();
var or__30182__auto__ = ret;
if(cljs.core.truth_(or__30182__auto__)){
return or__30182__auto__;
} else {
if(cljs.core.contains_QMARK_.call(null,opts,new cljs.core.Keyword(null,"default","default",-1987822328))){
var temp__5278__auto__ = (function (){var and__30170__auto__ = cljs.core.async.impl.protocols.active_QMARK_.call(null,flag);
if(cljs.core.truth_(and__30170__auto__)){
return cljs.core.async.impl.protocols.commit.call(null,flag);
} else {
return and__30170__auto__;
}
})();
if(cljs.core.truth_(temp__5278__auto__)){
var got = temp__5278__auto__;
return cljs.core.async.impl.channels.box.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"default","default",-1987822328).cljs$core$IFn$_invoke$arity$1(opts),new cljs.core.Keyword(null,"default","default",-1987822328)], null));
} else {
return null;
}
} else {
return null;
}
}
});
/**
 * Completes at most one of several channel operations. Must be called
 * inside a (go ...) block. ports is a vector of channel endpoints,
 * which can be either a channel to take from or a vector of
 *   [channel-to-put-to val-to-put], in any combination. Takes will be
 *   made as if by <!, and puts will be made as if by >!. Unless
 *   the :priority option is true, if more than one port operation is
 *   ready a non-deterministic choice will be made. If no operation is
 *   ready and a :default value is supplied, [default-val :default] will
 *   be returned, otherwise alts! will park until the first operation to
 *   become ready completes. Returns [val port] of the completed
 *   operation, where val is the value taken for takes, and a
 *   boolean (true unless already closed, as per put!) for puts.
 * 
 *   opts are passed as :key val ... Supported options:
 * 
 *   :default val - the value to use if none of the operations are immediately ready
 *   :priority true - (default nil) when true, the operations will be tried in order.
 * 
 *   Note: there is no guarantee that the port exps or val exprs will be
 *   used, nor in what order should they be, so they should not be
 *   depended upon for side effects.
 */
cljs.core.async.alts_BANG_ = (function cljs$core$async$alts_BANG_(var_args){
var args__31466__auto__ = [];
var len__31459__auto___41453 = arguments.length;
var i__31460__auto___41454 = (0);
while(true){
if((i__31460__auto___41454 < len__31459__auto___41453)){
args__31466__auto__.push((arguments[i__31460__auto___41454]));

var G__41455 = (i__31460__auto___41454 + (1));
i__31460__auto___41454 = G__41455;
continue;
} else {
}
break;
}

var argseq__31467__auto__ = ((((1) < args__31466__auto__.length))?(new cljs.core.IndexedSeq(args__31466__auto__.slice((1)),(0),null)):null);
return cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__31467__auto__);
});

cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (ports,p__41450){
var map__41451 = p__41450;
var map__41451__$1 = ((((!((map__41451 == null)))?((((map__41451.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__41451.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__41451):map__41451);
var opts = map__41451__$1;
throw (new Error("alts! used not in (go ...) block"));
});

cljs.core.async.alts_BANG_.cljs$lang$maxFixedArity = (1);

cljs.core.async.alts_BANG_.cljs$lang$applyTo = (function (seq41448){
var G__41449 = cljs.core.first.call(null,seq41448);
var seq41448__$1 = cljs.core.next.call(null,seq41448);
return cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic(G__41449,seq41448__$1);
});

/**
 * Puts a val into port if it's possible to do so immediately.
 *   nil values are not allowed. Never blocks. Returns true if offer succeeds.
 */
cljs.core.async.offer_BANG_ = (function cljs$core$async$offer_BANG_(port,val){
var ret = cljs.core.async.impl.protocols.put_BANG_.call(null,port,val,cljs.core.async.fn_handler.call(null,cljs.core.async.nop,false));
if(cljs.core.truth_(ret)){
return cljs.core.deref.call(null,ret);
} else {
return null;
}
});
/**
 * Takes a val from port if it's possible to do so immediately.
 *   Never blocks. Returns value if successful, nil otherwise.
 */
cljs.core.async.poll_BANG_ = (function cljs$core$async$poll_BANG_(port){
var ret = cljs.core.async.impl.protocols.take_BANG_.call(null,port,cljs.core.async.fn_handler.call(null,cljs.core.async.nop,false));
if(cljs.core.truth_(ret)){
return cljs.core.deref.call(null,ret);
} else {
return null;
}
});
/**
 * Takes elements from the from channel and supplies them to the to
 * channel. By default, the to channel will be closed when the from
 * channel closes, but can be determined by the close?  parameter. Will
 * stop consuming the from channel if the to channel closes
 */
cljs.core.async.pipe = (function cljs$core$async$pipe(var_args){
var G__41457 = arguments.length;
switch (G__41457) {
case 2:
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$2 = (function (from,to){
return cljs.core.async.pipe.call(null,from,to,true);
});

cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3 = (function (from,to,close_QMARK_){
var c__35058__auto___41503 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__35058__auto___41503){
return (function (){
var f__35059__auto__ = (function (){var switch__34968__auto__ = ((function (c__35058__auto___41503){
return (function (state_41481){
var state_val_41482 = (state_41481[(1)]);
if((state_val_41482 === (7))){
var inst_41477 = (state_41481[(2)]);
var state_41481__$1 = state_41481;
var statearr_41483_41504 = state_41481__$1;
(statearr_41483_41504[(2)] = inst_41477);

(statearr_41483_41504[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41482 === (1))){
var state_41481__$1 = state_41481;
var statearr_41484_41505 = state_41481__$1;
(statearr_41484_41505[(2)] = null);

(statearr_41484_41505[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41482 === (4))){
var inst_41460 = (state_41481[(7)]);
var inst_41460__$1 = (state_41481[(2)]);
var inst_41461 = (inst_41460__$1 == null);
var state_41481__$1 = (function (){var statearr_41485 = state_41481;
(statearr_41485[(7)] = inst_41460__$1);

return statearr_41485;
})();
if(cljs.core.truth_(inst_41461)){
var statearr_41486_41506 = state_41481__$1;
(statearr_41486_41506[(1)] = (5));

} else {
var statearr_41487_41507 = state_41481__$1;
(statearr_41487_41507[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41482 === (13))){
var state_41481__$1 = state_41481;
var statearr_41488_41508 = state_41481__$1;
(statearr_41488_41508[(2)] = null);

(statearr_41488_41508[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41482 === (6))){
var inst_41460 = (state_41481[(7)]);
var state_41481__$1 = state_41481;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_41481__$1,(11),to,inst_41460);
} else {
if((state_val_41482 === (3))){
var inst_41479 = (state_41481[(2)]);
var state_41481__$1 = state_41481;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_41481__$1,inst_41479);
} else {
if((state_val_41482 === (12))){
var state_41481__$1 = state_41481;
var statearr_41489_41509 = state_41481__$1;
(statearr_41489_41509[(2)] = null);

(statearr_41489_41509[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41482 === (2))){
var state_41481__$1 = state_41481;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_41481__$1,(4),from);
} else {
if((state_val_41482 === (11))){
var inst_41470 = (state_41481[(2)]);
var state_41481__$1 = state_41481;
if(cljs.core.truth_(inst_41470)){
var statearr_41490_41510 = state_41481__$1;
(statearr_41490_41510[(1)] = (12));

} else {
var statearr_41491_41511 = state_41481__$1;
(statearr_41491_41511[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41482 === (9))){
var state_41481__$1 = state_41481;
var statearr_41492_41512 = state_41481__$1;
(statearr_41492_41512[(2)] = null);

(statearr_41492_41512[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41482 === (5))){
var state_41481__$1 = state_41481;
if(cljs.core.truth_(close_QMARK_)){
var statearr_41493_41513 = state_41481__$1;
(statearr_41493_41513[(1)] = (8));

} else {
var statearr_41494_41514 = state_41481__$1;
(statearr_41494_41514[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41482 === (14))){
var inst_41475 = (state_41481[(2)]);
var state_41481__$1 = state_41481;
var statearr_41495_41515 = state_41481__$1;
(statearr_41495_41515[(2)] = inst_41475);

(statearr_41495_41515[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41482 === (10))){
var inst_41467 = (state_41481[(2)]);
var state_41481__$1 = state_41481;
var statearr_41496_41516 = state_41481__$1;
(statearr_41496_41516[(2)] = inst_41467);

(statearr_41496_41516[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41482 === (8))){
var inst_41464 = cljs.core.async.close_BANG_.call(null,to);
var state_41481__$1 = state_41481;
var statearr_41497_41517 = state_41481__$1;
(statearr_41497_41517[(2)] = inst_41464);

(statearr_41497_41517[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__35058__auto___41503))
;
return ((function (switch__34968__auto__,c__35058__auto___41503){
return (function() {
var cljs$core$async$state_machine__34969__auto__ = null;
var cljs$core$async$state_machine__34969__auto____0 = (function (){
var statearr_41498 = [null,null,null,null,null,null,null,null];
(statearr_41498[(0)] = cljs$core$async$state_machine__34969__auto__);

(statearr_41498[(1)] = (1));

return statearr_41498;
});
var cljs$core$async$state_machine__34969__auto____1 = (function (state_41481){
while(true){
var ret_value__34970__auto__ = (function (){try{while(true){
var result__34971__auto__ = switch__34968__auto__.call(null,state_41481);
if(cljs.core.keyword_identical_QMARK_.call(null,result__34971__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__34971__auto__;
}
break;
}
}catch (e41499){if((e41499 instanceof Object)){
var ex__34972__auto__ = e41499;
var statearr_41500_41518 = state_41481;
(statearr_41500_41518[(5)] = ex__34972__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_41481);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e41499;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__34970__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__41519 = state_41481;
state_41481 = G__41519;
continue;
} else {
return ret_value__34970__auto__;
}
break;
}
});
cljs$core$async$state_machine__34969__auto__ = function(state_41481){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__34969__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__34969__auto____1.call(this,state_41481);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__34969__auto____0;
cljs$core$async$state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__34969__auto____1;
return cljs$core$async$state_machine__34969__auto__;
})()
;})(switch__34968__auto__,c__35058__auto___41503))
})();
var state__35060__auto__ = (function (){var statearr_41501 = f__35059__auto__.call(null);
(statearr_41501[(6)] = c__35058__auto___41503);

return statearr_41501;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__35060__auto__);
});})(c__35058__auto___41503))
);


return to;
});

cljs.core.async.pipe.cljs$lang$maxFixedArity = 3;

cljs.core.async.pipeline_STAR_ = (function cljs$core$async$pipeline_STAR_(n,to,xf,from,close_QMARK_,ex_handler,type){
if((n > (0))){
} else {
throw (new Error("Assert failed: (pos? n)"));
}

var jobs = cljs.core.async.chan.call(null,n);
var results = cljs.core.async.chan.call(null,n);
var process__$1 = ((function (jobs,results){
return (function (p__41520){
var vec__41521 = p__41520;
var v = cljs.core.nth.call(null,vec__41521,(0),null);
var p = cljs.core.nth.call(null,vec__41521,(1),null);
var job = vec__41521;
if((job == null)){
cljs.core.async.close_BANG_.call(null,results);

return null;
} else {
var res = cljs.core.async.chan.call(null,(1),xf,ex_handler);
var c__35058__auto___41692 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__35058__auto___41692,res,vec__41521,v,p,job,jobs,results){
return (function (){
var f__35059__auto__ = (function (){var switch__34968__auto__ = ((function (c__35058__auto___41692,res,vec__41521,v,p,job,jobs,results){
return (function (state_41528){
var state_val_41529 = (state_41528[(1)]);
if((state_val_41529 === (1))){
var state_41528__$1 = state_41528;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_41528__$1,(2),res,v);
} else {
if((state_val_41529 === (2))){
var inst_41525 = (state_41528[(2)]);
var inst_41526 = cljs.core.async.close_BANG_.call(null,res);
var state_41528__$1 = (function (){var statearr_41530 = state_41528;
(statearr_41530[(7)] = inst_41525);

return statearr_41530;
})();
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_41528__$1,inst_41526);
} else {
return null;
}
}
});})(c__35058__auto___41692,res,vec__41521,v,p,job,jobs,results))
;
return ((function (switch__34968__auto__,c__35058__auto___41692,res,vec__41521,v,p,job,jobs,results){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____0 = (function (){
var statearr_41531 = [null,null,null,null,null,null,null,null];
(statearr_41531[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__);

(statearr_41531[(1)] = (1));

return statearr_41531;
});
var cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____1 = (function (state_41528){
while(true){
var ret_value__34970__auto__ = (function (){try{while(true){
var result__34971__auto__ = switch__34968__auto__.call(null,state_41528);
if(cljs.core.keyword_identical_QMARK_.call(null,result__34971__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__34971__auto__;
}
break;
}
}catch (e41532){if((e41532 instanceof Object)){
var ex__34972__auto__ = e41532;
var statearr_41533_41693 = state_41528;
(statearr_41533_41693[(5)] = ex__34972__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_41528);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e41532;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__34970__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__41694 = state_41528;
state_41528 = G__41694;
continue;
} else {
return ret_value__34970__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__ = function(state_41528){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____1.call(this,state_41528);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__;
})()
;})(switch__34968__auto__,c__35058__auto___41692,res,vec__41521,v,p,job,jobs,results))
})();
var state__35060__auto__ = (function (){var statearr_41534 = f__35059__auto__.call(null);
(statearr_41534[(6)] = c__35058__auto___41692);

return statearr_41534;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__35060__auto__);
});})(c__35058__auto___41692,res,vec__41521,v,p,job,jobs,results))
);


cljs.core.async.put_BANG_.call(null,p,res);

return true;
}
});})(jobs,results))
;
var async = ((function (jobs,results,process__$1){
return (function (p__41535){
var vec__41536 = p__41535;
var v = cljs.core.nth.call(null,vec__41536,(0),null);
var p = cljs.core.nth.call(null,vec__41536,(1),null);
var job = vec__41536;
if((job == null)){
cljs.core.async.close_BANG_.call(null,results);

return null;
} else {
var res = cljs.core.async.chan.call(null,(1));
xf.call(null,v,res);

cljs.core.async.put_BANG_.call(null,p,res);

return true;
}
});})(jobs,results,process__$1))
;
var n__31225__auto___41695 = n;
var __41696 = (0);
while(true){
if((__41696 < n__31225__auto___41695)){
var G__41539_41697 = type;
var G__41539_41698__$1 = (((G__41539_41697 instanceof cljs.core.Keyword))?G__41539_41697.fqn:null);
switch (G__41539_41698__$1) {
case "compute":
var c__35058__auto___41700 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (__41696,c__35058__auto___41700,G__41539_41697,G__41539_41698__$1,n__31225__auto___41695,jobs,results,process__$1,async){
return (function (){
var f__35059__auto__ = (function (){var switch__34968__auto__ = ((function (__41696,c__35058__auto___41700,G__41539_41697,G__41539_41698__$1,n__31225__auto___41695,jobs,results,process__$1,async){
return (function (state_41552){
var state_val_41553 = (state_41552[(1)]);
if((state_val_41553 === (1))){
var state_41552__$1 = state_41552;
var statearr_41554_41701 = state_41552__$1;
(statearr_41554_41701[(2)] = null);

(statearr_41554_41701[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41553 === (2))){
var state_41552__$1 = state_41552;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_41552__$1,(4),jobs);
} else {
if((state_val_41553 === (3))){
var inst_41550 = (state_41552[(2)]);
var state_41552__$1 = state_41552;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_41552__$1,inst_41550);
} else {
if((state_val_41553 === (4))){
var inst_41542 = (state_41552[(2)]);
var inst_41543 = process__$1.call(null,inst_41542);
var state_41552__$1 = state_41552;
if(cljs.core.truth_(inst_41543)){
var statearr_41555_41702 = state_41552__$1;
(statearr_41555_41702[(1)] = (5));

} else {
var statearr_41556_41703 = state_41552__$1;
(statearr_41556_41703[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41553 === (5))){
var state_41552__$1 = state_41552;
var statearr_41557_41704 = state_41552__$1;
(statearr_41557_41704[(2)] = null);

(statearr_41557_41704[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41553 === (6))){
var state_41552__$1 = state_41552;
var statearr_41558_41705 = state_41552__$1;
(statearr_41558_41705[(2)] = null);

(statearr_41558_41705[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41553 === (7))){
var inst_41548 = (state_41552[(2)]);
var state_41552__$1 = state_41552;
var statearr_41559_41706 = state_41552__$1;
(statearr_41559_41706[(2)] = inst_41548);

(statearr_41559_41706[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(__41696,c__35058__auto___41700,G__41539_41697,G__41539_41698__$1,n__31225__auto___41695,jobs,results,process__$1,async))
;
return ((function (__41696,switch__34968__auto__,c__35058__auto___41700,G__41539_41697,G__41539_41698__$1,n__31225__auto___41695,jobs,results,process__$1,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____0 = (function (){
var statearr_41560 = [null,null,null,null,null,null,null];
(statearr_41560[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__);

(statearr_41560[(1)] = (1));

return statearr_41560;
});
var cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____1 = (function (state_41552){
while(true){
var ret_value__34970__auto__ = (function (){try{while(true){
var result__34971__auto__ = switch__34968__auto__.call(null,state_41552);
if(cljs.core.keyword_identical_QMARK_.call(null,result__34971__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__34971__auto__;
}
break;
}
}catch (e41561){if((e41561 instanceof Object)){
var ex__34972__auto__ = e41561;
var statearr_41562_41707 = state_41552;
(statearr_41562_41707[(5)] = ex__34972__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_41552);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e41561;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__34970__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__41708 = state_41552;
state_41552 = G__41708;
continue;
} else {
return ret_value__34970__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__ = function(state_41552){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____1.call(this,state_41552);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__;
})()
;})(__41696,switch__34968__auto__,c__35058__auto___41700,G__41539_41697,G__41539_41698__$1,n__31225__auto___41695,jobs,results,process__$1,async))
})();
var state__35060__auto__ = (function (){var statearr_41563 = f__35059__auto__.call(null);
(statearr_41563[(6)] = c__35058__auto___41700);

return statearr_41563;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__35060__auto__);
});})(__41696,c__35058__auto___41700,G__41539_41697,G__41539_41698__$1,n__31225__auto___41695,jobs,results,process__$1,async))
);


break;
case "async":
var c__35058__auto___41709 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (__41696,c__35058__auto___41709,G__41539_41697,G__41539_41698__$1,n__31225__auto___41695,jobs,results,process__$1,async){
return (function (){
var f__35059__auto__ = (function (){var switch__34968__auto__ = ((function (__41696,c__35058__auto___41709,G__41539_41697,G__41539_41698__$1,n__31225__auto___41695,jobs,results,process__$1,async){
return (function (state_41576){
var state_val_41577 = (state_41576[(1)]);
if((state_val_41577 === (1))){
var state_41576__$1 = state_41576;
var statearr_41578_41710 = state_41576__$1;
(statearr_41578_41710[(2)] = null);

(statearr_41578_41710[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41577 === (2))){
var state_41576__$1 = state_41576;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_41576__$1,(4),jobs);
} else {
if((state_val_41577 === (3))){
var inst_41574 = (state_41576[(2)]);
var state_41576__$1 = state_41576;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_41576__$1,inst_41574);
} else {
if((state_val_41577 === (4))){
var inst_41566 = (state_41576[(2)]);
var inst_41567 = async.call(null,inst_41566);
var state_41576__$1 = state_41576;
if(cljs.core.truth_(inst_41567)){
var statearr_41579_41711 = state_41576__$1;
(statearr_41579_41711[(1)] = (5));

} else {
var statearr_41580_41712 = state_41576__$1;
(statearr_41580_41712[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41577 === (5))){
var state_41576__$1 = state_41576;
var statearr_41581_41713 = state_41576__$1;
(statearr_41581_41713[(2)] = null);

(statearr_41581_41713[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41577 === (6))){
var state_41576__$1 = state_41576;
var statearr_41582_41714 = state_41576__$1;
(statearr_41582_41714[(2)] = null);

(statearr_41582_41714[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41577 === (7))){
var inst_41572 = (state_41576[(2)]);
var state_41576__$1 = state_41576;
var statearr_41583_41715 = state_41576__$1;
(statearr_41583_41715[(2)] = inst_41572);

(statearr_41583_41715[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(__41696,c__35058__auto___41709,G__41539_41697,G__41539_41698__$1,n__31225__auto___41695,jobs,results,process__$1,async))
;
return ((function (__41696,switch__34968__auto__,c__35058__auto___41709,G__41539_41697,G__41539_41698__$1,n__31225__auto___41695,jobs,results,process__$1,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____0 = (function (){
var statearr_41584 = [null,null,null,null,null,null,null];
(statearr_41584[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__);

(statearr_41584[(1)] = (1));

return statearr_41584;
});
var cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____1 = (function (state_41576){
while(true){
var ret_value__34970__auto__ = (function (){try{while(true){
var result__34971__auto__ = switch__34968__auto__.call(null,state_41576);
if(cljs.core.keyword_identical_QMARK_.call(null,result__34971__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__34971__auto__;
}
break;
}
}catch (e41585){if((e41585 instanceof Object)){
var ex__34972__auto__ = e41585;
var statearr_41586_41716 = state_41576;
(statearr_41586_41716[(5)] = ex__34972__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_41576);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e41585;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__34970__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__41717 = state_41576;
state_41576 = G__41717;
continue;
} else {
return ret_value__34970__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__ = function(state_41576){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____1.call(this,state_41576);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__;
})()
;})(__41696,switch__34968__auto__,c__35058__auto___41709,G__41539_41697,G__41539_41698__$1,n__31225__auto___41695,jobs,results,process__$1,async))
})();
var state__35060__auto__ = (function (){var statearr_41587 = f__35059__auto__.call(null);
(statearr_41587[(6)] = c__35058__auto___41709);

return statearr_41587;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__35060__auto__);
});})(__41696,c__35058__auto___41709,G__41539_41697,G__41539_41698__$1,n__31225__auto___41695,jobs,results,process__$1,async))
);


break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__41539_41698__$1)].join('')));

}

var G__41718 = (__41696 + (1));
__41696 = G__41718;
continue;
} else {
}
break;
}

var c__35058__auto___41719 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__35058__auto___41719,jobs,results,process__$1,async){
return (function (){
var f__35059__auto__ = (function (){var switch__34968__auto__ = ((function (c__35058__auto___41719,jobs,results,process__$1,async){
return (function (state_41609){
var state_val_41610 = (state_41609[(1)]);
if((state_val_41610 === (1))){
var state_41609__$1 = state_41609;
var statearr_41611_41720 = state_41609__$1;
(statearr_41611_41720[(2)] = null);

(statearr_41611_41720[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41610 === (2))){
var state_41609__$1 = state_41609;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_41609__$1,(4),from);
} else {
if((state_val_41610 === (3))){
var inst_41607 = (state_41609[(2)]);
var state_41609__$1 = state_41609;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_41609__$1,inst_41607);
} else {
if((state_val_41610 === (4))){
var inst_41590 = (state_41609[(7)]);
var inst_41590__$1 = (state_41609[(2)]);
var inst_41591 = (inst_41590__$1 == null);
var state_41609__$1 = (function (){var statearr_41612 = state_41609;
(statearr_41612[(7)] = inst_41590__$1);

return statearr_41612;
})();
if(cljs.core.truth_(inst_41591)){
var statearr_41613_41721 = state_41609__$1;
(statearr_41613_41721[(1)] = (5));

} else {
var statearr_41614_41722 = state_41609__$1;
(statearr_41614_41722[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41610 === (5))){
var inst_41593 = cljs.core.async.close_BANG_.call(null,jobs);
var state_41609__$1 = state_41609;
var statearr_41615_41723 = state_41609__$1;
(statearr_41615_41723[(2)] = inst_41593);

(statearr_41615_41723[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41610 === (6))){
var inst_41590 = (state_41609[(7)]);
var inst_41595 = (state_41609[(8)]);
var inst_41595__$1 = cljs.core.async.chan.call(null,(1));
var inst_41596 = cljs.core.PersistentVector.EMPTY_NODE;
var inst_41597 = [inst_41590,inst_41595__$1];
var inst_41598 = (new cljs.core.PersistentVector(null,2,(5),inst_41596,inst_41597,null));
var state_41609__$1 = (function (){var statearr_41616 = state_41609;
(statearr_41616[(8)] = inst_41595__$1);

return statearr_41616;
})();
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_41609__$1,(8),jobs,inst_41598);
} else {
if((state_val_41610 === (7))){
var inst_41605 = (state_41609[(2)]);
var state_41609__$1 = state_41609;
var statearr_41617_41724 = state_41609__$1;
(statearr_41617_41724[(2)] = inst_41605);

(statearr_41617_41724[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41610 === (8))){
var inst_41595 = (state_41609[(8)]);
var inst_41600 = (state_41609[(2)]);
var state_41609__$1 = (function (){var statearr_41618 = state_41609;
(statearr_41618[(9)] = inst_41600);

return statearr_41618;
})();
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_41609__$1,(9),results,inst_41595);
} else {
if((state_val_41610 === (9))){
var inst_41602 = (state_41609[(2)]);
var state_41609__$1 = (function (){var statearr_41619 = state_41609;
(statearr_41619[(10)] = inst_41602);

return statearr_41619;
})();
var statearr_41620_41725 = state_41609__$1;
(statearr_41620_41725[(2)] = null);

(statearr_41620_41725[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
});})(c__35058__auto___41719,jobs,results,process__$1,async))
;
return ((function (switch__34968__auto__,c__35058__auto___41719,jobs,results,process__$1,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____0 = (function (){
var statearr_41621 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_41621[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__);

(statearr_41621[(1)] = (1));

return statearr_41621;
});
var cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____1 = (function (state_41609){
while(true){
var ret_value__34970__auto__ = (function (){try{while(true){
var result__34971__auto__ = switch__34968__auto__.call(null,state_41609);
if(cljs.core.keyword_identical_QMARK_.call(null,result__34971__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__34971__auto__;
}
break;
}
}catch (e41622){if((e41622 instanceof Object)){
var ex__34972__auto__ = e41622;
var statearr_41623_41726 = state_41609;
(statearr_41623_41726[(5)] = ex__34972__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_41609);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e41622;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__34970__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__41727 = state_41609;
state_41609 = G__41727;
continue;
} else {
return ret_value__34970__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__ = function(state_41609){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____1.call(this,state_41609);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__;
})()
;})(switch__34968__auto__,c__35058__auto___41719,jobs,results,process__$1,async))
})();
var state__35060__auto__ = (function (){var statearr_41624 = f__35059__auto__.call(null);
(statearr_41624[(6)] = c__35058__auto___41719);

return statearr_41624;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__35060__auto__);
});})(c__35058__auto___41719,jobs,results,process__$1,async))
);


var c__35058__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__35058__auto__,jobs,results,process__$1,async){
return (function (){
var f__35059__auto__ = (function (){var switch__34968__auto__ = ((function (c__35058__auto__,jobs,results,process__$1,async){
return (function (state_41662){
var state_val_41663 = (state_41662[(1)]);
if((state_val_41663 === (7))){
var inst_41658 = (state_41662[(2)]);
var state_41662__$1 = state_41662;
var statearr_41664_41728 = state_41662__$1;
(statearr_41664_41728[(2)] = inst_41658);

(statearr_41664_41728[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41663 === (20))){
var state_41662__$1 = state_41662;
var statearr_41665_41729 = state_41662__$1;
(statearr_41665_41729[(2)] = null);

(statearr_41665_41729[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41663 === (1))){
var state_41662__$1 = state_41662;
var statearr_41666_41730 = state_41662__$1;
(statearr_41666_41730[(2)] = null);

(statearr_41666_41730[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41663 === (4))){
var inst_41627 = (state_41662[(7)]);
var inst_41627__$1 = (state_41662[(2)]);
var inst_41628 = (inst_41627__$1 == null);
var state_41662__$1 = (function (){var statearr_41667 = state_41662;
(statearr_41667[(7)] = inst_41627__$1);

return statearr_41667;
})();
if(cljs.core.truth_(inst_41628)){
var statearr_41668_41731 = state_41662__$1;
(statearr_41668_41731[(1)] = (5));

} else {
var statearr_41669_41732 = state_41662__$1;
(statearr_41669_41732[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41663 === (15))){
var inst_41640 = (state_41662[(8)]);
var state_41662__$1 = state_41662;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_41662__$1,(18),to,inst_41640);
} else {
if((state_val_41663 === (21))){
var inst_41653 = (state_41662[(2)]);
var state_41662__$1 = state_41662;
var statearr_41670_41733 = state_41662__$1;
(statearr_41670_41733[(2)] = inst_41653);

(statearr_41670_41733[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41663 === (13))){
var inst_41655 = (state_41662[(2)]);
var state_41662__$1 = (function (){var statearr_41671 = state_41662;
(statearr_41671[(9)] = inst_41655);

return statearr_41671;
})();
var statearr_41672_41734 = state_41662__$1;
(statearr_41672_41734[(2)] = null);

(statearr_41672_41734[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41663 === (6))){
var inst_41627 = (state_41662[(7)]);
var state_41662__$1 = state_41662;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_41662__$1,(11),inst_41627);
} else {
if((state_val_41663 === (17))){
var inst_41648 = (state_41662[(2)]);
var state_41662__$1 = state_41662;
if(cljs.core.truth_(inst_41648)){
var statearr_41673_41735 = state_41662__$1;
(statearr_41673_41735[(1)] = (19));

} else {
var statearr_41674_41736 = state_41662__$1;
(statearr_41674_41736[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41663 === (3))){
var inst_41660 = (state_41662[(2)]);
var state_41662__$1 = state_41662;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_41662__$1,inst_41660);
} else {
if((state_val_41663 === (12))){
var inst_41637 = (state_41662[(10)]);
var state_41662__$1 = state_41662;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_41662__$1,(14),inst_41637);
} else {
if((state_val_41663 === (2))){
var state_41662__$1 = state_41662;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_41662__$1,(4),results);
} else {
if((state_val_41663 === (19))){
var state_41662__$1 = state_41662;
var statearr_41675_41737 = state_41662__$1;
(statearr_41675_41737[(2)] = null);

(statearr_41675_41737[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41663 === (11))){
var inst_41637 = (state_41662[(2)]);
var state_41662__$1 = (function (){var statearr_41676 = state_41662;
(statearr_41676[(10)] = inst_41637);

return statearr_41676;
})();
var statearr_41677_41738 = state_41662__$1;
(statearr_41677_41738[(2)] = null);

(statearr_41677_41738[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41663 === (9))){
var state_41662__$1 = state_41662;
var statearr_41678_41739 = state_41662__$1;
(statearr_41678_41739[(2)] = null);

(statearr_41678_41739[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41663 === (5))){
var state_41662__$1 = state_41662;
if(cljs.core.truth_(close_QMARK_)){
var statearr_41679_41740 = state_41662__$1;
(statearr_41679_41740[(1)] = (8));

} else {
var statearr_41680_41741 = state_41662__$1;
(statearr_41680_41741[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41663 === (14))){
var inst_41642 = (state_41662[(11)]);
var inst_41640 = (state_41662[(8)]);
var inst_41640__$1 = (state_41662[(2)]);
var inst_41641 = (inst_41640__$1 == null);
var inst_41642__$1 = cljs.core.not.call(null,inst_41641);
var state_41662__$1 = (function (){var statearr_41681 = state_41662;
(statearr_41681[(11)] = inst_41642__$1);

(statearr_41681[(8)] = inst_41640__$1);

return statearr_41681;
})();
if(inst_41642__$1){
var statearr_41682_41742 = state_41662__$1;
(statearr_41682_41742[(1)] = (15));

} else {
var statearr_41683_41743 = state_41662__$1;
(statearr_41683_41743[(1)] = (16));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41663 === (16))){
var inst_41642 = (state_41662[(11)]);
var state_41662__$1 = state_41662;
var statearr_41684_41744 = state_41662__$1;
(statearr_41684_41744[(2)] = inst_41642);

(statearr_41684_41744[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41663 === (10))){
var inst_41634 = (state_41662[(2)]);
var state_41662__$1 = state_41662;
var statearr_41685_41745 = state_41662__$1;
(statearr_41685_41745[(2)] = inst_41634);

(statearr_41685_41745[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41663 === (18))){
var inst_41645 = (state_41662[(2)]);
var state_41662__$1 = state_41662;
var statearr_41686_41746 = state_41662__$1;
(statearr_41686_41746[(2)] = inst_41645);

(statearr_41686_41746[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41663 === (8))){
var inst_41631 = cljs.core.async.close_BANG_.call(null,to);
var state_41662__$1 = state_41662;
var statearr_41687_41747 = state_41662__$1;
(statearr_41687_41747[(2)] = inst_41631);

(statearr_41687_41747[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__35058__auto__,jobs,results,process__$1,async))
;
return ((function (switch__34968__auto__,c__35058__auto__,jobs,results,process__$1,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____0 = (function (){
var statearr_41688 = [null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_41688[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__);

(statearr_41688[(1)] = (1));

return statearr_41688;
});
var cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____1 = (function (state_41662){
while(true){
var ret_value__34970__auto__ = (function (){try{while(true){
var result__34971__auto__ = switch__34968__auto__.call(null,state_41662);
if(cljs.core.keyword_identical_QMARK_.call(null,result__34971__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__34971__auto__;
}
break;
}
}catch (e41689){if((e41689 instanceof Object)){
var ex__34972__auto__ = e41689;
var statearr_41690_41748 = state_41662;
(statearr_41690_41748[(5)] = ex__34972__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_41662);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e41689;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__34970__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__41749 = state_41662;
state_41662 = G__41749;
continue;
} else {
return ret_value__34970__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__ = function(state_41662){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____1.call(this,state_41662);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__34969__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__34969__auto__;
})()
;})(switch__34968__auto__,c__35058__auto__,jobs,results,process__$1,async))
})();
var state__35060__auto__ = (function (){var statearr_41691 = f__35059__auto__.call(null);
(statearr_41691[(6)] = c__35058__auto__);

return statearr_41691;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__35060__auto__);
});})(c__35058__auto__,jobs,results,process__$1,async))
);

return c__35058__auto__;
});
/**
 * Takes elements from the from channel and supplies them to the to
 *   channel, subject to the async function af, with parallelism n. af
 *   must be a function of two arguments, the first an input value and
 *   the second a channel on which to place the result(s). af must close!
 *   the channel before returning.  The presumption is that af will
 *   return immediately, having launched some asynchronous operation
 *   whose completion/callback will manipulate the result channel. Outputs
 *   will be returned in order relative to  the inputs. By default, the to
 *   channel will be closed when the from channel closes, but can be
 *   determined by the close?  parameter. Will stop consuming the from
 *   channel if the to channel closes.
 */
cljs.core.async.pipeline_async = (function cljs$core$async$pipeline_async(var_args){
var G__41751 = arguments.length;
switch (G__41751) {
case 4:
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
case 5:
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$4 = (function (n,to,af,from){
return cljs.core.async.pipeline_async.call(null,n,to,af,from,true);
});

cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5 = (function (n,to,af,from,close_QMARK_){
return cljs.core.async.pipeline_STAR_.call(null,n,to,af,from,close_QMARK_,null,new cljs.core.Keyword(null,"async","async",1050769601));
});

cljs.core.async.pipeline_async.cljs$lang$maxFixedArity = 5;

/**
 * Takes elements from the from channel and supplies them to the to
 *   channel, subject to the transducer xf, with parallelism n. Because
 *   it is parallel, the transducer will be applied independently to each
 *   element, not across elements, and may produce zero or more outputs
 *   per input.  Outputs will be returned in order relative to the
 *   inputs. By default, the to channel will be closed when the from
 *   channel closes, but can be determined by the close?  parameter. Will
 *   stop consuming the from channel if the to channel closes.
 * 
 *   Note this is supplied for API compatibility with the Clojure version.
 *   Values of N > 1 will not result in actual concurrency in a
 *   single-threaded runtime.
 */
cljs.core.async.pipeline = (function cljs$core$async$pipeline(var_args){
var G__41754 = arguments.length;
switch (G__41754) {
case 4:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
case 5:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]));

break;
case 6:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]),(arguments[(5)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$4 = (function (n,to,xf,from){
return cljs.core.async.pipeline.call(null,n,to,xf,from,true);
});

cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5 = (function (n,to,xf,from,close_QMARK_){
return cljs.core.async.pipeline.call(null,n,to,xf,from,close_QMARK_,null);
});

cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6 = (function (n,to,xf,from,close_QMARK_,ex_handler){
return cljs.core.async.pipeline_STAR_.call(null,n,to,xf,from,close_QMARK_,ex_handler,new cljs.core.Keyword(null,"compute","compute",1555393130));
});

cljs.core.async.pipeline.cljs$lang$maxFixedArity = 6;

/**
 * Takes a predicate and a source channel and returns a vector of two
 *   channels, the first of which will contain the values for which the
 *   predicate returned true, the second those for which it returned
 *   false.
 * 
 *   The out channels will be unbuffered by default, or two buf-or-ns can
 *   be supplied. The channels will close after the source channel has
 *   closed.
 */
cljs.core.async.split = (function cljs$core$async$split(var_args){
var G__41757 = arguments.length;
switch (G__41757) {
case 2:
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 4:
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.split.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.split.call(null,p,ch,null,null);
});

cljs.core.async.split.cljs$core$IFn$_invoke$arity$4 = (function (p,ch,t_buf_or_n,f_buf_or_n){
var tc = cljs.core.async.chan.call(null,t_buf_or_n);
var fc = cljs.core.async.chan.call(null,f_buf_or_n);
var c__35058__auto___41806 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__35058__auto___41806,tc,fc){
return (function (){
var f__35059__auto__ = (function (){var switch__34968__auto__ = ((function (c__35058__auto___41806,tc,fc){
return (function (state_41783){
var state_val_41784 = (state_41783[(1)]);
if((state_val_41784 === (7))){
var inst_41779 = (state_41783[(2)]);
var state_41783__$1 = state_41783;
var statearr_41785_41807 = state_41783__$1;
(statearr_41785_41807[(2)] = inst_41779);

(statearr_41785_41807[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41784 === (1))){
var state_41783__$1 = state_41783;
var statearr_41786_41808 = state_41783__$1;
(statearr_41786_41808[(2)] = null);

(statearr_41786_41808[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41784 === (4))){
var inst_41760 = (state_41783[(7)]);
var inst_41760__$1 = (state_41783[(2)]);
var inst_41761 = (inst_41760__$1 == null);
var state_41783__$1 = (function (){var statearr_41787 = state_41783;
(statearr_41787[(7)] = inst_41760__$1);

return statearr_41787;
})();
if(cljs.core.truth_(inst_41761)){
var statearr_41788_41809 = state_41783__$1;
(statearr_41788_41809[(1)] = (5));

} else {
var statearr_41789_41810 = state_41783__$1;
(statearr_41789_41810[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41784 === (13))){
var state_41783__$1 = state_41783;
var statearr_41790_41811 = state_41783__$1;
(statearr_41790_41811[(2)] = null);

(statearr_41790_41811[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41784 === (6))){
var inst_41760 = (state_41783[(7)]);
var inst_41766 = p.call(null,inst_41760);
var state_41783__$1 = state_41783;
if(cljs.core.truth_(inst_41766)){
var statearr_41791_41812 = state_41783__$1;
(statearr_41791_41812[(1)] = (9));

} else {
var statearr_41792_41813 = state_41783__$1;
(statearr_41792_41813[(1)] = (10));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41784 === (3))){
var inst_41781 = (state_41783[(2)]);
var state_41783__$1 = state_41783;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_41783__$1,inst_41781);
} else {
if((state_val_41784 === (12))){
var state_41783__$1 = state_41783;
var statearr_41793_41814 = state_41783__$1;
(statearr_41793_41814[(2)] = null);

(statearr_41793_41814[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41784 === (2))){
var state_41783__$1 = state_41783;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_41783__$1,(4),ch);
} else {
if((state_val_41784 === (11))){
var inst_41760 = (state_41783[(7)]);
var inst_41770 = (state_41783[(2)]);
var state_41783__$1 = state_41783;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_41783__$1,(8),inst_41770,inst_41760);
} else {
if((state_val_41784 === (9))){
var state_41783__$1 = state_41783;
var statearr_41794_41815 = state_41783__$1;
(statearr_41794_41815[(2)] = tc);

(statearr_41794_41815[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41784 === (5))){
var inst_41763 = cljs.core.async.close_BANG_.call(null,tc);
var inst_41764 = cljs.core.async.close_BANG_.call(null,fc);
var state_41783__$1 = (function (){var statearr_41795 = state_41783;
(statearr_41795[(8)] = inst_41763);

return statearr_41795;
})();
var statearr_41796_41816 = state_41783__$1;
(statearr_41796_41816[(2)] = inst_41764);

(statearr_41796_41816[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41784 === (14))){
var inst_41777 = (state_41783[(2)]);
var state_41783__$1 = state_41783;
var statearr_41797_41817 = state_41783__$1;
(statearr_41797_41817[(2)] = inst_41777);

(statearr_41797_41817[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41784 === (10))){
var state_41783__$1 = state_41783;
var statearr_41798_41818 = state_41783__$1;
(statearr_41798_41818[(2)] = fc);

(statearr_41798_41818[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41784 === (8))){
var inst_41772 = (state_41783[(2)]);
var state_41783__$1 = state_41783;
if(cljs.core.truth_(inst_41772)){
var statearr_41799_41819 = state_41783__$1;
(statearr_41799_41819[(1)] = (12));

} else {
var statearr_41800_41820 = state_41783__$1;
(statearr_41800_41820[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__35058__auto___41806,tc,fc))
;
return ((function (switch__34968__auto__,c__35058__auto___41806,tc,fc){
return (function() {
var cljs$core$async$state_machine__34969__auto__ = null;
var cljs$core$async$state_machine__34969__auto____0 = (function (){
var statearr_41801 = [null,null,null,null,null,null,null,null,null];
(statearr_41801[(0)] = cljs$core$async$state_machine__34969__auto__);

(statearr_41801[(1)] = (1));

return statearr_41801;
});
var cljs$core$async$state_machine__34969__auto____1 = (function (state_41783){
while(true){
var ret_value__34970__auto__ = (function (){try{while(true){
var result__34971__auto__ = switch__34968__auto__.call(null,state_41783);
if(cljs.core.keyword_identical_QMARK_.call(null,result__34971__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__34971__auto__;
}
break;
}
}catch (e41802){if((e41802 instanceof Object)){
var ex__34972__auto__ = e41802;
var statearr_41803_41821 = state_41783;
(statearr_41803_41821[(5)] = ex__34972__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_41783);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e41802;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__34970__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__41822 = state_41783;
state_41783 = G__41822;
continue;
} else {
return ret_value__34970__auto__;
}
break;
}
});
cljs$core$async$state_machine__34969__auto__ = function(state_41783){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__34969__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__34969__auto____1.call(this,state_41783);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__34969__auto____0;
cljs$core$async$state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__34969__auto____1;
return cljs$core$async$state_machine__34969__auto__;
})()
;})(switch__34968__auto__,c__35058__auto___41806,tc,fc))
})();
var state__35060__auto__ = (function (){var statearr_41804 = f__35059__auto__.call(null);
(statearr_41804[(6)] = c__35058__auto___41806);

return statearr_41804;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__35060__auto__);
});})(c__35058__auto___41806,tc,fc))
);


return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [tc,fc], null);
});

cljs.core.async.split.cljs$lang$maxFixedArity = 4;

/**
 * f should be a function of 2 arguments. Returns a channel containing
 *   the single result of applying f to init and the first item from the
 *   channel, then applying f to that result and the 2nd item, etc. If
 *   the channel closes without yielding items, returns init and f is not
 *   called. ch must close before reduce produces a result.
 */
cljs.core.async.reduce = (function cljs$core$async$reduce(f,init,ch){
var c__35058__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__35058__auto__){
return (function (){
var f__35059__auto__ = (function (){var switch__34968__auto__ = ((function (c__35058__auto__){
return (function (state_41843){
var state_val_41844 = (state_41843[(1)]);
if((state_val_41844 === (7))){
var inst_41839 = (state_41843[(2)]);
var state_41843__$1 = state_41843;
var statearr_41845_41863 = state_41843__$1;
(statearr_41845_41863[(2)] = inst_41839);

(statearr_41845_41863[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41844 === (1))){
var inst_41823 = init;
var state_41843__$1 = (function (){var statearr_41846 = state_41843;
(statearr_41846[(7)] = inst_41823);

return statearr_41846;
})();
var statearr_41847_41864 = state_41843__$1;
(statearr_41847_41864[(2)] = null);

(statearr_41847_41864[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41844 === (4))){
var inst_41826 = (state_41843[(8)]);
var inst_41826__$1 = (state_41843[(2)]);
var inst_41827 = (inst_41826__$1 == null);
var state_41843__$1 = (function (){var statearr_41848 = state_41843;
(statearr_41848[(8)] = inst_41826__$1);

return statearr_41848;
})();
if(cljs.core.truth_(inst_41827)){
var statearr_41849_41865 = state_41843__$1;
(statearr_41849_41865[(1)] = (5));

} else {
var statearr_41850_41866 = state_41843__$1;
(statearr_41850_41866[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41844 === (6))){
var inst_41830 = (state_41843[(9)]);
var inst_41823 = (state_41843[(7)]);
var inst_41826 = (state_41843[(8)]);
var inst_41830__$1 = f.call(null,inst_41823,inst_41826);
var inst_41831 = cljs.core.reduced_QMARK_.call(null,inst_41830__$1);
var state_41843__$1 = (function (){var statearr_41851 = state_41843;
(statearr_41851[(9)] = inst_41830__$1);

return statearr_41851;
})();
if(inst_41831){
var statearr_41852_41867 = state_41843__$1;
(statearr_41852_41867[(1)] = (8));

} else {
var statearr_41853_41868 = state_41843__$1;
(statearr_41853_41868[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41844 === (3))){
var inst_41841 = (state_41843[(2)]);
var state_41843__$1 = state_41843;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_41843__$1,inst_41841);
} else {
if((state_val_41844 === (2))){
var state_41843__$1 = state_41843;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_41843__$1,(4),ch);
} else {
if((state_val_41844 === (9))){
var inst_41830 = (state_41843[(9)]);
var inst_41823 = inst_41830;
var state_41843__$1 = (function (){var statearr_41854 = state_41843;
(statearr_41854[(7)] = inst_41823);

return statearr_41854;
})();
var statearr_41855_41869 = state_41843__$1;
(statearr_41855_41869[(2)] = null);

(statearr_41855_41869[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41844 === (5))){
var inst_41823 = (state_41843[(7)]);
var state_41843__$1 = state_41843;
var statearr_41856_41870 = state_41843__$1;
(statearr_41856_41870[(2)] = inst_41823);

(statearr_41856_41870[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41844 === (10))){
var inst_41837 = (state_41843[(2)]);
var state_41843__$1 = state_41843;
var statearr_41857_41871 = state_41843__$1;
(statearr_41857_41871[(2)] = inst_41837);

(statearr_41857_41871[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41844 === (8))){
var inst_41830 = (state_41843[(9)]);
var inst_41833 = cljs.core.deref.call(null,inst_41830);
var state_41843__$1 = state_41843;
var statearr_41858_41872 = state_41843__$1;
(statearr_41858_41872[(2)] = inst_41833);

(statearr_41858_41872[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
});})(c__35058__auto__))
;
return ((function (switch__34968__auto__,c__35058__auto__){
return (function() {
var cljs$core$async$reduce_$_state_machine__34969__auto__ = null;
var cljs$core$async$reduce_$_state_machine__34969__auto____0 = (function (){
var statearr_41859 = [null,null,null,null,null,null,null,null,null,null];
(statearr_41859[(0)] = cljs$core$async$reduce_$_state_machine__34969__auto__);

(statearr_41859[(1)] = (1));

return statearr_41859;
});
var cljs$core$async$reduce_$_state_machine__34969__auto____1 = (function (state_41843){
while(true){
var ret_value__34970__auto__ = (function (){try{while(true){
var result__34971__auto__ = switch__34968__auto__.call(null,state_41843);
if(cljs.core.keyword_identical_QMARK_.call(null,result__34971__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__34971__auto__;
}
break;
}
}catch (e41860){if((e41860 instanceof Object)){
var ex__34972__auto__ = e41860;
var statearr_41861_41873 = state_41843;
(statearr_41861_41873[(5)] = ex__34972__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_41843);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e41860;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__34970__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__41874 = state_41843;
state_41843 = G__41874;
continue;
} else {
return ret_value__34970__auto__;
}
break;
}
});
cljs$core$async$reduce_$_state_machine__34969__auto__ = function(state_41843){
switch(arguments.length){
case 0:
return cljs$core$async$reduce_$_state_machine__34969__auto____0.call(this);
case 1:
return cljs$core$async$reduce_$_state_machine__34969__auto____1.call(this,state_41843);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$reduce_$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$reduce_$_state_machine__34969__auto____0;
cljs$core$async$reduce_$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$reduce_$_state_machine__34969__auto____1;
return cljs$core$async$reduce_$_state_machine__34969__auto__;
})()
;})(switch__34968__auto__,c__35058__auto__))
})();
var state__35060__auto__ = (function (){var statearr_41862 = f__35059__auto__.call(null);
(statearr_41862[(6)] = c__35058__auto__);

return statearr_41862;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__35060__auto__);
});})(c__35058__auto__))
);

return c__35058__auto__;
});
/**
 * async/reduces a channel with a transformation (xform f).
 *   Returns a channel containing the result.  ch must close before
 *   transduce produces a result.
 */
cljs.core.async.transduce = (function cljs$core$async$transduce(xform,f,init,ch){
var f__$1 = xform.call(null,f);
var c__35058__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__35058__auto__,f__$1){
return (function (){
var f__35059__auto__ = (function (){var switch__34968__auto__ = ((function (c__35058__auto__,f__$1){
return (function (state_41880){
var state_val_41881 = (state_41880[(1)]);
if((state_val_41881 === (1))){
var inst_41875 = cljs.core.async.reduce.call(null,f__$1,init,ch);
var state_41880__$1 = state_41880;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_41880__$1,(2),inst_41875);
} else {
if((state_val_41881 === (2))){
var inst_41877 = (state_41880[(2)]);
var inst_41878 = f__$1.call(null,inst_41877);
var state_41880__$1 = state_41880;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_41880__$1,inst_41878);
} else {
return null;
}
}
});})(c__35058__auto__,f__$1))
;
return ((function (switch__34968__auto__,c__35058__auto__,f__$1){
return (function() {
var cljs$core$async$transduce_$_state_machine__34969__auto__ = null;
var cljs$core$async$transduce_$_state_machine__34969__auto____0 = (function (){
var statearr_41882 = [null,null,null,null,null,null,null];
(statearr_41882[(0)] = cljs$core$async$transduce_$_state_machine__34969__auto__);

(statearr_41882[(1)] = (1));

return statearr_41882;
});
var cljs$core$async$transduce_$_state_machine__34969__auto____1 = (function (state_41880){
while(true){
var ret_value__34970__auto__ = (function (){try{while(true){
var result__34971__auto__ = switch__34968__auto__.call(null,state_41880);
if(cljs.core.keyword_identical_QMARK_.call(null,result__34971__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__34971__auto__;
}
break;
}
}catch (e41883){if((e41883 instanceof Object)){
var ex__34972__auto__ = e41883;
var statearr_41884_41886 = state_41880;
(statearr_41884_41886[(5)] = ex__34972__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_41880);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e41883;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__34970__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__41887 = state_41880;
state_41880 = G__41887;
continue;
} else {
return ret_value__34970__auto__;
}
break;
}
});
cljs$core$async$transduce_$_state_machine__34969__auto__ = function(state_41880){
switch(arguments.length){
case 0:
return cljs$core$async$transduce_$_state_machine__34969__auto____0.call(this);
case 1:
return cljs$core$async$transduce_$_state_machine__34969__auto____1.call(this,state_41880);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$transduce_$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$transduce_$_state_machine__34969__auto____0;
cljs$core$async$transduce_$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$transduce_$_state_machine__34969__auto____1;
return cljs$core$async$transduce_$_state_machine__34969__auto__;
})()
;})(switch__34968__auto__,c__35058__auto__,f__$1))
})();
var state__35060__auto__ = (function (){var statearr_41885 = f__35059__auto__.call(null);
(statearr_41885[(6)] = c__35058__auto__);

return statearr_41885;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__35060__auto__);
});})(c__35058__auto__,f__$1))
);

return c__35058__auto__;
});
/**
 * Puts the contents of coll into the supplied channel.
 * 
 *   By default the channel will be closed after the items are copied,
 *   but can be determined by the close? parameter.
 * 
 *   Returns a channel which will close after the items are copied.
 */
cljs.core.async.onto_chan = (function cljs$core$async$onto_chan(var_args){
var G__41889 = arguments.length;
switch (G__41889) {
case 2:
return cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$2 = (function (ch,coll){
return cljs.core.async.onto_chan.call(null,ch,coll,true);
});

cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$3 = (function (ch,coll,close_QMARK_){
var c__35058__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__35058__auto__){
return (function (){
var f__35059__auto__ = (function (){var switch__34968__auto__ = ((function (c__35058__auto__){
return (function (state_41914){
var state_val_41915 = (state_41914[(1)]);
if((state_val_41915 === (7))){
var inst_41896 = (state_41914[(2)]);
var state_41914__$1 = state_41914;
var statearr_41916_41937 = state_41914__$1;
(statearr_41916_41937[(2)] = inst_41896);

(statearr_41916_41937[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41915 === (1))){
var inst_41890 = cljs.core.seq.call(null,coll);
var inst_41891 = inst_41890;
var state_41914__$1 = (function (){var statearr_41917 = state_41914;
(statearr_41917[(7)] = inst_41891);

return statearr_41917;
})();
var statearr_41918_41938 = state_41914__$1;
(statearr_41918_41938[(2)] = null);

(statearr_41918_41938[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41915 === (4))){
var inst_41891 = (state_41914[(7)]);
var inst_41894 = cljs.core.first.call(null,inst_41891);
var state_41914__$1 = state_41914;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_41914__$1,(7),ch,inst_41894);
} else {
if((state_val_41915 === (13))){
var inst_41908 = (state_41914[(2)]);
var state_41914__$1 = state_41914;
var statearr_41919_41939 = state_41914__$1;
(statearr_41919_41939[(2)] = inst_41908);

(statearr_41919_41939[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41915 === (6))){
var inst_41899 = (state_41914[(2)]);
var state_41914__$1 = state_41914;
if(cljs.core.truth_(inst_41899)){
var statearr_41920_41940 = state_41914__$1;
(statearr_41920_41940[(1)] = (8));

} else {
var statearr_41921_41941 = state_41914__$1;
(statearr_41921_41941[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41915 === (3))){
var inst_41912 = (state_41914[(2)]);
var state_41914__$1 = state_41914;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_41914__$1,inst_41912);
} else {
if((state_val_41915 === (12))){
var state_41914__$1 = state_41914;
var statearr_41922_41942 = state_41914__$1;
(statearr_41922_41942[(2)] = null);

(statearr_41922_41942[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41915 === (2))){
var inst_41891 = (state_41914[(7)]);
var state_41914__$1 = state_41914;
if(cljs.core.truth_(inst_41891)){
var statearr_41923_41943 = state_41914__$1;
(statearr_41923_41943[(1)] = (4));

} else {
var statearr_41924_41944 = state_41914__$1;
(statearr_41924_41944[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41915 === (11))){
var inst_41905 = cljs.core.async.close_BANG_.call(null,ch);
var state_41914__$1 = state_41914;
var statearr_41925_41945 = state_41914__$1;
(statearr_41925_41945[(2)] = inst_41905);

(statearr_41925_41945[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41915 === (9))){
var state_41914__$1 = state_41914;
if(cljs.core.truth_(close_QMARK_)){
var statearr_41926_41946 = state_41914__$1;
(statearr_41926_41946[(1)] = (11));

} else {
var statearr_41927_41947 = state_41914__$1;
(statearr_41927_41947[(1)] = (12));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41915 === (5))){
var inst_41891 = (state_41914[(7)]);
var state_41914__$1 = state_41914;
var statearr_41928_41948 = state_41914__$1;
(statearr_41928_41948[(2)] = inst_41891);

(statearr_41928_41948[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41915 === (10))){
var inst_41910 = (state_41914[(2)]);
var state_41914__$1 = state_41914;
var statearr_41929_41949 = state_41914__$1;
(statearr_41929_41949[(2)] = inst_41910);

(statearr_41929_41949[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_41915 === (8))){
var inst_41891 = (state_41914[(7)]);
var inst_41901 = cljs.core.next.call(null,inst_41891);
var inst_41891__$1 = inst_41901;
var state_41914__$1 = (function (){var statearr_41930 = state_41914;
(statearr_41930[(7)] = inst_41891__$1);

return statearr_41930;
})();
var statearr_41931_41950 = state_41914__$1;
(statearr_41931_41950[(2)] = null);

(statearr_41931_41950[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__35058__auto__))
;
return ((function (switch__34968__auto__,c__35058__auto__){
return (function() {
var cljs$core$async$state_machine__34969__auto__ = null;
var cljs$core$async$state_machine__34969__auto____0 = (function (){
var statearr_41932 = [null,null,null,null,null,null,null,null];
(statearr_41932[(0)] = cljs$core$async$state_machine__34969__auto__);

(statearr_41932[(1)] = (1));

return statearr_41932;
});
var cljs$core$async$state_machine__34969__auto____1 = (function (state_41914){
while(true){
var ret_value__34970__auto__ = (function (){try{while(true){
var result__34971__auto__ = switch__34968__auto__.call(null,state_41914);
if(cljs.core.keyword_identical_QMARK_.call(null,result__34971__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__34971__auto__;
}
break;
}
}catch (e41933){if((e41933 instanceof Object)){
var ex__34972__auto__ = e41933;
var statearr_41934_41951 = state_41914;
(statearr_41934_41951[(5)] = ex__34972__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_41914);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e41933;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__34970__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__41952 = state_41914;
state_41914 = G__41952;
continue;
} else {
return ret_value__34970__auto__;
}
break;
}
});
cljs$core$async$state_machine__34969__auto__ = function(state_41914){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__34969__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__34969__auto____1.call(this,state_41914);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__34969__auto____0;
cljs$core$async$state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__34969__auto____1;
return cljs$core$async$state_machine__34969__auto__;
})()
;})(switch__34968__auto__,c__35058__auto__))
})();
var state__35060__auto__ = (function (){var statearr_41935 = f__35059__auto__.call(null);
(statearr_41935[(6)] = c__35058__auto__);

return statearr_41935;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__35060__auto__);
});})(c__35058__auto__))
);

return c__35058__auto__;
});

cljs.core.async.onto_chan.cljs$lang$maxFixedArity = 3;

/**
 * Creates and returns a channel which contains the contents of coll,
 *   closing when exhausted.
 */
cljs.core.async.to_chan = (function cljs$core$async$to_chan(coll){
var ch = cljs.core.async.chan.call(null,cljs.core.bounded_count.call(null,(100),coll));
cljs.core.async.onto_chan.call(null,ch,coll);

return ch;
});

/**
 * @interface
 */
cljs.core.async.Mux = function(){};

cljs.core.async.muxch_STAR_ = (function cljs$core$async$muxch_STAR_(_){
if((!((_ == null))) && (!((_.cljs$core$async$Mux$muxch_STAR_$arity$1 == null)))){
return _.cljs$core$async$Mux$muxch_STAR_$arity$1(_);
} else {
var x__30915__auto__ = (((_ == null))?null:_);
var m__30916__auto__ = (cljs.core.async.muxch_STAR_[goog.typeOf(x__30915__auto__)]);
if(!((m__30916__auto__ == null))){
return m__30916__auto__.call(null,_);
} else {
var m__30916__auto____$1 = (cljs.core.async.muxch_STAR_["_"]);
if(!((m__30916__auto____$1 == null))){
return m__30916__auto____$1.call(null,_);
} else {
throw cljs.core.missing_protocol.call(null,"Mux.muxch*",_);
}
}
}
});


/**
 * @interface
 */
cljs.core.async.Mult = function(){};

cljs.core.async.tap_STAR_ = (function cljs$core$async$tap_STAR_(m,ch,close_QMARK_){
if((!((m == null))) && (!((m.cljs$core$async$Mult$tap_STAR_$arity$3 == null)))){
return m.cljs$core$async$Mult$tap_STAR_$arity$3(m,ch,close_QMARK_);
} else {
var x__30915__auto__ = (((m == null))?null:m);
var m__30916__auto__ = (cljs.core.async.tap_STAR_[goog.typeOf(x__30915__auto__)]);
if(!((m__30916__auto__ == null))){
return m__30916__auto__.call(null,m,ch,close_QMARK_);
} else {
var m__30916__auto____$1 = (cljs.core.async.tap_STAR_["_"]);
if(!((m__30916__auto____$1 == null))){
return m__30916__auto____$1.call(null,m,ch,close_QMARK_);
} else {
throw cljs.core.missing_protocol.call(null,"Mult.tap*",m);
}
}
}
});

cljs.core.async.untap_STAR_ = (function cljs$core$async$untap_STAR_(m,ch){
if((!((m == null))) && (!((m.cljs$core$async$Mult$untap_STAR_$arity$2 == null)))){
return m.cljs$core$async$Mult$untap_STAR_$arity$2(m,ch);
} else {
var x__30915__auto__ = (((m == null))?null:m);
var m__30916__auto__ = (cljs.core.async.untap_STAR_[goog.typeOf(x__30915__auto__)]);
if(!((m__30916__auto__ == null))){
return m__30916__auto__.call(null,m,ch);
} else {
var m__30916__auto____$1 = (cljs.core.async.untap_STAR_["_"]);
if(!((m__30916__auto____$1 == null))){
return m__30916__auto____$1.call(null,m,ch);
} else {
throw cljs.core.missing_protocol.call(null,"Mult.untap*",m);
}
}
}
});

cljs.core.async.untap_all_STAR_ = (function cljs$core$async$untap_all_STAR_(m){
if((!((m == null))) && (!((m.cljs$core$async$Mult$untap_all_STAR_$arity$1 == null)))){
return m.cljs$core$async$Mult$untap_all_STAR_$arity$1(m);
} else {
var x__30915__auto__ = (((m == null))?null:m);
var m__30916__auto__ = (cljs.core.async.untap_all_STAR_[goog.typeOf(x__30915__auto__)]);
if(!((m__30916__auto__ == null))){
return m__30916__auto__.call(null,m);
} else {
var m__30916__auto____$1 = (cljs.core.async.untap_all_STAR_["_"]);
if(!((m__30916__auto____$1 == null))){
return m__30916__auto____$1.call(null,m);
} else {
throw cljs.core.missing_protocol.call(null,"Mult.untap-all*",m);
}
}
}
});

/**
 * Creates and returns a mult(iple) of the supplied channel. Channels
 *   containing copies of the channel can be created with 'tap', and
 *   detached with 'untap'.
 * 
 *   Each item is distributed to all taps in parallel and synchronously,
 *   i.e. each tap must accept before the next item is distributed. Use
 *   buffering/windowing to prevent slow taps from holding up the mult.
 * 
 *   Items received when there are no taps get dropped.
 * 
 *   If a tap puts to a closed channel, it will be removed from the mult.
 */
cljs.core.async.mult = (function cljs$core$async$mult(ch){
var cs = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var m = (function (){
if(typeof cljs.core.async.t_cljs$core$async41953 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.Mult}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async41953 = (function (ch,cs,meta41954){
this.ch = ch;
this.cs = cs;
this.meta41954 = meta41954;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async41953.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (cs){
return (function (_41955,meta41954__$1){
var self__ = this;
var _41955__$1 = this;
return (new cljs.core.async.t_cljs$core$async41953(self__.ch,self__.cs,meta41954__$1));
});})(cs))
;

cljs.core.async.t_cljs$core$async41953.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (cs){
return (function (_41955){
var self__ = this;
var _41955__$1 = this;
return self__.meta41954;
});})(cs))
;

cljs.core.async.t_cljs$core$async41953.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async41953.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = ((function (cs){
return (function (_){
var self__ = this;
var ___$1 = this;
return self__.ch;
});})(cs))
;

cljs.core.async.t_cljs$core$async41953.prototype.cljs$core$async$Mult$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async41953.prototype.cljs$core$async$Mult$tap_STAR_$arity$3 = ((function (cs){
return (function (_,ch__$1,close_QMARK_){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.assoc,ch__$1,close_QMARK_);

return null;
});})(cs))
;

cljs.core.async.t_cljs$core$async41953.prototype.cljs$core$async$Mult$untap_STAR_$arity$2 = ((function (cs){
return (function (_,ch__$1){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.dissoc,ch__$1);

return null;
});})(cs))
;

cljs.core.async.t_cljs$core$async41953.prototype.cljs$core$async$Mult$untap_all_STAR_$arity$1 = ((function (cs){
return (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_.call(null,self__.cs,cljs.core.PersistentArrayMap.EMPTY);

return null;
});})(cs))
;

cljs.core.async.t_cljs$core$async41953.getBasis = ((function (cs){
return (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"cs","cs",-117024463,null),new cljs.core.Symbol(null,"meta41954","meta41954",-741097798,null)], null);
});})(cs))
;

cljs.core.async.t_cljs$core$async41953.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async41953.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async41953";

cljs.core.async.t_cljs$core$async41953.cljs$lang$ctorPrWriter = ((function (cs){
return (function (this__30853__auto__,writer__30854__auto__,opt__30855__auto__){
return cljs.core._write.call(null,writer__30854__auto__,"cljs.core.async/t_cljs$core$async41953");
});})(cs))
;

cljs.core.async.__GT_t_cljs$core$async41953 = ((function (cs){
return (function cljs$core$async$mult_$___GT_t_cljs$core$async41953(ch__$1,cs__$1,meta41954){
return (new cljs.core.async.t_cljs$core$async41953(ch__$1,cs__$1,meta41954));
});})(cs))
;

}

return (new cljs.core.async.t_cljs$core$async41953(ch,cs,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var dchan = cljs.core.async.chan.call(null,(1));
var dctr = cljs.core.atom.call(null,null);
var done = ((function (cs,m,dchan,dctr){
return (function (_){
if((cljs.core.swap_BANG_.call(null,dctr,cljs.core.dec) === (0))){
return cljs.core.async.put_BANG_.call(null,dchan,true);
} else {
return null;
}
});})(cs,m,dchan,dctr))
;
var c__35058__auto___42175 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__35058__auto___42175,cs,m,dchan,dctr,done){
return (function (){
var f__35059__auto__ = (function (){var switch__34968__auto__ = ((function (c__35058__auto___42175,cs,m,dchan,dctr,done){
return (function (state_42090){
var state_val_42091 = (state_42090[(1)]);
if((state_val_42091 === (7))){
var inst_42086 = (state_42090[(2)]);
var state_42090__$1 = state_42090;
var statearr_42092_42176 = state_42090__$1;
(statearr_42092_42176[(2)] = inst_42086);

(statearr_42092_42176[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42091 === (20))){
var inst_41989 = (state_42090[(7)]);
var inst_42001 = cljs.core.first.call(null,inst_41989);
var inst_42002 = cljs.core.nth.call(null,inst_42001,(0),null);
var inst_42003 = cljs.core.nth.call(null,inst_42001,(1),null);
var state_42090__$1 = (function (){var statearr_42093 = state_42090;
(statearr_42093[(8)] = inst_42002);

return statearr_42093;
})();
if(cljs.core.truth_(inst_42003)){
var statearr_42094_42177 = state_42090__$1;
(statearr_42094_42177[(1)] = (22));

} else {
var statearr_42095_42178 = state_42090__$1;
(statearr_42095_42178[(1)] = (23));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42091 === (27))){
var inst_41958 = (state_42090[(9)]);
var inst_42033 = (state_42090[(10)]);
var inst_42038 = (state_42090[(11)]);
var inst_42031 = (state_42090[(12)]);
var inst_42038__$1 = cljs.core._nth.call(null,inst_42031,inst_42033);
var inst_42039 = cljs.core.async.put_BANG_.call(null,inst_42038__$1,inst_41958,done);
var state_42090__$1 = (function (){var statearr_42096 = state_42090;
(statearr_42096[(11)] = inst_42038__$1);

return statearr_42096;
})();
if(cljs.core.truth_(inst_42039)){
var statearr_42097_42179 = state_42090__$1;
(statearr_42097_42179[(1)] = (30));

} else {
var statearr_42098_42180 = state_42090__$1;
(statearr_42098_42180[(1)] = (31));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42091 === (1))){
var state_42090__$1 = state_42090;
var statearr_42099_42181 = state_42090__$1;
(statearr_42099_42181[(2)] = null);

(statearr_42099_42181[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42091 === (24))){
var inst_41989 = (state_42090[(7)]);
var inst_42008 = (state_42090[(2)]);
var inst_42009 = cljs.core.next.call(null,inst_41989);
var inst_41967 = inst_42009;
var inst_41968 = null;
var inst_41969 = (0);
var inst_41970 = (0);
var state_42090__$1 = (function (){var statearr_42100 = state_42090;
(statearr_42100[(13)] = inst_41969);

(statearr_42100[(14)] = inst_41970);

(statearr_42100[(15)] = inst_42008);

(statearr_42100[(16)] = inst_41968);

(statearr_42100[(17)] = inst_41967);

return statearr_42100;
})();
var statearr_42101_42182 = state_42090__$1;
(statearr_42101_42182[(2)] = null);

(statearr_42101_42182[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42091 === (39))){
var state_42090__$1 = state_42090;
var statearr_42105_42183 = state_42090__$1;
(statearr_42105_42183[(2)] = null);

(statearr_42105_42183[(1)] = (41));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42091 === (4))){
var inst_41958 = (state_42090[(9)]);
var inst_41958__$1 = (state_42090[(2)]);
var inst_41959 = (inst_41958__$1 == null);
var state_42090__$1 = (function (){var statearr_42106 = state_42090;
(statearr_42106[(9)] = inst_41958__$1);

return statearr_42106;
})();
if(cljs.core.truth_(inst_41959)){
var statearr_42107_42184 = state_42090__$1;
(statearr_42107_42184[(1)] = (5));

} else {
var statearr_42108_42185 = state_42090__$1;
(statearr_42108_42185[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42091 === (15))){
var inst_41969 = (state_42090[(13)]);
var inst_41970 = (state_42090[(14)]);
var inst_41968 = (state_42090[(16)]);
var inst_41967 = (state_42090[(17)]);
var inst_41985 = (state_42090[(2)]);
var inst_41986 = (inst_41970 + (1));
var tmp42102 = inst_41969;
var tmp42103 = inst_41968;
var tmp42104 = inst_41967;
var inst_41967__$1 = tmp42104;
var inst_41968__$1 = tmp42103;
var inst_41969__$1 = tmp42102;
var inst_41970__$1 = inst_41986;
var state_42090__$1 = (function (){var statearr_42109 = state_42090;
(statearr_42109[(13)] = inst_41969__$1);

(statearr_42109[(14)] = inst_41970__$1);

(statearr_42109[(16)] = inst_41968__$1);

(statearr_42109[(18)] = inst_41985);

(statearr_42109[(17)] = inst_41967__$1);

return statearr_42109;
})();
var statearr_42110_42186 = state_42090__$1;
(statearr_42110_42186[(2)] = null);

(statearr_42110_42186[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42091 === (21))){
var inst_42012 = (state_42090[(2)]);
var state_42090__$1 = state_42090;
var statearr_42114_42187 = state_42090__$1;
(statearr_42114_42187[(2)] = inst_42012);

(statearr_42114_42187[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42091 === (31))){
var inst_42038 = (state_42090[(11)]);
var inst_42042 = done.call(null,null);
var inst_42043 = cljs.core.async.untap_STAR_.call(null,m,inst_42038);
var state_42090__$1 = (function (){var statearr_42115 = state_42090;
(statearr_42115[(19)] = inst_42042);

return statearr_42115;
})();
var statearr_42116_42188 = state_42090__$1;
(statearr_42116_42188[(2)] = inst_42043);

(statearr_42116_42188[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42091 === (32))){
var inst_42032 = (state_42090[(20)]);
var inst_42030 = (state_42090[(21)]);
var inst_42033 = (state_42090[(10)]);
var inst_42031 = (state_42090[(12)]);
var inst_42045 = (state_42090[(2)]);
var inst_42046 = (inst_42033 + (1));
var tmp42111 = inst_42032;
var tmp42112 = inst_42030;
var tmp42113 = inst_42031;
var inst_42030__$1 = tmp42112;
var inst_42031__$1 = tmp42113;
var inst_42032__$1 = tmp42111;
var inst_42033__$1 = inst_42046;
var state_42090__$1 = (function (){var statearr_42117 = state_42090;
(statearr_42117[(22)] = inst_42045);

(statearr_42117[(20)] = inst_42032__$1);

(statearr_42117[(21)] = inst_42030__$1);

(statearr_42117[(10)] = inst_42033__$1);

(statearr_42117[(12)] = inst_42031__$1);

return statearr_42117;
})();
var statearr_42118_42189 = state_42090__$1;
(statearr_42118_42189[(2)] = null);

(statearr_42118_42189[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42091 === (40))){
var inst_42058 = (state_42090[(23)]);
var inst_42062 = done.call(null,null);
var inst_42063 = cljs.core.async.untap_STAR_.call(null,m,inst_42058);
var state_42090__$1 = (function (){var statearr_42119 = state_42090;
(statearr_42119[(24)] = inst_42062);

return statearr_42119;
})();
var statearr_42120_42190 = state_42090__$1;
(statearr_42120_42190[(2)] = inst_42063);

(statearr_42120_42190[(1)] = (41));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42091 === (33))){
var inst_42049 = (state_42090[(25)]);
var inst_42051 = cljs.core.chunked_seq_QMARK_.call(null,inst_42049);
var state_42090__$1 = state_42090;
if(inst_42051){
var statearr_42121_42191 = state_42090__$1;
(statearr_42121_42191[(1)] = (36));

} else {
var statearr_42122_42192 = state_42090__$1;
(statearr_42122_42192[(1)] = (37));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42091 === (13))){
var inst_41979 = (state_42090[(26)]);
var inst_41982 = cljs.core.async.close_BANG_.call(null,inst_41979);
var state_42090__$1 = state_42090;
var statearr_42123_42193 = state_42090__$1;
(statearr_42123_42193[(2)] = inst_41982);

(statearr_42123_42193[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42091 === (22))){
var inst_42002 = (state_42090[(8)]);
var inst_42005 = cljs.core.async.close_BANG_.call(null,inst_42002);
var state_42090__$1 = state_42090;
var statearr_42124_42194 = state_42090__$1;
(statearr_42124_42194[(2)] = inst_42005);

(statearr_42124_42194[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42091 === (36))){
var inst_42049 = (state_42090[(25)]);
var inst_42053 = cljs.core.chunk_first.call(null,inst_42049);
var inst_42054 = cljs.core.chunk_rest.call(null,inst_42049);
var inst_42055 = cljs.core.count.call(null,inst_42053);
var inst_42030 = inst_42054;
var inst_42031 = inst_42053;
var inst_42032 = inst_42055;
var inst_42033 = (0);
var state_42090__$1 = (function (){var statearr_42125 = state_42090;
(statearr_42125[(20)] = inst_42032);

(statearr_42125[(21)] = inst_42030);

(statearr_42125[(10)] = inst_42033);

(statearr_42125[(12)] = inst_42031);

return statearr_42125;
})();
var statearr_42126_42195 = state_42090__$1;
(statearr_42126_42195[(2)] = null);

(statearr_42126_42195[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42091 === (41))){
var inst_42049 = (state_42090[(25)]);
var inst_42065 = (state_42090[(2)]);
var inst_42066 = cljs.core.next.call(null,inst_42049);
var inst_42030 = inst_42066;
var inst_42031 = null;
var inst_42032 = (0);
var inst_42033 = (0);
var state_42090__$1 = (function (){var statearr_42127 = state_42090;
(statearr_42127[(20)] = inst_42032);

(statearr_42127[(21)] = inst_42030);

(statearr_42127[(10)] = inst_42033);

(statearr_42127[(27)] = inst_42065);

(statearr_42127[(12)] = inst_42031);

return statearr_42127;
})();
var statearr_42128_42196 = state_42090__$1;
(statearr_42128_42196[(2)] = null);

(statearr_42128_42196[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42091 === (43))){
var state_42090__$1 = state_42090;
var statearr_42129_42197 = state_42090__$1;
(statearr_42129_42197[(2)] = null);

(statearr_42129_42197[(1)] = (44));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42091 === (29))){
var inst_42074 = (state_42090[(2)]);
var state_42090__$1 = state_42090;
var statearr_42130_42198 = state_42090__$1;
(statearr_42130_42198[(2)] = inst_42074);

(statearr_42130_42198[(1)] = (26));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42091 === (44))){
var inst_42083 = (state_42090[(2)]);
var state_42090__$1 = (function (){var statearr_42131 = state_42090;
(statearr_42131[(28)] = inst_42083);

return statearr_42131;
})();
var statearr_42132_42199 = state_42090__$1;
(statearr_42132_42199[(2)] = null);

(statearr_42132_42199[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42091 === (6))){
var inst_42022 = (state_42090[(29)]);
var inst_42021 = cljs.core.deref.call(null,cs);
var inst_42022__$1 = cljs.core.keys.call(null,inst_42021);
var inst_42023 = cljs.core.count.call(null,inst_42022__$1);
var inst_42024 = cljs.core.reset_BANG_.call(null,dctr,inst_42023);
var inst_42029 = cljs.core.seq.call(null,inst_42022__$1);
var inst_42030 = inst_42029;
var inst_42031 = null;
var inst_42032 = (0);
var inst_42033 = (0);
var state_42090__$1 = (function (){var statearr_42133 = state_42090;
(statearr_42133[(20)] = inst_42032);

(statearr_42133[(29)] = inst_42022__$1);

(statearr_42133[(21)] = inst_42030);

(statearr_42133[(10)] = inst_42033);

(statearr_42133[(30)] = inst_42024);

(statearr_42133[(12)] = inst_42031);

return statearr_42133;
})();
var statearr_42134_42200 = state_42090__$1;
(statearr_42134_42200[(2)] = null);

(statearr_42134_42200[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42091 === (28))){
var inst_42030 = (state_42090[(21)]);
var inst_42049 = (state_42090[(25)]);
var inst_42049__$1 = cljs.core.seq.call(null,inst_42030);
var state_42090__$1 = (function (){var statearr_42135 = state_42090;
(statearr_42135[(25)] = inst_42049__$1);

return statearr_42135;
})();
if(inst_42049__$1){
var statearr_42136_42201 = state_42090__$1;
(statearr_42136_42201[(1)] = (33));

} else {
var statearr_42137_42202 = state_42090__$1;
(statearr_42137_42202[(1)] = (34));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42091 === (25))){
var inst_42032 = (state_42090[(20)]);
var inst_42033 = (state_42090[(10)]);
var inst_42035 = (inst_42033 < inst_42032);
var inst_42036 = inst_42035;
var state_42090__$1 = state_42090;
if(cljs.core.truth_(inst_42036)){
var statearr_42138_42203 = state_42090__$1;
(statearr_42138_42203[(1)] = (27));

} else {
var statearr_42139_42204 = state_42090__$1;
(statearr_42139_42204[(1)] = (28));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42091 === (34))){
var state_42090__$1 = state_42090;
var statearr_42140_42205 = state_42090__$1;
(statearr_42140_42205[(2)] = null);

(statearr_42140_42205[(1)] = (35));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42091 === (17))){
var state_42090__$1 = state_42090;
var statearr_42141_42206 = state_42090__$1;
(statearr_42141_42206[(2)] = null);

(statearr_42141_42206[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42091 === (3))){
var inst_42088 = (state_42090[(2)]);
var state_42090__$1 = state_42090;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_42090__$1,inst_42088);
} else {
if((state_val_42091 === (12))){
var inst_42017 = (state_42090[(2)]);
var state_42090__$1 = state_42090;
var statearr_42142_42207 = state_42090__$1;
(statearr_42142_42207[(2)] = inst_42017);

(statearr_42142_42207[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42091 === (2))){
var state_42090__$1 = state_42090;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_42090__$1,(4),ch);
} else {
if((state_val_42091 === (23))){
var state_42090__$1 = state_42090;
var statearr_42143_42208 = state_42090__$1;
(statearr_42143_42208[(2)] = null);

(statearr_42143_42208[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42091 === (35))){
var inst_42072 = (state_42090[(2)]);
var state_42090__$1 = state_42090;
var statearr_42144_42209 = state_42090__$1;
(statearr_42144_42209[(2)] = inst_42072);

(statearr_42144_42209[(1)] = (29));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42091 === (19))){
var inst_41989 = (state_42090[(7)]);
var inst_41993 = cljs.core.chunk_first.call(null,inst_41989);
var inst_41994 = cljs.core.chunk_rest.call(null,inst_41989);
var inst_41995 = cljs.core.count.call(null,inst_41993);
var inst_41967 = inst_41994;
var inst_41968 = inst_41993;
var inst_41969 = inst_41995;
var inst_41970 = (0);
var state_42090__$1 = (function (){var statearr_42145 = state_42090;
(statearr_42145[(13)] = inst_41969);

(statearr_42145[(14)] = inst_41970);

(statearr_42145[(16)] = inst_41968);

(statearr_42145[(17)] = inst_41967);

return statearr_42145;
})();
var statearr_42146_42210 = state_42090__$1;
(statearr_42146_42210[(2)] = null);

(statearr_42146_42210[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42091 === (11))){
var inst_41989 = (state_42090[(7)]);
var inst_41967 = (state_42090[(17)]);
var inst_41989__$1 = cljs.core.seq.call(null,inst_41967);
var state_42090__$1 = (function (){var statearr_42147 = state_42090;
(statearr_42147[(7)] = inst_41989__$1);

return statearr_42147;
})();
if(inst_41989__$1){
var statearr_42148_42211 = state_42090__$1;
(statearr_42148_42211[(1)] = (16));

} else {
var statearr_42149_42212 = state_42090__$1;
(statearr_42149_42212[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42091 === (9))){
var inst_42019 = (state_42090[(2)]);
var state_42090__$1 = state_42090;
var statearr_42150_42213 = state_42090__$1;
(statearr_42150_42213[(2)] = inst_42019);

(statearr_42150_42213[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42091 === (5))){
var inst_41965 = cljs.core.deref.call(null,cs);
var inst_41966 = cljs.core.seq.call(null,inst_41965);
var inst_41967 = inst_41966;
var inst_41968 = null;
var inst_41969 = (0);
var inst_41970 = (0);
var state_42090__$1 = (function (){var statearr_42151 = state_42090;
(statearr_42151[(13)] = inst_41969);

(statearr_42151[(14)] = inst_41970);

(statearr_42151[(16)] = inst_41968);

(statearr_42151[(17)] = inst_41967);

return statearr_42151;
})();
var statearr_42152_42214 = state_42090__$1;
(statearr_42152_42214[(2)] = null);

(statearr_42152_42214[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42091 === (14))){
var state_42090__$1 = state_42090;
var statearr_42153_42215 = state_42090__$1;
(statearr_42153_42215[(2)] = null);

(statearr_42153_42215[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42091 === (45))){
var inst_42080 = (state_42090[(2)]);
var state_42090__$1 = state_42090;
var statearr_42154_42216 = state_42090__$1;
(statearr_42154_42216[(2)] = inst_42080);

(statearr_42154_42216[(1)] = (44));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42091 === (26))){
var inst_42022 = (state_42090[(29)]);
var inst_42076 = (state_42090[(2)]);
var inst_42077 = cljs.core.seq.call(null,inst_42022);
var state_42090__$1 = (function (){var statearr_42155 = state_42090;
(statearr_42155[(31)] = inst_42076);

return statearr_42155;
})();
if(inst_42077){
var statearr_42156_42217 = state_42090__$1;
(statearr_42156_42217[(1)] = (42));

} else {
var statearr_42157_42218 = state_42090__$1;
(statearr_42157_42218[(1)] = (43));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42091 === (16))){
var inst_41989 = (state_42090[(7)]);
var inst_41991 = cljs.core.chunked_seq_QMARK_.call(null,inst_41989);
var state_42090__$1 = state_42090;
if(inst_41991){
var statearr_42158_42219 = state_42090__$1;
(statearr_42158_42219[(1)] = (19));

} else {
var statearr_42159_42220 = state_42090__$1;
(statearr_42159_42220[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42091 === (38))){
var inst_42069 = (state_42090[(2)]);
var state_42090__$1 = state_42090;
var statearr_42160_42221 = state_42090__$1;
(statearr_42160_42221[(2)] = inst_42069);

(statearr_42160_42221[(1)] = (35));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42091 === (30))){
var state_42090__$1 = state_42090;
var statearr_42161_42222 = state_42090__$1;
(statearr_42161_42222[(2)] = null);

(statearr_42161_42222[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42091 === (10))){
var inst_41970 = (state_42090[(14)]);
var inst_41968 = (state_42090[(16)]);
var inst_41978 = cljs.core._nth.call(null,inst_41968,inst_41970);
var inst_41979 = cljs.core.nth.call(null,inst_41978,(0),null);
var inst_41980 = cljs.core.nth.call(null,inst_41978,(1),null);
var state_42090__$1 = (function (){var statearr_42162 = state_42090;
(statearr_42162[(26)] = inst_41979);

return statearr_42162;
})();
if(cljs.core.truth_(inst_41980)){
var statearr_42163_42223 = state_42090__$1;
(statearr_42163_42223[(1)] = (13));

} else {
var statearr_42164_42224 = state_42090__$1;
(statearr_42164_42224[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42091 === (18))){
var inst_42015 = (state_42090[(2)]);
var state_42090__$1 = state_42090;
var statearr_42165_42225 = state_42090__$1;
(statearr_42165_42225[(2)] = inst_42015);

(statearr_42165_42225[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42091 === (42))){
var state_42090__$1 = state_42090;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_42090__$1,(45),dchan);
} else {
if((state_val_42091 === (37))){
var inst_42049 = (state_42090[(25)]);
var inst_41958 = (state_42090[(9)]);
var inst_42058 = (state_42090[(23)]);
var inst_42058__$1 = cljs.core.first.call(null,inst_42049);
var inst_42059 = cljs.core.async.put_BANG_.call(null,inst_42058__$1,inst_41958,done);
var state_42090__$1 = (function (){var statearr_42166 = state_42090;
(statearr_42166[(23)] = inst_42058__$1);

return statearr_42166;
})();
if(cljs.core.truth_(inst_42059)){
var statearr_42167_42226 = state_42090__$1;
(statearr_42167_42226[(1)] = (39));

} else {
var statearr_42168_42227 = state_42090__$1;
(statearr_42168_42227[(1)] = (40));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42091 === (8))){
var inst_41969 = (state_42090[(13)]);
var inst_41970 = (state_42090[(14)]);
var inst_41972 = (inst_41970 < inst_41969);
var inst_41973 = inst_41972;
var state_42090__$1 = state_42090;
if(cljs.core.truth_(inst_41973)){
var statearr_42169_42228 = state_42090__$1;
(statearr_42169_42228[(1)] = (10));

} else {
var statearr_42170_42229 = state_42090__$1;
(statearr_42170_42229[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__35058__auto___42175,cs,m,dchan,dctr,done))
;
return ((function (switch__34968__auto__,c__35058__auto___42175,cs,m,dchan,dctr,done){
return (function() {
var cljs$core$async$mult_$_state_machine__34969__auto__ = null;
var cljs$core$async$mult_$_state_machine__34969__auto____0 = (function (){
var statearr_42171 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_42171[(0)] = cljs$core$async$mult_$_state_machine__34969__auto__);

(statearr_42171[(1)] = (1));

return statearr_42171;
});
var cljs$core$async$mult_$_state_machine__34969__auto____1 = (function (state_42090){
while(true){
var ret_value__34970__auto__ = (function (){try{while(true){
var result__34971__auto__ = switch__34968__auto__.call(null,state_42090);
if(cljs.core.keyword_identical_QMARK_.call(null,result__34971__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__34971__auto__;
}
break;
}
}catch (e42172){if((e42172 instanceof Object)){
var ex__34972__auto__ = e42172;
var statearr_42173_42230 = state_42090;
(statearr_42173_42230[(5)] = ex__34972__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_42090);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e42172;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__34970__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__42231 = state_42090;
state_42090 = G__42231;
continue;
} else {
return ret_value__34970__auto__;
}
break;
}
});
cljs$core$async$mult_$_state_machine__34969__auto__ = function(state_42090){
switch(arguments.length){
case 0:
return cljs$core$async$mult_$_state_machine__34969__auto____0.call(this);
case 1:
return cljs$core$async$mult_$_state_machine__34969__auto____1.call(this,state_42090);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$mult_$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mult_$_state_machine__34969__auto____0;
cljs$core$async$mult_$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mult_$_state_machine__34969__auto____1;
return cljs$core$async$mult_$_state_machine__34969__auto__;
})()
;})(switch__34968__auto__,c__35058__auto___42175,cs,m,dchan,dctr,done))
})();
var state__35060__auto__ = (function (){var statearr_42174 = f__35059__auto__.call(null);
(statearr_42174[(6)] = c__35058__auto___42175);

return statearr_42174;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__35060__auto__);
});})(c__35058__auto___42175,cs,m,dchan,dctr,done))
);


return m;
});
/**
 * Copies the mult source onto the supplied channel.
 * 
 *   By default the channel will be closed when the source closes,
 *   but can be determined by the close? parameter.
 */
cljs.core.async.tap = (function cljs$core$async$tap(var_args){
var G__42233 = arguments.length;
switch (G__42233) {
case 2:
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.tap.cljs$core$IFn$_invoke$arity$2 = (function (mult,ch){
return cljs.core.async.tap.call(null,mult,ch,true);
});

cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3 = (function (mult,ch,close_QMARK_){
cljs.core.async.tap_STAR_.call(null,mult,ch,close_QMARK_);

return ch;
});

cljs.core.async.tap.cljs$lang$maxFixedArity = 3;

/**
 * Disconnects a target channel from a mult
 */
cljs.core.async.untap = (function cljs$core$async$untap(mult,ch){
return cljs.core.async.untap_STAR_.call(null,mult,ch);
});
/**
 * Disconnects all target channels from a mult
 */
cljs.core.async.untap_all = (function cljs$core$async$untap_all(mult){
return cljs.core.async.untap_all_STAR_.call(null,mult);
});

/**
 * @interface
 */
cljs.core.async.Mix = function(){};

cljs.core.async.admix_STAR_ = (function cljs$core$async$admix_STAR_(m,ch){
if((!((m == null))) && (!((m.cljs$core$async$Mix$admix_STAR_$arity$2 == null)))){
return m.cljs$core$async$Mix$admix_STAR_$arity$2(m,ch);
} else {
var x__30915__auto__ = (((m == null))?null:m);
var m__30916__auto__ = (cljs.core.async.admix_STAR_[goog.typeOf(x__30915__auto__)]);
if(!((m__30916__auto__ == null))){
return m__30916__auto__.call(null,m,ch);
} else {
var m__30916__auto____$1 = (cljs.core.async.admix_STAR_["_"]);
if(!((m__30916__auto____$1 == null))){
return m__30916__auto____$1.call(null,m,ch);
} else {
throw cljs.core.missing_protocol.call(null,"Mix.admix*",m);
}
}
}
});

cljs.core.async.unmix_STAR_ = (function cljs$core$async$unmix_STAR_(m,ch){
if((!((m == null))) && (!((m.cljs$core$async$Mix$unmix_STAR_$arity$2 == null)))){
return m.cljs$core$async$Mix$unmix_STAR_$arity$2(m,ch);
} else {
var x__30915__auto__ = (((m == null))?null:m);
var m__30916__auto__ = (cljs.core.async.unmix_STAR_[goog.typeOf(x__30915__auto__)]);
if(!((m__30916__auto__ == null))){
return m__30916__auto__.call(null,m,ch);
} else {
var m__30916__auto____$1 = (cljs.core.async.unmix_STAR_["_"]);
if(!((m__30916__auto____$1 == null))){
return m__30916__auto____$1.call(null,m,ch);
} else {
throw cljs.core.missing_protocol.call(null,"Mix.unmix*",m);
}
}
}
});

cljs.core.async.unmix_all_STAR_ = (function cljs$core$async$unmix_all_STAR_(m){
if((!((m == null))) && (!((m.cljs$core$async$Mix$unmix_all_STAR_$arity$1 == null)))){
return m.cljs$core$async$Mix$unmix_all_STAR_$arity$1(m);
} else {
var x__30915__auto__ = (((m == null))?null:m);
var m__30916__auto__ = (cljs.core.async.unmix_all_STAR_[goog.typeOf(x__30915__auto__)]);
if(!((m__30916__auto__ == null))){
return m__30916__auto__.call(null,m);
} else {
var m__30916__auto____$1 = (cljs.core.async.unmix_all_STAR_["_"]);
if(!((m__30916__auto____$1 == null))){
return m__30916__auto____$1.call(null,m);
} else {
throw cljs.core.missing_protocol.call(null,"Mix.unmix-all*",m);
}
}
}
});

cljs.core.async.toggle_STAR_ = (function cljs$core$async$toggle_STAR_(m,state_map){
if((!((m == null))) && (!((m.cljs$core$async$Mix$toggle_STAR_$arity$2 == null)))){
return m.cljs$core$async$Mix$toggle_STAR_$arity$2(m,state_map);
} else {
var x__30915__auto__ = (((m == null))?null:m);
var m__30916__auto__ = (cljs.core.async.toggle_STAR_[goog.typeOf(x__30915__auto__)]);
if(!((m__30916__auto__ == null))){
return m__30916__auto__.call(null,m,state_map);
} else {
var m__30916__auto____$1 = (cljs.core.async.toggle_STAR_["_"]);
if(!((m__30916__auto____$1 == null))){
return m__30916__auto____$1.call(null,m,state_map);
} else {
throw cljs.core.missing_protocol.call(null,"Mix.toggle*",m);
}
}
}
});

cljs.core.async.solo_mode_STAR_ = (function cljs$core$async$solo_mode_STAR_(m,mode){
if((!((m == null))) && (!((m.cljs$core$async$Mix$solo_mode_STAR_$arity$2 == null)))){
return m.cljs$core$async$Mix$solo_mode_STAR_$arity$2(m,mode);
} else {
var x__30915__auto__ = (((m == null))?null:m);
var m__30916__auto__ = (cljs.core.async.solo_mode_STAR_[goog.typeOf(x__30915__auto__)]);
if(!((m__30916__auto__ == null))){
return m__30916__auto__.call(null,m,mode);
} else {
var m__30916__auto____$1 = (cljs.core.async.solo_mode_STAR_["_"]);
if(!((m__30916__auto____$1 == null))){
return m__30916__auto____$1.call(null,m,mode);
} else {
throw cljs.core.missing_protocol.call(null,"Mix.solo-mode*",m);
}
}
}
});

cljs.core.async.ioc_alts_BANG_ = (function cljs$core$async$ioc_alts_BANG_(var_args){
var args__31466__auto__ = [];
var len__31459__auto___42245 = arguments.length;
var i__31460__auto___42246 = (0);
while(true){
if((i__31460__auto___42246 < len__31459__auto___42245)){
args__31466__auto__.push((arguments[i__31460__auto___42246]));

var G__42247 = (i__31460__auto___42246 + (1));
i__31460__auto___42246 = G__42247;
continue;
} else {
}
break;
}

var argseq__31467__auto__ = ((((3) < args__31466__auto__.length))?(new cljs.core.IndexedSeq(args__31466__auto__.slice((3)),(0),null)):null);
return cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),argseq__31467__auto__);
});

cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (state,cont_block,ports,p__42239){
var map__42240 = p__42239;
var map__42240__$1 = ((((!((map__42240 == null)))?((((map__42240.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__42240.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__42240):map__42240);
var opts = map__42240__$1;
var statearr_42242_42248 = state;
(statearr_42242_42248[(1)] = cont_block);


var temp__5278__auto__ = cljs.core.async.do_alts.call(null,((function (map__42240,map__42240__$1,opts){
return (function (val){
var statearr_42243_42249 = state;
(statearr_42243_42249[(2)] = val);


return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state);
});})(map__42240,map__42240__$1,opts))
,ports,opts);
if(cljs.core.truth_(temp__5278__auto__)){
var cb = temp__5278__auto__;
var statearr_42244_42250 = state;
(statearr_42244_42250[(2)] = cljs.core.deref.call(null,cb));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
});

cljs.core.async.ioc_alts_BANG_.cljs$lang$maxFixedArity = (3);

cljs.core.async.ioc_alts_BANG_.cljs$lang$applyTo = (function (seq42235){
var G__42236 = cljs.core.first.call(null,seq42235);
var seq42235__$1 = cljs.core.next.call(null,seq42235);
var G__42237 = cljs.core.first.call(null,seq42235__$1);
var seq42235__$2 = cljs.core.next.call(null,seq42235__$1);
var G__42238 = cljs.core.first.call(null,seq42235__$2);
var seq42235__$3 = cljs.core.next.call(null,seq42235__$2);
return cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic(G__42236,G__42237,G__42238,seq42235__$3);
});

/**
 * Creates and returns a mix of one or more input channels which will
 *   be put on the supplied out channel. Input sources can be added to
 *   the mix with 'admix', and removed with 'unmix'. A mix supports
 *   soloing, muting and pausing multiple inputs atomically using
 *   'toggle', and can solo using either muting or pausing as determined
 *   by 'solo-mode'.
 * 
 *   Each channel can have zero or more boolean modes set via 'toggle':
 * 
 *   :solo - when true, only this (ond other soloed) channel(s) will appear
 *        in the mix output channel. :mute and :pause states of soloed
 *        channels are ignored. If solo-mode is :mute, non-soloed
 *        channels are muted, if :pause, non-soloed channels are
 *        paused.
 * 
 *   :mute - muted channels will have their contents consumed but not included in the mix
 *   :pause - paused channels will not have their contents consumed (and thus also not included in the mix)
 */
cljs.core.async.mix = (function cljs$core$async$mix(out){
var cs = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var solo_modes = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"pause","pause",-2095325672),null,new cljs.core.Keyword(null,"mute","mute",1151223646),null], null), null);
var attrs = cljs.core.conj.call(null,solo_modes,new cljs.core.Keyword(null,"solo","solo",-316350075));
var solo_mode = cljs.core.atom.call(null,new cljs.core.Keyword(null,"mute","mute",1151223646));
var change = cljs.core.async.chan.call(null);
var changed = ((function (cs,solo_modes,attrs,solo_mode,change){
return (function (){
return cljs.core.async.put_BANG_.call(null,change,true);
});})(cs,solo_modes,attrs,solo_mode,change))
;
var pick = ((function (cs,solo_modes,attrs,solo_mode,change,changed){
return (function (attr,chs){
return cljs.core.reduce_kv.call(null,((function (cs,solo_modes,attrs,solo_mode,change,changed){
return (function (ret,c,v){
if(cljs.core.truth_(attr.call(null,v))){
return cljs.core.conj.call(null,ret,c);
} else {
return ret;
}
});})(cs,solo_modes,attrs,solo_mode,change,changed))
,cljs.core.PersistentHashSet.EMPTY,chs);
});})(cs,solo_modes,attrs,solo_mode,change,changed))
;
var calc_state = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick){
return (function (){
var chs = cljs.core.deref.call(null,cs);
var mode = cljs.core.deref.call(null,solo_mode);
var solos = pick.call(null,new cljs.core.Keyword(null,"solo","solo",-316350075),chs);
var pauses = pick.call(null,new cljs.core.Keyword(null,"pause","pause",-2095325672),chs);
return new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"solos","solos",1441458643),solos,new cljs.core.Keyword(null,"mutes","mutes",1068806309),pick.call(null,new cljs.core.Keyword(null,"mute","mute",1151223646),chs),new cljs.core.Keyword(null,"reads","reads",-1215067361),cljs.core.conj.call(null,(((cljs.core._EQ_.call(null,mode,new cljs.core.Keyword(null,"pause","pause",-2095325672))) && (!(cljs.core.empty_QMARK_.call(null,solos))))?cljs.core.vec.call(null,solos):cljs.core.vec.call(null,cljs.core.remove.call(null,pauses,cljs.core.keys.call(null,chs)))),change)], null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick))
;
var m = (function (){
if(typeof cljs.core.async.t_cljs$core$async42251 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mix}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async42251 = (function (out,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,meta42252){
this.out = out;
this.cs = cs;
this.solo_modes = solo_modes;
this.attrs = attrs;
this.solo_mode = solo_mode;
this.change = change;
this.changed = changed;
this.pick = pick;
this.calc_state = calc_state;
this.meta42252 = meta42252;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async42251.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_42253,meta42252__$1){
var self__ = this;
var _42253__$1 = this;
return (new cljs.core.async.t_cljs$core$async42251(self__.out,self__.cs,self__.solo_modes,self__.attrs,self__.solo_mode,self__.change,self__.changed,self__.pick,self__.calc_state,meta42252__$1));
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async42251.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_42253){
var self__ = this;
var _42253__$1 = this;
return self__.meta42252;
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async42251.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async42251.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_){
var self__ = this;
var ___$1 = this;
return self__.out;
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async42251.prototype.cljs$core$async$Mix$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async42251.prototype.cljs$core$async$Mix$admix_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,ch){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.assoc,ch,cljs.core.PersistentArrayMap.EMPTY);

return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async42251.prototype.cljs$core$async$Mix$unmix_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,ch){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.dissoc,ch);

return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async42251.prototype.cljs$core$async$Mix$unmix_all_STAR_$arity$1 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_.call(null,self__.cs,cljs.core.PersistentArrayMap.EMPTY);

return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async42251.prototype.cljs$core$async$Mix$toggle_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,state_map){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.partial.call(null,cljs.core.merge_with,cljs.core.merge),state_map);

return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async42251.prototype.cljs$core$async$Mix$solo_mode_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,mode){
var self__ = this;
var ___$1 = this;
if(cljs.core.truth_(self__.solo_modes.call(null,mode))){
} else {
throw (new Error(["Assert failed: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(["mode must be one of: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(self__.solo_modes)].join('')),"\n","(solo-modes mode)"].join('')));
}

cljs.core.reset_BANG_.call(null,self__.solo_mode,mode);

return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async42251.getBasis = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (){
return new cljs.core.PersistentVector(null, 10, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"out","out",729986010,null),new cljs.core.Symbol(null,"cs","cs",-117024463,null),new cljs.core.Symbol(null,"solo-modes","solo-modes",882180540,null),new cljs.core.Symbol(null,"attrs","attrs",-450137186,null),new cljs.core.Symbol(null,"solo-mode","solo-mode",2031788074,null),new cljs.core.Symbol(null,"change","change",477485025,null),new cljs.core.Symbol(null,"changed","changed",-2083710852,null),new cljs.core.Symbol(null,"pick","pick",1300068175,null),new cljs.core.Symbol(null,"calc-state","calc-state",-349968968,null),new cljs.core.Symbol(null,"meta42252","meta42252",1367668491,null)], null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async42251.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async42251.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async42251";

cljs.core.async.t_cljs$core$async42251.cljs$lang$ctorPrWriter = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (this__30853__auto__,writer__30854__auto__,opt__30855__auto__){
return cljs.core._write.call(null,writer__30854__auto__,"cljs.core.async/t_cljs$core$async42251");
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.__GT_t_cljs$core$async42251 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function cljs$core$async$mix_$___GT_t_cljs$core$async42251(out__$1,cs__$1,solo_modes__$1,attrs__$1,solo_mode__$1,change__$1,changed__$1,pick__$1,calc_state__$1,meta42252){
return (new cljs.core.async.t_cljs$core$async42251(out__$1,cs__$1,solo_modes__$1,attrs__$1,solo_mode__$1,change__$1,changed__$1,pick__$1,calc_state__$1,meta42252));
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

}

return (new cljs.core.async.t_cljs$core$async42251(out,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var c__35058__auto___42415 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__35058__auto___42415,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m){
return (function (){
var f__35059__auto__ = (function (){var switch__34968__auto__ = ((function (c__35058__auto___42415,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m){
return (function (state_42355){
var state_val_42356 = (state_42355[(1)]);
if((state_val_42356 === (7))){
var inst_42270 = (state_42355[(2)]);
var state_42355__$1 = state_42355;
var statearr_42357_42416 = state_42355__$1;
(statearr_42357_42416[(2)] = inst_42270);

(statearr_42357_42416[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42356 === (20))){
var inst_42282 = (state_42355[(7)]);
var state_42355__$1 = state_42355;
var statearr_42358_42417 = state_42355__$1;
(statearr_42358_42417[(2)] = inst_42282);

(statearr_42358_42417[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42356 === (27))){
var state_42355__$1 = state_42355;
var statearr_42359_42418 = state_42355__$1;
(statearr_42359_42418[(2)] = null);

(statearr_42359_42418[(1)] = (28));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42356 === (1))){
var inst_42257 = (state_42355[(8)]);
var inst_42257__$1 = calc_state.call(null);
var inst_42259 = (inst_42257__$1 == null);
var inst_42260 = cljs.core.not.call(null,inst_42259);
var state_42355__$1 = (function (){var statearr_42360 = state_42355;
(statearr_42360[(8)] = inst_42257__$1);

return statearr_42360;
})();
if(inst_42260){
var statearr_42361_42419 = state_42355__$1;
(statearr_42361_42419[(1)] = (2));

} else {
var statearr_42362_42420 = state_42355__$1;
(statearr_42362_42420[(1)] = (3));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42356 === (24))){
var inst_42306 = (state_42355[(9)]);
var inst_42315 = (state_42355[(10)]);
var inst_42329 = (state_42355[(11)]);
var inst_42329__$1 = inst_42306.call(null,inst_42315);
var state_42355__$1 = (function (){var statearr_42363 = state_42355;
(statearr_42363[(11)] = inst_42329__$1);

return statearr_42363;
})();
if(cljs.core.truth_(inst_42329__$1)){
var statearr_42364_42421 = state_42355__$1;
(statearr_42364_42421[(1)] = (29));

} else {
var statearr_42365_42422 = state_42355__$1;
(statearr_42365_42422[(1)] = (30));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42356 === (4))){
var inst_42273 = (state_42355[(2)]);
var state_42355__$1 = state_42355;
if(cljs.core.truth_(inst_42273)){
var statearr_42366_42423 = state_42355__$1;
(statearr_42366_42423[(1)] = (8));

} else {
var statearr_42367_42424 = state_42355__$1;
(statearr_42367_42424[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42356 === (15))){
var inst_42300 = (state_42355[(2)]);
var state_42355__$1 = state_42355;
if(cljs.core.truth_(inst_42300)){
var statearr_42368_42425 = state_42355__$1;
(statearr_42368_42425[(1)] = (19));

} else {
var statearr_42369_42426 = state_42355__$1;
(statearr_42369_42426[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42356 === (21))){
var inst_42305 = (state_42355[(12)]);
var inst_42305__$1 = (state_42355[(2)]);
var inst_42306 = cljs.core.get.call(null,inst_42305__$1,new cljs.core.Keyword(null,"solos","solos",1441458643));
var inst_42307 = cljs.core.get.call(null,inst_42305__$1,new cljs.core.Keyword(null,"mutes","mutes",1068806309));
var inst_42308 = cljs.core.get.call(null,inst_42305__$1,new cljs.core.Keyword(null,"reads","reads",-1215067361));
var state_42355__$1 = (function (){var statearr_42370 = state_42355;
(statearr_42370[(9)] = inst_42306);

(statearr_42370[(13)] = inst_42307);

(statearr_42370[(12)] = inst_42305__$1);

return statearr_42370;
})();
return cljs.core.async.ioc_alts_BANG_.call(null,state_42355__$1,(22),inst_42308);
} else {
if((state_val_42356 === (31))){
var inst_42337 = (state_42355[(2)]);
var state_42355__$1 = state_42355;
if(cljs.core.truth_(inst_42337)){
var statearr_42371_42427 = state_42355__$1;
(statearr_42371_42427[(1)] = (32));

} else {
var statearr_42372_42428 = state_42355__$1;
(statearr_42372_42428[(1)] = (33));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42356 === (32))){
var inst_42314 = (state_42355[(14)]);
var state_42355__$1 = state_42355;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_42355__$1,(35),out,inst_42314);
} else {
if((state_val_42356 === (33))){
var inst_42305 = (state_42355[(12)]);
var inst_42282 = inst_42305;
var state_42355__$1 = (function (){var statearr_42373 = state_42355;
(statearr_42373[(7)] = inst_42282);

return statearr_42373;
})();
var statearr_42374_42429 = state_42355__$1;
(statearr_42374_42429[(2)] = null);

(statearr_42374_42429[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42356 === (13))){
var inst_42282 = (state_42355[(7)]);
var inst_42289 = inst_42282.cljs$lang$protocol_mask$partition0$;
var inst_42290 = (inst_42289 & (64));
var inst_42291 = inst_42282.cljs$core$ISeq$;
var inst_42292 = (cljs.core.PROTOCOL_SENTINEL === inst_42291);
var inst_42293 = (inst_42290) || (inst_42292);
var state_42355__$1 = state_42355;
if(cljs.core.truth_(inst_42293)){
var statearr_42375_42430 = state_42355__$1;
(statearr_42375_42430[(1)] = (16));

} else {
var statearr_42376_42431 = state_42355__$1;
(statearr_42376_42431[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42356 === (22))){
var inst_42314 = (state_42355[(14)]);
var inst_42315 = (state_42355[(10)]);
var inst_42313 = (state_42355[(2)]);
var inst_42314__$1 = cljs.core.nth.call(null,inst_42313,(0),null);
var inst_42315__$1 = cljs.core.nth.call(null,inst_42313,(1),null);
var inst_42316 = (inst_42314__$1 == null);
var inst_42317 = cljs.core._EQ_.call(null,inst_42315__$1,change);
var inst_42318 = (inst_42316) || (inst_42317);
var state_42355__$1 = (function (){var statearr_42377 = state_42355;
(statearr_42377[(14)] = inst_42314__$1);

(statearr_42377[(10)] = inst_42315__$1);

return statearr_42377;
})();
if(cljs.core.truth_(inst_42318)){
var statearr_42378_42432 = state_42355__$1;
(statearr_42378_42432[(1)] = (23));

} else {
var statearr_42379_42433 = state_42355__$1;
(statearr_42379_42433[(1)] = (24));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42356 === (36))){
var inst_42305 = (state_42355[(12)]);
var inst_42282 = inst_42305;
var state_42355__$1 = (function (){var statearr_42380 = state_42355;
(statearr_42380[(7)] = inst_42282);

return statearr_42380;
})();
var statearr_42381_42434 = state_42355__$1;
(statearr_42381_42434[(2)] = null);

(statearr_42381_42434[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42356 === (29))){
var inst_42329 = (state_42355[(11)]);
var state_42355__$1 = state_42355;
var statearr_42382_42435 = state_42355__$1;
(statearr_42382_42435[(2)] = inst_42329);

(statearr_42382_42435[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42356 === (6))){
var state_42355__$1 = state_42355;
var statearr_42383_42436 = state_42355__$1;
(statearr_42383_42436[(2)] = false);

(statearr_42383_42436[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42356 === (28))){
var inst_42325 = (state_42355[(2)]);
var inst_42326 = calc_state.call(null);
var inst_42282 = inst_42326;
var state_42355__$1 = (function (){var statearr_42384 = state_42355;
(statearr_42384[(7)] = inst_42282);

(statearr_42384[(15)] = inst_42325);

return statearr_42384;
})();
var statearr_42385_42437 = state_42355__$1;
(statearr_42385_42437[(2)] = null);

(statearr_42385_42437[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42356 === (25))){
var inst_42351 = (state_42355[(2)]);
var state_42355__$1 = state_42355;
var statearr_42386_42438 = state_42355__$1;
(statearr_42386_42438[(2)] = inst_42351);

(statearr_42386_42438[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42356 === (34))){
var inst_42349 = (state_42355[(2)]);
var state_42355__$1 = state_42355;
var statearr_42387_42439 = state_42355__$1;
(statearr_42387_42439[(2)] = inst_42349);

(statearr_42387_42439[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42356 === (17))){
var state_42355__$1 = state_42355;
var statearr_42388_42440 = state_42355__$1;
(statearr_42388_42440[(2)] = false);

(statearr_42388_42440[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42356 === (3))){
var state_42355__$1 = state_42355;
var statearr_42389_42441 = state_42355__$1;
(statearr_42389_42441[(2)] = false);

(statearr_42389_42441[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42356 === (12))){
var inst_42353 = (state_42355[(2)]);
var state_42355__$1 = state_42355;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_42355__$1,inst_42353);
} else {
if((state_val_42356 === (2))){
var inst_42257 = (state_42355[(8)]);
var inst_42262 = inst_42257.cljs$lang$protocol_mask$partition0$;
var inst_42263 = (inst_42262 & (64));
var inst_42264 = inst_42257.cljs$core$ISeq$;
var inst_42265 = (cljs.core.PROTOCOL_SENTINEL === inst_42264);
var inst_42266 = (inst_42263) || (inst_42265);
var state_42355__$1 = state_42355;
if(cljs.core.truth_(inst_42266)){
var statearr_42390_42442 = state_42355__$1;
(statearr_42390_42442[(1)] = (5));

} else {
var statearr_42391_42443 = state_42355__$1;
(statearr_42391_42443[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42356 === (23))){
var inst_42314 = (state_42355[(14)]);
var inst_42320 = (inst_42314 == null);
var state_42355__$1 = state_42355;
if(cljs.core.truth_(inst_42320)){
var statearr_42392_42444 = state_42355__$1;
(statearr_42392_42444[(1)] = (26));

} else {
var statearr_42393_42445 = state_42355__$1;
(statearr_42393_42445[(1)] = (27));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42356 === (35))){
var inst_42340 = (state_42355[(2)]);
var state_42355__$1 = state_42355;
if(cljs.core.truth_(inst_42340)){
var statearr_42394_42446 = state_42355__$1;
(statearr_42394_42446[(1)] = (36));

} else {
var statearr_42395_42447 = state_42355__$1;
(statearr_42395_42447[(1)] = (37));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42356 === (19))){
var inst_42282 = (state_42355[(7)]);
var inst_42302 = cljs.core.apply.call(null,cljs.core.hash_map,inst_42282);
var state_42355__$1 = state_42355;
var statearr_42396_42448 = state_42355__$1;
(statearr_42396_42448[(2)] = inst_42302);

(statearr_42396_42448[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42356 === (11))){
var inst_42282 = (state_42355[(7)]);
var inst_42286 = (inst_42282 == null);
var inst_42287 = cljs.core.not.call(null,inst_42286);
var state_42355__$1 = state_42355;
if(inst_42287){
var statearr_42397_42449 = state_42355__$1;
(statearr_42397_42449[(1)] = (13));

} else {
var statearr_42398_42450 = state_42355__$1;
(statearr_42398_42450[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42356 === (9))){
var inst_42257 = (state_42355[(8)]);
var state_42355__$1 = state_42355;
var statearr_42399_42451 = state_42355__$1;
(statearr_42399_42451[(2)] = inst_42257);

(statearr_42399_42451[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42356 === (5))){
var state_42355__$1 = state_42355;
var statearr_42400_42452 = state_42355__$1;
(statearr_42400_42452[(2)] = true);

(statearr_42400_42452[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42356 === (14))){
var state_42355__$1 = state_42355;
var statearr_42401_42453 = state_42355__$1;
(statearr_42401_42453[(2)] = false);

(statearr_42401_42453[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42356 === (26))){
var inst_42315 = (state_42355[(10)]);
var inst_42322 = cljs.core.swap_BANG_.call(null,cs,cljs.core.dissoc,inst_42315);
var state_42355__$1 = state_42355;
var statearr_42402_42454 = state_42355__$1;
(statearr_42402_42454[(2)] = inst_42322);

(statearr_42402_42454[(1)] = (28));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42356 === (16))){
var state_42355__$1 = state_42355;
var statearr_42403_42455 = state_42355__$1;
(statearr_42403_42455[(2)] = true);

(statearr_42403_42455[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42356 === (38))){
var inst_42345 = (state_42355[(2)]);
var state_42355__$1 = state_42355;
var statearr_42404_42456 = state_42355__$1;
(statearr_42404_42456[(2)] = inst_42345);

(statearr_42404_42456[(1)] = (34));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42356 === (30))){
var inst_42306 = (state_42355[(9)]);
var inst_42307 = (state_42355[(13)]);
var inst_42315 = (state_42355[(10)]);
var inst_42332 = cljs.core.empty_QMARK_.call(null,inst_42306);
var inst_42333 = inst_42307.call(null,inst_42315);
var inst_42334 = cljs.core.not.call(null,inst_42333);
var inst_42335 = (inst_42332) && (inst_42334);
var state_42355__$1 = state_42355;
var statearr_42405_42457 = state_42355__$1;
(statearr_42405_42457[(2)] = inst_42335);

(statearr_42405_42457[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42356 === (10))){
var inst_42257 = (state_42355[(8)]);
var inst_42278 = (state_42355[(2)]);
var inst_42279 = cljs.core.get.call(null,inst_42278,new cljs.core.Keyword(null,"solos","solos",1441458643));
var inst_42280 = cljs.core.get.call(null,inst_42278,new cljs.core.Keyword(null,"mutes","mutes",1068806309));
var inst_42281 = cljs.core.get.call(null,inst_42278,new cljs.core.Keyword(null,"reads","reads",-1215067361));
var inst_42282 = inst_42257;
var state_42355__$1 = (function (){var statearr_42406 = state_42355;
(statearr_42406[(16)] = inst_42281);

(statearr_42406[(17)] = inst_42279);

(statearr_42406[(18)] = inst_42280);

(statearr_42406[(7)] = inst_42282);

return statearr_42406;
})();
var statearr_42407_42458 = state_42355__$1;
(statearr_42407_42458[(2)] = null);

(statearr_42407_42458[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42356 === (18))){
var inst_42297 = (state_42355[(2)]);
var state_42355__$1 = state_42355;
var statearr_42408_42459 = state_42355__$1;
(statearr_42408_42459[(2)] = inst_42297);

(statearr_42408_42459[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42356 === (37))){
var state_42355__$1 = state_42355;
var statearr_42409_42460 = state_42355__$1;
(statearr_42409_42460[(2)] = null);

(statearr_42409_42460[(1)] = (38));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42356 === (8))){
var inst_42257 = (state_42355[(8)]);
var inst_42275 = cljs.core.apply.call(null,cljs.core.hash_map,inst_42257);
var state_42355__$1 = state_42355;
var statearr_42410_42461 = state_42355__$1;
(statearr_42410_42461[(2)] = inst_42275);

(statearr_42410_42461[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__35058__auto___42415,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m))
;
return ((function (switch__34968__auto__,c__35058__auto___42415,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m){
return (function() {
var cljs$core$async$mix_$_state_machine__34969__auto__ = null;
var cljs$core$async$mix_$_state_machine__34969__auto____0 = (function (){
var statearr_42411 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_42411[(0)] = cljs$core$async$mix_$_state_machine__34969__auto__);

(statearr_42411[(1)] = (1));

return statearr_42411;
});
var cljs$core$async$mix_$_state_machine__34969__auto____1 = (function (state_42355){
while(true){
var ret_value__34970__auto__ = (function (){try{while(true){
var result__34971__auto__ = switch__34968__auto__.call(null,state_42355);
if(cljs.core.keyword_identical_QMARK_.call(null,result__34971__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__34971__auto__;
}
break;
}
}catch (e42412){if((e42412 instanceof Object)){
var ex__34972__auto__ = e42412;
var statearr_42413_42462 = state_42355;
(statearr_42413_42462[(5)] = ex__34972__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_42355);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e42412;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__34970__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__42463 = state_42355;
state_42355 = G__42463;
continue;
} else {
return ret_value__34970__auto__;
}
break;
}
});
cljs$core$async$mix_$_state_machine__34969__auto__ = function(state_42355){
switch(arguments.length){
case 0:
return cljs$core$async$mix_$_state_machine__34969__auto____0.call(this);
case 1:
return cljs$core$async$mix_$_state_machine__34969__auto____1.call(this,state_42355);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$mix_$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mix_$_state_machine__34969__auto____0;
cljs$core$async$mix_$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mix_$_state_machine__34969__auto____1;
return cljs$core$async$mix_$_state_machine__34969__auto__;
})()
;})(switch__34968__auto__,c__35058__auto___42415,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m))
})();
var state__35060__auto__ = (function (){var statearr_42414 = f__35059__auto__.call(null);
(statearr_42414[(6)] = c__35058__auto___42415);

return statearr_42414;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__35060__auto__);
});})(c__35058__auto___42415,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m))
);


return m;
});
/**
 * Adds ch as an input to the mix
 */
cljs.core.async.admix = (function cljs$core$async$admix(mix,ch){
return cljs.core.async.admix_STAR_.call(null,mix,ch);
});
/**
 * Removes ch as an input to the mix
 */
cljs.core.async.unmix = (function cljs$core$async$unmix(mix,ch){
return cljs.core.async.unmix_STAR_.call(null,mix,ch);
});
/**
 * removes all inputs from the mix
 */
cljs.core.async.unmix_all = (function cljs$core$async$unmix_all(mix){
return cljs.core.async.unmix_all_STAR_.call(null,mix);
});
/**
 * Atomically sets the state(s) of one or more channels in a mix. The
 *   state map is a map of channels -> channel-state-map. A
 *   channel-state-map is a map of attrs -> boolean, where attr is one or
 *   more of :mute, :pause or :solo. Any states supplied are merged with
 *   the current state.
 * 
 *   Note that channels can be added to a mix via toggle, which can be
 *   used to add channels in a particular (e.g. paused) state.
 */
cljs.core.async.toggle = (function cljs$core$async$toggle(mix,state_map){
return cljs.core.async.toggle_STAR_.call(null,mix,state_map);
});
/**
 * Sets the solo mode of the mix. mode must be one of :mute or :pause
 */
cljs.core.async.solo_mode = (function cljs$core$async$solo_mode(mix,mode){
return cljs.core.async.solo_mode_STAR_.call(null,mix,mode);
});

/**
 * @interface
 */
cljs.core.async.Pub = function(){};

cljs.core.async.sub_STAR_ = (function cljs$core$async$sub_STAR_(p,v,ch,close_QMARK_){
if((!((p == null))) && (!((p.cljs$core$async$Pub$sub_STAR_$arity$4 == null)))){
return p.cljs$core$async$Pub$sub_STAR_$arity$4(p,v,ch,close_QMARK_);
} else {
var x__30915__auto__ = (((p == null))?null:p);
var m__30916__auto__ = (cljs.core.async.sub_STAR_[goog.typeOf(x__30915__auto__)]);
if(!((m__30916__auto__ == null))){
return m__30916__auto__.call(null,p,v,ch,close_QMARK_);
} else {
var m__30916__auto____$1 = (cljs.core.async.sub_STAR_["_"]);
if(!((m__30916__auto____$1 == null))){
return m__30916__auto____$1.call(null,p,v,ch,close_QMARK_);
} else {
throw cljs.core.missing_protocol.call(null,"Pub.sub*",p);
}
}
}
});

cljs.core.async.unsub_STAR_ = (function cljs$core$async$unsub_STAR_(p,v,ch){
if((!((p == null))) && (!((p.cljs$core$async$Pub$unsub_STAR_$arity$3 == null)))){
return p.cljs$core$async$Pub$unsub_STAR_$arity$3(p,v,ch);
} else {
var x__30915__auto__ = (((p == null))?null:p);
var m__30916__auto__ = (cljs.core.async.unsub_STAR_[goog.typeOf(x__30915__auto__)]);
if(!((m__30916__auto__ == null))){
return m__30916__auto__.call(null,p,v,ch);
} else {
var m__30916__auto____$1 = (cljs.core.async.unsub_STAR_["_"]);
if(!((m__30916__auto____$1 == null))){
return m__30916__auto____$1.call(null,p,v,ch);
} else {
throw cljs.core.missing_protocol.call(null,"Pub.unsub*",p);
}
}
}
});

cljs.core.async.unsub_all_STAR_ = (function cljs$core$async$unsub_all_STAR_(var_args){
var G__42465 = arguments.length;
switch (G__42465) {
case 1:
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1 = (function (p){
if((!((p == null))) && (!((p.cljs$core$async$Pub$unsub_all_STAR_$arity$1 == null)))){
return p.cljs$core$async$Pub$unsub_all_STAR_$arity$1(p);
} else {
var x__30915__auto__ = (((p == null))?null:p);
var m__30916__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__30915__auto__)]);
if(!((m__30916__auto__ == null))){
return m__30916__auto__.call(null,p);
} else {
var m__30916__auto____$1 = (cljs.core.async.unsub_all_STAR_["_"]);
if(!((m__30916__auto____$1 == null))){
return m__30916__auto____$1.call(null,p);
} else {
throw cljs.core.missing_protocol.call(null,"Pub.unsub-all*",p);
}
}
}
});

cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2 = (function (p,v){
if((!((p == null))) && (!((p.cljs$core$async$Pub$unsub_all_STAR_$arity$2 == null)))){
return p.cljs$core$async$Pub$unsub_all_STAR_$arity$2(p,v);
} else {
var x__30915__auto__ = (((p == null))?null:p);
var m__30916__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__30915__auto__)]);
if(!((m__30916__auto__ == null))){
return m__30916__auto__.call(null,p,v);
} else {
var m__30916__auto____$1 = (cljs.core.async.unsub_all_STAR_["_"]);
if(!((m__30916__auto____$1 == null))){
return m__30916__auto____$1.call(null,p,v);
} else {
throw cljs.core.missing_protocol.call(null,"Pub.unsub-all*",p);
}
}
}
});

cljs.core.async.unsub_all_STAR_.cljs$lang$maxFixedArity = 2;


/**
 * Creates and returns a pub(lication) of the supplied channel,
 *   partitioned into topics by the topic-fn. topic-fn will be applied to
 *   each value on the channel and the result will determine the 'topic'
 *   on which that value will be put. Channels can be subscribed to
 *   receive copies of topics using 'sub', and unsubscribed using
 *   'unsub'. Each topic will be handled by an internal mult on a
 *   dedicated channel. By default these internal channels are
 *   unbuffered, but a buf-fn can be supplied which, given a topic,
 *   creates a buffer with desired properties.
 * 
 *   Each item is distributed to all subs in parallel and synchronously,
 *   i.e. each sub must accept before the next item is distributed. Use
 *   buffering/windowing to prevent slow subs from holding up the pub.
 * 
 *   Items received when there are no matching subs get dropped.
 * 
 *   Note that if buf-fns are used then each topic is handled
 *   asynchronously, i.e. if a channel is subscribed to more than one
 *   topic it should not expect them to be interleaved identically with
 *   the source.
 */
cljs.core.async.pub = (function cljs$core$async$pub(var_args){
var G__42469 = arguments.length;
switch (G__42469) {
case 2:
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.pub.cljs$core$IFn$_invoke$arity$2 = (function (ch,topic_fn){
return cljs.core.async.pub.call(null,ch,topic_fn,cljs.core.constantly.call(null,null));
});

cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3 = (function (ch,topic_fn,buf_fn){
var mults = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var ensure_mult = ((function (mults){
return (function (topic){
var or__30182__auto__ = cljs.core.get.call(null,cljs.core.deref.call(null,mults),topic);
if(cljs.core.truth_(or__30182__auto__)){
return or__30182__auto__;
} else {
return cljs.core.get.call(null,cljs.core.swap_BANG_.call(null,mults,((function (or__30182__auto__,mults){
return (function (p1__42467_SHARP_){
if(cljs.core.truth_(p1__42467_SHARP_.call(null,topic))){
return p1__42467_SHARP_;
} else {
return cljs.core.assoc.call(null,p1__42467_SHARP_,topic,cljs.core.async.mult.call(null,cljs.core.async.chan.call(null,buf_fn.call(null,topic))));
}
});})(or__30182__auto__,mults))
),topic);
}
});})(mults))
;
var p = (function (){
if(typeof cljs.core.async.t_cljs$core$async42470 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.Pub}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async42470 = (function (ch,topic_fn,buf_fn,mults,ensure_mult,meta42471){
this.ch = ch;
this.topic_fn = topic_fn;
this.buf_fn = buf_fn;
this.mults = mults;
this.ensure_mult = ensure_mult;
this.meta42471 = meta42471;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async42470.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (mults,ensure_mult){
return (function (_42472,meta42471__$1){
var self__ = this;
var _42472__$1 = this;
return (new cljs.core.async.t_cljs$core$async42470(self__.ch,self__.topic_fn,self__.buf_fn,self__.mults,self__.ensure_mult,meta42471__$1));
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async42470.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (mults,ensure_mult){
return (function (_42472){
var self__ = this;
var _42472__$1 = this;
return self__.meta42471;
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async42470.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async42470.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = ((function (mults,ensure_mult){
return (function (_){
var self__ = this;
var ___$1 = this;
return self__.ch;
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async42470.prototype.cljs$core$async$Pub$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async42470.prototype.cljs$core$async$Pub$sub_STAR_$arity$4 = ((function (mults,ensure_mult){
return (function (p,topic,ch__$1,close_QMARK_){
var self__ = this;
var p__$1 = this;
var m = self__.ensure_mult.call(null,topic);
return cljs.core.async.tap.call(null,m,ch__$1,close_QMARK_);
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async42470.prototype.cljs$core$async$Pub$unsub_STAR_$arity$3 = ((function (mults,ensure_mult){
return (function (p,topic,ch__$1){
var self__ = this;
var p__$1 = this;
var temp__5278__auto__ = cljs.core.get.call(null,cljs.core.deref.call(null,self__.mults),topic);
if(cljs.core.truth_(temp__5278__auto__)){
var m = temp__5278__auto__;
return cljs.core.async.untap.call(null,m,ch__$1);
} else {
return null;
}
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async42470.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$1 = ((function (mults,ensure_mult){
return (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.reset_BANG_.call(null,self__.mults,cljs.core.PersistentArrayMap.EMPTY);
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async42470.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$2 = ((function (mults,ensure_mult){
return (function (_,topic){
var self__ = this;
var ___$1 = this;
return cljs.core.swap_BANG_.call(null,self__.mults,cljs.core.dissoc,topic);
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async42470.getBasis = ((function (mults,ensure_mult){
return (function (){
return new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"topic-fn","topic-fn",-862449736,null),new cljs.core.Symbol(null,"buf-fn","buf-fn",-1200281591,null),new cljs.core.Symbol(null,"mults","mults",-461114485,null),new cljs.core.Symbol(null,"ensure-mult","ensure-mult",1796584816,null),new cljs.core.Symbol(null,"meta42471","meta42471",1896351365,null)], null);
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async42470.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async42470.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async42470";

cljs.core.async.t_cljs$core$async42470.cljs$lang$ctorPrWriter = ((function (mults,ensure_mult){
return (function (this__30853__auto__,writer__30854__auto__,opt__30855__auto__){
return cljs.core._write.call(null,writer__30854__auto__,"cljs.core.async/t_cljs$core$async42470");
});})(mults,ensure_mult))
;

cljs.core.async.__GT_t_cljs$core$async42470 = ((function (mults,ensure_mult){
return (function cljs$core$async$__GT_t_cljs$core$async42470(ch__$1,topic_fn__$1,buf_fn__$1,mults__$1,ensure_mult__$1,meta42471){
return (new cljs.core.async.t_cljs$core$async42470(ch__$1,topic_fn__$1,buf_fn__$1,mults__$1,ensure_mult__$1,meta42471));
});})(mults,ensure_mult))
;

}

return (new cljs.core.async.t_cljs$core$async42470(ch,topic_fn,buf_fn,mults,ensure_mult,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var c__35058__auto___42590 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__35058__auto___42590,mults,ensure_mult,p){
return (function (){
var f__35059__auto__ = (function (){var switch__34968__auto__ = ((function (c__35058__auto___42590,mults,ensure_mult,p){
return (function (state_42544){
var state_val_42545 = (state_42544[(1)]);
if((state_val_42545 === (7))){
var inst_42540 = (state_42544[(2)]);
var state_42544__$1 = state_42544;
var statearr_42546_42591 = state_42544__$1;
(statearr_42546_42591[(2)] = inst_42540);

(statearr_42546_42591[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42545 === (20))){
var state_42544__$1 = state_42544;
var statearr_42547_42592 = state_42544__$1;
(statearr_42547_42592[(2)] = null);

(statearr_42547_42592[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42545 === (1))){
var state_42544__$1 = state_42544;
var statearr_42548_42593 = state_42544__$1;
(statearr_42548_42593[(2)] = null);

(statearr_42548_42593[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42545 === (24))){
var inst_42523 = (state_42544[(7)]);
var inst_42532 = cljs.core.swap_BANG_.call(null,mults,cljs.core.dissoc,inst_42523);
var state_42544__$1 = state_42544;
var statearr_42549_42594 = state_42544__$1;
(statearr_42549_42594[(2)] = inst_42532);

(statearr_42549_42594[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42545 === (4))){
var inst_42475 = (state_42544[(8)]);
var inst_42475__$1 = (state_42544[(2)]);
var inst_42476 = (inst_42475__$1 == null);
var state_42544__$1 = (function (){var statearr_42550 = state_42544;
(statearr_42550[(8)] = inst_42475__$1);

return statearr_42550;
})();
if(cljs.core.truth_(inst_42476)){
var statearr_42551_42595 = state_42544__$1;
(statearr_42551_42595[(1)] = (5));

} else {
var statearr_42552_42596 = state_42544__$1;
(statearr_42552_42596[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42545 === (15))){
var inst_42517 = (state_42544[(2)]);
var state_42544__$1 = state_42544;
var statearr_42553_42597 = state_42544__$1;
(statearr_42553_42597[(2)] = inst_42517);

(statearr_42553_42597[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42545 === (21))){
var inst_42537 = (state_42544[(2)]);
var state_42544__$1 = (function (){var statearr_42554 = state_42544;
(statearr_42554[(9)] = inst_42537);

return statearr_42554;
})();
var statearr_42555_42598 = state_42544__$1;
(statearr_42555_42598[(2)] = null);

(statearr_42555_42598[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42545 === (13))){
var inst_42499 = (state_42544[(10)]);
var inst_42501 = cljs.core.chunked_seq_QMARK_.call(null,inst_42499);
var state_42544__$1 = state_42544;
if(inst_42501){
var statearr_42556_42599 = state_42544__$1;
(statearr_42556_42599[(1)] = (16));

} else {
var statearr_42557_42600 = state_42544__$1;
(statearr_42557_42600[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42545 === (22))){
var inst_42529 = (state_42544[(2)]);
var state_42544__$1 = state_42544;
if(cljs.core.truth_(inst_42529)){
var statearr_42558_42601 = state_42544__$1;
(statearr_42558_42601[(1)] = (23));

} else {
var statearr_42559_42602 = state_42544__$1;
(statearr_42559_42602[(1)] = (24));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42545 === (6))){
var inst_42525 = (state_42544[(11)]);
var inst_42523 = (state_42544[(7)]);
var inst_42475 = (state_42544[(8)]);
var inst_42523__$1 = topic_fn.call(null,inst_42475);
var inst_42524 = cljs.core.deref.call(null,mults);
var inst_42525__$1 = cljs.core.get.call(null,inst_42524,inst_42523__$1);
var state_42544__$1 = (function (){var statearr_42560 = state_42544;
(statearr_42560[(11)] = inst_42525__$1);

(statearr_42560[(7)] = inst_42523__$1);

return statearr_42560;
})();
if(cljs.core.truth_(inst_42525__$1)){
var statearr_42561_42603 = state_42544__$1;
(statearr_42561_42603[(1)] = (19));

} else {
var statearr_42562_42604 = state_42544__$1;
(statearr_42562_42604[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42545 === (25))){
var inst_42534 = (state_42544[(2)]);
var state_42544__$1 = state_42544;
var statearr_42563_42605 = state_42544__$1;
(statearr_42563_42605[(2)] = inst_42534);

(statearr_42563_42605[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42545 === (17))){
var inst_42499 = (state_42544[(10)]);
var inst_42508 = cljs.core.first.call(null,inst_42499);
var inst_42509 = cljs.core.async.muxch_STAR_.call(null,inst_42508);
var inst_42510 = cljs.core.async.close_BANG_.call(null,inst_42509);
var inst_42511 = cljs.core.next.call(null,inst_42499);
var inst_42485 = inst_42511;
var inst_42486 = null;
var inst_42487 = (0);
var inst_42488 = (0);
var state_42544__$1 = (function (){var statearr_42564 = state_42544;
(statearr_42564[(12)] = inst_42487);

(statearr_42564[(13)] = inst_42510);

(statearr_42564[(14)] = inst_42488);

(statearr_42564[(15)] = inst_42486);

(statearr_42564[(16)] = inst_42485);

return statearr_42564;
})();
var statearr_42565_42606 = state_42544__$1;
(statearr_42565_42606[(2)] = null);

(statearr_42565_42606[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42545 === (3))){
var inst_42542 = (state_42544[(2)]);
var state_42544__$1 = state_42544;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_42544__$1,inst_42542);
} else {
if((state_val_42545 === (12))){
var inst_42519 = (state_42544[(2)]);
var state_42544__$1 = state_42544;
var statearr_42566_42607 = state_42544__$1;
(statearr_42566_42607[(2)] = inst_42519);

(statearr_42566_42607[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42545 === (2))){
var state_42544__$1 = state_42544;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_42544__$1,(4),ch);
} else {
if((state_val_42545 === (23))){
var state_42544__$1 = state_42544;
var statearr_42567_42608 = state_42544__$1;
(statearr_42567_42608[(2)] = null);

(statearr_42567_42608[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42545 === (19))){
var inst_42525 = (state_42544[(11)]);
var inst_42475 = (state_42544[(8)]);
var inst_42527 = cljs.core.async.muxch_STAR_.call(null,inst_42525);
var state_42544__$1 = state_42544;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_42544__$1,(22),inst_42527,inst_42475);
} else {
if((state_val_42545 === (11))){
var inst_42499 = (state_42544[(10)]);
var inst_42485 = (state_42544[(16)]);
var inst_42499__$1 = cljs.core.seq.call(null,inst_42485);
var state_42544__$1 = (function (){var statearr_42568 = state_42544;
(statearr_42568[(10)] = inst_42499__$1);

return statearr_42568;
})();
if(inst_42499__$1){
var statearr_42569_42609 = state_42544__$1;
(statearr_42569_42609[(1)] = (13));

} else {
var statearr_42570_42610 = state_42544__$1;
(statearr_42570_42610[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42545 === (9))){
var inst_42521 = (state_42544[(2)]);
var state_42544__$1 = state_42544;
var statearr_42571_42611 = state_42544__$1;
(statearr_42571_42611[(2)] = inst_42521);

(statearr_42571_42611[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42545 === (5))){
var inst_42482 = cljs.core.deref.call(null,mults);
var inst_42483 = cljs.core.vals.call(null,inst_42482);
var inst_42484 = cljs.core.seq.call(null,inst_42483);
var inst_42485 = inst_42484;
var inst_42486 = null;
var inst_42487 = (0);
var inst_42488 = (0);
var state_42544__$1 = (function (){var statearr_42572 = state_42544;
(statearr_42572[(12)] = inst_42487);

(statearr_42572[(14)] = inst_42488);

(statearr_42572[(15)] = inst_42486);

(statearr_42572[(16)] = inst_42485);

return statearr_42572;
})();
var statearr_42573_42612 = state_42544__$1;
(statearr_42573_42612[(2)] = null);

(statearr_42573_42612[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42545 === (14))){
var state_42544__$1 = state_42544;
var statearr_42577_42613 = state_42544__$1;
(statearr_42577_42613[(2)] = null);

(statearr_42577_42613[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42545 === (16))){
var inst_42499 = (state_42544[(10)]);
var inst_42503 = cljs.core.chunk_first.call(null,inst_42499);
var inst_42504 = cljs.core.chunk_rest.call(null,inst_42499);
var inst_42505 = cljs.core.count.call(null,inst_42503);
var inst_42485 = inst_42504;
var inst_42486 = inst_42503;
var inst_42487 = inst_42505;
var inst_42488 = (0);
var state_42544__$1 = (function (){var statearr_42578 = state_42544;
(statearr_42578[(12)] = inst_42487);

(statearr_42578[(14)] = inst_42488);

(statearr_42578[(15)] = inst_42486);

(statearr_42578[(16)] = inst_42485);

return statearr_42578;
})();
var statearr_42579_42614 = state_42544__$1;
(statearr_42579_42614[(2)] = null);

(statearr_42579_42614[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42545 === (10))){
var inst_42487 = (state_42544[(12)]);
var inst_42488 = (state_42544[(14)]);
var inst_42486 = (state_42544[(15)]);
var inst_42485 = (state_42544[(16)]);
var inst_42493 = cljs.core._nth.call(null,inst_42486,inst_42488);
var inst_42494 = cljs.core.async.muxch_STAR_.call(null,inst_42493);
var inst_42495 = cljs.core.async.close_BANG_.call(null,inst_42494);
var inst_42496 = (inst_42488 + (1));
var tmp42574 = inst_42487;
var tmp42575 = inst_42486;
var tmp42576 = inst_42485;
var inst_42485__$1 = tmp42576;
var inst_42486__$1 = tmp42575;
var inst_42487__$1 = tmp42574;
var inst_42488__$1 = inst_42496;
var state_42544__$1 = (function (){var statearr_42580 = state_42544;
(statearr_42580[(12)] = inst_42487__$1);

(statearr_42580[(17)] = inst_42495);

(statearr_42580[(14)] = inst_42488__$1);

(statearr_42580[(15)] = inst_42486__$1);

(statearr_42580[(16)] = inst_42485__$1);

return statearr_42580;
})();
var statearr_42581_42615 = state_42544__$1;
(statearr_42581_42615[(2)] = null);

(statearr_42581_42615[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42545 === (18))){
var inst_42514 = (state_42544[(2)]);
var state_42544__$1 = state_42544;
var statearr_42582_42616 = state_42544__$1;
(statearr_42582_42616[(2)] = inst_42514);

(statearr_42582_42616[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42545 === (8))){
var inst_42487 = (state_42544[(12)]);
var inst_42488 = (state_42544[(14)]);
var inst_42490 = (inst_42488 < inst_42487);
var inst_42491 = inst_42490;
var state_42544__$1 = state_42544;
if(cljs.core.truth_(inst_42491)){
var statearr_42583_42617 = state_42544__$1;
(statearr_42583_42617[(1)] = (10));

} else {
var statearr_42584_42618 = state_42544__$1;
(statearr_42584_42618[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__35058__auto___42590,mults,ensure_mult,p))
;
return ((function (switch__34968__auto__,c__35058__auto___42590,mults,ensure_mult,p){
return (function() {
var cljs$core$async$state_machine__34969__auto__ = null;
var cljs$core$async$state_machine__34969__auto____0 = (function (){
var statearr_42585 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_42585[(0)] = cljs$core$async$state_machine__34969__auto__);

(statearr_42585[(1)] = (1));

return statearr_42585;
});
var cljs$core$async$state_machine__34969__auto____1 = (function (state_42544){
while(true){
var ret_value__34970__auto__ = (function (){try{while(true){
var result__34971__auto__ = switch__34968__auto__.call(null,state_42544);
if(cljs.core.keyword_identical_QMARK_.call(null,result__34971__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__34971__auto__;
}
break;
}
}catch (e42586){if((e42586 instanceof Object)){
var ex__34972__auto__ = e42586;
var statearr_42587_42619 = state_42544;
(statearr_42587_42619[(5)] = ex__34972__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_42544);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e42586;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__34970__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__42620 = state_42544;
state_42544 = G__42620;
continue;
} else {
return ret_value__34970__auto__;
}
break;
}
});
cljs$core$async$state_machine__34969__auto__ = function(state_42544){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__34969__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__34969__auto____1.call(this,state_42544);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__34969__auto____0;
cljs$core$async$state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__34969__auto____1;
return cljs$core$async$state_machine__34969__auto__;
})()
;})(switch__34968__auto__,c__35058__auto___42590,mults,ensure_mult,p))
})();
var state__35060__auto__ = (function (){var statearr_42588 = f__35059__auto__.call(null);
(statearr_42588[(6)] = c__35058__auto___42590);

return statearr_42588;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__35060__auto__);
});})(c__35058__auto___42590,mults,ensure_mult,p))
);


return p;
});

cljs.core.async.pub.cljs$lang$maxFixedArity = 3;

/**
 * Subscribes a channel to a topic of a pub.
 * 
 *   By default the channel will be closed when the source closes,
 *   but can be determined by the close? parameter.
 */
cljs.core.async.sub = (function cljs$core$async$sub(var_args){
var G__42622 = arguments.length;
switch (G__42622) {
case 3:
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.sub.cljs$core$IFn$_invoke$arity$3 = (function (p,topic,ch){
return cljs.core.async.sub.call(null,p,topic,ch,true);
});

cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4 = (function (p,topic,ch,close_QMARK_){
return cljs.core.async.sub_STAR_.call(null,p,topic,ch,close_QMARK_);
});

cljs.core.async.sub.cljs$lang$maxFixedArity = 4;

/**
 * Unsubscribes a channel from a topic of a pub
 */
cljs.core.async.unsub = (function cljs$core$async$unsub(p,topic,ch){
return cljs.core.async.unsub_STAR_.call(null,p,topic,ch);
});
/**
 * Unsubscribes all channels from a pub, or a topic of a pub
 */
cljs.core.async.unsub_all = (function cljs$core$async$unsub_all(var_args){
var G__42625 = arguments.length;
switch (G__42625) {
case 1:
return cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$1 = (function (p){
return cljs.core.async.unsub_all_STAR_.call(null,p);
});

cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$2 = (function (p,topic){
return cljs.core.async.unsub_all_STAR_.call(null,p,topic);
});

cljs.core.async.unsub_all.cljs$lang$maxFixedArity = 2;

/**
 * Takes a function and a collection of source channels, and returns a
 *   channel which contains the values produced by applying f to the set
 *   of first items taken from each source channel, followed by applying
 *   f to the set of second items from each channel, until any one of the
 *   channels is closed, at which point the output channel will be
 *   closed. The returned channel will be unbuffered by default, or a
 *   buf-or-n can be supplied
 */
cljs.core.async.map = (function cljs$core$async$map(var_args){
var G__42628 = arguments.length;
switch (G__42628) {
case 2:
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.map.cljs$core$IFn$_invoke$arity$2 = (function (f,chs){
return cljs.core.async.map.call(null,f,chs,null);
});

cljs.core.async.map.cljs$core$IFn$_invoke$arity$3 = (function (f,chs,buf_or_n){
var chs__$1 = cljs.core.vec.call(null,chs);
var out = cljs.core.async.chan.call(null,buf_or_n);
var cnt = cljs.core.count.call(null,chs__$1);
var rets = cljs.core.object_array.call(null,cnt);
var dchan = cljs.core.async.chan.call(null,(1));
var dctr = cljs.core.atom.call(null,null);
var done = cljs.core.mapv.call(null,((function (chs__$1,out,cnt,rets,dchan,dctr){
return (function (i){
return ((function (chs__$1,out,cnt,rets,dchan,dctr){
return (function (ret){
(rets[i] = ret);

if((cljs.core.swap_BANG_.call(null,dctr,cljs.core.dec) === (0))){
return cljs.core.async.put_BANG_.call(null,dchan,rets.slice((0)));
} else {
return null;
}
});
;})(chs__$1,out,cnt,rets,dchan,dctr))
});})(chs__$1,out,cnt,rets,dchan,dctr))
,cljs.core.range.call(null,cnt));
var c__35058__auto___42695 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__35058__auto___42695,chs__$1,out,cnt,rets,dchan,dctr,done){
return (function (){
var f__35059__auto__ = (function (){var switch__34968__auto__ = ((function (c__35058__auto___42695,chs__$1,out,cnt,rets,dchan,dctr,done){
return (function (state_42667){
var state_val_42668 = (state_42667[(1)]);
if((state_val_42668 === (7))){
var state_42667__$1 = state_42667;
var statearr_42669_42696 = state_42667__$1;
(statearr_42669_42696[(2)] = null);

(statearr_42669_42696[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42668 === (1))){
var state_42667__$1 = state_42667;
var statearr_42670_42697 = state_42667__$1;
(statearr_42670_42697[(2)] = null);

(statearr_42670_42697[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42668 === (4))){
var inst_42631 = (state_42667[(7)]);
var inst_42633 = (inst_42631 < cnt);
var state_42667__$1 = state_42667;
if(cljs.core.truth_(inst_42633)){
var statearr_42671_42698 = state_42667__$1;
(statearr_42671_42698[(1)] = (6));

} else {
var statearr_42672_42699 = state_42667__$1;
(statearr_42672_42699[(1)] = (7));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42668 === (15))){
var inst_42663 = (state_42667[(2)]);
var state_42667__$1 = state_42667;
var statearr_42673_42700 = state_42667__$1;
(statearr_42673_42700[(2)] = inst_42663);

(statearr_42673_42700[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42668 === (13))){
var inst_42656 = cljs.core.async.close_BANG_.call(null,out);
var state_42667__$1 = state_42667;
var statearr_42674_42701 = state_42667__$1;
(statearr_42674_42701[(2)] = inst_42656);

(statearr_42674_42701[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42668 === (6))){
var state_42667__$1 = state_42667;
var statearr_42675_42702 = state_42667__$1;
(statearr_42675_42702[(2)] = null);

(statearr_42675_42702[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42668 === (3))){
var inst_42665 = (state_42667[(2)]);
var state_42667__$1 = state_42667;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_42667__$1,inst_42665);
} else {
if((state_val_42668 === (12))){
var inst_42653 = (state_42667[(8)]);
var inst_42653__$1 = (state_42667[(2)]);
var inst_42654 = cljs.core.some.call(null,cljs.core.nil_QMARK_,inst_42653__$1);
var state_42667__$1 = (function (){var statearr_42676 = state_42667;
(statearr_42676[(8)] = inst_42653__$1);

return statearr_42676;
})();
if(cljs.core.truth_(inst_42654)){
var statearr_42677_42703 = state_42667__$1;
(statearr_42677_42703[(1)] = (13));

} else {
var statearr_42678_42704 = state_42667__$1;
(statearr_42678_42704[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42668 === (2))){
var inst_42630 = cljs.core.reset_BANG_.call(null,dctr,cnt);
var inst_42631 = (0);
var state_42667__$1 = (function (){var statearr_42679 = state_42667;
(statearr_42679[(7)] = inst_42631);

(statearr_42679[(9)] = inst_42630);

return statearr_42679;
})();
var statearr_42680_42705 = state_42667__$1;
(statearr_42680_42705[(2)] = null);

(statearr_42680_42705[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42668 === (11))){
var inst_42631 = (state_42667[(7)]);
var _ = cljs.core.async.impl.ioc_helpers.add_exception_frame.call(null,state_42667,(10),Object,null,(9));
var inst_42640 = chs__$1.call(null,inst_42631);
var inst_42641 = done.call(null,inst_42631);
var inst_42642 = cljs.core.async.take_BANG_.call(null,inst_42640,inst_42641);
var state_42667__$1 = state_42667;
var statearr_42681_42706 = state_42667__$1;
(statearr_42681_42706[(2)] = inst_42642);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_42667__$1);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42668 === (9))){
var inst_42631 = (state_42667[(7)]);
var inst_42644 = (state_42667[(2)]);
var inst_42645 = (inst_42631 + (1));
var inst_42631__$1 = inst_42645;
var state_42667__$1 = (function (){var statearr_42682 = state_42667;
(statearr_42682[(7)] = inst_42631__$1);

(statearr_42682[(10)] = inst_42644);

return statearr_42682;
})();
var statearr_42683_42707 = state_42667__$1;
(statearr_42683_42707[(2)] = null);

(statearr_42683_42707[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42668 === (5))){
var inst_42651 = (state_42667[(2)]);
var state_42667__$1 = (function (){var statearr_42684 = state_42667;
(statearr_42684[(11)] = inst_42651);

return statearr_42684;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_42667__$1,(12),dchan);
} else {
if((state_val_42668 === (14))){
var inst_42653 = (state_42667[(8)]);
var inst_42658 = cljs.core.apply.call(null,f,inst_42653);
var state_42667__$1 = state_42667;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_42667__$1,(16),out,inst_42658);
} else {
if((state_val_42668 === (16))){
var inst_42660 = (state_42667[(2)]);
var state_42667__$1 = (function (){var statearr_42685 = state_42667;
(statearr_42685[(12)] = inst_42660);

return statearr_42685;
})();
var statearr_42686_42708 = state_42667__$1;
(statearr_42686_42708[(2)] = null);

(statearr_42686_42708[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42668 === (10))){
var inst_42635 = (state_42667[(2)]);
var inst_42636 = cljs.core.swap_BANG_.call(null,dctr,cljs.core.dec);
var state_42667__$1 = (function (){var statearr_42687 = state_42667;
(statearr_42687[(13)] = inst_42635);

return statearr_42687;
})();
var statearr_42688_42709 = state_42667__$1;
(statearr_42688_42709[(2)] = inst_42636);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_42667__$1);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42668 === (8))){
var inst_42649 = (state_42667[(2)]);
var state_42667__$1 = state_42667;
var statearr_42689_42710 = state_42667__$1;
(statearr_42689_42710[(2)] = inst_42649);

(statearr_42689_42710[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__35058__auto___42695,chs__$1,out,cnt,rets,dchan,dctr,done))
;
return ((function (switch__34968__auto__,c__35058__auto___42695,chs__$1,out,cnt,rets,dchan,dctr,done){
return (function() {
var cljs$core$async$state_machine__34969__auto__ = null;
var cljs$core$async$state_machine__34969__auto____0 = (function (){
var statearr_42690 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_42690[(0)] = cljs$core$async$state_machine__34969__auto__);

(statearr_42690[(1)] = (1));

return statearr_42690;
});
var cljs$core$async$state_machine__34969__auto____1 = (function (state_42667){
while(true){
var ret_value__34970__auto__ = (function (){try{while(true){
var result__34971__auto__ = switch__34968__auto__.call(null,state_42667);
if(cljs.core.keyword_identical_QMARK_.call(null,result__34971__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__34971__auto__;
}
break;
}
}catch (e42691){if((e42691 instanceof Object)){
var ex__34972__auto__ = e42691;
var statearr_42692_42711 = state_42667;
(statearr_42692_42711[(5)] = ex__34972__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_42667);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e42691;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__34970__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__42712 = state_42667;
state_42667 = G__42712;
continue;
} else {
return ret_value__34970__auto__;
}
break;
}
});
cljs$core$async$state_machine__34969__auto__ = function(state_42667){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__34969__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__34969__auto____1.call(this,state_42667);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__34969__auto____0;
cljs$core$async$state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__34969__auto____1;
return cljs$core$async$state_machine__34969__auto__;
})()
;})(switch__34968__auto__,c__35058__auto___42695,chs__$1,out,cnt,rets,dchan,dctr,done))
})();
var state__35060__auto__ = (function (){var statearr_42693 = f__35059__auto__.call(null);
(statearr_42693[(6)] = c__35058__auto___42695);

return statearr_42693;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__35060__auto__);
});})(c__35058__auto___42695,chs__$1,out,cnt,rets,dchan,dctr,done))
);


return out;
});

cljs.core.async.map.cljs$lang$maxFixedArity = 3;

/**
 * Takes a collection of source channels and returns a channel which
 *   contains all values taken from them. The returned channel will be
 *   unbuffered by default, or a buf-or-n can be supplied. The channel
 *   will close after all the source channels have closed.
 */
cljs.core.async.merge = (function cljs$core$async$merge(var_args){
var G__42715 = arguments.length;
switch (G__42715) {
case 1:
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.merge.cljs$core$IFn$_invoke$arity$1 = (function (chs){
return cljs.core.async.merge.call(null,chs,null);
});

cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2 = (function (chs,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__35058__auto___42769 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__35058__auto___42769,out){
return (function (){
var f__35059__auto__ = (function (){var switch__34968__auto__ = ((function (c__35058__auto___42769,out){
return (function (state_42747){
var state_val_42748 = (state_42747[(1)]);
if((state_val_42748 === (7))){
var inst_42726 = (state_42747[(7)]);
var inst_42727 = (state_42747[(8)]);
var inst_42726__$1 = (state_42747[(2)]);
var inst_42727__$1 = cljs.core.nth.call(null,inst_42726__$1,(0),null);
var inst_42728 = cljs.core.nth.call(null,inst_42726__$1,(1),null);
var inst_42729 = (inst_42727__$1 == null);
var state_42747__$1 = (function (){var statearr_42749 = state_42747;
(statearr_42749[(7)] = inst_42726__$1);

(statearr_42749[(9)] = inst_42728);

(statearr_42749[(8)] = inst_42727__$1);

return statearr_42749;
})();
if(cljs.core.truth_(inst_42729)){
var statearr_42750_42770 = state_42747__$1;
(statearr_42750_42770[(1)] = (8));

} else {
var statearr_42751_42771 = state_42747__$1;
(statearr_42751_42771[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42748 === (1))){
var inst_42716 = cljs.core.vec.call(null,chs);
var inst_42717 = inst_42716;
var state_42747__$1 = (function (){var statearr_42752 = state_42747;
(statearr_42752[(10)] = inst_42717);

return statearr_42752;
})();
var statearr_42753_42772 = state_42747__$1;
(statearr_42753_42772[(2)] = null);

(statearr_42753_42772[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42748 === (4))){
var inst_42717 = (state_42747[(10)]);
var state_42747__$1 = state_42747;
return cljs.core.async.ioc_alts_BANG_.call(null,state_42747__$1,(7),inst_42717);
} else {
if((state_val_42748 === (6))){
var inst_42743 = (state_42747[(2)]);
var state_42747__$1 = state_42747;
var statearr_42754_42773 = state_42747__$1;
(statearr_42754_42773[(2)] = inst_42743);

(statearr_42754_42773[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42748 === (3))){
var inst_42745 = (state_42747[(2)]);
var state_42747__$1 = state_42747;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_42747__$1,inst_42745);
} else {
if((state_val_42748 === (2))){
var inst_42717 = (state_42747[(10)]);
var inst_42719 = cljs.core.count.call(null,inst_42717);
var inst_42720 = (inst_42719 > (0));
var state_42747__$1 = state_42747;
if(cljs.core.truth_(inst_42720)){
var statearr_42756_42774 = state_42747__$1;
(statearr_42756_42774[(1)] = (4));

} else {
var statearr_42757_42775 = state_42747__$1;
(statearr_42757_42775[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42748 === (11))){
var inst_42717 = (state_42747[(10)]);
var inst_42736 = (state_42747[(2)]);
var tmp42755 = inst_42717;
var inst_42717__$1 = tmp42755;
var state_42747__$1 = (function (){var statearr_42758 = state_42747;
(statearr_42758[(10)] = inst_42717__$1);

(statearr_42758[(11)] = inst_42736);

return statearr_42758;
})();
var statearr_42759_42776 = state_42747__$1;
(statearr_42759_42776[(2)] = null);

(statearr_42759_42776[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42748 === (9))){
var inst_42727 = (state_42747[(8)]);
var state_42747__$1 = state_42747;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_42747__$1,(11),out,inst_42727);
} else {
if((state_val_42748 === (5))){
var inst_42741 = cljs.core.async.close_BANG_.call(null,out);
var state_42747__$1 = state_42747;
var statearr_42760_42777 = state_42747__$1;
(statearr_42760_42777[(2)] = inst_42741);

(statearr_42760_42777[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42748 === (10))){
var inst_42739 = (state_42747[(2)]);
var state_42747__$1 = state_42747;
var statearr_42761_42778 = state_42747__$1;
(statearr_42761_42778[(2)] = inst_42739);

(statearr_42761_42778[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42748 === (8))){
var inst_42717 = (state_42747[(10)]);
var inst_42726 = (state_42747[(7)]);
var inst_42728 = (state_42747[(9)]);
var inst_42727 = (state_42747[(8)]);
var inst_42731 = (function (){var cs = inst_42717;
var vec__42722 = inst_42726;
var v = inst_42727;
var c = inst_42728;
return ((function (cs,vec__42722,v,c,inst_42717,inst_42726,inst_42728,inst_42727,state_val_42748,c__35058__auto___42769,out){
return (function (p1__42713_SHARP_){
return cljs.core.not_EQ_.call(null,c,p1__42713_SHARP_);
});
;})(cs,vec__42722,v,c,inst_42717,inst_42726,inst_42728,inst_42727,state_val_42748,c__35058__auto___42769,out))
})();
var inst_42732 = cljs.core.filterv.call(null,inst_42731,inst_42717);
var inst_42717__$1 = inst_42732;
var state_42747__$1 = (function (){var statearr_42762 = state_42747;
(statearr_42762[(10)] = inst_42717__$1);

return statearr_42762;
})();
var statearr_42763_42779 = state_42747__$1;
(statearr_42763_42779[(2)] = null);

(statearr_42763_42779[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__35058__auto___42769,out))
;
return ((function (switch__34968__auto__,c__35058__auto___42769,out){
return (function() {
var cljs$core$async$state_machine__34969__auto__ = null;
var cljs$core$async$state_machine__34969__auto____0 = (function (){
var statearr_42764 = [null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_42764[(0)] = cljs$core$async$state_machine__34969__auto__);

(statearr_42764[(1)] = (1));

return statearr_42764;
});
var cljs$core$async$state_machine__34969__auto____1 = (function (state_42747){
while(true){
var ret_value__34970__auto__ = (function (){try{while(true){
var result__34971__auto__ = switch__34968__auto__.call(null,state_42747);
if(cljs.core.keyword_identical_QMARK_.call(null,result__34971__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__34971__auto__;
}
break;
}
}catch (e42765){if((e42765 instanceof Object)){
var ex__34972__auto__ = e42765;
var statearr_42766_42780 = state_42747;
(statearr_42766_42780[(5)] = ex__34972__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_42747);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e42765;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__34970__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__42781 = state_42747;
state_42747 = G__42781;
continue;
} else {
return ret_value__34970__auto__;
}
break;
}
});
cljs$core$async$state_machine__34969__auto__ = function(state_42747){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__34969__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__34969__auto____1.call(this,state_42747);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__34969__auto____0;
cljs$core$async$state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__34969__auto____1;
return cljs$core$async$state_machine__34969__auto__;
})()
;})(switch__34968__auto__,c__35058__auto___42769,out))
})();
var state__35060__auto__ = (function (){var statearr_42767 = f__35059__auto__.call(null);
(statearr_42767[(6)] = c__35058__auto___42769);

return statearr_42767;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__35060__auto__);
});})(c__35058__auto___42769,out))
);


return out;
});

cljs.core.async.merge.cljs$lang$maxFixedArity = 2;

/**
 * Returns a channel containing the single (collection) result of the
 *   items taken from the channel conjoined to the supplied
 *   collection. ch must close before into produces a result.
 */
cljs.core.async.into = (function cljs$core$async$into(coll,ch){
return cljs.core.async.reduce.call(null,cljs.core.conj,coll,ch);
});
/**
 * Returns a channel that will return, at most, n items from ch. After n items
 * have been returned, or ch has been closed, the return chanel will close.
 * 
 *   The output channel is unbuffered by default, unless buf-or-n is given.
 */
cljs.core.async.take = (function cljs$core$async$take(var_args){
var G__42783 = arguments.length;
switch (G__42783) {
case 2:
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.take.cljs$core$IFn$_invoke$arity$2 = (function (n,ch){
return cljs.core.async.take.call(null,n,ch,null);
});

cljs.core.async.take.cljs$core$IFn$_invoke$arity$3 = (function (n,ch,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__35058__auto___42828 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__35058__auto___42828,out){
return (function (){
var f__35059__auto__ = (function (){var switch__34968__auto__ = ((function (c__35058__auto___42828,out){
return (function (state_42807){
var state_val_42808 = (state_42807[(1)]);
if((state_val_42808 === (7))){
var inst_42789 = (state_42807[(7)]);
var inst_42789__$1 = (state_42807[(2)]);
var inst_42790 = (inst_42789__$1 == null);
var inst_42791 = cljs.core.not.call(null,inst_42790);
var state_42807__$1 = (function (){var statearr_42809 = state_42807;
(statearr_42809[(7)] = inst_42789__$1);

return statearr_42809;
})();
if(inst_42791){
var statearr_42810_42829 = state_42807__$1;
(statearr_42810_42829[(1)] = (8));

} else {
var statearr_42811_42830 = state_42807__$1;
(statearr_42811_42830[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42808 === (1))){
var inst_42784 = (0);
var state_42807__$1 = (function (){var statearr_42812 = state_42807;
(statearr_42812[(8)] = inst_42784);

return statearr_42812;
})();
var statearr_42813_42831 = state_42807__$1;
(statearr_42813_42831[(2)] = null);

(statearr_42813_42831[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42808 === (4))){
var state_42807__$1 = state_42807;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_42807__$1,(7),ch);
} else {
if((state_val_42808 === (6))){
var inst_42802 = (state_42807[(2)]);
var state_42807__$1 = state_42807;
var statearr_42814_42832 = state_42807__$1;
(statearr_42814_42832[(2)] = inst_42802);

(statearr_42814_42832[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42808 === (3))){
var inst_42804 = (state_42807[(2)]);
var inst_42805 = cljs.core.async.close_BANG_.call(null,out);
var state_42807__$1 = (function (){var statearr_42815 = state_42807;
(statearr_42815[(9)] = inst_42804);

return statearr_42815;
})();
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_42807__$1,inst_42805);
} else {
if((state_val_42808 === (2))){
var inst_42784 = (state_42807[(8)]);
var inst_42786 = (inst_42784 < n);
var state_42807__$1 = state_42807;
if(cljs.core.truth_(inst_42786)){
var statearr_42816_42833 = state_42807__$1;
(statearr_42816_42833[(1)] = (4));

} else {
var statearr_42817_42834 = state_42807__$1;
(statearr_42817_42834[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42808 === (11))){
var inst_42784 = (state_42807[(8)]);
var inst_42794 = (state_42807[(2)]);
var inst_42795 = (inst_42784 + (1));
var inst_42784__$1 = inst_42795;
var state_42807__$1 = (function (){var statearr_42818 = state_42807;
(statearr_42818[(10)] = inst_42794);

(statearr_42818[(8)] = inst_42784__$1);

return statearr_42818;
})();
var statearr_42819_42835 = state_42807__$1;
(statearr_42819_42835[(2)] = null);

(statearr_42819_42835[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42808 === (9))){
var state_42807__$1 = state_42807;
var statearr_42820_42836 = state_42807__$1;
(statearr_42820_42836[(2)] = null);

(statearr_42820_42836[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42808 === (5))){
var state_42807__$1 = state_42807;
var statearr_42821_42837 = state_42807__$1;
(statearr_42821_42837[(2)] = null);

(statearr_42821_42837[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42808 === (10))){
var inst_42799 = (state_42807[(2)]);
var state_42807__$1 = state_42807;
var statearr_42822_42838 = state_42807__$1;
(statearr_42822_42838[(2)] = inst_42799);

(statearr_42822_42838[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42808 === (8))){
var inst_42789 = (state_42807[(7)]);
var state_42807__$1 = state_42807;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_42807__$1,(11),out,inst_42789);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__35058__auto___42828,out))
;
return ((function (switch__34968__auto__,c__35058__auto___42828,out){
return (function() {
var cljs$core$async$state_machine__34969__auto__ = null;
var cljs$core$async$state_machine__34969__auto____0 = (function (){
var statearr_42823 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_42823[(0)] = cljs$core$async$state_machine__34969__auto__);

(statearr_42823[(1)] = (1));

return statearr_42823;
});
var cljs$core$async$state_machine__34969__auto____1 = (function (state_42807){
while(true){
var ret_value__34970__auto__ = (function (){try{while(true){
var result__34971__auto__ = switch__34968__auto__.call(null,state_42807);
if(cljs.core.keyword_identical_QMARK_.call(null,result__34971__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__34971__auto__;
}
break;
}
}catch (e42824){if((e42824 instanceof Object)){
var ex__34972__auto__ = e42824;
var statearr_42825_42839 = state_42807;
(statearr_42825_42839[(5)] = ex__34972__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_42807);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e42824;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__34970__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__42840 = state_42807;
state_42807 = G__42840;
continue;
} else {
return ret_value__34970__auto__;
}
break;
}
});
cljs$core$async$state_machine__34969__auto__ = function(state_42807){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__34969__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__34969__auto____1.call(this,state_42807);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__34969__auto____0;
cljs$core$async$state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__34969__auto____1;
return cljs$core$async$state_machine__34969__auto__;
})()
;})(switch__34968__auto__,c__35058__auto___42828,out))
})();
var state__35060__auto__ = (function (){var statearr_42826 = f__35059__auto__.call(null);
(statearr_42826[(6)] = c__35058__auto___42828);

return statearr_42826;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__35060__auto__);
});})(c__35058__auto___42828,out))
);


return out;
});

cljs.core.async.take.cljs$lang$maxFixedArity = 3;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.map_LT_ = (function cljs$core$async$map_LT_(f,ch){
if(typeof cljs.core.async.t_cljs$core$async42842 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async42842 = (function (f,ch,meta42843){
this.f = f;
this.ch = ch;
this.meta42843 = meta42843;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async42842.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_42844,meta42843__$1){
var self__ = this;
var _42844__$1 = this;
return (new cljs.core.async.t_cljs$core$async42842(self__.f,self__.ch,meta42843__$1));
});

cljs.core.async.t_cljs$core$async42842.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_42844){
var self__ = this;
var _42844__$1 = this;
return self__.meta42843;
});

cljs.core.async.t_cljs$core$async42842.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async42842.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_.call(null,self__.ch);
});

cljs.core.async.t_cljs$core$async42842.prototype.cljs$core$async$impl$protocols$Channel$closed_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.closed_QMARK_.call(null,self__.ch);
});

cljs.core.async.t_cljs$core$async42842.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async42842.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
var ret = cljs.core.async.impl.protocols.take_BANG_.call(null,self__.ch,(function (){
if(typeof cljs.core.async.t_cljs$core$async42845 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async42845 = (function (f,ch,meta42843,_,fn1,meta42846){
this.f = f;
this.ch = ch;
this.meta42843 = meta42843;
this._ = _;
this.fn1 = fn1;
this.meta42846 = meta42846;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async42845.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (___$1){
return (function (_42847,meta42846__$1){
var self__ = this;
var _42847__$1 = this;
return (new cljs.core.async.t_cljs$core$async42845(self__.f,self__.ch,self__.meta42843,self__._,self__.fn1,meta42846__$1));
});})(___$1))
;

cljs.core.async.t_cljs$core$async42845.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (___$1){
return (function (_42847){
var self__ = this;
var _42847__$1 = this;
return self__.meta42846;
});})(___$1))
;

cljs.core.async.t_cljs$core$async42845.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async42845.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = ((function (___$1){
return (function (___$1){
var self__ = this;
var ___$2 = this;
return cljs.core.async.impl.protocols.active_QMARK_.call(null,self__.fn1);
});})(___$1))
;

cljs.core.async.t_cljs$core$async42845.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = ((function (___$1){
return (function (___$1){
var self__ = this;
var ___$2 = this;
return true;
});})(___$1))
;

cljs.core.async.t_cljs$core$async42845.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = ((function (___$1){
return (function (___$1){
var self__ = this;
var ___$2 = this;
var f1 = cljs.core.async.impl.protocols.commit.call(null,self__.fn1);
return ((function (f1,___$2,___$1){
return (function (p1__42841_SHARP_){
return f1.call(null,(((p1__42841_SHARP_ == null))?null:self__.f.call(null,p1__42841_SHARP_)));
});
;})(f1,___$2,___$1))
});})(___$1))
;

cljs.core.async.t_cljs$core$async42845.getBasis = ((function (___$1){
return (function (){
return new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta42843","meta42843",-1067059318,null),cljs.core.with_meta(new cljs.core.Symbol(null,"_","_",-1201019570,null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"tag","tag",-1290361223),new cljs.core.Symbol("cljs.core.async","t_cljs$core$async42842","cljs.core.async/t_cljs$core$async42842",904256086,null)], null)),new cljs.core.Symbol(null,"fn1","fn1",895834444,null),new cljs.core.Symbol(null,"meta42846","meta42846",-474369029,null)], null);
});})(___$1))
;

cljs.core.async.t_cljs$core$async42845.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async42845.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async42845";

cljs.core.async.t_cljs$core$async42845.cljs$lang$ctorPrWriter = ((function (___$1){
return (function (this__30853__auto__,writer__30854__auto__,opt__30855__auto__){
return cljs.core._write.call(null,writer__30854__auto__,"cljs.core.async/t_cljs$core$async42845");
});})(___$1))
;

cljs.core.async.__GT_t_cljs$core$async42845 = ((function (___$1){
return (function cljs$core$async$map_LT__$___GT_t_cljs$core$async42845(f__$1,ch__$1,meta42843__$1,___$2,fn1__$1,meta42846){
return (new cljs.core.async.t_cljs$core$async42845(f__$1,ch__$1,meta42843__$1,___$2,fn1__$1,meta42846));
});})(___$1))
;

}

return (new cljs.core.async.t_cljs$core$async42845(self__.f,self__.ch,self__.meta42843,___$1,fn1,cljs.core.PersistentArrayMap.EMPTY));
})()
);
if(cljs.core.truth_((function (){var and__30170__auto__ = ret;
if(cljs.core.truth_(and__30170__auto__)){
return !((cljs.core.deref.call(null,ret) == null));
} else {
return and__30170__auto__;
}
})())){
return cljs.core.async.impl.channels.box.call(null,self__.f.call(null,cljs.core.deref.call(null,ret)));
} else {
return ret;
}
});

cljs.core.async.t_cljs$core$async42842.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async42842.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.put_BANG_.call(null,self__.ch,val,fn1);
});

cljs.core.async.t_cljs$core$async42842.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta42843","meta42843",-1067059318,null)], null);
});

cljs.core.async.t_cljs$core$async42842.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async42842.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async42842";

cljs.core.async.t_cljs$core$async42842.cljs$lang$ctorPrWriter = (function (this__30853__auto__,writer__30854__auto__,opt__30855__auto__){
return cljs.core._write.call(null,writer__30854__auto__,"cljs.core.async/t_cljs$core$async42842");
});

cljs.core.async.__GT_t_cljs$core$async42842 = (function cljs$core$async$map_LT__$___GT_t_cljs$core$async42842(f__$1,ch__$1,meta42843){
return (new cljs.core.async.t_cljs$core$async42842(f__$1,ch__$1,meta42843));
});

}

return (new cljs.core.async.t_cljs$core$async42842(f,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.map_GT_ = (function cljs$core$async$map_GT_(f,ch){
if(typeof cljs.core.async.t_cljs$core$async42848 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async42848 = (function (f,ch,meta42849){
this.f = f;
this.ch = ch;
this.meta42849 = meta42849;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async42848.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_42850,meta42849__$1){
var self__ = this;
var _42850__$1 = this;
return (new cljs.core.async.t_cljs$core$async42848(self__.f,self__.ch,meta42849__$1));
});

cljs.core.async.t_cljs$core$async42848.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_42850){
var self__ = this;
var _42850__$1 = this;
return self__.meta42849;
});

cljs.core.async.t_cljs$core$async42848.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async42848.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_.call(null,self__.ch);
});

cljs.core.async.t_cljs$core$async42848.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async42848.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.take_BANG_.call(null,self__.ch,fn1);
});

cljs.core.async.t_cljs$core$async42848.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async42848.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.put_BANG_.call(null,self__.ch,self__.f.call(null,val),fn1);
});

cljs.core.async.t_cljs$core$async42848.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta42849","meta42849",-1510360325,null)], null);
});

cljs.core.async.t_cljs$core$async42848.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async42848.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async42848";

cljs.core.async.t_cljs$core$async42848.cljs$lang$ctorPrWriter = (function (this__30853__auto__,writer__30854__auto__,opt__30855__auto__){
return cljs.core._write.call(null,writer__30854__auto__,"cljs.core.async/t_cljs$core$async42848");
});

cljs.core.async.__GT_t_cljs$core$async42848 = (function cljs$core$async$map_GT__$___GT_t_cljs$core$async42848(f__$1,ch__$1,meta42849){
return (new cljs.core.async.t_cljs$core$async42848(f__$1,ch__$1,meta42849));
});

}

return (new cljs.core.async.t_cljs$core$async42848(f,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.filter_GT_ = (function cljs$core$async$filter_GT_(p,ch){
if(typeof cljs.core.async.t_cljs$core$async42851 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async42851 = (function (p,ch,meta42852){
this.p = p;
this.ch = ch;
this.meta42852 = meta42852;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
cljs.core.async.t_cljs$core$async42851.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_42853,meta42852__$1){
var self__ = this;
var _42853__$1 = this;
return (new cljs.core.async.t_cljs$core$async42851(self__.p,self__.ch,meta42852__$1));
});

cljs.core.async.t_cljs$core$async42851.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_42853){
var self__ = this;
var _42853__$1 = this;
return self__.meta42852;
});

cljs.core.async.t_cljs$core$async42851.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async42851.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_.call(null,self__.ch);
});

cljs.core.async.t_cljs$core$async42851.prototype.cljs$core$async$impl$protocols$Channel$closed_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.closed_QMARK_.call(null,self__.ch);
});

cljs.core.async.t_cljs$core$async42851.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async42851.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.take_BANG_.call(null,self__.ch,fn1);
});

cljs.core.async.t_cljs$core$async42851.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL;

cljs.core.async.t_cljs$core$async42851.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
if(cljs.core.truth_(self__.p.call(null,val))){
return cljs.core.async.impl.protocols.put_BANG_.call(null,self__.ch,val,fn1);
} else {
return cljs.core.async.impl.channels.box.call(null,cljs.core.not.call(null,cljs.core.async.impl.protocols.closed_QMARK_.call(null,self__.ch)));
}
});

cljs.core.async.t_cljs$core$async42851.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"p","p",1791580836,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta42852","meta42852",-1581161347,null)], null);
});

cljs.core.async.t_cljs$core$async42851.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async42851.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async42851";

cljs.core.async.t_cljs$core$async42851.cljs$lang$ctorPrWriter = (function (this__30853__auto__,writer__30854__auto__,opt__30855__auto__){
return cljs.core._write.call(null,writer__30854__auto__,"cljs.core.async/t_cljs$core$async42851");
});

cljs.core.async.__GT_t_cljs$core$async42851 = (function cljs$core$async$filter_GT__$___GT_t_cljs$core$async42851(p__$1,ch__$1,meta42852){
return (new cljs.core.async.t_cljs$core$async42851(p__$1,ch__$1,meta42852));
});

}

return (new cljs.core.async.t_cljs$core$async42851(p,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.remove_GT_ = (function cljs$core$async$remove_GT_(p,ch){
return cljs.core.async.filter_GT_.call(null,cljs.core.complement.call(null,p),ch);
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.filter_LT_ = (function cljs$core$async$filter_LT_(var_args){
var G__42855 = arguments.length;
switch (G__42855) {
case 2:
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.filter_LT_.call(null,p,ch,null);
});

cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3 = (function (p,ch,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__35058__auto___42895 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__35058__auto___42895,out){
return (function (){
var f__35059__auto__ = (function (){var switch__34968__auto__ = ((function (c__35058__auto___42895,out){
return (function (state_42876){
var state_val_42877 = (state_42876[(1)]);
if((state_val_42877 === (7))){
var inst_42872 = (state_42876[(2)]);
var state_42876__$1 = state_42876;
var statearr_42878_42896 = state_42876__$1;
(statearr_42878_42896[(2)] = inst_42872);

(statearr_42878_42896[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42877 === (1))){
var state_42876__$1 = state_42876;
var statearr_42879_42897 = state_42876__$1;
(statearr_42879_42897[(2)] = null);

(statearr_42879_42897[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42877 === (4))){
var inst_42858 = (state_42876[(7)]);
var inst_42858__$1 = (state_42876[(2)]);
var inst_42859 = (inst_42858__$1 == null);
var state_42876__$1 = (function (){var statearr_42880 = state_42876;
(statearr_42880[(7)] = inst_42858__$1);

return statearr_42880;
})();
if(cljs.core.truth_(inst_42859)){
var statearr_42881_42898 = state_42876__$1;
(statearr_42881_42898[(1)] = (5));

} else {
var statearr_42882_42899 = state_42876__$1;
(statearr_42882_42899[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42877 === (6))){
var inst_42858 = (state_42876[(7)]);
var inst_42863 = p.call(null,inst_42858);
var state_42876__$1 = state_42876;
if(cljs.core.truth_(inst_42863)){
var statearr_42883_42900 = state_42876__$1;
(statearr_42883_42900[(1)] = (8));

} else {
var statearr_42884_42901 = state_42876__$1;
(statearr_42884_42901[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42877 === (3))){
var inst_42874 = (state_42876[(2)]);
var state_42876__$1 = state_42876;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_42876__$1,inst_42874);
} else {
if((state_val_42877 === (2))){
var state_42876__$1 = state_42876;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_42876__$1,(4),ch);
} else {
if((state_val_42877 === (11))){
var inst_42866 = (state_42876[(2)]);
var state_42876__$1 = state_42876;
var statearr_42885_42902 = state_42876__$1;
(statearr_42885_42902[(2)] = inst_42866);

(statearr_42885_42902[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42877 === (9))){
var state_42876__$1 = state_42876;
var statearr_42886_42903 = state_42876__$1;
(statearr_42886_42903[(2)] = null);

(statearr_42886_42903[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42877 === (5))){
var inst_42861 = cljs.core.async.close_BANG_.call(null,out);
var state_42876__$1 = state_42876;
var statearr_42887_42904 = state_42876__$1;
(statearr_42887_42904[(2)] = inst_42861);

(statearr_42887_42904[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42877 === (10))){
var inst_42869 = (state_42876[(2)]);
var state_42876__$1 = (function (){var statearr_42888 = state_42876;
(statearr_42888[(8)] = inst_42869);

return statearr_42888;
})();
var statearr_42889_42905 = state_42876__$1;
(statearr_42889_42905[(2)] = null);

(statearr_42889_42905[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42877 === (8))){
var inst_42858 = (state_42876[(7)]);
var state_42876__$1 = state_42876;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_42876__$1,(11),out,inst_42858);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__35058__auto___42895,out))
;
return ((function (switch__34968__auto__,c__35058__auto___42895,out){
return (function() {
var cljs$core$async$state_machine__34969__auto__ = null;
var cljs$core$async$state_machine__34969__auto____0 = (function (){
var statearr_42890 = [null,null,null,null,null,null,null,null,null];
(statearr_42890[(0)] = cljs$core$async$state_machine__34969__auto__);

(statearr_42890[(1)] = (1));

return statearr_42890;
});
var cljs$core$async$state_machine__34969__auto____1 = (function (state_42876){
while(true){
var ret_value__34970__auto__ = (function (){try{while(true){
var result__34971__auto__ = switch__34968__auto__.call(null,state_42876);
if(cljs.core.keyword_identical_QMARK_.call(null,result__34971__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__34971__auto__;
}
break;
}
}catch (e42891){if((e42891 instanceof Object)){
var ex__34972__auto__ = e42891;
var statearr_42892_42906 = state_42876;
(statearr_42892_42906[(5)] = ex__34972__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_42876);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e42891;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__34970__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__42907 = state_42876;
state_42876 = G__42907;
continue;
} else {
return ret_value__34970__auto__;
}
break;
}
});
cljs$core$async$state_machine__34969__auto__ = function(state_42876){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__34969__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__34969__auto____1.call(this,state_42876);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__34969__auto____0;
cljs$core$async$state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__34969__auto____1;
return cljs$core$async$state_machine__34969__auto__;
})()
;})(switch__34968__auto__,c__35058__auto___42895,out))
})();
var state__35060__auto__ = (function (){var statearr_42893 = f__35059__auto__.call(null);
(statearr_42893[(6)] = c__35058__auto___42895);

return statearr_42893;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__35060__auto__);
});})(c__35058__auto___42895,out))
);


return out;
});

cljs.core.async.filter_LT_.cljs$lang$maxFixedArity = 3;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.remove_LT_ = (function cljs$core$async$remove_LT_(var_args){
var G__42909 = arguments.length;
switch (G__42909) {
case 2:
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.remove_LT_.call(null,p,ch,null);
});

cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3 = (function (p,ch,buf_or_n){
return cljs.core.async.filter_LT_.call(null,cljs.core.complement.call(null,p),ch,buf_or_n);
});

cljs.core.async.remove_LT_.cljs$lang$maxFixedArity = 3;

cljs.core.async.mapcat_STAR_ = (function cljs$core$async$mapcat_STAR_(f,in$,out){
var c__35058__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__35058__auto__){
return (function (){
var f__35059__auto__ = (function (){var switch__34968__auto__ = ((function (c__35058__auto__){
return (function (state_42972){
var state_val_42973 = (state_42972[(1)]);
if((state_val_42973 === (7))){
var inst_42968 = (state_42972[(2)]);
var state_42972__$1 = state_42972;
var statearr_42974_43012 = state_42972__$1;
(statearr_42974_43012[(2)] = inst_42968);

(statearr_42974_43012[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42973 === (20))){
var inst_42938 = (state_42972[(7)]);
var inst_42949 = (state_42972[(2)]);
var inst_42950 = cljs.core.next.call(null,inst_42938);
var inst_42924 = inst_42950;
var inst_42925 = null;
var inst_42926 = (0);
var inst_42927 = (0);
var state_42972__$1 = (function (){var statearr_42975 = state_42972;
(statearr_42975[(8)] = inst_42949);

(statearr_42975[(9)] = inst_42926);

(statearr_42975[(10)] = inst_42927);

(statearr_42975[(11)] = inst_42925);

(statearr_42975[(12)] = inst_42924);

return statearr_42975;
})();
var statearr_42976_43013 = state_42972__$1;
(statearr_42976_43013[(2)] = null);

(statearr_42976_43013[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42973 === (1))){
var state_42972__$1 = state_42972;
var statearr_42977_43014 = state_42972__$1;
(statearr_42977_43014[(2)] = null);

(statearr_42977_43014[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42973 === (4))){
var inst_42913 = (state_42972[(13)]);
var inst_42913__$1 = (state_42972[(2)]);
var inst_42914 = (inst_42913__$1 == null);
var state_42972__$1 = (function (){var statearr_42978 = state_42972;
(statearr_42978[(13)] = inst_42913__$1);

return statearr_42978;
})();
if(cljs.core.truth_(inst_42914)){
var statearr_42979_43015 = state_42972__$1;
(statearr_42979_43015[(1)] = (5));

} else {
var statearr_42980_43016 = state_42972__$1;
(statearr_42980_43016[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42973 === (15))){
var state_42972__$1 = state_42972;
var statearr_42984_43017 = state_42972__$1;
(statearr_42984_43017[(2)] = null);

(statearr_42984_43017[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42973 === (21))){
var state_42972__$1 = state_42972;
var statearr_42985_43018 = state_42972__$1;
(statearr_42985_43018[(2)] = null);

(statearr_42985_43018[(1)] = (23));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42973 === (13))){
var inst_42926 = (state_42972[(9)]);
var inst_42927 = (state_42972[(10)]);
var inst_42925 = (state_42972[(11)]);
var inst_42924 = (state_42972[(12)]);
var inst_42934 = (state_42972[(2)]);
var inst_42935 = (inst_42927 + (1));
var tmp42981 = inst_42926;
var tmp42982 = inst_42925;
var tmp42983 = inst_42924;
var inst_42924__$1 = tmp42983;
var inst_42925__$1 = tmp42982;
var inst_42926__$1 = tmp42981;
var inst_42927__$1 = inst_42935;
var state_42972__$1 = (function (){var statearr_42986 = state_42972;
(statearr_42986[(9)] = inst_42926__$1);

(statearr_42986[(10)] = inst_42927__$1);

(statearr_42986[(14)] = inst_42934);

(statearr_42986[(11)] = inst_42925__$1);

(statearr_42986[(12)] = inst_42924__$1);

return statearr_42986;
})();
var statearr_42987_43019 = state_42972__$1;
(statearr_42987_43019[(2)] = null);

(statearr_42987_43019[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42973 === (22))){
var state_42972__$1 = state_42972;
var statearr_42988_43020 = state_42972__$1;
(statearr_42988_43020[(2)] = null);

(statearr_42988_43020[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42973 === (6))){
var inst_42913 = (state_42972[(13)]);
var inst_42922 = f.call(null,inst_42913);
var inst_42923 = cljs.core.seq.call(null,inst_42922);
var inst_42924 = inst_42923;
var inst_42925 = null;
var inst_42926 = (0);
var inst_42927 = (0);
var state_42972__$1 = (function (){var statearr_42989 = state_42972;
(statearr_42989[(9)] = inst_42926);

(statearr_42989[(10)] = inst_42927);

(statearr_42989[(11)] = inst_42925);

(statearr_42989[(12)] = inst_42924);

return statearr_42989;
})();
var statearr_42990_43021 = state_42972__$1;
(statearr_42990_43021[(2)] = null);

(statearr_42990_43021[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42973 === (17))){
var inst_42938 = (state_42972[(7)]);
var inst_42942 = cljs.core.chunk_first.call(null,inst_42938);
var inst_42943 = cljs.core.chunk_rest.call(null,inst_42938);
var inst_42944 = cljs.core.count.call(null,inst_42942);
var inst_42924 = inst_42943;
var inst_42925 = inst_42942;
var inst_42926 = inst_42944;
var inst_42927 = (0);
var state_42972__$1 = (function (){var statearr_42991 = state_42972;
(statearr_42991[(9)] = inst_42926);

(statearr_42991[(10)] = inst_42927);

(statearr_42991[(11)] = inst_42925);

(statearr_42991[(12)] = inst_42924);

return statearr_42991;
})();
var statearr_42992_43022 = state_42972__$1;
(statearr_42992_43022[(2)] = null);

(statearr_42992_43022[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42973 === (3))){
var inst_42970 = (state_42972[(2)]);
var state_42972__$1 = state_42972;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_42972__$1,inst_42970);
} else {
if((state_val_42973 === (12))){
var inst_42958 = (state_42972[(2)]);
var state_42972__$1 = state_42972;
var statearr_42993_43023 = state_42972__$1;
(statearr_42993_43023[(2)] = inst_42958);

(statearr_42993_43023[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42973 === (2))){
var state_42972__$1 = state_42972;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_42972__$1,(4),in$);
} else {
if((state_val_42973 === (23))){
var inst_42966 = (state_42972[(2)]);
var state_42972__$1 = state_42972;
var statearr_42994_43024 = state_42972__$1;
(statearr_42994_43024[(2)] = inst_42966);

(statearr_42994_43024[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42973 === (19))){
var inst_42953 = (state_42972[(2)]);
var state_42972__$1 = state_42972;
var statearr_42995_43025 = state_42972__$1;
(statearr_42995_43025[(2)] = inst_42953);

(statearr_42995_43025[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42973 === (11))){
var inst_42938 = (state_42972[(7)]);
var inst_42924 = (state_42972[(12)]);
var inst_42938__$1 = cljs.core.seq.call(null,inst_42924);
var state_42972__$1 = (function (){var statearr_42996 = state_42972;
(statearr_42996[(7)] = inst_42938__$1);

return statearr_42996;
})();
if(inst_42938__$1){
var statearr_42997_43026 = state_42972__$1;
(statearr_42997_43026[(1)] = (14));

} else {
var statearr_42998_43027 = state_42972__$1;
(statearr_42998_43027[(1)] = (15));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42973 === (9))){
var inst_42960 = (state_42972[(2)]);
var inst_42961 = cljs.core.async.impl.protocols.closed_QMARK_.call(null,out);
var state_42972__$1 = (function (){var statearr_42999 = state_42972;
(statearr_42999[(15)] = inst_42960);

return statearr_42999;
})();
if(cljs.core.truth_(inst_42961)){
var statearr_43000_43028 = state_42972__$1;
(statearr_43000_43028[(1)] = (21));

} else {
var statearr_43001_43029 = state_42972__$1;
(statearr_43001_43029[(1)] = (22));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42973 === (5))){
var inst_42916 = cljs.core.async.close_BANG_.call(null,out);
var state_42972__$1 = state_42972;
var statearr_43002_43030 = state_42972__$1;
(statearr_43002_43030[(2)] = inst_42916);

(statearr_43002_43030[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42973 === (14))){
var inst_42938 = (state_42972[(7)]);
var inst_42940 = cljs.core.chunked_seq_QMARK_.call(null,inst_42938);
var state_42972__$1 = state_42972;
if(inst_42940){
var statearr_43003_43031 = state_42972__$1;
(statearr_43003_43031[(1)] = (17));

} else {
var statearr_43004_43032 = state_42972__$1;
(statearr_43004_43032[(1)] = (18));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42973 === (16))){
var inst_42956 = (state_42972[(2)]);
var state_42972__$1 = state_42972;
var statearr_43005_43033 = state_42972__$1;
(statearr_43005_43033[(2)] = inst_42956);

(statearr_43005_43033[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_42973 === (10))){
var inst_42927 = (state_42972[(10)]);
var inst_42925 = (state_42972[(11)]);
var inst_42932 = cljs.core._nth.call(null,inst_42925,inst_42927);
var state_42972__$1 = state_42972;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_42972__$1,(13),out,inst_42932);
} else {
if((state_val_42973 === (18))){
var inst_42938 = (state_42972[(7)]);
var inst_42947 = cljs.core.first.call(null,inst_42938);
var state_42972__$1 = state_42972;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_42972__$1,(20),out,inst_42947);
} else {
if((state_val_42973 === (8))){
var inst_42926 = (state_42972[(9)]);
var inst_42927 = (state_42972[(10)]);
var inst_42929 = (inst_42927 < inst_42926);
var inst_42930 = inst_42929;
var state_42972__$1 = state_42972;
if(cljs.core.truth_(inst_42930)){
var statearr_43006_43034 = state_42972__$1;
(statearr_43006_43034[(1)] = (10));

} else {
var statearr_43007_43035 = state_42972__$1;
(statearr_43007_43035[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__35058__auto__))
;
return ((function (switch__34968__auto__,c__35058__auto__){
return (function() {
var cljs$core$async$mapcat_STAR__$_state_machine__34969__auto__ = null;
var cljs$core$async$mapcat_STAR__$_state_machine__34969__auto____0 = (function (){
var statearr_43008 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_43008[(0)] = cljs$core$async$mapcat_STAR__$_state_machine__34969__auto__);

(statearr_43008[(1)] = (1));

return statearr_43008;
});
var cljs$core$async$mapcat_STAR__$_state_machine__34969__auto____1 = (function (state_42972){
while(true){
var ret_value__34970__auto__ = (function (){try{while(true){
var result__34971__auto__ = switch__34968__auto__.call(null,state_42972);
if(cljs.core.keyword_identical_QMARK_.call(null,result__34971__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__34971__auto__;
}
break;
}
}catch (e43009){if((e43009 instanceof Object)){
var ex__34972__auto__ = e43009;
var statearr_43010_43036 = state_42972;
(statearr_43010_43036[(5)] = ex__34972__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_42972);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e43009;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__34970__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__43037 = state_42972;
state_42972 = G__43037;
continue;
} else {
return ret_value__34970__auto__;
}
break;
}
});
cljs$core$async$mapcat_STAR__$_state_machine__34969__auto__ = function(state_42972){
switch(arguments.length){
case 0:
return cljs$core$async$mapcat_STAR__$_state_machine__34969__auto____0.call(this);
case 1:
return cljs$core$async$mapcat_STAR__$_state_machine__34969__auto____1.call(this,state_42972);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$mapcat_STAR__$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mapcat_STAR__$_state_machine__34969__auto____0;
cljs$core$async$mapcat_STAR__$_state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mapcat_STAR__$_state_machine__34969__auto____1;
return cljs$core$async$mapcat_STAR__$_state_machine__34969__auto__;
})()
;})(switch__34968__auto__,c__35058__auto__))
})();
var state__35060__auto__ = (function (){var statearr_43011 = f__35059__auto__.call(null);
(statearr_43011[(6)] = c__35058__auto__);

return statearr_43011;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__35060__auto__);
});})(c__35058__auto__))
);

return c__35058__auto__;
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.mapcat_LT_ = (function cljs$core$async$mapcat_LT_(var_args){
var G__43039 = arguments.length;
switch (G__43039) {
case 2:
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$2 = (function (f,in$){
return cljs.core.async.mapcat_LT_.call(null,f,in$,null);
});

cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3 = (function (f,in$,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
cljs.core.async.mapcat_STAR_.call(null,f,in$,out);

return out;
});

cljs.core.async.mapcat_LT_.cljs$lang$maxFixedArity = 3;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.mapcat_GT_ = (function cljs$core$async$mapcat_GT_(var_args){
var G__43042 = arguments.length;
switch (G__43042) {
case 2:
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$2 = (function (f,out){
return cljs.core.async.mapcat_GT_.call(null,f,out,null);
});

cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3 = (function (f,out,buf_or_n){
var in$ = cljs.core.async.chan.call(null,buf_or_n);
cljs.core.async.mapcat_STAR_.call(null,f,in$,out);

return in$;
});

cljs.core.async.mapcat_GT_.cljs$lang$maxFixedArity = 3;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.unique = (function cljs$core$async$unique(var_args){
var G__43045 = arguments.length;
switch (G__43045) {
case 1:
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.unique.cljs$core$IFn$_invoke$arity$1 = (function (ch){
return cljs.core.async.unique.call(null,ch,null);
});

cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2 = (function (ch,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__35058__auto___43092 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__35058__auto___43092,out){
return (function (){
var f__35059__auto__ = (function (){var switch__34968__auto__ = ((function (c__35058__auto___43092,out){
return (function (state_43069){
var state_val_43070 = (state_43069[(1)]);
if((state_val_43070 === (7))){
var inst_43064 = (state_43069[(2)]);
var state_43069__$1 = state_43069;
var statearr_43071_43093 = state_43069__$1;
(statearr_43071_43093[(2)] = inst_43064);

(statearr_43071_43093[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43070 === (1))){
var inst_43046 = null;
var state_43069__$1 = (function (){var statearr_43072 = state_43069;
(statearr_43072[(7)] = inst_43046);

return statearr_43072;
})();
var statearr_43073_43094 = state_43069__$1;
(statearr_43073_43094[(2)] = null);

(statearr_43073_43094[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43070 === (4))){
var inst_43049 = (state_43069[(8)]);
var inst_43049__$1 = (state_43069[(2)]);
var inst_43050 = (inst_43049__$1 == null);
var inst_43051 = cljs.core.not.call(null,inst_43050);
var state_43069__$1 = (function (){var statearr_43074 = state_43069;
(statearr_43074[(8)] = inst_43049__$1);

return statearr_43074;
})();
if(inst_43051){
var statearr_43075_43095 = state_43069__$1;
(statearr_43075_43095[(1)] = (5));

} else {
var statearr_43076_43096 = state_43069__$1;
(statearr_43076_43096[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43070 === (6))){
var state_43069__$1 = state_43069;
var statearr_43077_43097 = state_43069__$1;
(statearr_43077_43097[(2)] = null);

(statearr_43077_43097[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43070 === (3))){
var inst_43066 = (state_43069[(2)]);
var inst_43067 = cljs.core.async.close_BANG_.call(null,out);
var state_43069__$1 = (function (){var statearr_43078 = state_43069;
(statearr_43078[(9)] = inst_43066);

return statearr_43078;
})();
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_43069__$1,inst_43067);
} else {
if((state_val_43070 === (2))){
var state_43069__$1 = state_43069;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_43069__$1,(4),ch);
} else {
if((state_val_43070 === (11))){
var inst_43049 = (state_43069[(8)]);
var inst_43058 = (state_43069[(2)]);
var inst_43046 = inst_43049;
var state_43069__$1 = (function (){var statearr_43079 = state_43069;
(statearr_43079[(7)] = inst_43046);

(statearr_43079[(10)] = inst_43058);

return statearr_43079;
})();
var statearr_43080_43098 = state_43069__$1;
(statearr_43080_43098[(2)] = null);

(statearr_43080_43098[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43070 === (9))){
var inst_43049 = (state_43069[(8)]);
var state_43069__$1 = state_43069;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_43069__$1,(11),out,inst_43049);
} else {
if((state_val_43070 === (5))){
var inst_43046 = (state_43069[(7)]);
var inst_43049 = (state_43069[(8)]);
var inst_43053 = cljs.core._EQ_.call(null,inst_43049,inst_43046);
var state_43069__$1 = state_43069;
if(inst_43053){
var statearr_43082_43099 = state_43069__$1;
(statearr_43082_43099[(1)] = (8));

} else {
var statearr_43083_43100 = state_43069__$1;
(statearr_43083_43100[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43070 === (10))){
var inst_43061 = (state_43069[(2)]);
var state_43069__$1 = state_43069;
var statearr_43084_43101 = state_43069__$1;
(statearr_43084_43101[(2)] = inst_43061);

(statearr_43084_43101[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43070 === (8))){
var inst_43046 = (state_43069[(7)]);
var tmp43081 = inst_43046;
var inst_43046__$1 = tmp43081;
var state_43069__$1 = (function (){var statearr_43085 = state_43069;
(statearr_43085[(7)] = inst_43046__$1);

return statearr_43085;
})();
var statearr_43086_43102 = state_43069__$1;
(statearr_43086_43102[(2)] = null);

(statearr_43086_43102[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__35058__auto___43092,out))
;
return ((function (switch__34968__auto__,c__35058__auto___43092,out){
return (function() {
var cljs$core$async$state_machine__34969__auto__ = null;
var cljs$core$async$state_machine__34969__auto____0 = (function (){
var statearr_43087 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_43087[(0)] = cljs$core$async$state_machine__34969__auto__);

(statearr_43087[(1)] = (1));

return statearr_43087;
});
var cljs$core$async$state_machine__34969__auto____1 = (function (state_43069){
while(true){
var ret_value__34970__auto__ = (function (){try{while(true){
var result__34971__auto__ = switch__34968__auto__.call(null,state_43069);
if(cljs.core.keyword_identical_QMARK_.call(null,result__34971__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__34971__auto__;
}
break;
}
}catch (e43088){if((e43088 instanceof Object)){
var ex__34972__auto__ = e43088;
var statearr_43089_43103 = state_43069;
(statearr_43089_43103[(5)] = ex__34972__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_43069);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e43088;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__34970__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__43104 = state_43069;
state_43069 = G__43104;
continue;
} else {
return ret_value__34970__auto__;
}
break;
}
});
cljs$core$async$state_machine__34969__auto__ = function(state_43069){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__34969__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__34969__auto____1.call(this,state_43069);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__34969__auto____0;
cljs$core$async$state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__34969__auto____1;
return cljs$core$async$state_machine__34969__auto__;
})()
;})(switch__34968__auto__,c__35058__auto___43092,out))
})();
var state__35060__auto__ = (function (){var statearr_43090 = f__35059__auto__.call(null);
(statearr_43090[(6)] = c__35058__auto___43092);

return statearr_43090;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__35060__auto__);
});})(c__35058__auto___43092,out))
);


return out;
});

cljs.core.async.unique.cljs$lang$maxFixedArity = 2;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.partition = (function cljs$core$async$partition(var_args){
var G__43106 = arguments.length;
switch (G__43106) {
case 2:
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.partition.cljs$core$IFn$_invoke$arity$2 = (function (n,ch){
return cljs.core.async.partition.call(null,n,ch,null);
});

cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3 = (function (n,ch,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__35058__auto___43172 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__35058__auto___43172,out){
return (function (){
var f__35059__auto__ = (function (){var switch__34968__auto__ = ((function (c__35058__auto___43172,out){
return (function (state_43144){
var state_val_43145 = (state_43144[(1)]);
if((state_val_43145 === (7))){
var inst_43140 = (state_43144[(2)]);
var state_43144__$1 = state_43144;
var statearr_43146_43173 = state_43144__$1;
(statearr_43146_43173[(2)] = inst_43140);

(statearr_43146_43173[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43145 === (1))){
var inst_43107 = (new Array(n));
var inst_43108 = inst_43107;
var inst_43109 = (0);
var state_43144__$1 = (function (){var statearr_43147 = state_43144;
(statearr_43147[(7)] = inst_43109);

(statearr_43147[(8)] = inst_43108);

return statearr_43147;
})();
var statearr_43148_43174 = state_43144__$1;
(statearr_43148_43174[(2)] = null);

(statearr_43148_43174[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43145 === (4))){
var inst_43112 = (state_43144[(9)]);
var inst_43112__$1 = (state_43144[(2)]);
var inst_43113 = (inst_43112__$1 == null);
var inst_43114 = cljs.core.not.call(null,inst_43113);
var state_43144__$1 = (function (){var statearr_43149 = state_43144;
(statearr_43149[(9)] = inst_43112__$1);

return statearr_43149;
})();
if(inst_43114){
var statearr_43150_43175 = state_43144__$1;
(statearr_43150_43175[(1)] = (5));

} else {
var statearr_43151_43176 = state_43144__$1;
(statearr_43151_43176[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43145 === (15))){
var inst_43134 = (state_43144[(2)]);
var state_43144__$1 = state_43144;
var statearr_43152_43177 = state_43144__$1;
(statearr_43152_43177[(2)] = inst_43134);

(statearr_43152_43177[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43145 === (13))){
var state_43144__$1 = state_43144;
var statearr_43153_43178 = state_43144__$1;
(statearr_43153_43178[(2)] = null);

(statearr_43153_43178[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43145 === (6))){
var inst_43109 = (state_43144[(7)]);
var inst_43130 = (inst_43109 > (0));
var state_43144__$1 = state_43144;
if(cljs.core.truth_(inst_43130)){
var statearr_43154_43179 = state_43144__$1;
(statearr_43154_43179[(1)] = (12));

} else {
var statearr_43155_43180 = state_43144__$1;
(statearr_43155_43180[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43145 === (3))){
var inst_43142 = (state_43144[(2)]);
var state_43144__$1 = state_43144;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_43144__$1,inst_43142);
} else {
if((state_val_43145 === (12))){
var inst_43108 = (state_43144[(8)]);
var inst_43132 = cljs.core.vec.call(null,inst_43108);
var state_43144__$1 = state_43144;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_43144__$1,(15),out,inst_43132);
} else {
if((state_val_43145 === (2))){
var state_43144__$1 = state_43144;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_43144__$1,(4),ch);
} else {
if((state_val_43145 === (11))){
var inst_43124 = (state_43144[(2)]);
var inst_43125 = (new Array(n));
var inst_43108 = inst_43125;
var inst_43109 = (0);
var state_43144__$1 = (function (){var statearr_43156 = state_43144;
(statearr_43156[(7)] = inst_43109);

(statearr_43156[(8)] = inst_43108);

(statearr_43156[(10)] = inst_43124);

return statearr_43156;
})();
var statearr_43157_43181 = state_43144__$1;
(statearr_43157_43181[(2)] = null);

(statearr_43157_43181[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43145 === (9))){
var inst_43108 = (state_43144[(8)]);
var inst_43122 = cljs.core.vec.call(null,inst_43108);
var state_43144__$1 = state_43144;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_43144__$1,(11),out,inst_43122);
} else {
if((state_val_43145 === (5))){
var inst_43117 = (state_43144[(11)]);
var inst_43109 = (state_43144[(7)]);
var inst_43108 = (state_43144[(8)]);
var inst_43112 = (state_43144[(9)]);
var inst_43116 = (inst_43108[inst_43109] = inst_43112);
var inst_43117__$1 = (inst_43109 + (1));
var inst_43118 = (inst_43117__$1 < n);
var state_43144__$1 = (function (){var statearr_43158 = state_43144;
(statearr_43158[(11)] = inst_43117__$1);

(statearr_43158[(12)] = inst_43116);

return statearr_43158;
})();
if(cljs.core.truth_(inst_43118)){
var statearr_43159_43182 = state_43144__$1;
(statearr_43159_43182[(1)] = (8));

} else {
var statearr_43160_43183 = state_43144__$1;
(statearr_43160_43183[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43145 === (14))){
var inst_43137 = (state_43144[(2)]);
var inst_43138 = cljs.core.async.close_BANG_.call(null,out);
var state_43144__$1 = (function (){var statearr_43162 = state_43144;
(statearr_43162[(13)] = inst_43137);

return statearr_43162;
})();
var statearr_43163_43184 = state_43144__$1;
(statearr_43163_43184[(2)] = inst_43138);

(statearr_43163_43184[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43145 === (10))){
var inst_43128 = (state_43144[(2)]);
var state_43144__$1 = state_43144;
var statearr_43164_43185 = state_43144__$1;
(statearr_43164_43185[(2)] = inst_43128);

(statearr_43164_43185[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43145 === (8))){
var inst_43117 = (state_43144[(11)]);
var inst_43108 = (state_43144[(8)]);
var tmp43161 = inst_43108;
var inst_43108__$1 = tmp43161;
var inst_43109 = inst_43117;
var state_43144__$1 = (function (){var statearr_43165 = state_43144;
(statearr_43165[(7)] = inst_43109);

(statearr_43165[(8)] = inst_43108__$1);

return statearr_43165;
})();
var statearr_43166_43186 = state_43144__$1;
(statearr_43166_43186[(2)] = null);

(statearr_43166_43186[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__35058__auto___43172,out))
;
return ((function (switch__34968__auto__,c__35058__auto___43172,out){
return (function() {
var cljs$core$async$state_machine__34969__auto__ = null;
var cljs$core$async$state_machine__34969__auto____0 = (function (){
var statearr_43167 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_43167[(0)] = cljs$core$async$state_machine__34969__auto__);

(statearr_43167[(1)] = (1));

return statearr_43167;
});
var cljs$core$async$state_machine__34969__auto____1 = (function (state_43144){
while(true){
var ret_value__34970__auto__ = (function (){try{while(true){
var result__34971__auto__ = switch__34968__auto__.call(null,state_43144);
if(cljs.core.keyword_identical_QMARK_.call(null,result__34971__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__34971__auto__;
}
break;
}
}catch (e43168){if((e43168 instanceof Object)){
var ex__34972__auto__ = e43168;
var statearr_43169_43187 = state_43144;
(statearr_43169_43187[(5)] = ex__34972__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_43144);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e43168;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__34970__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__43188 = state_43144;
state_43144 = G__43188;
continue;
} else {
return ret_value__34970__auto__;
}
break;
}
});
cljs$core$async$state_machine__34969__auto__ = function(state_43144){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__34969__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__34969__auto____1.call(this,state_43144);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__34969__auto____0;
cljs$core$async$state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__34969__auto____1;
return cljs$core$async$state_machine__34969__auto__;
})()
;})(switch__34968__auto__,c__35058__auto___43172,out))
})();
var state__35060__auto__ = (function (){var statearr_43170 = f__35059__auto__.call(null);
(statearr_43170[(6)] = c__35058__auto___43172);

return statearr_43170;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__35060__auto__);
});})(c__35058__auto___43172,out))
);


return out;
});

cljs.core.async.partition.cljs$lang$maxFixedArity = 3;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.partition_by = (function cljs$core$async$partition_by(var_args){
var G__43190 = arguments.length;
switch (G__43190) {
case 2:
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$2 = (function (f,ch){
return cljs.core.async.partition_by.call(null,f,ch,null);
});

cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3 = (function (f,ch,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__35058__auto___43260 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__35058__auto___43260,out){
return (function (){
var f__35059__auto__ = (function (){var switch__34968__auto__ = ((function (c__35058__auto___43260,out){
return (function (state_43232){
var state_val_43233 = (state_43232[(1)]);
if((state_val_43233 === (7))){
var inst_43228 = (state_43232[(2)]);
var state_43232__$1 = state_43232;
var statearr_43234_43261 = state_43232__$1;
(statearr_43234_43261[(2)] = inst_43228);

(statearr_43234_43261[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43233 === (1))){
var inst_43191 = [];
var inst_43192 = inst_43191;
var inst_43193 = new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",-69252123);
var state_43232__$1 = (function (){var statearr_43235 = state_43232;
(statearr_43235[(7)] = inst_43193);

(statearr_43235[(8)] = inst_43192);

return statearr_43235;
})();
var statearr_43236_43262 = state_43232__$1;
(statearr_43236_43262[(2)] = null);

(statearr_43236_43262[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43233 === (4))){
var inst_43196 = (state_43232[(9)]);
var inst_43196__$1 = (state_43232[(2)]);
var inst_43197 = (inst_43196__$1 == null);
var inst_43198 = cljs.core.not.call(null,inst_43197);
var state_43232__$1 = (function (){var statearr_43237 = state_43232;
(statearr_43237[(9)] = inst_43196__$1);

return statearr_43237;
})();
if(inst_43198){
var statearr_43238_43263 = state_43232__$1;
(statearr_43238_43263[(1)] = (5));

} else {
var statearr_43239_43264 = state_43232__$1;
(statearr_43239_43264[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43233 === (15))){
var inst_43222 = (state_43232[(2)]);
var state_43232__$1 = state_43232;
var statearr_43240_43265 = state_43232__$1;
(statearr_43240_43265[(2)] = inst_43222);

(statearr_43240_43265[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43233 === (13))){
var state_43232__$1 = state_43232;
var statearr_43241_43266 = state_43232__$1;
(statearr_43241_43266[(2)] = null);

(statearr_43241_43266[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43233 === (6))){
var inst_43192 = (state_43232[(8)]);
var inst_43217 = inst_43192.length;
var inst_43218 = (inst_43217 > (0));
var state_43232__$1 = state_43232;
if(cljs.core.truth_(inst_43218)){
var statearr_43242_43267 = state_43232__$1;
(statearr_43242_43267[(1)] = (12));

} else {
var statearr_43243_43268 = state_43232__$1;
(statearr_43243_43268[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43233 === (3))){
var inst_43230 = (state_43232[(2)]);
var state_43232__$1 = state_43232;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_43232__$1,inst_43230);
} else {
if((state_val_43233 === (12))){
var inst_43192 = (state_43232[(8)]);
var inst_43220 = cljs.core.vec.call(null,inst_43192);
var state_43232__$1 = state_43232;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_43232__$1,(15),out,inst_43220);
} else {
if((state_val_43233 === (2))){
var state_43232__$1 = state_43232;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_43232__$1,(4),ch);
} else {
if((state_val_43233 === (11))){
var inst_43200 = (state_43232[(10)]);
var inst_43196 = (state_43232[(9)]);
var inst_43210 = (state_43232[(2)]);
var inst_43211 = [];
var inst_43212 = inst_43211.push(inst_43196);
var inst_43192 = inst_43211;
var inst_43193 = inst_43200;
var state_43232__$1 = (function (){var statearr_43244 = state_43232;
(statearr_43244[(11)] = inst_43212);

(statearr_43244[(7)] = inst_43193);

(statearr_43244[(8)] = inst_43192);

(statearr_43244[(12)] = inst_43210);

return statearr_43244;
})();
var statearr_43245_43269 = state_43232__$1;
(statearr_43245_43269[(2)] = null);

(statearr_43245_43269[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43233 === (9))){
var inst_43192 = (state_43232[(8)]);
var inst_43208 = cljs.core.vec.call(null,inst_43192);
var state_43232__$1 = state_43232;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_43232__$1,(11),out,inst_43208);
} else {
if((state_val_43233 === (5))){
var inst_43193 = (state_43232[(7)]);
var inst_43200 = (state_43232[(10)]);
var inst_43196 = (state_43232[(9)]);
var inst_43200__$1 = f.call(null,inst_43196);
var inst_43201 = cljs.core._EQ_.call(null,inst_43200__$1,inst_43193);
var inst_43202 = cljs.core.keyword_identical_QMARK_.call(null,inst_43193,new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",-69252123));
var inst_43203 = (inst_43201) || (inst_43202);
var state_43232__$1 = (function (){var statearr_43246 = state_43232;
(statearr_43246[(10)] = inst_43200__$1);

return statearr_43246;
})();
if(cljs.core.truth_(inst_43203)){
var statearr_43247_43270 = state_43232__$1;
(statearr_43247_43270[(1)] = (8));

} else {
var statearr_43248_43271 = state_43232__$1;
(statearr_43248_43271[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43233 === (14))){
var inst_43225 = (state_43232[(2)]);
var inst_43226 = cljs.core.async.close_BANG_.call(null,out);
var state_43232__$1 = (function (){var statearr_43250 = state_43232;
(statearr_43250[(13)] = inst_43225);

return statearr_43250;
})();
var statearr_43251_43272 = state_43232__$1;
(statearr_43251_43272[(2)] = inst_43226);

(statearr_43251_43272[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43233 === (10))){
var inst_43215 = (state_43232[(2)]);
var state_43232__$1 = state_43232;
var statearr_43252_43273 = state_43232__$1;
(statearr_43252_43273[(2)] = inst_43215);

(statearr_43252_43273[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_43233 === (8))){
var inst_43192 = (state_43232[(8)]);
var inst_43200 = (state_43232[(10)]);
var inst_43196 = (state_43232[(9)]);
var inst_43205 = inst_43192.push(inst_43196);
var tmp43249 = inst_43192;
var inst_43192__$1 = tmp43249;
var inst_43193 = inst_43200;
var state_43232__$1 = (function (){var statearr_43253 = state_43232;
(statearr_43253[(7)] = inst_43193);

(statearr_43253[(14)] = inst_43205);

(statearr_43253[(8)] = inst_43192__$1);

return statearr_43253;
})();
var statearr_43254_43274 = state_43232__$1;
(statearr_43254_43274[(2)] = null);

(statearr_43254_43274[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__35058__auto___43260,out))
;
return ((function (switch__34968__auto__,c__35058__auto___43260,out){
return (function() {
var cljs$core$async$state_machine__34969__auto__ = null;
var cljs$core$async$state_machine__34969__auto____0 = (function (){
var statearr_43255 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_43255[(0)] = cljs$core$async$state_machine__34969__auto__);

(statearr_43255[(1)] = (1));

return statearr_43255;
});
var cljs$core$async$state_machine__34969__auto____1 = (function (state_43232){
while(true){
var ret_value__34970__auto__ = (function (){try{while(true){
var result__34971__auto__ = switch__34968__auto__.call(null,state_43232);
if(cljs.core.keyword_identical_QMARK_.call(null,result__34971__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__34971__auto__;
}
break;
}
}catch (e43256){if((e43256 instanceof Object)){
var ex__34972__auto__ = e43256;
var statearr_43257_43275 = state_43232;
(statearr_43257_43275[(5)] = ex__34972__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_43232);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e43256;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__34970__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__43276 = state_43232;
state_43232 = G__43276;
continue;
} else {
return ret_value__34970__auto__;
}
break;
}
});
cljs$core$async$state_machine__34969__auto__ = function(state_43232){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__34969__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__34969__auto____1.call(this,state_43232);
}
throw(new Error('Invalid arity: ' + (arguments.length - 1)));
};
cljs$core$async$state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__34969__auto____0;
cljs$core$async$state_machine__34969__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__34969__auto____1;
return cljs$core$async$state_machine__34969__auto__;
})()
;})(switch__34968__auto__,c__35058__auto___43260,out))
})();
var state__35060__auto__ = (function (){var statearr_43258 = f__35059__auto__.call(null);
(statearr_43258[(6)] = c__35058__auto___43260);

return statearr_43258;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__35060__auto__);
});})(c__35058__auto___43260,out))
);


return out;
});

cljs.core.async.partition_by.cljs$lang$maxFixedArity = 3;


//# sourceMappingURL=async.js.map?rel=1507225498006

// Compiled by ClojureScript 1.9.908 {}
goog.provide('cljs.repl');
goog.require('cljs.core');
goog.require('cljs.spec.alpha');
cljs.repl.print_doc = (function cljs$repl$print_doc(p__45837){
var map__45838 = p__45837;
var map__45838__$1 = ((((!((map__45838 == null)))?((((map__45838.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__45838.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__45838):map__45838);
var m = map__45838__$1;
var n = cljs.core.get.call(null,map__45838__$1,new cljs.core.Keyword(null,"ns","ns",441598760));
var nm = cljs.core.get.call(null,map__45838__$1,new cljs.core.Keyword(null,"name","name",1843675177));
cljs.core.println.call(null,"-------------------------");

cljs.core.println.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1((function (){var temp__5278__auto__ = new cljs.core.Keyword(null,"ns","ns",441598760).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(temp__5278__auto__)){
var ns = temp__5278__auto__;
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(ns),"/"].join('');
} else {
return null;
}
})()),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join(''));

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"Protocol");
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m))){
var seq__45840_45862 = cljs.core.seq.call(null,new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m));
var chunk__45841_45863 = null;
var count__45842_45864 = (0);
var i__45843_45865 = (0);
while(true){
if((i__45843_45865 < count__45842_45864)){
var f_45866 = cljs.core._nth.call(null,chunk__45841_45863,i__45843_45865);
cljs.core.println.call(null,"  ",f_45866);

var G__45867 = seq__45840_45862;
var G__45868 = chunk__45841_45863;
var G__45869 = count__45842_45864;
var G__45870 = (i__45843_45865 + (1));
seq__45840_45862 = G__45867;
chunk__45841_45863 = G__45868;
count__45842_45864 = G__45869;
i__45843_45865 = G__45870;
continue;
} else {
var temp__5278__auto___45871 = cljs.core.seq.call(null,seq__45840_45862);
if(temp__5278__auto___45871){
var seq__45840_45872__$1 = temp__5278__auto___45871;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__45840_45872__$1)){
var c__31113__auto___45873 = cljs.core.chunk_first.call(null,seq__45840_45872__$1);
var G__45874 = cljs.core.chunk_rest.call(null,seq__45840_45872__$1);
var G__45875 = c__31113__auto___45873;
var G__45876 = cljs.core.count.call(null,c__31113__auto___45873);
var G__45877 = (0);
seq__45840_45862 = G__45874;
chunk__45841_45863 = G__45875;
count__45842_45864 = G__45876;
i__45843_45865 = G__45877;
continue;
} else {
var f_45878 = cljs.core.first.call(null,seq__45840_45872__$1);
cljs.core.println.call(null,"  ",f_45878);

var G__45879 = cljs.core.next.call(null,seq__45840_45872__$1);
var G__45880 = null;
var G__45881 = (0);
var G__45882 = (0);
seq__45840_45862 = G__45879;
chunk__45841_45863 = G__45880;
count__45842_45864 = G__45881;
i__45843_45865 = G__45882;
continue;
}
} else {
}
}
break;
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m))){
var arglists_45883 = new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_((function (){var or__30182__auto__ = new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(or__30182__auto__)){
return or__30182__auto__;
} else {
return new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m);
}
})())){
cljs.core.prn.call(null,arglists_45883);
} else {
cljs.core.prn.call(null,((cljs.core._EQ_.call(null,new cljs.core.Symbol(null,"quote","quote",1377916282,null),cljs.core.first.call(null,arglists_45883)))?cljs.core.second.call(null,arglists_45883):arglists_45883));
}
} else {
}
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"special-form","special-form",-1326536374).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"Special Form");

cljs.core.println.call(null," ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m));

if(cljs.core.contains_QMARK_.call(null,m,new cljs.core.Keyword(null,"url","url",276297046))){
if(cljs.core.truth_(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))){
return cljs.core.println.call(null,["\n  Please see http://clojure.org/",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))].join(''));
} else {
return null;
}
} else {
return cljs.core.println.call(null,["\n  Please see http://clojure.org/special_forms#",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join(''));
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"Macro");
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"REPL Special Function");
} else {
}

cljs.core.println.call(null," ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m));

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
var seq__45844_45884 = cljs.core.seq.call(null,new cljs.core.Keyword(null,"methods","methods",453930866).cljs$core$IFn$_invoke$arity$1(m));
var chunk__45845_45885 = null;
var count__45846_45886 = (0);
var i__45847_45887 = (0);
while(true){
if((i__45847_45887 < count__45846_45886)){
var vec__45848_45888 = cljs.core._nth.call(null,chunk__45845_45885,i__45847_45887);
var name_45889 = cljs.core.nth.call(null,vec__45848_45888,(0),null);
var map__45851_45890 = cljs.core.nth.call(null,vec__45848_45888,(1),null);
var map__45851_45891__$1 = ((((!((map__45851_45890 == null)))?((((map__45851_45890.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__45851_45890.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__45851_45890):map__45851_45890);
var doc_45892 = cljs.core.get.call(null,map__45851_45891__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var arglists_45893 = cljs.core.get.call(null,map__45851_45891__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
cljs.core.println.call(null);

cljs.core.println.call(null," ",name_45889);

cljs.core.println.call(null," ",arglists_45893);

if(cljs.core.truth_(doc_45892)){
cljs.core.println.call(null," ",doc_45892);
} else {
}

var G__45894 = seq__45844_45884;
var G__45895 = chunk__45845_45885;
var G__45896 = count__45846_45886;
var G__45897 = (i__45847_45887 + (1));
seq__45844_45884 = G__45894;
chunk__45845_45885 = G__45895;
count__45846_45886 = G__45896;
i__45847_45887 = G__45897;
continue;
} else {
var temp__5278__auto___45898 = cljs.core.seq.call(null,seq__45844_45884);
if(temp__5278__auto___45898){
var seq__45844_45899__$1 = temp__5278__auto___45898;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__45844_45899__$1)){
var c__31113__auto___45900 = cljs.core.chunk_first.call(null,seq__45844_45899__$1);
var G__45901 = cljs.core.chunk_rest.call(null,seq__45844_45899__$1);
var G__45902 = c__31113__auto___45900;
var G__45903 = cljs.core.count.call(null,c__31113__auto___45900);
var G__45904 = (0);
seq__45844_45884 = G__45901;
chunk__45845_45885 = G__45902;
count__45846_45886 = G__45903;
i__45847_45887 = G__45904;
continue;
} else {
var vec__45853_45905 = cljs.core.first.call(null,seq__45844_45899__$1);
var name_45906 = cljs.core.nth.call(null,vec__45853_45905,(0),null);
var map__45856_45907 = cljs.core.nth.call(null,vec__45853_45905,(1),null);
var map__45856_45908__$1 = ((((!((map__45856_45907 == null)))?((((map__45856_45907.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__45856_45907.cljs$core$ISeq$)))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__45856_45907):map__45856_45907);
var doc_45909 = cljs.core.get.call(null,map__45856_45908__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var arglists_45910 = cljs.core.get.call(null,map__45856_45908__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
cljs.core.println.call(null);

cljs.core.println.call(null," ",name_45906);

cljs.core.println.call(null," ",arglists_45910);

if(cljs.core.truth_(doc_45909)){
cljs.core.println.call(null," ",doc_45909);
} else {
}

var G__45911 = cljs.core.next.call(null,seq__45844_45899__$1);
var G__45912 = null;
var G__45913 = (0);
var G__45914 = (0);
seq__45844_45884 = G__45911;
chunk__45845_45885 = G__45912;
count__45846_45886 = G__45913;
i__45847_45887 = G__45914;
continue;
}
} else {
}
}
break;
}
} else {
}

if(cljs.core.truth_(n)){
var temp__5278__auto__ = cljs.spec.alpha.get_spec.call(null,cljs.core.symbol.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.ns_name.call(null,n))].join(''),cljs.core.name.call(null,nm)));
if(cljs.core.truth_(temp__5278__auto__)){
var fnspec = temp__5278__auto__;
cljs.core.print.call(null,"Spec");

var seq__45858 = cljs.core.seq.call(null,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"args","args",1315556576),new cljs.core.Keyword(null,"ret","ret",-468222814),new cljs.core.Keyword(null,"fn","fn",-1175266204)], null));
var chunk__45859 = null;
var count__45860 = (0);
var i__45861 = (0);
while(true){
if((i__45861 < count__45860)){
var role = cljs.core._nth.call(null,chunk__45859,i__45861);
var temp__5278__auto___45915__$1 = cljs.core.get.call(null,fnspec,role);
if(cljs.core.truth_(temp__5278__auto___45915__$1)){
var spec_45916 = temp__5278__auto___45915__$1;
cljs.core.print.call(null,["\n ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.name.call(null,role)),":"].join(''),cljs.spec.alpha.describe.call(null,spec_45916));
} else {
}

var G__45917 = seq__45858;
var G__45918 = chunk__45859;
var G__45919 = count__45860;
var G__45920 = (i__45861 + (1));
seq__45858 = G__45917;
chunk__45859 = G__45918;
count__45860 = G__45919;
i__45861 = G__45920;
continue;
} else {
var temp__5278__auto____$1 = cljs.core.seq.call(null,seq__45858);
if(temp__5278__auto____$1){
var seq__45858__$1 = temp__5278__auto____$1;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__45858__$1)){
var c__31113__auto__ = cljs.core.chunk_first.call(null,seq__45858__$1);
var G__45921 = cljs.core.chunk_rest.call(null,seq__45858__$1);
var G__45922 = c__31113__auto__;
var G__45923 = cljs.core.count.call(null,c__31113__auto__);
var G__45924 = (0);
seq__45858 = G__45921;
chunk__45859 = G__45922;
count__45860 = G__45923;
i__45861 = G__45924;
continue;
} else {
var role = cljs.core.first.call(null,seq__45858__$1);
var temp__5278__auto___45925__$2 = cljs.core.get.call(null,fnspec,role);
if(cljs.core.truth_(temp__5278__auto___45925__$2)){
var spec_45926 = temp__5278__auto___45925__$2;
cljs.core.print.call(null,["\n ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.name.call(null,role)),":"].join(''),cljs.spec.alpha.describe.call(null,spec_45926));
} else {
}

var G__45927 = cljs.core.next.call(null,seq__45858__$1);
var G__45928 = null;
var G__45929 = (0);
var G__45930 = (0);
seq__45858 = G__45927;
chunk__45859 = G__45928;
count__45860 = G__45929;
i__45861 = G__45930;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return null;
}
} else {
return null;
}
}
});

//# sourceMappingURL=repl.js.map?rel=1507225501378
